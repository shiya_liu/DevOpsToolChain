# DevOpsToolChain
![](Ops/Ansible/assets/devops.png)

本项目用于DevOps常用工具链的文档整理，按照日常工作内容分为运维技能系列文章(Ops)、项目实践(Project)、故障处理(Fault)、实验验证(Expt)、技能杂项(Misc)，五类内容。会着重梳理 **实现流程** **实现原理**  **体系构建** 部分。

目的是快速复习/上手熟悉devops工具链的使用

涉及监控、CICD、制品库、数据库、容器、编排工具等多个常用技术栈

**说明** 本repo所整理的内容均基于环境ubunt20.04 arm64架构(特殊标注的机器环境除外)

<details>
<summary>👏🏻运维系列文档的格式模板</summary>

参考文档 :walking:

简单介绍 :waning_gibbous_moon:

安装部署 :watermelon:

配置文件 :whale:

实现流程 :airplane:

日志分析 :whale2:

数据清理 :alarm_clock:

定制需求 :tada:

实战案例 :tanabata_tree:

升级步骤 :tangerine:

迁移步骤 :tea:

高可用集群 :telephone:

自动化处理  :wavy_dash:

api调用 :thumbsup:
</details>

<details>
<summary>✋🏻项目实践文档的格式模板</summary>

参考文档  :yellow_heart:

背景介绍 :baby:

机器环境  :yum:

实践思路:umbrella:

实践步骤  :kiss:

结果验证  :dancer:
</details>

<details>
<summary>👌🏻故障处理文档的格式模板</summary>

参考文档  :u6e80: :underage:

问题描述  :icecream:

问题现象  :information_desk_person:

解决方法  :ocean:
</details>





**目前进展**

- [x] Jenkins
- [x] gitlab-ci
- [x] ansible
- [x] zabbix
- [x] prometheus
- [x] docker
- [x] harbor
- [x] jumpserver
- [x] redis
- [ ] mongodb
- [ ] sonarqube
- [ ] nomad
- [ ] pxe
- [ ] git
- [ ] terraform
- [ ] consul
- [ ] rabbitmq
- [ ] ELK
- [ ] graylog
- [ ] minio
- [ ] mysql
- [ ] FastDFS
- [ ] Ceph
- [ ] GlusterFS
- [ ] memcached

