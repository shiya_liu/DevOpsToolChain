[TOC]



# 简介

官网[Kubernetes 文档 | Kubernetes](https://kubernetes.io/zh-cn/docs/home/)

思维导图：[(10条消息) kubernetes 思维导图_哈哈和呵呵的博客-CSDN博客](https://blog.csdn.net/wangbaosongmsn/article/details/119480989)

Kubernetes 是一个可移植、可扩展的开源平台，用于管理容器化的工作负载和服务，可促进声明式配置和自动化。 Kubernetes 拥有一个庞大且快速增长的生态，其服务、支持和工具的使用范围相当广泛。

Kubernetes 这个名字源于希腊语，意为“舵手”或“飞行员”。k8s 这个缩写是因为 k 和 s 之间有八个字符的关系。 Google 在 2014 年开源了 Kubernetes 项目。 



Kubernetes：

- 不限制支持的应用程序类型。 Kubernetes 旨在支持极其多种多样的工作负载，包括无状态、有状态和数据处理工作负载。 如果应用程序可以在容器中运行，那么它应该可以在 Kubernetes 上很好地运行。
- 不部署源代码，也不构建你的应用程序。 持续集成（CI）、交付和部署（CI/CD）工作流取决于组织的文化和偏好以及技术要求。
- 不提供应用程序级别的服务作为内置服务，例如中间件（例如消息中间件）、 数据处理框架（例如 Spark）、数据库（例如 MySQL）、缓存、集群存储系统 （例如 Ceph）。这样的组件可以在 Kubernetes 上运行，并且/或者可以由运行在 Kubernetes 上的应用程序通过可移植机制（例如[开放服务代理](https://openservicebrokerapi.org/)）来访问。

- 不是日志记录、监视或警报的解决方案。 它集成了一些功能作为概念证明，并提供了收集和导出指标的机制。
- 不提供也不要求配置用的语言、系统（例如 jsonnet），它提供了声明性 API， 该声明性 API 可以由任意形式的声明性规范所构成。
- 不提供也不采用任何全面的机器配置、维护、管理或自我修复系统。
- 此外，Kubernetes 不仅仅是一个编排系统，实际上它消除了编排的需要。 编排的技术定义是执行已定义的工作流程：首先执行 A，然后执行 B，再执行 C。 而 Kubernetes 包含了一组独立可组合的控制过程，可以持续地将当前状态驱动到所提供的预期状态。 你不需要在乎如何从 A 移动到 C，也不需要集中控制，这使得系统更易于使用且功能更强大、 系统更健壮，更为弹性和可扩展。





## 集群组件

### master组件

```shell
kube-apiserver:kubernetes API，集群的统一入口，各组件协调者，以RESTFUL API提供接口服务，所有对象资源的增删改查和监听操作都交给API Server处理后再交给ETCD存储

kube-controller-manager： 处理集群中常规后台任务，一个资源对应一个控制器，而Controller Manager就是负责管理这些控制器的

kube-scheduler：根据调度算法为新创建的Pod选择一个Node节点，可以任意部署，可以部署在同一个节点上，也可以部署在不同的节点上。

etcd：分布式键值存储系统。用于保存集群状态数据，比如Pod、Service等对象信息
```

### node组件

```shell
kubelet：kubelet是Master节点上的Agent，管理本机运行容器的生命周期，比如创建容器、Pod挂在数据卷、下载Secret、获取容器和节点状态等工作。kubelet将每个Pod转换成一组容器

kube-proxy：在Node节点上实现Pod网络代理，维护网络规则和四层负载均衡工作。
```

### 第三方组件

```shell
calico: https://docs.projectcalico.org/v3.8/manifests/calico.yaml
flannel: https://github.com/flannel-io/flannel/blob/master/Documentation/kube-flannel.yml
dashboard: https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
coredns:主要用于k8s集群中service的dns解析;为系统内置服务发现功能，会为每个Service配置DNS名称，并允许集群内的客户端直接使用此名称发送访问请求，而Service则通过iptables或ipvs内建负载均衡机制。coredns作用是实现pod跨主机通信。
Ingress Controller:
```



## 基本概念

```shell
Pod：K8s最小部署单位，一组容器的集合
Deplpyment：最常见的控制器，用于更高级的部署和管理Pod
Service: 为一组Pod提供负载均衡，对外提供统一访入口
Label:标签，附加到某个资源上，用于关联对象，查询和筛选
Namespace：命名空间，将对象逻辑上隔离，也利于权限控制
```

![image-20230421231952295](README.assets/image-20230421231952295.png)

`pod、deployment、service之间的通过标签进行关联`

```shell
[root@k8s_master01 ~]# kubectl get pods --show-labels
NAME                    READY   STATUS    RESTARTS   AGE     LABELS
web2-7d78cf6476-g6szp   1/1     Running   0          3m48s   app=web2,pod-template-hash=7d78cf6476
[root@k8s_master01 ~]#
[root@k8s_master01 ~]#
[root@k8s_master01 ~]# kubectl get deployment --show-labels
NAME   READY   UP-TO-DATE   AVAILABLE   AGE   LABELS
web2   1/1     1            1           4m    app=web2
[root@k8s_master01 ~]# kubectl get service --show-labels
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)        AGE     LABELS
kubernetes   ClusterIP   10.0.0.1     <none>        443/TCP        24h     component=apiserver,provider=kubernetes
web2         NodePort    10.0.0.76    <none>        80:31047/TCP   3m37s   app=web2

```

### 命名空间

```shell
namespace：Kubernetes将资源对象逻辑上隔离从而形成多个虚拟集群
应用场景：
    根据不同团队划分命名空间
    根据项目划分命名空间

kubectl get namespace
    default:默认命名空间
    kube-system:k8s系统方面的命名空间
    kube-public:公开的命名空间，谁都可以访问
    kube-node-lease:k8s内部命名空间

两种方式指定资源的命名空间：
    命令行-n
    yaml资源元数据中指定namespace字段
```



## 工作流程

![image-20230420153801022](README.assets/image-20230420153801022.png)

```shell
1、运维人员向kube-apiserver发出指令（我想干什么，我期望事情是什么状态）
2、api响应命令,通过一系列认证授权,把pod数据存储到etcd,创建deployment资源并初始化。(期望状态）
3、controller通过list-watch机制,监测发现新的deployment,将该资源加入到内部工作队列,发现该资源没有关联的pod和replicaset,启用deployment controller创建replicaset资源,再启用replicaset controller创建pod。
4、所有controller被创建完成后.将deployment,replicaset,pod资源更新存储到etcd。
5、scheduler通过list-watch机制,监测发现新的pod,经过主机过滤、主机打分规则,将pod绑定(binding)到合适的主机。
6、将绑定结果存储到etcd。
7、kubelet每隔 20s(可以自定义)向apiserver通过NodeName 获取自身Node上所要运行的pod清单.通过与自己的内部缓存进行比较,新增加pod。
8、kubelet创建pod。
9、kube-proxy为新创建的pod注册动态DNS到CoreOS。给pod的service添加iptables/ipvs规则，用于服务发现和负载均衡。
10、controller通过control loop（控制循环）将当前pod状态与用户所期望的状态做对比，如果当前状态与用户期望状态不同，则controller会将pod修改为用户期望状态，实在不行会将此pod删掉，然后重新创建pod。
```



kubelet操作容器

```shell
kubelet ---> CRI(docker shim) ---> dockerd ---> containerd ---> shim+runC ---> container
			可以直接更换为containerd
```



## 资源控制器

### Pod

#### 创建过程

```shell
1.用户通过kubectl或其他API客户端提交Pod Spec给API Server
2.API server尝试着将Pod对象的相关信息存入ETCD中，待写入操作执行完成，API server会返回确认信息至客户端
3.API server开始反映etcd中的状态变化
4.所有的kubernetes组件均使用 watch 机制来跟踪检查API server上相关的变动
5.kube-scheduler通过其 watch观察到API server创建了新的Pod对象但尚未绑定到任何工作做节点上
6.kube-scheduler为Pod对象挑选一个工作节点并将结果信息更新到API server中
7.调度结果信息由API server更新至etcd存储系统，而且API server也开始反应此Pod对象的调度结果
8.Pod被调度到的目标工作节点上的kubelet尝试在当前节点调用docker启动容器，并将容器的结果状态发送到API server
9.API server 将pod信息存入ETCD系统中
10.在ETCD确认写入操作成功完成后，API server将确认信息发送到相关的kubelet，事件将通过它被接受。
```



#### 生命周期

```shell
1.初始化容器：
一个pod可以拥有任意数量的init容器。init容器时顺序执行的，并且仅当最后一个init容器执行完毕才会去启动容器。换句话说，init容器也可以用来延迟pod的主容器的启动。

2.生命周期钩子：
pod允许定义两种类型的生命周期钩子，启动后(post-start)钩子和停止前(pre-stop)钩子;这些生命周期钩子是基于每个容器来指定的，和init容器不同的是，init容器时应用到整个pod。而这些钩子是针对容器的，是在容器启动后和停止前执行的。

3.容器探针：
是kubectl对容器周期性执行的健康状态诊断。分为两种： Liveness(存活性探测)， Readiness(就绪性检测)
Liveness(存活性探测)：判断容器是否处于runnning状态，策略是重启容器
Readiness(就绪性检测)：判断容器是否准备就绪并对外提供服务，将容器设置为不可用，不接受service转发的请求

4.容器的重启策略： 
注：一旦Pod绑定到一个节点上，就不会被重新绑定到另一个节点上，要么重启，要么终止

5.pod的终止过程
终止过程主要分为如下几个步骤：
(1)用户发出删除 pod 命令
(2)Pod 对象随着时间的推移更新，在宽限期（默认情况下30秒），pod 被视为“dead”状态
(3)将 pod 标记为“Terminating”状态
(4)第三步同时运行，监控到 pod 对象为“Terminating”状态的同时启动 pod 关闭过程
(5)第三步同时进行，endpoints 控制器监控到 pod 对象关闭，将pod与service匹配的 endpoints 列表中删除
(6)如果 pod 中定义了 preStop 钩子处理程序，则 pod 被标记为“Terminating”状态时以同步的方式启动执行；若宽限期结束后，preStop 仍未执行结束，第二步会重新执行并额外获得一个2秒的小宽限期
(7)Pod 内对象的容器收到 TERM 信号
(8)宽限期结束之后，若存在任何一个运行的进程，pod 会收到 SIGKILL 信号
(9)Kubelet 请求 API Server 将此 Pod 资源宽限期设置为0从而完成删除操作
```

#### 容器类型

- infrastructure container：基础容器，维护整个Pod网络空间
- init container：初始化容器，先于业务容器执行
- containers:业务容器

#### init container

- 在pod中可以有一个或者多个初始化容器
- 初始化容器执行成功后，应用的容器才能被执行
- 初始化容器是仅运行一次的任务

当配置了init container后 pod的状态会出现init状态



#### 资源共享

- 共享网络：共享网络空间，pause容器实现
- 共享存储：共享volume

#### 重启策略

- Always:当容器终止退出后，总是重启容器，默认策略
- OnFailure：当容器一场退出时，才会重启容器
- Never:当容器终止退出，从不重启容器

#### 健康检查

- livenessProbe（存活检查）：如果检查失败，将杀死容器，根据pod的restartPolicy来操作
- readinessProbe（就绪检查）：如果检查失败，Kubernetes会把Pod从service endpoints中剔除

支持一下三种检查方法：

- httpGet:发送HTTP请求，返回200-400范围状态码为成功
- exec：执行shell命令返回状态码是0为成功
- tcpSocket：发起TCP Socket建立成功

#### 状态

- Pending：创建了pod资源并存入etcd中，但尚未完成调度。
- ContainerCreating：Pod 的调度完成，被分配到指定 Node 上。处于容器创建的过程中。通常是在拉取镜像的过程中。
- Running：Pod 包含的所有容器都已经成功创建，并且成功运行起来。
- Succeeded：Pod中的所有容器都已经成功终止并且不会被重启
- Failed：所有容器都已经终止，但至少有一个容器终止失败，也就是说容器返回了非0值的退出状态或已经被系统终止。
- Unknown：因为某些原因无法取得 Pod 的状态。这种情况通常是因为与 Pod 所在主机通信失败。

#### 镜像拉取策略

- Always：镜像不存在时总是从指定的仓库中获取镜像
- ifNotPresent：仅当本地镜像不存在时才会从仓库中获取镜像
- Never：禁止从仓库获取镜像，仅使用本地镜像

#### 静态pod

```shell
静态pod 是由 kubelet 管理的只在特定node上存在的pod；静态pod总是由kubelet创建的，并且只在kubelet所在的Node上运行。
静态pod 不能通过 api-server来管理，无法和 RC，RS，Deployment或者 DaemonSet进行关联并且 kubelet无法对静态pod 进行健康检查。

创建静态 pod 的两种方式:
1、本地配置文件方式
kubelet 启动时由 --pod-manifest-path 指定的目录（默认/etc/kubernetes/manifests），kubelet会定期扫描这个目录，并根据这个目录下的 .yaml 或 .json 文件进行创建和更新操作

2、HTTP仓库配置文件方式
--manifest-url，kubelet定期从 url获取 文件，其余操作和 第一种方式一样。

```





### Deployment

适用于无状态服务

工作方式：deployment管理replicaset，replicaset管理pod。Deployment每次发布都会创建一个RS作为记录，用于实现回滚

- 支持replicaset的所有功能
- 支持发布的停止、继续
- 支持版本的滚动更新和版本回退



[k8s之deployment详解 - 王叫兽 - 博客园 (cnblogs.com)](https://www.cnblogs.com/younger5/p/15033157.html)



![image-20230423095245211](README.assets/image-20230423095245211.png)



```shell
旧RS：web777-56b96cfd4    -> nginx:1.16
新RS：web777-6bc79b6795   -> nginx:1.17


Normal  ScalingReplicaSet  17m   deployment-controller  Scaled up replica set web777-56b96cfd4 to 3
Normal  ScalingReplicaSet  13m   deployment-controller  Scaled up replica set web777-6bc79b6795 to 1
Normal  ScalingReplicaSet  12m   deployment-controller  Scaled down replica set web777-56b96cfd4 to 2
Normal  ScalingReplicaSet  12m   deployment-controller  Scaled up replica set web777-6bc79b6795 to 2
Normal  ScalingReplicaSet  12m   deployment-controller  Scaled down replica set web777-56b96cfd4 to 1
Normal  ScalingReplicaSet  12m   deployment-controller  Scaled up replica set web777-6bc79b6795 to 3
Normal  ScalingReplicaSet  12m   deployment-controller  Scaled down replica set web777-56b96cfd4 to 0


deployment -> replicaset -> pod 

当我们执行apply部署应用时，deployment会创建一个RS，并告诉RS web777-56b96cfd4 扩容副本为3（也是你预期副本数量replicas）

当我们执行apply更新一个新镜像：
第一步：扩容RS web777-6bc79b6795 为1个副本 # 更新一个新的pod
第二步：缩容RS web777-56b96cfd4 为2个副本 # 缩容第一个旧的pod
第三步：扩容RS web777-6bc79b6795 为2个副本 #更新第二个pod
第四步：缩容RS web777-56b96cfd4 为1个副本 #  缩容第二个pod
第五步：扩容RS web777-6bc79b6795 为3个副本 # 更新第三个pod
第六步：缩容RS web777-56b96cfd4 为0个副本 # 缩容第三个pod
```



### Daemonset

确保全部或者某些节点上必须运行一个 Pod的工作负载资源（守护进程），当有节点加入集群时， 也会为他们新增一个 Pod。

通过创建DaemonSet 可以确保 守护进程pod 被调度到每个可用节点上运行。如果不设置污点容忍，带有污点的node将不会运行DaemonSet。

使用场景：

- 集群守护进程，如Kured、node-problem-detector
- 日志收集守护进程，如fluentd、logstash
- 监控守护进程，如promethues node-exporter

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd-elasticsearch
  namespace: kube-system
  labels:
    k8s-app: fluentd-logging
spec:
  selector:
    matchLabels:
      name: fluentd-elasticsearch
  template:
    metadata:
      labels:
        name: fluentd-elasticsearch
    spec:
      tolerations:
      # these tolerations are to have the daemonset runnable on control plane nodes
      # remove them if your control plane nodes should not run pods
      - key: node-role.kubernetes.io/control-plane
        operator: Exists
        effect: NoSchedule
      - key: node-role.kubernetes.io/master
        operator: Exists
        effect: NoSchedule
      containers:
      - name: fluentd-elasticsearch
        image: quay.io/fluentd_elasticsearch/fluentd:v2.5.2
        resources:
          limits:
            memory: 200Mi
          requests:
            cpu: 100m
            memory: 200Mi
        volumeMounts:
        - name: varlog
          mountPath: /var/log
      terminationGracePeriodSeconds: 30
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
```



### StatefulSet

适用于有状态服务

为了解决有状态服务的问题，它所管理的Pod拥有固定的Pod名称，启停顺序，在StatefulSet中，Pod名字称为网络标识(hostname)，还必须要用到共享存储。

在Deployment中，与之对应的服务是service，而在StatefulSet中与之对应的headless service;与service的区别就是它没有Cluster IP，解析它的名称时将返回该Headless Service对应的全部Pod的Endpoint列表。

[(10条消息) k8s之StatefulSet详解_最美dee时光的博客-CSDN博客](https://blog.csdn.net/weixin_44729138/article/details/106054025)



### Job/Cronjob

job:运行一次性容器

cronjob:定时运行一次性容器



### ReplicaSet

新一代的ReplicatinController，推荐使用ReplicaSet

用途：Pod副本数量管理，不断对比当前Pod数量与期望Pod数量



### ReplicatinController







### HorizontalPodAutoscaler







## 资源调度



### 资源限制

容器资源限制：

- resource.limits.cpu
- resource.limits.memory

容器使用的最小资源需求，作为容器调度时资源分配的依据：

- resource.requests.cpu
- resource.requests.memory

K8S 会根据requests的值去查找有足够资源的Node调度此Pod

### 污点与容忍度

Taints（污点）：避免Pod调度到特定的Node上

Tolerations(容忍):允许pod调度到持有Taints的node上

应用场景：

- 专用节点：根据业务线将Node分组管理，希望在默认情况下不调度该节点，只有配置了污点容忍才允许分配
- 配备特殊硬件：部分Node配有SSD硬盘、GPU，希望在默认情况下不调度该节点，只有配置了污点容忍才允许分配
- 基于Taint的驱逐

污点类型：

- NoSchedule：一定不能被调度
- PreferNoSchedule：尽量不要调度，非必须配置容忍
- NoExecute：不仅不会调度，还会驱逐Node上已有的Pod



### 标签

- nodeSelector:用于将Pod调度到匹配Label的Node上，如果没有匹配的标签会调度失败。
- nodeName: 指定节点名称，用于将Pod调度到指定的Node上，不经过调度器，无视污点

作用：

- 约束pod到特定的节点运行
- 完全匹配节点标签

应用场景：

- 专用节点：根据业务线将Node分组管理
- 配备特殊硬件：部分Node配有SSD硬盘、GPU等



### 软亲和性与硬亲和性

nodeAffinity:节点亲和类似于nodeSelector，可根据节点上的标签来约束Pod可以调度到那些节点。

相比于nodeSelector：

- 匹配方式有更多的逻辑组合，不只是字符串的完全相等，支持的操作符有：In、Notin、Exists、DoesNotExist、Gt、Lt

- 调度分为软策略和应策略，而不是硬性要求：

  - 软（preferred）：尝试满足，但不保证
  - 硬(required)：必须满足

  

  

  软策略

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  affinity:
    nodeAffinity:
      preferredDuringSchedulingIgnoredDuringExecution: # 软策略
      - weight: 1 # 权重 1-100
        preference:
          matchExpressions:
          - key: disktype
            operator: In
            values:
            - ssd          
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
```

硬策略

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution: # 硬策略
        nodeSelectorTerms:
        - matchExpressions:
          - key: disktype
            operator: In
            values:
            - ssd            
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
```





## 网络模式

CNI网络模型：（所有网络组件都需要满足一下要求：）

- 一个Pod一个IP
- 所有Pod可以与任何其他Pod之间直接通信
- 所有节点可以与所有Pod直接通信
- Pod内部获取到的IP地址与其他Pod或节点与其通信时的IP地址是同一个（Pod的IP地址需要在集群内 唯一 + 可见）



Kubernetes中主要存在四种类型的通信：

**同一pod内的容器间通信**（pod内通信，pod内的容器共享同一网络名称空间）

**同节点pod间通信**(docker0)

**pod跨节点通信**（flannel、calico）

**外部访问集群**（Nodeport、LoadBalance（service自带的两种外部访问集群的方式）、Ingress(NGINX、HAproxy、Traefik、Envoy、Istio)）

[一篇文章为你图解kubernetes网络通信原理 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/81667781)



Service与Ingress的区别：[《Kubernetes》，你需要掌握的 Service 和 Ingress - 腾讯云开发者社区-腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1840236)



### Service

Service引入主要是解决Pod的动态变化，提供统一的访问入口:

防止pod失联，准备找到提供同一个服务的Pod（服务发现）

定义一组Pod的访问策略（负载均衡）

service通过标签关联一组pod

service使用`iptables`或`ipvs`为一组pod提供负载均衡能力

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app.kubernetes.io/name: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
```



service类型：

- ClusterIp：集群内部使用
- NodePort：对外暴露应用
- LoadBalancer：对外暴露应用
- ExternalName：



service 实现方式：iptables vs ipvs:

- Iptables：灵活，功能强大；规则遍历匹配和更新，呈线性时延
- ipvs:工作在内核态，有更好的性能；调度算法丰富：rr、wrr、lc、wlc、ip hash



#### ClusterIp



#### NodePort



#### LoadBalancer



#### ExternalName



#### Headless



### Ingress

Ingress为弥补NodePort不足而生;Ingress公开了从集群外部到集群内服务的HTTP和HTTPS路由的规则集合，而具体实现流量路由则是由Ingress Controller负责。

NodePort存在的不足：

- 一个端口只能一个服务使用，端口需提前规划
- 只支持4层负载均衡



ingress ：K8S中的一个抽象资源，给管理员提供一个暴露应用的入口定义方法

Ingress Controller： 根据Ingress生成具体的路由规则，并对Pod负载均衡；为集群提供全局的负载均衡能力



## 存储模式

### 临时存储

#### emptydir

是一个临时存储卷，与pod生命周期保存在一起，如果pod被删除，卷也会被删除

应用场景：Pod中容器之间数据共享

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: registry.k8s.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /cache
      name: cache-volume
  volumes:
  - name: cache-volume
    emptyDir:
      sizeLimit: 500Mi
```



#### gitRepo

(已经被弃用)

### 节点存储

#### hostPath

### 网络存储

#### NFS

#### RBD

#### GlusterFS

#### Cinder

### ConfigMap



### Secret









## 网格服务





## CRD开发





## Operator开发









# 具体使用

## kubeadm安装K8S

内容太多，后续补充:[CKA认证-kubeadm1.20安装部署K8S - 热气球！ - 博客园 (cnblogs.com)](https://www.cnblogs.com/liushiya/p/14872165.html)

## 二进制安装K8S

内容太多，后续补充:[二进制部署K8S v1.20.0 - 热气球！ - 博客园 (cnblogs.com)](https://www.cnblogs.com/liushiya/p/16851204.html)

kubeadm init工作流程：

```ini
1、[preflight] 环境检查，拉取镜像
2、[certs] 证书生成
3、[kubeconfig] kubeconfig文件生成
4、[kubelet-start] 生成kubelet配置文件并启动
5、[control-plane] 静态pod启动master组件，包括了etcd
6、[mark-control-plane] 给master节点打一个roles和污点
7、[bootstrap-token] 引导kubelet生成证书
8、[addons] 安装coredns和kube-proxy
```





## 常用命令

[k8s运维命令大全 - 腾讯云开发者社区-腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1910711)



kubectl:[命令行工具 (kubectl) | Kubernetes](https://kubernetes.io/zh-cn/docs/reference/kubectl/)

```shell
基础命令
  create        从文件或stdin创建资源。
  expose        采用复制控制器、服务、部署或pod，并将其作为新的Kubernetes服务公开
  run           在群集上运行特定映像
  set           设置对象的特定功能

中级命令
  explain       资源文件
  get           显示一个或多个资源
  edit          编辑服务器上的资源
  delete        按文件名、stdin、资源和名称或按资源和标签选择器删除资源

部署命令：
  rollout       管理资源的推出
  scale         为部署、复制集或复制控制器设置新的大小
  autoscale     自动扩展部署、复制集或复制控制器

群集管理命令：
  certificate   修改证书资源。
  cluster-info  显示群集信息
  top           显示资源（CPU/内存/存储）使用情况。
  cordon        将节点标记为不可调度
  uncordon      将节点标记为可调度
  drain         排水节点，为维护做准备
  taint         更新一个或多个节点上的污点

疑难解答和调试命令：
  describe      显示特定资源或资源组的详细信息
  logs          打印吊舱中容器的日志
  attach        连接到正在运行的容器
  exec          在容器中执行命令
  port-forward  将一个或多个本地端口转发到一个pod
  proxy         运行Kubernetes API服务器的代理
  cp            将文件和目录复制到容器和从容器复制文件和目录。
  auth          检查授权
  debug         创建调试会话以排除工作负载和节点的故障

高级命令：
  diff          区分实际版本和潜在应用版本
  apply         通过文件名或stdin将配置应用于资源
  patch         更新资源的字段
  replace       用文件名或stdin替换资源
  wait          实验：等待一个或多个资源上的特定条件。
  kustomize     从一个目录或一个远程url构建一个kustomization目标。

设置命令：
  label         更新资源上的标签
  annotate      更新资源上的批注
  completion    为指定的shell（bash或zsh）输出shell完成代码

其他命令：
  api-resources 打印服务器上支持的API资源
  api-versions  以“组/版本”的形式在服务器上打印支持的API版本
  config        修改kubeconfig文件
  plugin        提供用于与插件交互的实用程序。
  version       打印客户端和服务器版本信息

Usage:
  kubectl [flags] [options]

Use "kubectl <command> --help" for more information about a given command.
Use "kubectl options" for a list of global command-line options (applies to all commands).


```

44个命令

| 操作          | 语法                                                         | 描述                                                         |
| ------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| alpha         | `kubectl alpha SUBCOMMAND [flags]`                           | 列出与 alpha 特性对应的可用命令，这些特性在 Kubernetes 集群中默认情况下是不启用的。 |
| annotate      | `kubectl annotate (-f FILENAME | TYPE NAME | TYPE/NAME) KEY_1=VAL_1 ... KEY_N=VAL_N [--overwrite] [--all] [--resource-version=version] [flags]` | 添加或更新一个或多个资源的注解。                             |
| api-resources | `kubectl api-resources [flags]`                              | 列出可用的 API 资源。                                        |
| api-versions  | `kubectl api-versions [flags]`                               | 列出可用的 API 版本。                                        |
| apply         | `kubectl apply -f FILENAME [flags]`                          | 从文件或 stdin 对资源应用配置更改。                          |
| attach        | `kubectl attach POD -c CONTAINER [-i] [-t] [flags]`          | 挂接到正在运行的容器，查看输出流或与容器（stdin）交互。      |
| auth          | `kubectl auth [flags] [options]`                             | 检查授权。                                                   |
| autoscale     | `kubectl autoscale (-f FILENAME | TYPE NAME | TYPE/NAME) [--min=MINPODS] --max=MAXPODS [--cpu-percent=CPU] [flags]` | 自动扩缩由副本控制器管理的一组 pod。                         |
| certificate   | `kubectl certificate SUBCOMMAND [options]`                   | 修改证书资源。                                               |
| cluster-info  | `kubectl cluster-info [flags]`                               | 显示有关集群中主服务器和服务的端口信息。                     |
| completion    | `kubectl completion SHELL [options]`                         | 为指定的 Shell（Bash 或 Zsh）输出 Shell 补齐代码。           |
| config        | `kubectl config SUBCOMMAND [flags]`                          | 修改 kubeconfig 文件。有关详细信息，请参阅各个子命令。       |
| convert       | `kubectl convert -f FILENAME [options]`                      | 在不同的 API 版本之间转换配置文件。配置文件可以是 YAML 或 JSON 格式。注意 - 需要安装 `kubectl-convert` 插件。 |
| cordon        | `kubectl cordon NODE [options]`                              | 将节点标记为不可调度。                                       |
| cp            | `kubectl cp <file-spec-src> <file-spec-dest> [options]`      | 从容器复制文件、目录或将文件、目录复制到容器。               |
| create        | `kubectl create -f FILENAME [flags]`                         | 从文件或 stdin 创建一个或多个资源。                          |
| delete        | `kubectl delete (-f FILENAME | TYPE [NAME | /NAME | -l label | --all]) [flags]` | 基于文件、标准输入或通过指定标签选择器、名称、资源选择器或资源本身，删除资源。 |
| describe      | `kubectl describe (-f FILENAME | TYPE [NAME_PREFIX | /NAME | -l label]) [flags]` | 显示一个或多个资源的详细状态。                               |
| diff          | `kubectl diff -f FILENAME [flags]`                           | 在当前起作用的配置和文件或标准输之间作对比 (**BETA**)        |
| drain         | `kubectl drain NODE [options]`                               | 腾空节点以准备维护。                                         |
| edit          | `kubectl edit (-f FILENAME | TYPE NAME | TYPE/NAME) [flags]` | 使用默认编辑器编辑和更新服务器上一个或多个资源的定义。       |
| events        | `kubectl events`                                             | 列举事件。                                                   |
| exec          | `kubectl exec POD [-c CONTAINER] [-i] [-t] [flags] [-- COMMAND [args...]]` | 对 Pod 中的容器执行命令。                                    |
| explain       | `kubectl explain [--recursive=false] [flags]`                | 获取多种资源的文档。例如 Pod、Node、Service 等。             |
| expose        | `kubectl expose (-f FILENAME | TYPE NAME | TYPE/NAME) [--port=port] [--protocol=TCP|UDP] [--target-port=number-or-name] [--name=name] [--external-ip=external-ip-of-service] [--type=type] [flags]` | 将副本控制器、服务或 Pod 作为新的 Kubernetes 服务暴露。      |
| get           | `kubectl get (-f FILENAME | TYPE [NAME | /NAME | -l label]) [--watch] [--sort-by=FIELD] [[-o | --output]=OUTPUT_FORMAT] [flags]` | 列出一个或多个资源。                                         |
| kustomize     | `kubectl kustomize``[flags] [options]``                      | 列出从 kustomization.yaml 文件中的指令生成的一组 API 资源。参数必须是包含文件的目录的路径，或者是 git 存储库 URL，其路径后缀相对于存储库根目录指定了相同的路径。 |
| label         | `kubectl label (-f FILENAME | TYPE NAME | TYPE/NAME) KEY_1=VAL_1 ... KEY_N=VAL_N [--overwrite] [--all] [--resource-version=version] [flags]` | 添加或更新一个或多个资源的标签。                             |
| logs          | `kubectl logs POD [-c CONTAINER] [--follow] [flags]`         | 打印 Pod 中容器的日志。                                      |
| options       | `kubectl options`                                            | 全局命令行选项列表，这些选项适用于所有命令。                 |
| patch         | `kubectl patch (-f FILENAME | TYPE NAME | TYPE/NAME) --patch PATCH [flags]` | 使用策略合并流程更新资源的一个或多个字段。                   |
| plugin        | `kubectl plugin [flags] [options]`                           | 提供用于与插件交互的实用程序。                               |
| port-forward  | `kubectl port-forward POD [LOCAL_PORT:]REMOTE_PORT [...[LOCAL_PORT_N:]REMOTE_PORT_N] [flags]` | 将一个或多个本地端口转发到一个 Pod。                         |
| proxy         | `kubectl proxy [--port=PORT] [--www=static-dir] [--www-prefix=prefix] [--api-prefix=prefix] [flags]` | 运行访问 Kubernetes API 服务器的代理。                       |
| replace       | `kubectl replace -f FILENAME`                                | 基于文件或标准输入替换资源。                                 |
| `rollout`     | `kubectl rollout SUBCOMMAND [options]`                       | 管理资源的上线。有效的资源类型包括：Deployment、 DaemonSet 和 StatefulSet。 |
| `run`         | `kubectl run NAME --image=image [--env="key=value"] [--port=port] [--dry-run=server | client | none] [--overrides=inline-json] [flags]` | 在集群上运行指定的镜像。                                     |
| `scale`       | `kubectl scale (-f FILENAME | TYPE NAME | TYPE/NAME) --replicas=COUNT [--resource-version=version] [--current-replicas=count] [flags]` | 更新指定副本控制器的大小。                                   |
| `set`         | `kubectl set SUBCOMMAND [options]`                           | 配置应用资源。                                               |
| `taint`       | `kubectl taint NODE NAME KEY_1=VAL_1:TAINT_EFFECT_1 ... KEY_N=VAL_N:TAINT_EFFECT_N [options]` | 更新一个或多个节点上的污点。                                 |
| `top`         | `kubectl top [flags] [options]`                              | 显示资源（CPU、内存、存储）的使用情况。                      |
| `uncordon`    | `kubectl uncordon NODE [options]`                            | 将节点标记为可调度。                                         |
| `version`     | `kubectl version [--client] [flags]`                         | 显示运行在客户端和服务器上的 Kubernetes 版本。               |
| `wait`        | `kubectl wait ([-f FILENAME] | resource.group/resource.name | resource.group [(-l label | --all)]) [--for=delete|--for condition=available] [options]` | 实验特性：等待一种或多种资源的特定状况。                     |





### alpha

列出与 alpha 特性对应的可用命令，这些特性在 Kubernetes 集群中默认情况下是不启用的。

### annotate

添annotate加或更新一个或多个资源的注解。

```shell
#使用注释“description”和值“my frontend”更新pod“foo”。
#如果多次设置同一注释，则仅应用最后一个值
kubectl annotate pods foo description='my frontend'

#更新“pod.json”中按类型和名称标识的pod
kubectl annotate -f pod.json description='my frontend'

#使用注释“description”和值“my frontend running nginx”更新pod“foo”，覆盖任何现有价值。
kubectl annotate --overwrite pods foo description='my frontend running nginx'

#更新当前命名空间中的所有pod
kubectl annotate pods --all description='my frontend running nginx'

#仅当资源与版本1相比没有变化时，才更新pod“foo”。
kubectl annotate pods foo description='my frontend running nginx' --resource-version=1

#通过删除名为“description”的注释（如果存在）来更新pod“foo”。
#不需要--overwrite标志。
kubectl annotate pods foo description-


参数：
--all=false：选择指定资源类型的命名空间中的所有资源，包括未初始化的资源。
--allow-missing-template-keys=true：如果为true，则在中缺少字段或映射键时忽略模板中的任何错误
模板。仅适用于golang和jsonpath输出格式。
--dry-run='none'：必须是“none”、“server”或“client”。如果是客户端策略，则只打印
已发送，但未发送。如果是服务器策略，则在不保留资源的情况下提交服务器端请求。
--field-manager='kubectl-annotate': 用于跟踪字段所有权的经理的姓名。
--field-selector='': 要筛选的选择器（字段查询），支持“=”、“=”和“！=”。（例如--字段选择器key1=值1，key2=值2）。服务器仅支持每种类型的有限数量的字段查询。
-f, --filename=[]: 标识要更新注释性的资源的文件的文件名、目录或URL
--kustomize='': 处理kustomization目录。此标志不能与-f或-R一起使用。
--list=false: 如果为true，则显示给定资源的注释。
--local=false: 如果为true，注释将不会联系api服务器，而是在本地运行。
--output='': 输出格式。其中之一：json|yaml|name|go template|go template file|template|templatefile|jsonpath|jsonpath作为json|jsonpath文件。
--overwrite=false: 如果为true，则允许覆盖批注，否则拒绝以下批注更新覆盖现有注释。
--record=false: 在资源注释中记录当前的kubectl命令。如果设置为false，则不记录；命令如果设置为true，则记录该命令。如果未设置，则默认情况下仅当已存在。
--recursive=false: 递归地处理-f，--filename中使用的目录。当你想管理时很有用组织在同一目录中的相关清单。
--resource-version='': 如果非空，则只有当这是当前对象的资源版本。仅在指定单个资源时有效。
--selector='':如果非空，则只有当这是当前对象的资源版本。仅在指定单个资源时有效。
--template='': 模板字符串或模板文件的路径，当-o=转到模板，-o=转到样板文件时使用。这个模板格式为golang模板[http://golang.org/pkg/text/template/#pkg-概述]。
```



### api-resources

列出可用的 API 资源。

```shell
# 打印支持的API资源
kubectl api-resources

# 打印支持的API资源和更多信息
kubectl api-resources -o wide

#按列排序打印支持的API资源
kubectl api-resources --sort-by=name

#打印支持的命名空间资源
kubectl api-resources --namespaced=true

#打印支持的非命名空间资源
kubectl api-resources --namespaced=false

#打印具有特定APIGroup的受支持API资源
kubectl api-resources --api-group=extensions
参数：
      --api-group='': 限制为指定API组中的资源。
      --cached=false: 如果可用，请使用缓存的资源列表。
      --namespaced=true: 如果为false，则将返回非名称空间的资源，否则默认返回名称空间资源。
      --no-headers=false: 使用默认或自定义列输出格式时，不打印标题（默认打印标头）。
  -o, --output='': 输出格式。其中之一：wide|name。
      --sort-by='': 如果非空，请使用指定字段对资源列表进行排序。字段可以是“name”或“kind”。
      --verbs=[]: 限制为支持指定谓词的资源。

```



### api-versions

以“组/版本”的形式在服务器上打印支持的API版本

```shell
#打印支持的API版本
kubectl api-versions
```

### apply

通过文件名或stdin将配置应用于资源。必须指定资源名称。如果该资源尚不存在，则会创建该资源。若要使用“apply”，请始终首先使用“apply'”或“create”创建资源

```shell
#将pod.json中的配置应用于一个pod。
kubectl apply -f ./pod.json
#从包含kustomization.yaml的目录中应用资源，例如dir/kustomization.yaml。
kubectl apply -k dir/
#将传递到stdin的JSON应用到pod。
cat pod.json | kubectl apply -f -
#注意：--prune仍在Alpha中
#应用manifest.yaml中与label-app=nginx匹配的配置，并删除文件中没有的所有其他资源，并匹配label-app=enginx。
kubectl apply --prune -f manifest.yaml -l app=nginx
#应用manifest.yaml中的配置，并删除文件中没有的所有其他配置映射
kubectl apply --prune -f manifest.yaml --all --prune-whitelist=core/v1/ConfigMap

edit-last-applied 编辑资源/对象最近一次应用的配置注释
set-last-applied  将活动对象上最后应用的配置注释设置为与文件的内容相匹配。
view-last-applied 查看资源/对象最近一次应用的配置注释

参数
      --all=false: 选择指定资源类型的命名空间中的所有资源。
      --allow-missing-template-keys=true: 如果为true，则在中缺少字段或映射键时忽略模板中的任何错误模板。仅适用于golang和jsonpath输出格式。
      --cascade='background': 必须是“background”、“orphan”或“foreground”。选择删除级联策略对于从属项（例如，由ReplicationController创建的Pod）。默认为背景。
      --dry-run='none': 必须是“none”、“server”或“client”。如果是客户端策略，则只打印已发送，但未发送。如果是服务器策略，则在不保留资源的情况下提交服务器端请求。
      --field-manager='kubectl-client-side-apply': 用于跟踪字段所有权的经理的姓名。
  -f, --filename=[]: 包含要应用的配置
      --force=false: 如果为true，请立即从API中删除资源并绕过优雅的删除。请注意，立即删除某些资源可能会导致不一致或数据丢失，并且需要确认。
      --force-conflicts=false: 如果为true，则服务器端应用程序将针对冲突强制进行更改。
      --grace-period=-1: 指定给资源以正常终止的时间段（以秒为单位）。如果为负数，则忽略。设置为1表示立即停机。只有当--force为true（强制删除）时，才能将其设置为0。
  -k, --kustomize='': 处理Kustomatization目录。此标志不能与-f或-R一起使用。
      --openapi-patch=true: 如果为true，那么当openapi出现并且资源可以在openapi规范中找到。否则，请返回使用烘焙类型。
  -o, --output='': 输出格式：json|yaml|name|go-template|go-template-file|template|templatefile|jsonpath|jsonpath-as-json|jsonpath-file.
      --overwrite=true: 通过使用中的值自动解决修改配置和活动配置之间的冲突修改后的配置
      --prune=false: 自动删除未出现在configs和是通过application或create-save-config创建的。应与-l或-all一起使用。
      --prune-whitelist=[]: 使用<group/version/kind>为--prune覆盖默认白名单
      --record=false: 在资源注释中记录当前的kubectl命令。如果设置为false，则不记录命令如果设置为true，则记录该命令。如果未设置，则默认情况下仅当已存在。
  -R, --recursive=false: 递归地处理-f，--filename中使用的目录。当你想管理时很有用
组织在同一目录中的相关清单。 
  -l, --selector='': 要筛选的选择器（标签查询），支持“=”、“=”和“！=”。（例如-l key1=值1，key2=值2）
      --server-side=false: 如果为true，则应用程序在服务器中运行，而不是在客户端中运行。
      --template='': 模板字符串或模板文件的路径，当-o=转到模板，-o=转到样板文件时使用。这个
模板格式为golang模板[http://golang.org/pkg/text/template/#pkg-概述]。
      --timeout=0s: 在放弃删除之前等待的时间长度，零表示从对象的大小
      --validate=true: 如果为true，请在发送输入之前使用模式验证输入
      --wait=false: 如果为true，请等待资源用完后再返回。这将等待终结器。

```

### attach

挂接到正在运行的容器，查看输出流或与容器（stdin）交互。

```shell
#从运行pod mypod中获取输出，默认情况下使用第一个容器
kubectl attach mypod

#从pod mypod获取ruby容器的输出
kubectl attach mypod -c ruby-container

#切换到原始终端模式，将stdin从pod mypod发送到ruby容器中的“bash”
#并将“bash”中的stdout/stderr发送回客户端
kubectl attach mypod -c ruby-container -i -t

#从名为nginx的ReplicaSet的第一个pod获取输出
kubectl attach rs/nginx

Options:
  -c, --container='': 容器名称。如果省略，将选择吊舱中的第一个容器
      --pod-running-timeout=1m0s: 等待至少一次的时间长度（如5s、2m或3h，高于零）吊舱正在运行
  -i, --stdin=false: 将stdin传递到容器
  -t, --tty=false:Stdin是TTY

Usage:
  kubectl attach (POD | TYPE/NAME) -c CONTAINER [options]

```

### auth

检查授权

```shell


Available Commands:
  can-i       检查是否允许操作
  reconcile   协调RBAC角色、RoleBinding、ClusterRole和ClusterRole绑定对象的规则


#检查是否可以在任何命名空间中创建pod
[root@k8s_master01 ~]# kubectl auth can-i create pods --all-namespaces
yes


#检查是否可以在当前命名空间中列出部署
kubectl auth can-i list deployments.apps

# 检查我是否可以在当前命名空间中完成所有操作（“*”表示全部）
kubectl auth can-i '*' '*'

#检查是否可以在命名空间“foo”中获得名为“bar”的作业
kubectl auth can-i list jobs.batch/bar -n foo

#检查一下我是否能阅读pod日志
kubectl auth can-i get pods --subresource=log

#检查我是否可以访问URL/日志/
kubectl auth can-i get /logs/

#列出命名空间“foo”中允许的所有操作
kubectl auth can-i --list --namespace=foo

Options:
  -A, --all-namespaces=false: 如果为true，请检查所有命名空间中的指定操作。
      --list=false: 如果为true，则打印所有允许的操作。
      --no-headers=false: 如果为true，则打印不带标头的允许操作
  -q, --quiet=false: 如果为true，则取消输出，只返回退出代码。
      --subresource='': 子资源，如pod/log或deployment/scale

Usage:
  kubectl auth can-i VERB [TYPE | TYPE/NAME | NONRESOURCEURL] [options]



协调RBAC角色、角色绑定、ClusterRole和ClusterRole绑定对象的规则。
如果需要，将创建缺少的对象，并为带名称空间的对象创建包含名称空间。

现有角色将更新为包括输入对象中的权限，并删除额外的权限
--remove-extra-permissions is specified.

更新现有绑定以将主题包括在输入对象中，并在以下情况下删除额外的主题
--remove-extra-subjects 

这最好是“应用”RBAC资源，以便完成规则和主题的语义感知合并。


#从文件中协调rbac资源
kubectl auth reconcile -f my-rbac-rules.yaml

Options:
      --allow-missing-template-keys=true: 如果为true，则在中缺少字段或映射键时忽略模板中的任何错误模板。仅适用于golang和jsonpath输出格式。
      --dry-run='none':必须是“none”、“server”或“client”。如果是客户端策略，则只打印已发送，但未发送。如果是服务器策略，则在不保留资源的情况下提交服务器端请求。
  -f, --filename=[]: 标识要协调的资源的文件名、目录或URL。
  -k, --kustomize='': Process the kustomization directory. This flag can't be used together with -f or -R.
  -o, --output='': 输出格式:json|yaml|name|go-template|go-template-file|template|templatefile|jsonpath|jsonpath-as-json|jsonpath-file.
  -R, --recursive=false: 递归地处理-f，--filename中使用的目录。当你想管理时很有用组织在同一目录中的相关清单。
      --remove-extra-permissions=false:如果为true，则删除添加到角色的额外权限 
      --remove-extra-subjects=false: 如果为true，则删除添加到角色绑定的额外主题
      --template='': 模板字符串或模板文件的路径，当-o=转到模板，-o=转到样板文件时使用。这个模板格式为golang模板[http://golang.org/pkg/text/template/#pkg-概述]。

Usage:
  kubectl auth reconcile -f FILENAME [options]
```

### autoscale

创建一个自动缩放器，自动选择并设置在kubernetes集群中运行的pod数量。

```shell

按名称查找Deployment、ReplicaSet、StatefulSet或ReplicationController，并创建一个使用给定的资源作为参考。自动缩放器可以自动增加或减少部署在其中的吊舱数量,根据需要调整系统。


#自动缩放部署“foo”，pod数量在2到10之间，没有指定目标CPU利用率，因此将使用默认的自动缩放策略：
kubectl autoscale deployment foo --min=2 --max=10

#自动缩放复制控制器“foo”，pod数量在1到5之间，目标CPU利用率为80%：
kubectl autoscale rc foo --max=5 --cpu-percent=80

Options:
      --allow-missing-template-keys=true: 如果为true，则在中缺少字段或映射键时忽略模板中的任何错误模板。仅适用于golang和jsonpath输出格式。
      --cpu-percent=-1: 如果未指定或为负数，则将使用默认的自动缩放策略。
      --dry-run='none': 必须是“none”、“server”或“client”。如果是客户端策略，则只打印已发送，但未发送。如果是服务器策略，则在不保留资源的情况下提交服务器端请求。
      --field-manager='kubectl-autoscale': 用于跟踪字段所有权的经理的姓名。
  -f, --filename=[]: Filename, directory, or URL to files identifying the resource to autoscale.
  -k, --kustomize='': Process the kustomization directory. This flag can't be used together with -f or -R.
      --max=-1: 自动缩放器可以设置的吊舱数量的上限。必修的。
      --min=-1:  自动缩放器可以设置的吊舱数量的下限。如果未指定或负，则服务器将应用默认值。
--name='': 新创建的对象的名称。如果未指定，则将使用输入资源的名称。
  -o, --output='': Output format. One of:
json|yaml|name|go-template|go-template-file|template|templatefile|jsonpath|jsonpath-as-json|jsonpath-file.
      --record=false: 在资源注释中记录当前的kubectl命令。如果设置为false，则不记录命令如果设置为true，则记录该命令。如果未设置，则默认情况下仅当已存在。
  -R, --recursive=false: 递归地处理-f，--filename中使用的目录。当你想管理时很有用组织在同一目录中的相关清单。
      --save-config=false: 如果为true，则当前对象的配置将保存在其注释中。否则注释将保持不变。当您将来想要对此对象执行kubectl应用时，此标志非常有用。
      --template='': 模板字符串或模板文件的路径，当-o=转到模板，-o=转到样板文件时使用。这个模板格式为golang模板[http://golang.org/pkg/text/template/#pkg-概述]。

Usage:
  kubectl autoscale (-f FILENAME | TYPE NAME | TYPE/NAME) [--min=MINPODS] --max=MAXPODS [--cpu-percent=CPU] [options]

```

### certificate

修改证书资源。

```shell


Available Commands:
  approve     批准证书签名请求
  deny        拒绝证书签名请求

Usage:
  kubectl certificate SUBCOMMAND [options]
```



### cluster-info

显示控制平面和服务的地址，标签为kubernetes.io/cluster service=true若要进一步调试和诊断集群问题，请使用“kubectl cluster info dump”。

```shell
#打印控制平面和集群服务的地址
kubectl cluster-info
# 转储大量相关信息进行调试和诊断
kubectl cluster-info dump
```



### completion

为指定的shell（bash或zsh）输出shell完成代码。必须评估shell代码才能提供交互式完成kubectl命令。这可以通过从.bash_profile中获取它来实现。

```shell
yum install -y bash-completion
source <(kubectl completion bash)
kubectl completion bash > ~/.kube/completion.bash.inc
source "$HOME/.kube/completion.bash.inc"
>> $HOME/.bash_profile
source $HOME/.bash_profile
```



### config

使用子命令修改kubeconfig文件，如“kubectl config set current context my context”

```shell
Modify kubeconfig files using subcommands like "kubectl config set current-context my-context"

装载顺序遵循以下规则：

1.如果设置了--kubeconfig标志，则只加载该文件。该标志只能设置一次，并且不会发生合并。
2.如果设置了$KUBECONFIG环境变量，那么它将用作路径列表（系统的正常路径定界规则）。这些路径被合并。当一个值被修改时，它会在定义节的文件中被修改。创建值时，会在存在的第一个文件中创建该值。如果链中不存在任何文件，则会创建列表中的最后一个文件。
3.否则，将使用${HOME}/.kube/config，并且不会发生合并。

Available Commands:
  current-context 显示当前上下文
  delete-cluster  从kubeconfig中删除指定的集群
  delete-context  从kubeconfig中删除指定的上下文
  delete-user     从kubeconfig中删除指定的用户
  get-clusters    显示kubeconfig中定义的集群
  get-contexts    描述一个或多个上下文
  get-users       显示在kubeconfig中定义的用户
  rename-context  重命名kubeconfig文件中的上下文。
  set             在kubeconfig文件中设置单个值
  set-cluster     在kubeconfig中设置集群条目
  set-context     在kubeconfig中设置上下文条目
  set-credentials 在kubeconfig中设置用户条目
  unset           取消设置kubeconfig文件中的单个值
  use-context     在kubeconfig文件中设置当前上下文
  view            显示合并的kubeconfig设置或指定的kubecnfig文件

Usage:
  kubectl config SUBCOMMAND [options]

Use "kubectl <command> --help" for more information about a given command.
Use "kubectl options" for a list of global command-line options (applies to all commands).

```

### convert

在不同的API版本之间转换配置文件。可以接受YAML和JSON格式。

```shell
# 安装插件
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl-convert"
sudo install -o root -g root -m 0755 kubectl-convert /usr/local/bin/kubectl-convert




该命令以文件名、目录或URL作为输入，并将其转换为--output version标志指定的版本格式。如果未指定或不支持目标版本，请转换为最新版本。

默认输出将以YAML格式打印到stdout。可以使用-o选项更改为输出目的地。


#将“pod.yaml”转换为最新版本并打印到stdout。
kubectl convert -f pod.yaml

#将“pod.yaml”指定的资源的活动状态转换为最新版本
#并以JSON格式打印到stdout。
kubectl convert -f pod.yaml --local -o json

#将当前目录下的所有文件转换为最新版本并全部创建。
kubectl convert -f . | kubectl create -f -

Flags:
      --allow-missing-template-keys    If true, ignore any errors in templates when a field or map key is missing in the template. Only applies to golang and jsonpath output formats. (default true)
      --as string                      Username to impersonate for the operation. User could be a regular user or a service account in a namespace.
      --as-group stringArray           Group to impersonate for the operation, this flag can be repeated to specify multiple groups.
      --as-uid string                  UID to impersonate for the operation.
      --cache-dir string               Default cache directory (default "/root/.kube/cache")
      --certificate-authority string   Path to a cert file for the certificate authority
      --client-certificate string      Path to a client certificate file for TLS
      --client-key string              Path to a client key file for TLS
      --cluster string                 The name of the kubeconfig cluster to use
      --context string                 The name of the kubeconfig context to use
      --disable-compression            If true, opt-out of response compression for all requests to the server
  -f, --filename strings               Filename, directory, or URL to files to need to get converted.
  -h, --help                           help for convert
      --insecure-skip-tls-verify       If true, the server's certificate will not be checked for validity. This will make your HTTPS connections insecure
      --kubeconfig string              Path to the kubeconfig file to use for CLI requests.
  -k, --kustomize string               Process the kustomization directory. This flag can't be used together with -f or -R.
      --local                          If true, convert will NOT try to contact api-server but run locally. (default true)
      --log-flush-frequency duration   Maximum number of seconds between log flushes (default 5s)
      --match-server-version           Require server version to match client version
  -n, --namespace string               If present, the namespace scope for this CLI request
  -o, --output string                  Output format. One of: (json, yaml, name, go-template, go-template-file, template, templatefile, jsonpath, jsonpath-as-json, jsonpath-file). (default "yaml")
      --output-version string          Output the formatted object with the given group version (for ex: 'extensions/v1beta1').
      --password string                Password for basic authentication to the API server
  -R, --recursive                      Process the directory used in -f, --filename recursively. Useful when you want to manage related manifests organized within the same directory.
      --request-timeout string         The length of time to wait before giving up on a single server request. Non-zero values should contain a corresponding time unit (e.g. 1s, 2m, 3h). A value of zero means don't timeout requests. (default "0")
  -s, --server string                  The address and port of the Kubernetes API server
      --show-managed-fields            If true, keep the managedFields when printing objects in JSON or YAML format.
      --template string                Template string or path to template file to use when -o=go-template, -o=go-template-file. The template format is golang templates [http://golang.org/pkg/text/template/#pkg-overview].
      --tls-server-name string         Server name to use for server certificate validation. If it is not provided, the hostname used to contact the server is used
      --token string                   Bearer token for authentication to the API server
      --user string                    The name of the kubeconfig user to use
      --username string                Username for basic authentication to the API server
  -v, --v Level                        number for the log level verbosity
      --validate string[="strict"]     Must be one of: strict (or true), warn, ignore (or false).
                                                "true" or "strict" will use a schema to validate the input and fail the request if invalid. It will perform server side validation if ServerSideFieldValidation is enabled on the api-server, but will fall back to less reliable client-side validation if not.
                                                "warn" will warn about unknown or duplicate fields without blocking the request if server-side field validation is enabled on the API server, and behave as "ignore" otherwise.
                                                "false" or "ignore" will not perform any schema validation, silently dropping any unknown or duplicate fields. (default "strict")
      --vmodule moduleSpec             comma-separated list of pattern=N settings for file-filtered logging (only works for the default text log format)

```



### cordon

将节点标记为不可调度。

```shell
Mark node as unschedulable.


#将节点“foo”标记为不可调度。
kubectl cordon foo

```



### cp

将文件和目录复制到容器和从容器复制文件和目录。

```shell

# !!!重要提示！！！
#要求“tar”二进制文件存在于您的容器中
#图像。如果“tar”不存在，“kubectl cp”将失败。
#
#对于高级用例，如符号链接、通配符扩展或
#文件模式保存考虑使用“kubectl exec”。

#将/tmp/foo本地文件复制到命名空间＜some namespace＞中的远程pod中的/tmp/bar
tar cf - /tmp/foo | kubectl exec -i -n <some-namespace> <some-pod> -- tar xf - -C /tmp/bar

#将/tmp/foo从远程pod复制到本地/tmp/bar
kubectl exec -n <some-namespace> <some-pod> -- tar cf - /tmp/foo | tar xf - -C /tmp/bar

#将/tmp/foo_dir本地目录复制到默认命名空间中远程pod中的/tmp/bar_dir
kubectl cp /tmp/foo_dir <some-pod>:/tmp/bar_dir

#将/tmp/foo本地文件复制到特定容器中远程pod中的/tmp/bar
kubectl cp /tmp/foo <some-pod>:/tmp/bar -c <specific-container>

#将/tmp/foo本地文件复制到命名空间＜some namespace＞中的远程pod中的/tmp/bar
kubectl cp /tmp/foo <some-namespace>/<some-pod>:/tmp/bar

#将/tmp/foo从远程pod复制到本地/tmp/bar
kubectl cp <some-namespace>/<some-pod>:/tmp/foo /tmp/bar

Options:
  -c, --container='': 容器名称。如果省略，将选择吊舱中的第一个容器
      --no-preserve=false: 复制的文件/目录的所有权和权限将不会保留在容器中
```



### create

从文件或stdin创建资源。可以接受JSON和YAML格式。

```shell
# 使用pod.json中的数据创建一个pod。
kubectl create -f ./pod.json

# 根据传递到stdin的JSON创建一个pod。
cat pod.json | kubectl create -f -

# 在JSON中编辑docker-registry.yaml中的数据，然后使用编辑后的数据创建资源。
kubectl create -f docker-registry.yaml --edit -o json


#创建一个名为“pod reader”的ClusterRole，允许用户在pod上执行“get”、“watch”和“list”
kubectl create clusterrole pod-reader --verb=get,list,watch --resource=pods

# 使用指定的ResourceName创建名为“pod reader”的ClusterRole
kubectl create clusterrole pod-reader --verb=get --resource=pods --resource-name=readablepod
--resource-name=anotherpod

#使用指定的API组创建名为“foo”的ClusterRole
kubectl create clusterrole foo --verb=get,list,watch --resource=rs.extensions

#创建一个名为“foo”并指定了SubResource的ClusterRole
kubectl create clusterrole foo --verb=get,list,watch --resource=pods,pods/status

#创建一个指定了NonResourceURL的ClusterRole名称“foo”
kubectl create clusterrole "foo" --verb=get --non-resource-url=/logs/*

#创建指定了AggregationRule的ClusterRole名称“monitoring”
kubectl create clusterrole monitoring --aggregation-rule="rbac.example.com/aggregate-to-monitoring=true"


# 使用集群管理ClusterRole为user1、user2和group1创建ClusterRoleBinding
kubectl create clusterrolebinding cluster-admin --clusterrole=cluster-admin --user=user1 --user=user2 --group=group1



基于文件、目录或指定的文字值创建配置映射。
单个配置映射可以封装一个或多个键/值对。
当基于文件创建配置映射时，键将默认为文件的基本名称，值将默认为
到文件内容。如果基名称是无效密钥，则可以指定备用密钥。
当基于目录创建配置映射时，目录中基名称为有效密钥的每个文件都将
打包到configmap中。忽略除常规文件之外的任何目录条目（例如，
设备、管道等）。

Aliases:
configmap, cm

Examples:
#基于文件夹栏创建一个名为my-config的新配置映射
kubectl create configmap my-config --from-file=path/to/bar

#在磁盘上使用指定的键而不是文件基名称创建一个名为my config的新配置映射
kubectl create configmap my-config --from-file=key1=/path/to/bar/file1.txt --from-file=key2=/path/to/bar/file2.txt

#使用key1=config1和key2=config2创建一个名为my-config的新配置映射
kubectl create configmap my-config --from-literal=key1=config1 --from-literal=key2=config2

#从文件中的key=value对创建一个名为my-config的新配置映射
kubectl create configmap my-config --from-file=path/to/bar

#从env文件创建一个名为my-config的新配置映射file
kubectl create configmap my-config --from-env-file=path/to/bar.env



使用指定的名称创建一个cronjob。

Aliases:
cronjob, cj

Examples:
  #创建cronjob
  kubectl create cronjob my-job --image=busybox --schedule="*/1 * * * *"

  #使用命令创建cronjob
  kubectl create cronjob my-job --image=busybox --schedule="*/1 * * * *" -- date

使用指定的名称创建部署。

Aliases:
deployment, deploy

Examples:
#创建一个名为my dep的部署，运行busybox映像。
kubectl create deployment my-dep --image=busybox

#使用命令创建展开
  kubectl create deployment my-dep --image=busybox -- date

#创建一个名为my dep的部署，该部署使用3个副本运行nginx映像。
  kubectl create deployment my-dep --image=nginx --replicas=3

#创建一个名为my dep的部署，该部署运行busybox映像并公开端口5701。
  kubectl create deployment my-dep --image=busybox --port=5701


#创建一个名为“simple”的入口，将请求定向到foo.com/bar到svc
#svc1:8080带有tls机密“我的证书”
kubectl create ingress simple --rule="foo.com/bar=svc1:8080,tls=my-cert"

#创建一个“/path”的catch-all入口，指向服务svc:port和ingress Class作为“otheringress”
  kubectl create ingress catch-all --class=otheringress --rule="/path=svc:port"

#创建一个带有两个注释的入口：ingress.annotation1和ingress.aannotation2
kubectl create ingress annotated --class=default --rule="foo.com/bar=svc:port" \
--annotation ingress.annotation1=foo \
--annotation ingress.annotation2=bla

# 创建具有相同主机和多个路径的入口
  kubectl create ingress multipath --class=default \
  --rule="foo.com/=svc:port" \
  --rule="foo.com/admin/=svcadmin:portadmin"

  # 创建一个具有多个主机和pathType作为前缀的入口
  kubectl create ingress ingress1 --class=default \
  --rule="foo.com/path*=svc:8080" \
  --rule="bar.com/admin*=svc2:http"

  # 使用默认入口证书和不同的路径类型创建启用TLS的入口
  kubectl create ingress ingtls --class=default \
  --rule="foo.com/=svc:https,tls" \
  --rule="foo.com/path/subpath*=othersvc:8080"

  # 使用特定的机密和pathType作为前缀创建启用TLS的入口
  kubectl create ingress ingsecret --class=default \
  --rule="foo.com/*=svc:8080,tls=secret1"

  # 使用默认后端创建入口
  kubectl create ingress ingdefault --class=default \
  --default-backend=defaultsvc:http \
  --rule="foo.com/*=svc:8080,tls=secret1"


#创建作业
kubectl create job my-job --image=busybox

#使用命令创建作业
kubectl create job my-job --image=busybox -- date

#从名为“a-CronJob”的CronJob创建作业
kubectl create job test-job --from=cronjob/a-cronjob


# kubectl创建名称空间my名称空间
kubectl create namespace my-namespace



使用指定的名称、选择器和所需的最小可用吊舱创建吊舱中断预算


#创建一个名为my-pdb的pod中断预算，该预算将选择所有带有app=rails标签的pod
#并且要求它们中的至少一个在任何时间点可用。
kubectl create poddisruptionbudget my-pdb --selector=app=rails --min-available=1

#创建一个名为my-pdb的pod中断预算，该预算将选择所有带有app=nginx标签的pod
#并且需要所选择的吊舱中的至少一半在任何时间点可用。
  kubectl create pdb my-pdb --selector=app=nginx --min-available=50%


#创建一个名为high priority的优先级类
kubectl create priorityclass high-priority --value=1000 --description="high priority"

#创建一个名为default priority的优先级类，该优先级类被视为全局默认优先级
kubectl create priorityclass default-priority --value=1000 --global-default=true --description="default priority"

#创建一个名为high priority的优先级类，该类不能抢占优先级较低的pod
kubectl create priorityclass high-priority --value=1000 --description="high priority" --preemption-policy="Never"


#创建一个名为“我的配额”的新资源配额
kubectl create quota my-quota
--hard=cpu=1,memory=1G,pods=2,services=3,replicationcontrollers=2,resourcequotas=1,secrets=5,persistentvolumeclaims=10

#创建名为“尽力而为”的新资源配额
kubectl create quota best-effort --hard=pods=100 --scopes=BestEffort


# 创建一个名为“pod reader”的角色，允许用户在pod上执行“get”、“watch”和“list”
kubectl create role pod-reader --verb=get --verb=list --verb=watch --resource=pods

#使用指定的ResourceName创建一个名为“pod reader”的角色
kubectl create role pod-reader --verb=get --resource=pods --resource-name=readablepod --resource-name=anotherpod

#使用指定的API组创建名为“foo”的角色
kubectl create role foo --verb=get,list,watch --resource=rs.extensions

#创建一个名为“foo”并指定了SubResource的角色
kubectl create role foo --verb=get,list,watch --resource=pods,pods/status


# 使用admin-ClusterRole为user1、user2和group1创建RoleBinding
kubectl create rolebinding admin --clusterrole=admin --user=user1 --user=user2 --group=group1

Create a secret using specified subcommand.

Available Commands:
  docker-registry Create a secret for use with a Docker registry
  generic         Create a secret from a local file, directory or literal value
  tls             Create a TLS secret

Usage:
  kubectl create secret [flags] [options]


Create a service using specified subcommand.

Aliases:
service, svc

Available Commands:
  clusterip    Create a ClusterIP service.
  externalname Create an ExternalName service.
  loadbalancer Create a LoadBalancer service.
  nodeport     Create a NodePort service.

Usage:
  kubectl create service [flags] [options]



Create a service account with the specified name.

Aliases:
serviceaccount, sa

Examples:
  # 创建一个名为“我的服务帐户”的新服务帐户
  kubectl create serviceaccount my-service-account




```



### delete



```shell
按文件名、标准输入、资源和名称或按资源和标签选择器删除资源。
可以接受JSON和YAML格式。只能指定一种类型的参数：文件名、资源和名称，或者
资源和标签选择器。
一些资源，如pod，支持优雅的删除。这些资源在
强制终止（宽限期），但您可以使用--grace period标志覆盖该值，或者将--now传递给
将宽限期设置为1。因为这些资源通常代表集群中的实体，所以删除可能不是
立即确认。如果承载pod的节点出现故障或无法访问API服务器，则可能需要终止
明显长于宽限期。若要强制删除资源，必须指定--force标志。注意：只有一个
资源的子集支持优雅的删除。在并没有支持的情况下，--宽限期将被忽略。
重要提示：强制删除pod不会等待pod进程已终止的确认，这可能
让这些进程一直运行，直到节点检测到删除并完成优雅的删除。如果您的流程使用
共享存储或与远程API对话，并依靠pod的名称来识别自己，强制删除这些
pod可能会导致多个进程使用相同的标识在不同的机器上运行，这可能会导致数据
腐败或不一致。只有当您确定pod已终止时，或者如果您的应用程序可以
允许同一pod同时运行多个副本。此外，如果强制删除pod，调度器可能会放置新的pod
在节点释放这些资源之前，这些节点上的pod，并导致这些pod立即被逐出。
请注意，delete命令不进行资源版本检查，因此如果有人向资源权限提交更新
当您提交删除时，它们的更新将与其他资源一起丢失。

Examples:
#使用pod.json中指定的类型和名称删除一个pod。
kubectl delete -f ./pod.json

#从包含kustomization.yaml的目录中删除资源，例如dir/kustomization.yaml。
kubectl delete -k dir

#根据传递到stdin的JSON中的类型和名称删除pod。
cat pod.json | kubectl delete -f -

#删除具有相同名称“baz”和“foo”的pod和服务
kubectl delete pod,service baz foo

#删除标签名称为myLabel的pod和服务。
kubectl delete pods,services -l name=myLabel

#以最小延迟删除吊舱
kubectl delete pod foo --now

#强制删除死节点上的pod
kubectl delete pod foo --force

#删除所有pod
kubectl delete pods --all

```



### describe



```shell
显示特定资源或资源组的详细信息
打印所选资源的详细说明，包括事件或控制器等相关资源。你
可以按名称选择单个对象、该类型的所有对象、提供名称前缀或标签选择器。对于

#描述一个节点
kubectl describe nodes kubernetes-node-emt8.c.myproject.internal

#描述一个吊舱
kubectl describe pods/nginx

#描述一个吊舱
kubectl describe -f pod.json

#描述所有吊舱
kubectl describe pods

#按标签name=myLabel描述pod
kubectl describe po -l name=myLabel

#描述“前端”复制控制器管理的所有pod（rc创建的pod
#获取rc的名称作为pod中的前缀（name）。
kubectl describe pods frontend

```



### diff

通过文件名或stdin指定的当前联机配置和当前联机配置之间的差异配置

```shell

通过文件名或stdin指定的当前联机配置和当前联机配置之间的差异配置
如果适用的话。
输出始终为YAML。
KUBECTL_EXTERNAL_DIFF环境变量可用于选择您自己的DIFF命令。用户可以使用外部命令
也带有参数，例如：KUBECTL_EXTERNAL_DIFF=“colordiff-N-u”
默认情况下，路径中可用的“diff”命令将使用“-u”（统一diff）和“-N”（处理不存在的文件
为空）选项。
退出状态：0未发现任何差异。1发现差异。>1 Kuectl或diff失败，出现错误。
注意：如果使用KUBECTL_EXTERNAL_DIFF，则应遵循该约定。

#pod.json中包含的差异资源。
kubectl diff -f pod.json

#从stdin读取的Diff文件
cat service.yaml | kubectl diff -f -

```

### drain

```shell
排水节点，为维护做准备。
给定的节点将被标记为不可调度，以防止新的pod到达。”如果APIServer
支架http://kubernetes.io/docs/admin/disruptions/ . 否则，它将使用普通的DELETE来删除pod。这个
“drain”驱逐或删除除镜像pod（不能通过API服务器删除）之外的所有pod。如果有
守护进程集管理的pod，如果没有，drain将无法继续--忽略守护进程集，并且无论如何都不会删除任何
DaemonSet管理的pod，因为这些pod将立即被DaemonSet控制器取代，后者会忽略
不可切割的标记。如果存在既不是镜像pod也不是由ReplicationController管理的任何pod，
ReplicaSet、DaemonSet、StatefulSet或Job，则drain不会删除任何pod，除非使用--force--武力意志
如果一个或多个pod的管理资源丢失，也允许进行删除。
“drain”等待优雅的终止。在命令完成之前，您不应该在机器上操作。
当您准备好将节点重新投入服务时，请使用kubectl uncordon，这将使节点再次可调度。
http://kubernetes.io/images/docs/kubectl_drain.svg

#耗尽节点“foo”，即使其中有未由ReplicationController、ReplicaSet、Job、DaemonSet或StatefulSet管理的pod。
$ kubectl drain foo --force

#如上所述，但如果存在未由ReplicationController、ReplicaSet、Job、DaemonSet或StatefulSet管理的pod，则中止，并使用15分钟的宽限期。
$ kubectl drain foo --grace-period=900

Options:
      --delete-emptydir-data=false: Continue even if there are pods using emptyDir (local data that will be deleted when
the node is drained).
      --disable-eviction=false: Force drain to use delete, even if eviction is supported. This will bypass checking
PodDisruptionBudgets, use with caution.
      --dry-run='none': Must be "none", "server", or "client". If client strategy, only print the object that would be
sent, without sending it. If server strategy, submit server-side request without persisting the resource.
      --force=false: Continue even if there are pods not managed by a ReplicationController, ReplicaSet, Job, DaemonSet
or StatefulSet.
      --grace-period=-1: Period of time in seconds given to each pod to terminate gracefully. If negative, the default
value specified in the pod will be used.
      --ignore-daemonsets=false: Ignore DaemonSet-managed pods.
      --pod-selector='': Label selector to filter pods on the node
  -l, --selector='': Selector (label query) to filter on
      --skip-wait-for-delete-timeout=0: If pod DeletionTimestamp older than N seconds, skip waiting for the pod.
Seconds must be greater than 0 to skip.
      --timeout=0s: The length of time to wait before giving up, zero means infinite

```



### edit



```shell
从默认编辑器编辑资源。
edit命令允许您直接编辑任何可以通过命令行工具检索到的API资源。它将打开
由KUBE_editor或editor环境变量定义的编辑器，或者返回到Linux的“vi”或Linux的“notepad”
窗户。您可以编辑多个对象，尽管每次只应用一个更改。该命令接受文件名作为
以及命令行参数，尽管您指向的文件必须是以前保存的资源版本。
编辑是使用用于获取资源的API版本完成的。要使用特定的API版本进行编辑，请完全限定
资源、版本和组。
默认格式为YAML。要在JSON中进行编辑，请指定“-o JSON”。
标志--windows行结束符可以用于强制windows行结束符，否则将是操作的默认值
将使用系统。
如果在更新时发生错误，将在磁盘上创建一个临时文件，其中包含未应用的
变化。更新资源时最常见的错误是另一个编辑器更改服务器上的资源。当这个
发生时，您将不得不将更改应用于资源的较新版本，或将临时保存的副本更新为
包括最新的资源版本


#编辑名为“docker registry”的服务：
kubectl edit svc/docker-registry

#使用替代编辑器
KUBE_EDITOR="nano" kubectl edit svc/docker-registry

#使用v1 API格式在JSON中编辑作业“myjob”：
kubectl edit job.v1.batch/myjob -o json

#在YAML中编辑部署“mydeployment”，并将修改后的配置保存在其注释中：
kubectl edit deployment/mydeployment -o yaml --save-config

Options:
      --allow-missing-template-keys=true: If true, ignore any errors in templates when a field or map key is missing in
the template. Only applies to golang and jsonpath output formats.
      --field-manager='kubectl-edit': Name of the manager used to track field ownership.
  -f, --filename=[]: Filename, directory, or URL to files to use to edit the resource
  -k, --kustomize='': Process the kustomization directory. This flag can't be used together with -f or -R.
  -o, --output='': Output format. One of:
json|yaml|name|go-template|go-template-file|template|templatefile|jsonpath|jsonpath-as-json|jsonpath-file.
      --output-patch=false: Output the patch if the resource is edited.
      --record=false: Record current kubectl command in the resource annotation. If set to false, do not record the
command. If set to true, record the command. If not set, default to updating the existing annotation value only if one
already exists.
  -R, --recursive=false: Process the directory used in -f, --filename recursively. Useful when you want to manage
related manifests organized within the same directory.
      --save-config=false: If true, the configuration of current object will be saved in its annotation. Otherwise, the
annotation will be unchanged. This flag is useful when you want to perform kubectl apply on this object in the future.
      --template='': Template string or path to template file to use when -o=go-template, -o=go-template-file. The
template format is golang templates [http://golang.org/pkg/text/template/#pkg-overview].
      --validate=true: If true, use a schema to validate the input before sending it
      --windows-line-endings=false: Defaults to the line ending native to your platform.

Usage:
  kubectl edit (RESOURCE/NAME | -f FILENAME) [options]

Use "kubectl options" for a list of global command-line options (applies to all commands).

```

### events



```shell

```

### exec

在容器中执行命令。

```shell
#从pod mypod中获取运行“date”命令的输出，默认情况下使用第一个容器
kubectl exec mypod -- date

# 从pod mypod获取ruby容器中运行“date”命令的输出
kubectl exec mypod -c ruby-container -- date

#切换到原始终端模式，将stdin从pod mypod发送到ruby容器中的“bash”
#并将“bash”中的stdout/stderr发送回客户端
kubectl exec mypod -c ruby-container -i -t -- bash -il

#列出pod mypod的第一个容器中/usr的内容，并按修改时间排序。
#如果要在pod中执行的命令有任何共同的标志（例如-i），
#必须使用两个短划线（--）来分隔命令的标志/参数。
#另请注意，不要将命令及其标志/参数用引号括起来
#除非这是正常执行它的方式（即执行ls-t/usr，而不是“ls-t/ur”）。
kubectl exec mypod -i -t -- ls -t /usr

#从部署mydeployment的第一个pod中运行“date”命令获得输出，默认情况下使用第一个容器
kubectl exec deploy/mydeployment -- date

#从服务myservice的第一个pod中运行“date”命令获得输出，默认情况下使用第一个容器
kubectl exec svc/myservice -- date

Options:
  -c, --container='': Container name. If omitted, the first container in the pod will be chosen
  -f, --filename=[]: to use to exec into the resource
      --pod-running-timeout=1m0s: The length of time (like 5s, 2m, or 3h, higher than zero) to wait until at least one
pod is running
  -i, --stdin=false: Pass stdin to the container
  -t, --tty=false: Stdin is a TTY

Usage:
  kubectl exec (POD | TYPE/NAME) [-c CONTAINER] [flags] -- COMMAND [args...] [options]

Use "kubectl options" for a list of global command-line options (applies to all commands).

```



### explain

列出支持的资源的字段

```shell
此命令描述与每个受支持的API资源相关联的字段。字段通过简单的
JSONPath标识符：
＜类型＞<fieldName>[.<fieldName>]
添加--recursive标志可以一次显示所有字段，而不显示说明。每个字段的信息如下
以OpenAPI格式从服务器检索。
使用“kubectl-api-resources”可以获得受支持资源的完整列表。


#获取资源及其字段的文档
kubectl explain pods
#获取资源的特定字段的文档
kubectl explain pods.spec.containers

Options:
      --api-version='': Get different explanations for particular API version (API group/version)
      --recursive=false: Print the fields of fields (Currently only 1 level deep)

Usage:
  kubectl explain RESOURCE [options]

Use "kubectl options" for a list of global command-line options (applies to all commands).

```

### expose

将资源作为新的Kubernetes服务公开。

```shell
按名称查找部署、服务、副本集、复制控制器或pod，并为此使用选择器资源作为指定端口上新服务的选择器。部署集或副本集将公开为
仅当其选择器可转换为服务支持的选择器时，即当选择器仅包含matchLabels组件。请注意，如果没有通过--port指定端口，并且公开的资源有多个端口，则所有将被新服务重新使用。此外，如果没有指定标签，新服务将重复使用它公开的资源。
可能的资源包括（不区分大小写）：
pod（po）、服务（svc）、复制控制器（rc）、部署（deploy）、复制集（rs）


#为复制的nginx创建一个服务，该服务在端口80上提供服务，并连接到端口8000上的容器。
kubectl expose rc nginx --port=80 --target-port=8000

#为复制控制器创建一个服务，该复制控制器由“nginx-controller.yaml”中指定的类型和名称标识，它在端口80上提供服务，并连接到端口8000上的容器。
kubectl expose -f nginx-controller.yaml --port=80 --target-port=8000

#为一个pod有效的pod创建一个服务，该pod在端口444上服务，名称为“frontend”
kubectl expose pod valid-pod --port=444 --name=frontend

#基于上述服务创建第二个服务，将容器端口8443公开为端口443，并使用名称 “nginx https”
kubectl expose service nginx --port=443 --target-port=8443 --name=nginx-https

#为端口4100上的复制流应用程序创建一个服务，以平衡UDP流量并命名为“视频流”。
kubectl expose rc streamer --port=4100 --protocol=UDP --name=video-stream

#使用复制集为复制的nginx创建一个服务，该复制集在端口80上服务，并连接到端口8000上的容器。
kubectl expose rs nginx --port=80 --target-port=8000

#为nginx部署创建一个服务，该服务在端口80上提供服务，并连接到端口8000上的容器。
kubectl expose deployment nginx --port=80 --target-port=8000

Options:
      --allow-missing-template-keys=true: If true, ignore any errors in templates when a field or map key is missing in
the template. Only applies to golang and jsonpath output formats.
      --cluster-ip='': ClusterIP to be assigned to the service. Leave empty to auto-allocate, or set to 'None' to create
a headless service.
      --dry-run='none': Must be "none", "server", or "client". If client strategy, only print the object that would be
sent, without sending it. If server strategy, submit server-side request without persisting the resource.
      --external-ip='': Additional external IP address (not managed by Kubernetes) to accept for the service. If this IP
is routed to a node, the service can be accessed by this IP in addition to its generated service IP.
      --field-manager='kubectl-expose': Name of the manager used to track field ownership.
  -f, --filename=[]: Filename, directory, or URL to files identifying the resource to expose a service
      --generator='service/v2': The name of the API generator to use. There are 2 generators: 'service/v1' and
'service/v2'. The only difference between them is that service port in v1 is named 'default', while it is left unnamed
in v2. Default is 'service/v2'.
  -k, --kustomize='': Process the kustomization directory. This flag can't be used together with -f or -R.
  -l, --labels='': Labels to apply to the service created by this call.
      --load-balancer-ip='': IP to assign to the LoadBalancer. If empty, an ephemeral IP will be created and used
(cloud-provider specific).
      --name='': The name for the newly created object.
  -o, --output='': Output format. One of:
json|yaml|name|go-template|go-template-file|template|templatefile|jsonpath|jsonpath-as-json|jsonpath-file.
      --overrides='': An inline JSON override for the generated object. If this is non-empty, it is used to override the
generated object. Requires that the object supply a valid apiVersion field.
      --port='': The port that the service should serve on. Copied from the resource being exposed, if unspecified
      --protocol='': The network protocol for the service to be created. Default is 'TCP'.
      --record=false: Record current kubectl command in the resource annotation. If set to false, do not record the
command. If set to true, record the command. If not set, default to updating the existing annotation value only if one
already exists.
  -R, --recursive=false: Process the directory used in -f, --filename recursively. Useful when you want to manage
related manifests organized within the same directory.
      --save-config=false: If true, the configuration of current object will be saved in its annotation. Otherwise, the
annotation will be unchanged. This flag is useful when you want to perform kubectl apply on this object in the future.
      --selector='': A label selector to use for this service. Only equality-based selector requirements are supported.
If empty (the default) infer the selector from the replication controller or replica set.)
      --session-affinity='': If non-empty, set the session affinity for the service to this; legal values: 'None',
'ClientIP'
      --target-port='': Name or number for the port on the container that the service should direct traffic to.
Optional.
      --template='': Template string or path to template file to use when -o=go-template, -o=go-template-file. The
template format is golang templates [http://golang.org/pkg/text/template/#pkg-overview].
      --type='': Type for this service: ClusterIP, NodePort, LoadBalancer, or ExternalName. Default is 'ClusterIP'.

Usage:
  kubectl expose (-f FILENAME | TYPE NAME) [--port=port] [--protocol=TCP|UDP|SCTP] [--target-port=number-or-name]
[--name=name] [--external-ip=external-ip-of-service] [--type=type] [options]

Use "kubectl options" for a list of global command-line options (applies to all commands).

```



### get



```shell
显示一个或多个资源
打印有关指定资源的最重要信息的表格。您可以使用标签筛选列表
选择器和--select标志。如果所需的资源类型是按名称命名的，则您只能在当前的
命名空间，除非您传递--所有命名空间。
未初始化的对象不会显示，除非传递了-include Uninitialized。
通过将输出指定为“template”并提供Go模板作为--template标志的值，可以进行筛选
获取的资源的属性。
使用“kubectl-api-resources”可以获得受支持资源的完整列表。

#以ps输出格式列出所有pod。
kubectl get pods

#以ps输出格式列出所有pod，并提供更多信息（如节点名称）。
kubectl get pods -o wide

#以ps输出格式列出具有指定NAME的单个复制控制器。
kubectl get replicationcontroller web

#在“应用程序”API组的“v1”版本中，以JSON输出格式列出部署：
kubectl get deployments.v1.apps -o json

#以JSON输出格式列出单个pod。
kubectl get -o json pod web-pod-13je7

#以JSON输出格式列出一个由“pod.yaml”中指定的类型和名称标识的pod。
kubectl get -f pod.yaml -o json

#使用kustomization.yaml列出目录中的资源，例如dir/kustomization.yaml。
kubectl get -k dir/

#仅返回指定吊舱的相位值。
kubectl get -o template pod/web-pod-13je7 --template={{.status.phase}}

#仅返回指定吊舱的相位值。#在自定义列中列出资源信息。
kubectl get pod test-pod -o custom-columns=CONTAINER:.spec.containers[0].name,IMAGE:.spec.containers[0].image

#以ps输出格式将所有复制控制器和服务一起列出。
kubectl get rc,services

#按类型和名称列出一个或多个资源。
kubectl get rc/web service/frontend pods/web-pod-13je7

Options:
  -A, --all-namespaces=false: If present, list the requested object(s) across all namespaces. Namespace in current
context is ignored even if specified with --namespace.
      --allow-missing-template-keys=true: If true, ignore any errors in templates when a field or map key is missing in
the template. Only applies to golang and jsonpath output formats.
      --chunk-size=500: Return large lists in chunks rather than all at once. Pass 0 to disable. This flag is beta and
may change in the future.
      --field-selector='': Selector (field query) to filter on, supports '=', '==', and '!='.(e.g. --field-selector
key1=value1,key2=value2). The server only supports a limited number of field queries per type.
  -f, --filename=[]: Filename, directory, or URL to files identifying the resource to get from a server.
      --ignore-not-found=false: If the requested object does not exist the command will return exit code 0.
  -k, --kustomize='': Process the kustomization directory. This flag can't be used together with -f or -R.
  -L, --label-columns=[]: Accepts a comma separated list of labels that are going to be presented as columns. Names are
case-sensitive. You can also use multiple flag options like -L label1 -L label2...
      --no-headers=false: When using the default or custom-column output format, don't print headers (default print
headers).
  -o, --output='': Output format. One of:
json|yaml|wide|name|custom-columns=...|custom-columns-file=...|go-template=...|go-template-file=...|jsonpath=...|jsonpath-file=...
See custom columns [http://kubernetes.io/docs/user-guide/kubectl-overview/#custom-columns], golang template
[http://golang.org/pkg/text/template/#pkg-overview] and jsonpath template
[http://kubernetes.io/docs/user-guide/jsonpath].
      --output-watch-events=false: Output watch event objects when --watch or --watch-only is used. Existing objects are
output as initial ADDED events.
      --raw='': Raw URI to request from the server.  Uses the transport specified by the kubeconfig file.
  -R, --recursive=false: Process the directory used in -f, --filename recursively. Useful when you want to manage
related manifests organized within the same directory.
  -l, --selector='': Selector (label query) to filter on, supports '=', '==', and '!='.(e.g. -l key1=value1,key2=value2)
      --server-print=true: If true, have the server return the appropriate table output. Supports extension APIs and
CRDs.
      --show-kind=false: If present, list the resource type for the requested object(s).
      --show-labels=false: When printing, show all labels as the last column (default hide labels column)
      --sort-by='': If non-empty, sort list types using this field specification.  The field specification is expressed
as a JSONPath expression (e.g. '{.metadata.name}'). The field in the API resource specified by this JSONPath expression
must be an integer or a string.
      --template='': Template string or path to template file to use when -o=go-template, -o=go-template-file. The
template format is golang templates [http://golang.org/pkg/text/template/#pkg-overview].
  -w, --watch=false: After listing/getting the requested object, watch for changes. Uninitialized objects are excluded
if no object name is provided.
      --watch-only=false: Watch for changes to the requested object(s), without listing/getting first.

Usage:
  kubectl get
[(-o|--output=)json|yaml|wide|custom-columns=...|custom-columns-file=...|go-template=...|go-template-file=...|jsonpath=...|jsonpath-file=...]
(TYPE[.VERSION][.GROUP] [NAME | -l label] | TYPE[.VERSION][.GROUP]/NAME ...) [flags] [options]

Use "kubectl options" for a list of global command-line options (applies to all commands).

```

### kustomize

打印一组由kustomization.yaml文件中的指令生成的API资源。

```shell
参数必须是包含文件的目录的路径，或者是带有路径后缀的git存储库URL
针对存储库根指定相同的内容。

 kubectl kustomize somedir


#使用当前工作目录
kubectl kustomize .

# 使用一些共享配置目录
kubectl kustomize /home/configuration/production

#使用URL
kubectl kustomize github.com/kubernetes-sigs/kustomize.git/examples/helloWorld?ref=v1.0.6


```



### label

更新资源上的标签。

```shell
*标签键和值必须以字母或数字开头，并且可以包含字母、数字、连字符、点和下划线，每个最多63个字符。
*可选地，密钥可以以DNS子域前缀和单个“/”开头，如example.com/my-app
*如果--overwrite为true，则可以覆盖现有标签，否则尝试覆盖标签将导致错误。
*如果指定了--resource版本，则更新将使用此资源版本，否则现有的将使用资源版本。


#用标签“unhealth”和值“true”更新pod“foo”。
kubectl label pods foo unhealthy=true

#用标签“status”和值“unhealth”更新pod“foo”，覆盖任何现有值。
kubectl label --overwrite pods foo status=unhealthy

#更新命名空间中的所有pod
kubectl label pods --all status=unhealthy

#更新由“pod.json”中的类型和名称标识的pod
kubectl label -f pod.json status=unhealthy

# 仅当资源与版本1相比没有变化时，才更新pod“foo”。
kubectl label pods foo status=unhealthy --resource-version=1

#通过删除名为“bar”的标签（如果存在）来更新pod“foo”。
#不需要--overwrite标志。
kubectl label pods foo bar-

```



### logs

打印pod或指定资源中容器的日志。如果pod只有一个容器，那么容器名称是可选的。

```shell
#只使用一个容器从pod nginx返回快照日志
kubectl logs nginx

#使用多容器从pod nginx返回快照日志
kubectl logs nginx --all-containers=true

#返回标签app=nginx定义的pod中所有容器的快照日志
kubectl logs -lapp=nginx --all-containers=true

#从pod web-1返回先前终止的ruby容器日志的快照
kubectl logs -p -c ruby web-1

#在pod web-1中开始流式传输ruby容器的日志
kubectl logs -f -c ruby web-1

#开始从标签app=nginx定义的pod中的所有容器流式传输日志
kubectl logs -f -lapp=nginx --all-containers=true

#仅显示pod nginx中最近的20行输出
kubectl logs --tail=20 nginx

# 显示pod nginx在最后一个小时内编写的所有日志
kubectl logs --since=1h nginx

# 显示来自具有过期服务证书的kubelet的日志
kubectl logs --insecure-skip-tls-verify-backend nginx

# 从名为hello的作业的第一个容器返回快照日志
kubectl logs job/hello

# 从名为nginx的部署的容器nginx-1返回快照日志
kubectl logs deployment/nginx -c nginx-1

```



### options

```shell
以下选项可以传递给任何命令：

      --add-dir-header=false: If true, adds the file directory to the header of the log messages
      --alsologtostderr=false: log to standard error as well as files
      --as='': Username to impersonate for the operation
      --as-group=[]: Group to impersonate for the operation, this flag can be repeated to specify multiple groups.
      --cache-dir='/root/.kube/cache': Default cache directory
      --certificate-authority='': Path to a cert file for the certificate authority
      --client-certificate='': Path to a client certificate file for TLS
      --client-key='': Path to a client key file for TLS
      --cluster='': The name of the kubeconfig cluster to use
      --context='': The name of the kubeconfig context to use
      --insecure-skip-tls-verify=false: If true, the server's certificate will not be checked for validity. This will
make your HTTPS connections insecure
      --kubeconfig='': Path to the kubeconfig file to use for CLI requests.
      --log-backtrace-at=:0: when logging hits line file:N, emit a stack trace
      --log-dir='': If non-empty, write log files in this directory
      --log-file='': If non-empty, use this log file
      --log-file-max-size=1800: Defines the maximum size a log file can grow to. Unit is megabytes. If the value is 0,
the maximum file size is unlimited.
      --log-flush-frequency=5s: Maximum number of seconds between log flushes
      --logtostderr=true: log to standard error instead of files
      --match-server-version=false: Require server version to match client version
  -n, --namespace='': If present, the namespace scope for this CLI request
      --one-output=false: If true, only write logs to their native severity level (vs also writing to each lower
severity level
      --password='': Password for basic authentication to the API server
      --profile='none': Name of profile to capture. One of (none|cpu|heap|goroutine|threadcreate|block|mutex)
      --profile-output='profile.pprof': Name of the file to write the profile to
      --request-timeout='0': The length of time to wait before giving up on a single server request. Non-zero values
should contain a corresponding time unit (e.g. 1s, 2m, 3h). A value of zero means don't timeout requests.
  -s, --server='': The address and port of the Kubernetes API server
      --skip-headers=false: If true, avoid header prefixes in the log messages
      --skip-log-headers=false: If true, avoid headers when opening log files
      --stderrthreshold=2: logs at or above this threshold go to stderr
      --tls-server-name='': Server name to use for server certificate validation. If it is not provided, the hostname
used to contact the server is used
      --token='': Bearer token for authentication to the API server
      --user='': The name of the kubeconfig user to use
      --username='': Username for basic authentication to the API server
  -v, --v=0: number for the log level verbosity
      --vmodule=: comma-separated list of pattern=N settings for file-filtered logging
      --warnings-as-errors=false: Treat warnings received from the server as errors and exit with a non-zero exit code
```

### patch

```shell
使用策略合并补丁、JSON合并补丁或JSON补丁更新资源的字段。
可以接受JSON和YAML格式。

# 使用战略合并修补程序部分更新节点。将补丁指定为JSON。
kubectl patch node k8s-node-1 -p '{"spec":{"unschedulable":true}}'

# 使用战略合并修补程序部分更新节点。将修补程序指定为YAML。
kubectl patch node k8s-node-1 -p $'spec:\n unschedulable: true'

# 使用策略合并补丁部分更新由“node.json”中指定的类型和名称标识的节点。
kubectl patch -f node.json -p '{"spec":{"unschedulable":true}}'

# 更新容器的图像；spec.contators[*].name是必需的，因为它是一个合并键。
kubectl patch pod valid-pod -p '{"spec":{"containers":[{"name":"kubernetes-serve-hostname","image":"new image"}]}}'

# 使用带有位置数组的json补丁更新容器的图像。
kubectl patch pod valid-pod --type='json' -p='[{"op": "replace", "path": "/spec/containers/0/image", "value":"new
image"}]'
```

### plugin

提供用于与插件交互的实用程序。

```shell
插件提供的扩展功能不是主要命令行发行版的一部分。请参阅
有关如何编写自己的插件的更多信息，请参阅文档和示例。

发现和安装插件的最简单方法是通过kubernetes子项目krew。要安装krew，请访问
https://krew.sigs.k8s.io/docs/user-guide/setup/install/


Available Commands:
  list        列出用户PATH上所有可见的插件可执行文件

Usage:
  kubectl plugin [flags] [options]

Use "kubectl <command> --help" for more information about a given command.
Use "kubectl options" for a list of global command-line options (applies to all commands).


[root@k8s_master01 ~]# kubectl plugin  list
The following compatible plugins are available:

/usr/local/bin/kubectl-convert
Unable read directory "/root/bin" from your PATH: open /root/bin: no such file or directory. Skipping...
```

### port-forward

将一个或多个本地端口转发到一个pod。此命令要求节点安装“socat”。

```shell



使用资源类型/名称（如deploymy/mydeployment）来选择一个pod。如果省略，资源类型默认为“pod”。
如果有多个吊舱符合标准，则会自动选择一个吊舱。转发会话结束
当所选pod终止时，需要重新运行该命令以恢复转发。


#本地监听端口5000和6000，将数据转发到吊舱中的端口5000和六千
kubectl port-forward pod/mypod 5000 6000

#本地监听端口5000和6000，将数据转发到部署所选的pod中的端口5000和六千
kubectl port-forward deployment/mydeployment 5000 6000

#在本地侦听端口8443，转发到服务选择的pod中名为“https”的服务端口的targetPort
kubectl port-forward service/myservice 8443:https

#本地监听端口8888，转发到吊舱中的5000
kubectl port-forward pod/mypod 8888:5000

#监听所有地址的8888端口，转发到吊舱中的5000
kubectl port-forward --address 0.0.0.0 pod/mypod 8888:5000

# 监听本地主机上的8888端口并选择IP，转发到pod中的5000
kubectl port-forward --address localhost,10.19.21.23 pod/mypod 8888:5000

# 在本地监听一个随机端口，转发到吊舱中的5000
kubectl port-forward pod/mypod :5000

Options:
      --address=[localhost]: Addresses to listen on (comma separated). Only accepts IP addresses or localhost as a
value. When localhost is supplied, kubectl will try to bind on both 127.0.0.1 and ::1 and will fail if neither of these
addresses are available to bind.
      --pod-running-timeout=1m0s: The length of time (like 5s, 2m, or 3h, higher than zero) to wait until at least one
pod is running

Usage:
  kubectl port-forward TYPE/NAME [options] [LOCAL_PORT:]REMOTE_PORT [...[LOCAL_PORT_N:]REMOTE_PORT_N]

Use "kubectl options" for a list of global command-line options (applies to all commands).

```



### proxy



```shell
在本地主机和Kubernetes API服务器之间创建代理服务器或应用程序级网关。它还允许通过指定的HTTP路径提供静态内容。除了与静态内容路径匹配的路径外，所有传入数据都通过一个端口进入并转发到远程kubernetes API Server端口。


#要代理所有的kubernetes api，而不代理其他任何东西，请使用：
$ kubectl proxy --api-prefix=/

#要仅代理kubernetes api的一部分以及一些静态文件，请执行以下操作：
$ kubectl proxy --www=/my/files --www-prefix=/static/ --api-prefix=/api/

#上面的内容允许您“卷曲localhost:8001/api/v1/pods”。
#要在不同的根目录下代理整个kubernetes api，请使用：
$ kubectl proxy --api-prefix=/custom/

#上面让您可以“卷曲localhost:8001/custom/api/v1/pods”
#在端口8011上运行kubernetes apiserver的代理，从中提供静态内容/本地/www/
kubectl proxy --port=8011 --www=./local/www/

#在任意本地端口上运行kubernetes apiserver的代理。
#为服务器选择的端口将输出到stdout。
kubectl proxy --port=0

#运行kubernetes apiserver的代理，将api前缀更改为k8s api
#这使得例如pods api可以在localhost:8001/k8s api/v1/pods上使用/
kubectl proxy --api-prefix=/k8s-api


```



### replace

用文件名或stdin替换资源。

```shell

#使用pod.json中的数据替换一个pod。
kubectl replace -f ./pod.json

#根据传递到stdin的JSON替换pod。
cat pod.json | kubectl replace -f -

#将单个容器pod的映像版本（标记）更新为v4
kubectl get pod mypod -o yaml | sed 's/\(image: myimage\):.*$/\1:v4/' | kubectl replace -f -

#强制替换、删除然后重新创建资源
kubectl replace --force -f ./pod.json

```





## 配置文件







# 综合案例



























































































































# 问题记录

学习过程中容易混淆的点：

K8S中service有三种：ClusterIP、 NodePort、LoadBalancer（作用：自带的网络模式）

Ingress的作用：将内部服务暴露在外网

traefik的作用：将内部服务暴露在外网

Istio的作用：服务网格，应该是在将服务暴露到外网之上增加了一些网络功能的工具

calico和flannel的作用：控制容器间、 集群节点之间的网络通信:[k8s网络之Calico网络 - 金色旭光 - 博客园 (cnblogs.com)](https://www.cnblogs.com/goldsunshine/p/10701242.html#AtaT7QpZ)

[K8S ClusterIP、 NodePort、LoadBalancer和Ingress 区别 - 王叫兽 - 博客园 (cnblogs.com)](https://www.cnblogs.com/younger5/p/14784743.html)

区分kubernetes中的ingress和ingress controller：[08 . Kubernetes之 ingress及Ingress Controller - 腾讯云开发者社区-腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1706705)



node节点的kubelet无法启动

[kubelet跳过pod同步时出错-容器运行时已关闭 - 问答 - 腾讯云开发者社区-腾讯云 (tencent.com)](https://cloud.tencent.com/developer/ask/sof/620192)

[kubelet启动异常-陈桂林博客 (gl.sh.cn)](https://www.gl.sh.cn/2021/07/04/kubelet_qi_dong_yi_chang.html)





CA HPA VPA 的概念：[Kubernetes 的 HPA 原理详解 - 掘金 (juejin.cn)](https://juejin.cn/post/7209839016965931045)



K8S可以为命名空间做资源限制：[(10条消息) k8s做命名空间配额_神圣坤的博客-CSDN博客](https://blog.csdn.net/weixin_37934134/article/details/106774840#:~:text=k8s的解决方法是，通过RBAC将不同团队（or,项目）限制在不同的namespace下，通过resourceQuota来限制该namespace能够使用的资源。)

K8S中QoS pods被kill掉的场景与顺序：

```ini
Guaranteed：pod中的所有容器都必须对cpu和memory同时设置limits，如果有一个容器要设置requests，那么所有容器都要设置，并设置参数同limits一致，那么这个pod的QoS就是Guaranteed级别。
注：如果一个容器只指明limit而未设定request，则request的值等于limit值。

Burstable: pod中只要有一个容器的requests和limits的设置不相同，该pod的QoS即为Burstable。举例

Best-Effort：如果对于全部的resources来说requests与limits均未设置，该pod的QoS即为Best-Effort。


 QoS优先级:3种QoS优先级从有低到高（从左向右）：
 Best-Effort pods -> Burstable pods -> Guaranteed pods
 
Best-Effort 类型的pods：系统用完了全部内存时，该类型pods会最先被kill掉。

Burstable类型pods：系统用完了全部内存，且没有Best-Effort container可以被kill时，该类型pods会被kill掉。

Guaranteed pods：系统用完了全部内存、且没有Burstable与Best-Effort container可以被kill，该类型的pods会被kill掉。
 注：如果pod进程因使用超过预先设定的*limites*而非Node资源紧张情况，系统倾向于在其原所在的机器上重启该container或本机或其他重新创建一个pod
 

```





























# 面试题

## Kubernetes的组件以及作用

**Kubernetes中master核心组件**

1）etcd

是Kubernetes的存储状态的数据库（所有master的持续状态都存在etcd的一个实例中）；
它是一个集群分布式数据库，它可以提供分布式数据的一致性。

2）apiserver

Kubernetes的核心组件是API Server，它是Kubernetes系统和Etcd直接对话的唯一组件
提供了资源操作的唯一入口，并提供认证、授权、访问控制、API注册和发现等机制；

3）controller manager

controller manager 是通过API Server 进行协调的组件，绑定到单独的服务器Master上；
负责维护集群的状态，比如故障检测、自动扩展、滚动更新等；

4）scheduler

scheduler是通过API Server 进行协调的组件，绑定到单独的服务器Master上；
scheduler负责资源的调度，按照预定的调度策略将Pod调度到相应的机器上；

**Kubernetes中Node核心组件**

1）kubelet（Node Agent）

kubelet负责管理pods和它们上面的容器，images镜像、volumes、etc。
Agent负责监视绑定到其节点的一组Pod，并确保这些Pod正常运行，并且能实时返回这些Pod的运行状态。

2）Container runtime

Container runtime负责镜像管理以及Pod和容器的真正运行（CRI）；

3）kube-proxy

kube-proxy网络代理和负载均衡，负责为Service提供cluster内部的服务发现和负载均衡；

```text
kube-dns负责为整个集群提供DNS服务
Ingress Controller为服务提供外网入口
Heapster提供资源监控
Dashboard提供GUI
Federation提供跨可用区的集群
Fluentd-elasticsearch提供集群日志采集、存储与查询
```



## Kubenetes的工作过程

比如创建Pod流程

1. 用户管理员创建 Pod 的请求默认是通过kubectl 客户端管理命令 api server 组件进行交互的，默认会将请求发送给 API Server。
2. API Server 会根据请求的类型选择用何种 REST API 对请求作出处理。
3. REST Storage API 会对请求作相应的处理并将处理的结果存入高可用键值存储系统 Etcd 中。
4. 在 API Server 响应管理员kubectl 的请求后，Scheduler 会根据ETCD集群中运行 Pod情况 及 Node 信息进行判断，根据一组相关规则将pod分配到可以运行它们的节点上，并更新数据库，记录pod分配情况。
5. Kubelet监控数据库变化，管理后续pod的生命周期，发现被分配到它所在的节点上运行的那些pod，就会进行docker组件的启动，docker组件会启动对应的容器（pod），会在该节点上运行这个新pod。
6. kube-proxy运行在集群各个节点主机上，管理网络通信，如服务发现、负载均衡。例如当有数据发送到主机时，将其路由到正确的pod或容器。对于从主机上发出的数据，它可以基于请求地址发现远程服务器，并将数据正确路由，在某些情况下会使用轮训调度算法(Round-robin)将请求发送到集群中的多个实例。





![image-20230404151124708](README.assets/image-20230404151124708.png)

## Kubernetes的资源类型以及特性



## K8S证书过期如何替换





## pod状态一共有几种



## 如何将某一pod调度到指定的node上

```shell
方式1：强制固定
vim text.yaml
nodeName: worker01  #添加节点名参数
方式2：使用标签选择器（label-selector）
a.给节点添加标签
b.nodeSelector:    #标签选择器参数
    type: node_type #刚刚添加的类型
```



## 滚动发布、灰度发布、蓝绿发布、金丝雀发布的含义

