Kubeadm 安装k8s

1、基础设置

```shell
# 设置主机名
hostnamectl set-hostname master
hostnamectl set-hostname node01
hostnamectl set-hostname node02
# 检查每台机器的uuid 和 ip link
cat /sys/class/dmi/id/product_uuid
ip link
# 更换国内源
sudo cp /etc/apt/sources.list /etc/apt/sources.list_backup
sudo vim /etc/apt/sources.list
删除原有的内容，添加以下：
# 默认注释了源码仓库，如有需要可自行取消注释
deb https://mirrors.ustc.edu.cn/ubuntu-ports/ focal main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu-ports/ focal main main restricted universe multiverse
deb https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-updates main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-updates main restricted universe multiverse
deb https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-backports main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-backports main restricted universe multiverse
deb https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-security main restricted universe multiverse

sudo apt-get update
sudo apt-get upgrade

#关闭防火墙
systemctl disable --now ufw
# 关闭selinux  # 永久   # 临时 
sed -i 's/enforcing/disabled/' /etc/selinux/config 
setenforce 0 
# 设置时区
sudo cp /usr/share/zoneinfo/Asia/Shanghai  /etc/localtime -r
# 24小时制
echo '/etc/default/locale' > /etc/default/locale
# 关闭swap  # 临时    # 永久 
swapoff -a  
sed -ri 's/.*swap.*/#&/' /etc/fstab  
free -h

# 在master添加hosts 
cat >> /etc/hosts << EOF 
10.0.0.10 master 
10.0.0.11 node01
10.0.0.12 node02
EOF

# 将桥接的IPv4流量传递到iptables的链  # 生效 
cat > /etc/sysctl.d/k8s.conf << EOF 
net.bridge.bridge-nf-call-ip6tables = 1 
net.bridge.bridge-nf-call-iptables = 1 
EOF
sysctl --system  
# 加载br_netfilter
sudo modprobe br_netfilter
sudo sh -c 'echo "br_netfilter" > /etc/modules-load.d/br_netfilter.conf'

# 时间同步 
apt install ntpdate -y 
/usr/sbin/ntpdate ntp1.aliyun.com
crontab -l > crontab_conf ; echo "* * * * * /usr/sbin/ntpdate ntp1.aliyun.com >/dev/null 2>&1" >> crontab_conf && crontab crontab_conf && rm -f crontab_conf
 crontab   -l
 
# 设置limit
echo "* soft nofile 655360" >> /etc/security/limits.conf
echo "* hard nofile 655360" >> /etc/security/limits.conf
echo "* soft nproc 655360"  >> /etc/security/limits.conf
echo "* hard nproc 655360"  >> /etc/security/limits.conf
echo "* soft  memlock  unlimited"  >> /etc/security/limits.conf
echo "* hard memlock  unlimited"  >> /etc/security/limits.conf
echo "DefaultLimitNOFILE=1024000"  >> /etc/systemd/system.conf
echo "DefaultLimitNPROC=1024000"  >> /etc/systemd/system.conf
ulimit -Hn
```

2、安装containerd

```shell
# 安装containerd
apt install -y containerd
# 配置Containerd所需的模块
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF
systemctl restart systemd-modules-load.service
#配置Containerd所需的内核
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF
sysctl --system
# 创建Containerd的配置文件
mkdir -p /etc/containerd
containerd config default | tee /etc/containerd/config.toml
 sed -i "s#SystemdCgroup\ \=\ false#SystemdCgroup\ \=\ true#g" /etc/containerd/config.toml
cat /etc/containerd/config.toml | grep SystemdCgroup
# 修改默认镜像仓库
vim  /etc/containerd/config.toml 
sandbox_image = "registry.cn-hangzhou.aliyuncs.com/google_containers/pause:3.9"
# 设置开机启动
systemctl daemon-reload
systemctl start containerd
systemctl enable containerd
systemctl status containerd

# 检查
ctr version
```



3、安装kubeadm

```shell
apt update && apt install -y apt-transport-https

curl https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | apt-key add - 
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main
EOF
apt update
# 查看可用版本
apt-cache madison kubeadm
apt install -y kubelet=1.18.1-00 kubeadm=1.18.1-00 kubectl=1.18.1-00 
#apt install -y kubelet kubeadm kubectl

# 初始化获取要下载的镜像列表
kubeadm config images list
# 生成默认kubeadm.conf文件
# kubeadm config print init-defaults > kubeadm.conf

# 下载镜像
docker pull mirrorgcrio/kube-apiserver-arm64:v1.18.2
docker pull mirrorgcrio/kube-controller-manager-arm64:v1.18.2
docker pull mirrorgcrio/kube-scheduler-arm64:v1.18.2
docker pull mirrorgcrio/kube-proxy-arm64:v1.18.2
docker pull mirrorgcrio/etcd-arm64:3.4.3-0
docker pull mirrorgcrio/pause-arm64:3.2
docker pull tkestack/coredns-arm64:1.6.7
# 修改tag
docker tag mirrorgcrio/kube-apiserver-arm64:v1.18.2 registry.aliyuncs.com/google_containers/kube-apiserver:v1.18.2
docker tag mirrorgcrio/kube-scheduler-arm64:v1.18.2 registry.aliyuncs.com/google_containers/kube-scheduler:v1.18.2
docker tag mirrorgcrio/kube-controller-manager-arm64:v1.18.2 registry.aliyuncs.com/google_containers/kube-controller-manager:v1.18.2
docker tag mirrorgcrio/kube-proxy-arm64:v1.18.2 registry.aliyuncs.com/google_containers/kube-proxy:v1.18.2
docker tag mirrorgcrio/etcd-arm64:3.4.3-0 registry.aliyuncs.com/google_containers/etcd:3.4.3-0
docker tag mirrorgcrio/pause-arm64:3.2 registry.aliyuncs.com/google_containers/pause:3.2
docker tag tkestack/coredns-arm64:1.6.7 registry.aliyuncs.com/google_containers/coredns-arm64:1.6.7


# 初始化
kubeadm init --kubernetes-version=v1.18.2 --pod-network-cidr=172.22.0.0/16 --apiserver-advertise-address=10.0.0.10  -v 6 --image-repository=registry.aliyuncs.com/google_containers


# 部署网络
docker pull calico/cni:v3.14.1
kubectl apply -f https://docs.projectcalico.org/v3.14/manifests/calico.yaml
# 安装helm
wget https://mirrors.huaweicloud.com/helm/v3.0.2/helm-v3.0.2-linux-arm64.tar.gz
tar -zxvf helm-v3.0.2-linux-arm64.tar.gz
mv linux-arm64/helm  /usr/bin/

helm repo add stable https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts
helm repo add kaiyuanshe http://mirror.kaiyuanshe.cn/kubernetes/charts
helm repo add azure http://mirror.azure.cn/kubernetes/charts
helm repo add dandydev https://dandydeveloper.github.io/charts


# dashboard (dashboard采用kuboard)
sudo docker run -d \
  --restart=unless-stopped \
  --name=kuboard \
  -p 80:80/tcp \
  -p 10081:10081/tcp \
  -e KUBOARD_ENDPOINT="http://10.0.0.10:80" \
  -e KUBOARD_AGENT_SERVER_TCP_PORT="10081" \
  -v /root/kuboard-data:/data \
  eipwork/kuboard:v3
 
#在浏览器输入 http://your-host-ip:80 即可访问 Kuboard v3.x 的界面，登录方式：
#用户名： admin
#密 码： Kuboard123
```
