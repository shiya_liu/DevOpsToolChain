简介

官网：[灰日志架构 (graylog.org)](https://go2docs.graylog.org/5-1/what_is_graylog/what_is_graylog.htm)



安装部署

[(4条消息) Centos7 安装Graylog 5.0收集网络设备运行日志+卸载GrayLog服务_graylog 网络设备日志_祁小林林的博客-CSDN博客](https://blog.csdn.net/qixiaolinlin/article/details/129966703)

使用





Graylog 5.1 需要满足以下条件来保持与其软件依赖项的兼容性：

- OpenJDK 17（它嵌入在Graylog中，不需要单独安装。
- OpenSearch 1.x， 2.x 或 Elasticsearch 7.10.2：将日志读取到es中
- MongoDB 5.x 或 6.x: 存储graylog系统信息 比如用户等信息

基础环境设置

```shell
# 下载的包会换存在/var/cache/apt/archives/目录中
# 修改时区
sudo timedatectl set-timezone Asia/Shanghai
#同步时间
apt install ntpdate -y 
ntpdate time.windows.com
# 设置limit
echo "* soft nofile 655360" >> /etc/security/limits.conf
echo "* hard nofile 655360" >> /etc/security/limits.conf
echo "* soft nproc 655360"  >> /etc/security/limits.conf
echo "* hard nproc 655360"  >> /etc/security/limits.conf
echo "* soft  memlock  unlimited"  >> /etc/security/limits.conf
echo "* hard memlock  unlimited"  >> /etc/security/limits.conf
echo "DefaultLimitNOFILE=1024000"  >> /etc/systemd/system.conf
echo "DefaultLimitNPROC=1024000"  >> /etc/systemd/system.conf
ulimit -Hn
# 设置24小时制
vim /etc/default/locale
LANG=en_US.UTF-8
LC_TIME=en_DK.UTF-8
reboot
# 关闭swap  # 临时    # 永久 
swapoff -a  
sed -ri 's/.*swap.*/#&/' /etc/fstab  
```



1、MongoDB

```shell
wget https://fastdl.mongodb.org/linux/mongodb-linux-aarch64-ubuntu2004-6.0.2.tgz 
tar -zxvf mongodb-linux-aarch64-ubuntu2004-6.0.2.tgz
mv mongodb-linux-aarch64-ubuntu2004-6.0.2 /usr/local/mongodb

mkdir /etc/mongodb/
cd /etc/mongodb/
mkdir data data/db data/log
chmod 666 data/db data/log/
cat >>/etc/profile <<'EOF'
# mongodb
export MONGODB_HOME=/usr/local/mongodb
export PATH=$PATH:$MONGODB_HOME/bin
EOF
source /etc/profile 

cat >/etc/mongodb/mongodb.conf <<-EOF
# 数据库数据存放目录
dbpath=/etc/mongodb/data/db
# 日志文件存放目录
logpath=/etc/mongodb/data/log/mongodb.log
# 日志追加方式
logappend=true
# 端口
port=27017
# 是否认证
#auth=true
# 以守护进程方式在后台运行
fork=true
# 远程连接要指定ip，否则无法连接；0.0.0.0代表不限制ip访问
bind_ip=0.0.0.0
EOF


mongod -f /etc/mongodb/mongodb.conf
netstat  -ntpl
ps aux | grep mongo
mongod -f /etc/mongodb/mongodb.conf  --shutdown

cat >/lib/systemd/system/mongodb.service<<'EOF'
[Unit]
Description=mongodb
After=network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
ExecStart=/usr/local/mongodb/bin/mongod -f /etc/mongodb/mongodb.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/usr/local/mongodb/bin/mongod -f /etc/mongodb/mongodb.conf --shutdown
PrivateTmp=true
[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable mongodb.service
systemctl restart mongodb.service
```



2、es

```shell
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt install apt-transport-https -y
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update && sudo apt-get install elasticsearch=7.10.2

sudo tee -a /etc/elasticsearch/elasticsearch.yml > /dev/null <<EOT
cluster.name: graylog
action.auto_create_index: true
EOT

sudo systemctl daemon-reload
sudo systemctl enable elasticsearch.service
sudo systemctl restart elasticsearch.service
sudo systemctl --type=service --state=active | grep elasticsearch

root@lsy:~# curl 127.0.0.1:9200
{
  "name" : "lsy",
  "cluster_name" : "graylog",
  "cluster_uuid" : "K2v34z7kQ6OckwgSrZ02kw",
  "version" : {
    "number" : "7.17.13",
    "build_flavor" : "default",
    "build_type" : "deb",
    "build_hash" : "2b211dbb8bfdecaf7f5b44d356bdfe54b1050c13",
    "build_date" : "2023-08-31T17:33:19.958690787Z",
    "build_snapshot" : false,
    "lucene_version" : "8.11.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}


```

3、 graylog

```shell
wget https://packages.graylog2.org/repo/packages/graylog-5.1-repository_latest.deb
sudo dpkg -i graylog-5.1-repository_latest.deb
sudo apt-get update 
apt install graylog-server  -y
sudo systemctl enable graylog-server.service
sudo systemctl restart graylog-server.service
sudo systemctl status graylog-server.service
# 修改配置文件
vim /etc/graylog/server/server.conf
# 生成 password_secret
apt install -y pwgen && pwgen -N 1 -s 96

# 生成root_password_sha2
echo -n "Enter Password: " && head -1 </dev/stdin | tr -d '\n' | sha256sum | cut -d" " -f1
# 监听IP地址
http_bind_address = 0.0.0.0:9000
#设置时区
root_timezone = Asia/Shanghai
# 查看配置文件
root@lsy:~# grep -Ev '^$|^#' /etc/graylog/server/server.conf 
is_leader = true
node_id_file = /etc/graylog/server/node-id
password_secret = Ls67Myc4zkAvsv88EfgeP8m6mSSuScuG0GtdIBRbltlgwpmY9CpdruAQzoUwCRu4a4ZmipgsFHEBgUTfjCBBd9rTZyBODrcY
root_password_sha2 = a1159e9df3670d549d04524532629f5477ceb7deec9b45e47e8c009506ecb2c8
root_timezone = Asia/Shanghai
bin_dir = /usr/share/graylog-server/bin
data_dir = /var/lib/graylog-server
plugin_dir = /usr/share/graylog-server/plugin
http_bind_address = 0.0.0.0:9000
stream_aware_field_types=false
allow_leading_wildcard_searches = false
allow_highlighting = false
output_batch_size = 500
output_flush_interval = 1
output_fault_count_threshold = 5
output_fault_penalty_seconds = 30
processbuffer_processors = 5
outputbuffer_processors = 3
processor_wait_strategy = blocking
ring_size = 65536
inputbuffer_ring_size = 65536
inputbuffer_processors = 2
inputbuffer_wait_strategy = blocking
message_journal_enabled = true
message_journal_dir = /var/lib/graylog-server/journal
lb_recognition_period_seconds = 3
mongodb_uri = mongodb://localhost/graylog
mongodb_max_connections = 1000
```

登录web账户名：admin  密码：生成root_password_sha2时输入的密码



收集/var/log/syslog

```shell
# 添加配置
cat /etc/rsyslog.conf
*.*@@10.0.0.10:1514;RSYSLOG_SyslogProtocol23Format
systemctl restart  rsyslog.service
systemctl status  rsyslog.service
# web页面部署TCP syslog
注意port选择要和 /etc/rsyslog.conf中保持一致并且不可以小于1024

```

![image-20230919133700608](assets/image-20230919133700608.png)

收集/var/log/journal

```shell
# 下载journalbeat
sudo apt-get update && sudo apt-get install journalbeat
vim /etc/journalbeat/journalbeat.yml
output.logstash:
  # The Logstash hosts
  hosts: ["localhost:5044"]
  
systemctl enable journalbeat
systemctl start journalbeat
systemctl status journalbeat
# web页面部署Beats
port保持默认的5044 是要和上面journalbeat.yml文件中保持一致的
```

收集文本日志

```shell
apt install -y filebeat
# 修改配置文件
vim /etc/filebeat/filebeat.yml
filebeat.inputs:
- type: log
  paths:
    - /var/log/nginx/*.log
#全部文件内容
grep -Ev '^$|#' /etc/filebeat/filebeat.yml
filebeat.inputs:
- type: log
  paths:
    - /var/log/nginx/*.log
filebeat.config.modules:
  path: ${path.config}/modules.d/*.yml
  reload.enabled: false
setup.template.settings:
  index.number_of_shards: 1
setup.kibana:
output.logstash:
  hosts: ["localhost:5044"]
processors:
  - add_host_metadata:
      when.not.contains.tags: forwarded
  - add_cloud_metadata: ~
  - add_docker_metadata: ~
  - add_kubernetes_metadata: ~
  
sudo systemctl enable filebeat
sudo systemctl restart filebeat
sudo systemctl status filebeat
# web页面部署Beats
port保持默认的5044 是要和上面journalbeat.yml文件中保持一致的

```

![image-20230919142349678](assets/image-20230919142349678.png)

程序引入框架直接写入

python [灰色伐木 ·皮皮 (pypi.org)](https://pypi.org/project/graylogging/)

Java https://zhuanlan.zhihu.com/p/25937785



面板展示测试数据生成

```shell
apt install apache2-utils nginx -y 
ab -n 50000 -c 200 http://127.0.0.1/
```

面板展示需求

messages per source 面板保留：每个数据源占总体数据的百分比

message over time 面板保留：随着时间变化数据收集的曲线图

selected sources：每个数据源具体的数据条数

所有数据源中http200占比

仅在master数据源中http200占比

面板元素

```shell
# group by：分组依据
direction: 图标形式是横向还是纵向
	row 横向
	column 纵向
fields: 分组依据关键字 比如timestamp 按照时间来绘画横向图
Interval： 间隔时间
# metrics 度量值
function: 计算度量值的函数
	average 平均值
	cardinality 基数
	count 计数
	latest value 最新值
	maximum 最大限度
	minimum 最小限度
	percentile 百分比
	standard deviation 标准差
	sum 总合
	sum of squares 平方和
	variance 方差
	
field: 度量关键词
name: 横坐标释意

# sort 排序

# visualization 可视化

```



![image-20230920155421479](assets/image-20230920155421479.png)







WARN [LocalKafkaJournal] Journal utilization (100.0%) has gone over 95%.

```shell
```



Journal utilization is too high and may go over the limit soon. Please verify that your Elasticsearch cluster is healthy and fast enough. You may also want to review your Graylog journal settings and set a higher limit. (Node: *2611349a-e6ab-4f2e-ae40-b212bcf88eea*)

```shell
```



NotFoundError: Failed to execute 'removeChild' on 'Node': The node to be removed is not a child of this node.

```shell
```



修改es的内存限制

```shell
vim /etc/elasticsearch/jvm.options
```

更改graylog的时区

[Graylog 更改显示的时区（Display timezone）_CV大使的博客-CSDN博客](https://blog.csdn.net/timczm/article/details/132550755#:~:text=Graylog 更改显示的时区（Display timezone） 1 单击页面右上角的 user%2Fprofile 图标 2,Zone ”（时区）下拉菜单选择适当的时区 5 单击 “ Update Settings ”)
