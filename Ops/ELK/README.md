CPU是arm架构安装ELK

参考文档： [安装 Kibana |木花指南 [8.10\] |弹性的 (elastic.co)](https://www.elastic.co/guide/en/kibana/current/install.html)

作用：

- elasticsearch：
- kibana：
- logstash：
- filebeat：

1、基础环境设置

```shell
# 安装常用依赖包
apt install -y net-tools vim  git  make zlib zlib-devel gcc-c++ libtool openssl openssl-devel pcre pcre-devel gcc glibc cmake screen lrzsz tree dos2unix lsof tcpdump ntp setuptool psmisc openssl openssl-devel bind-utils traceroute epel-release bash-completion bash-completion-extras
# 关闭swap
swapoff -a
sed -ri 's/.*swap.*/#&/' /etc/fstab
free -h
# 关闭selinux
setenforce 0
sed -ri 's/SELINUX=enforcing/SELINUX=disabled/'  /etc/selinux/config
# 设置时区
timedatectl set-timezone Asia/Shanghai
# 时间同步
/usr/sbin/ntpdate ntp1.aliyun.com
crontab -l > crontab_conf ; echo "*/5 * * * * /usr/sbin/ntpdate ntp1.aliyun.com >/dev/null 2>&1" >> crontab_conf && crontab crontab_conf && rm -f crontab_conf
# 关闭 firewalld
systemctl stop firewalld
systemctl disable firewalld
```



2、设置源

```shell

```

3、修改配置文件



4、启动服务

```shell
systemctl start elasticsearch kibana logstash filebeat
systemctl status elasticsearch kibana logstash filebeat
systemctl enable elasticsearch kibana logstash filebeat





```



# 基础环境设置

修改网卡IP地址

```shell
vim /etc/netplan/00-installer-config.yaml
network:
  ethernets:
    ens160:
      addresses: [10.0.0.10/24]
      dhcp4: no
      optional: true
      gateway4: 10.0.0.2
      nameservers:
          addresses: [223.5.5.5,223.6.6.6]
  version: 2
# 配置生效
netplan apply

```

安装ELK

```shell
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elastic-keyring.gpg
sudo apt-get install apt-transport-https
echo "deb [signed-by=/usr/share/keyrings/elastic-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-8.x.list

apt-get update && sudo apt-get install  -y elasticsearch kibana logstash filebeat openjdk-8-*

systemctl start elasticsearch kibana logstash filebeat
systemctl status elasticsearch kibana logstash filebeat
systemctl enable elasticsearch kibana logstash filebeat
```

安装docker

```shell
apt install -y docker.io
```

安装nginx

```shell
apt install -y nginx
```

prometheus

```shell
apt install -y prometheus
systemctl status prometheus
systemctl status prometheus-node-exporter
```

grafana

```shell
sudo apt-get install -y adduser libfontconfig1 musl
wget https://dl.grafana.com/enterprise/release/grafana-enterprise_10.1.1_arm64.deb
sudo dpkg -i grafana-enterprise_10.1.1_arm64.deb
systemctl start grafana-server
systemctl status grafana-server

```

consul

```shell
 wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
 echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
 sudo apt update && sudo apt install consul -y
 systemctl status consul.service
```



mysql

```shell
apt install -y mysql-client mysql-server
```

夜莺

```shell
# install mysql
apt -y install mariadb*
systemctl enable mariadb
systemctl restart mariadb
systemctl status mariadb

# install redis
apt install -y redis
systemctl enable redis
systemctl restart redis
systemctl status redis

# 配置mysql密码
mysql -uroot
grant all privileges on *.* to 'root'@'%' identified by '1234' with grant option;
UPDATE mysql.user SET authentication_string=PASSWORD('1234') WHERE User='root';
FLUSH PRIVILEGES;

# install n9e
mkdir n9e && cd n9e
wget https://download.flashcat.cloud/n9e-v6.0.2-linux-arm64.tar.gz
tar xvf n9e-v6.0.2-linux-arm64.tar.gz
mysql -uroot -p1234 < n9e.sql
nohup ./n9e &> n9e.log &

netstat  -ntpl|grep 17000
```

cat

```shell
apt install -y openjdk-8-* tomcat* 
systemctl restart tomcat
systemctl status tomcat
# install mysql
apt -y install mariadb*
systemctl enable mariadb
systemctl restart mariadb
systemctl status mariadb

# 配置mysql密码
mysql -uroot
grant all privileges on *.* to 'root'@'%' identified by '1234' with grant option;
UPDATE mysql.user SET authentication_string=PASSWORD('1234') WHERE User='root';
FLUSH PRIVILEGES;
wget https://ghproxy.com/https://github.com/dianping/cat/releases/download/3.1.0/cat-home.war
wget https://ghproxy.com/https://github.com/dianping/cat/archive/refs/tags/3.1.0.tar.gz
mv cat-home.war cat.war
mv cat.war /usr/local/tomcat/webapps/
tar xf 3.1.0.tar.gz
rm -rf 3.1.0.tar.gz

mysql -uroot -p123456
create database if not exists cat default charset utf8 collate utf8_general_ci;
use cat;
source /root/CatSoftware/cat-3.1.0/script/CatApplication.sql
# 创建cat目录以及配置文件
mkdir -p /data/appdatas/cat/
cat > /data/appdatas/cat/datasources.xml <<EOF
<?xml version="1.0" encoding="utf-8"?>
<data-sources>
    <data-source id="cat">
        <maximum-pool-size>3</maximum-pool-size>
        <connection-timeout>1s</connection-timeout>
        <idle-timeout>10m</idle-timeout>
        <statement-cache-size>1000</statement-cache-size>
        <properties>
            <driver>com.mysql.jdbc.Driver</driver>
            <url><![CDATA[jdbc:mysql://10.0.0.10:3306/cat]]></url>  <!-- 请替换为真实数据库URL及Port  -->
            <user>root</user>  <!-- 请替换为真实数据库用户名  -->
            <password>123456</password>  <!-- 请替换为真实数据库密码  -->
            <connectionProperties><![CDATA[useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&socketTimeout=120000]]></connectionProperties>
        </properties>
    </data-source>
</data-sources>
EOF
# 创建client.xml
cat > /data/appdatas/cat/client.xml <<EOF
  <?xml version="1.0" encoding="utf-8"?>
  <config mode="client">
      <servers>
          <server ip="10.0.0.10" port="2280" http-port="8080"/>
          <server ip="10.0.0.11" port="2280" http-port="8080"/>
          <server ip="10.0.0.12" port="2280" http-port="8080"/>
      </servers>
  </config>
EOF

chmod -R 777 /data/

# 启动& 访问
/usr/local/tomcat/bin/startup.sh
http://ip:8080/cat/s/config?op=routerConfigUpdate
```

Gitlab

```shell
sudo apt update
sudo apt install ca-certificates curl openssh-server postfix
cd /tmp
curl -LO https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh
sudo bash /tmp/script.deb.sh
sudo apt install gitlab-ce -y
sudo ufw status
sudo ufw allow http
sudo ufw allow https
sudo ufw allow OpenSSH
sudo ufw status
# 执行下面的命令比较漫长 但全程不需要输入什么
sudo gitlab-ctl reconfigure
gitlab-ctl restart

systemctl status gitlab-runsvdir.service
systemctl enable gitlab-runsvdir.service
```

mongo

```shell
wget https://fastdl.mongodb.org/linux/mongodb-linux-aarch64-ubuntu2004-6.0.2.tgz 
tar -zxvf mongodb-linux-aarch64-ubuntu2004-6.0.2.tgz
mv mongodb-linux-aarch64-ubuntu2004-6.0.2 /usr/local/mongodb

mkdir /etc/mongodb/
cd /etc/mongodb/
mkdir data data/db data/log
chmod 666 data/db data/log/
cat >>/etc/profile <<'EOF'
# mongodb
export MONGODB_HOME=/usr/local/mongodb
export PATH=$PATH:$MONGODB_HOME/bin
EOF
source /etc/profile 

cat >/etc/mongodb/mongodb.conf <<-EOF
# 数据库数据存放目录
dbpath=/etc/mongodb/data/db
# 日志文件存放目录
logpath=/etc/mongodb/data/log/mongodb.log
# 日志追加方式
logappend=true
# 端口
port=27017
# 是否认证
#auth=true
# 以守护进程方式在后台运行
fork=true
# 远程连接要指定ip，否则无法连接；0.0.0.0代表不限制ip访问
bind_ip=0.0.0.0
EOF


mongod -f /etc/mongodb/mongodb.conf
netstat  -ntpl
ps aux | grep mongo
mongod -f /etc/mongodb/mongodb.conf  --shutdown

cat >/lib/systemd/system/mongodb.service<<'EOF'
[Unit]
Description=mongodb
After=network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
ExecStart=/usr/local/mongodb/bin/mongod -f /etc/mongodb/mongodb.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/usr/local/mongodb/bin/mongod -f /etc/mongodb/mongodb.conf --shutdown
PrivateTmp=true
[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl restart mongodb.service
```

jenkins

```shell
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | sudo tee \
    /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
    https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install fontconfig openjdk-11-jre -y 
sudo apt-get install jenkins -y
systemctl status jenkins

cat /var/lib/jenkins/secrets/initialAdminPassword
```

Zabbix:[下载Zabbix 6.0 LTS for Ubuntu (arm64) 20.04 (Focal), MySQL, Apache](https://www.zabbix.com/cn/download?zabbix=6.0&os_distribution=ubuntu_arm64&os_version=20.04&components=server_frontend_agent&db=mysql&ws=apache)

es

```shell
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update && sudo apt-get install elasticsearch



```

























