#!/bin/bash
HARBOR_ADDRESS='10.0.0.50'
HARBOR_PROJECT='test'
HARBOR_PWD='Harbor12345'
HARBOR_USER='admin'
# 登录harbor
echo "$HARBOR_PWD"|docker login $HARBOR_ADDRESS --username $HARBOR_USER --password-stdin

#逻辑
for image in $(docker images|grep -Ev 'REPOSITORY'|awk '{print $1}')
do
  # 过滤已经打标签的image
  if [[ $image =~ $HARBOR_ADDRESS ]]
  then
      continue
  else
      echo docker tag  $(docker images|grep "^$image"|grep -Ev "$HARBOR_ADDRESS/$HARBOR_PROJECT" |awk '{print $3}')  $HARBOR_ADDRESS/$HARBOR_PROJECT/$image:$(docker images|grep "^$image"|grep -Ev "$HARBOR_ADDRESS/$HARBOR_PROJECT"|awk '{print $2}')
      docker tag  $(docker images|grep "^$image"|grep -Ev "$HARBOR_ADDRESS/$HARBOR_PROJECT" |awk '{print $3}')  $HARBOR_ADDRESS/$HARBOR_PROJECT/$image:$(docker images|grep "^$image"|grep -Ev "$HARBOR_ADDRESS/$HARBOR_PROJECT"|awk '{print $2}')
      docker push $HARBOR_ADDRESS/$HARBOR_PROJECT/$image:$(docker images|grep "$image"|grep "$HARBOR_ADDRESS/$HARBOR_PROJECT"|awk '{print $2}')
  fi
done
