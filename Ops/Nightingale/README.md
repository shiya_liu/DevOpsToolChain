夜莺文档：

[文档 - 《夜莺（Nightingale）v2.x 使用手册》 - 书栈网 · BookStack](https://www.bookstack.cn/read/Nightingale/f994eb267b63d86b.md)

[夜莺简介 - 《夜莺（Nightingale）v5.6 使用手册》 - 书栈网 · BookStack](https://www.bookstack.cn/read/n9e-5.6-zh/_index.md)

官网：[Introduction - 夜莺云原生监控 (n9e.github.io)](https://n9e.github.io/docs/prologue/introduction/)

GitHub：[ccfos/nightingale: An enterprise-level cloud-native observability solution, which can be used as drop-in replacement of Prometheus for alerting and Grafana for visualization. (github.com)](https://github.com/ccfos/nightingale)



夜莺的优点：

1. 整合了zabbix的自动愈合脚本
2. prometheus的业务监控以及系统监控层面
3. ELK的日志监控系统
4. 可以使用统一的数据采集Telegraf，降低export的维护成本



安装

```shell
# install mysql
apt -y install mariadb*
systemctl enable mariadb
systemctl restart mariadb


# install redis
apt install -y redis
systemctl enable redis
systemctl restart redis

# 配置mysql密码
mysql -uroot
UPDATE mysql.user SET authentication_string=PASSWORD('1234') WHERE User='root';
FLUSH PRIVILEGES;

# install n9e
wget https://download.flashcat.cloud/n9e-v6.0.2-linux-arm64.tar.gz
tar xf n9e-v6.0.2-linux-arm64.tar.gz
nohup ./n9e &> n9e.log &

netstat  -ntpl|grep 17000
```

访问： http://ip:17000

账户密码： root/root.2020

![](./assets/image-20230818154543342.png)



安装prometheus

```shell
# install prometheus
# 获取二进制包
wget https://mirrors.tuna.tsinghua.edu.cn/github-release/prometheus/prometheus/LatestRelease/prometheus-2.37.9.linux-arm64.tar.gz 
tar xf  prometheus-2.37.9.linux-arm64.tar.gz 
# 配置成systemd管理
cat > /etc/systemd/system/prometheus.service << EOF
[Unit]
Description=Prometheus
Documentation=https://prometheus.io/
After=network.target
[Service]
ExecStart=/root/prometheus-2.37.9.linux-arm64/prometheus --config.file=/root/prometheus-2.37.9.linux-arm64/prometheus.yml --storage.tsdb.path=/root/prometheus-2.37.9.linux-arm64/data --web.listen-address=0.0.0.0:9090
WorkingDirectory=/root/prometheus-2.37.9.linux-arm64/
Restart=on-failure
[Install]
WantedBy=multi-user.target
EOF

# 启动alertmanager
systemctl daemon-reload
systemctl start prometheus
systemctl enable prometheus
systemctl restart prometheus
systemctl status prometheus

# install node_export
apt install -y prometheus
```

配置监控大盘

是否提供了图表数据模板？

































