学习文档
https://skywalking.apache.org/zh/2020-04-19-skywalking-quick-start/
官方文档：https://skywalking.apache.org/docs/main/next/en/setup/backend/backend-docker/



安装java环境

```shell
cd sky
tar xf jdk-17_linux-x64_bin.tar.gz
rm -rf jdk-17_linux-x64_bin.tar.gz
mv jdk-17.0.8 /usr/local/
echo "JAVA_HOME=/usr/local/jdk-17.0.8">> /etc/profile
echo 'PATH=$JAVA_HOME/bin:$PATH'>> /etc/profile
echo 'CLASSPATH=$JAVA_HOME/jre/lib/ext:$JAVA_HOME/lib/tools.jar'>> /etc/profile
echo "export PATH JAVA_HOME CLASSPATH">> /etc/profile
source /etc/profile
java -version
```





部署es

```shell
#cd /usr/local/
# wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-8.9.0-linux-x86_64.tar.gz
mv sky/elasticsearch-8.9.0-linux-x86_64.tar.gz /usr/local/
cd /usr/local/
tar -zxvf elasticsearch-8.9.0-linux-x86_64.tar.gz
rm -rf elasticsearch-8.9.0-linux-x86_64.tar.gz
# 创建es用户
useradd elsearch  
chown elsearch:elsearch -R /usr/local/elasticsearch-8.9.0/

# 配置/etc/profile
echo 'ES_HOME=/usr/local/elasticsearch-8.9.0'>> /etc/profile
echo 'PATH=$ES_HOME/bin:$PATH'>> /etc/profile
source /etc/profile


# 配置systemd管理
cat > /usr/lib/systemd/system/elasticsearch.service<< EOF
[Unit]
Description=elasticsearch
After=network.target
[Service]
Type=simple
User=elsearch
Group=elsearch
LimitNOFILE=100000
LimitNPROC=100000
Restart=no
ExecStart=/usr/local/elasticsearch-8.9.0/bin/elasticsearch
PrivateTmp=true
[Install]
WantedBy=multi-user.target
EOF

chmod +x /usr/lib/systemd/system/elasticsearch.service
systemctl start elasticsearch.service
systemctl status elasticsearch.service
netstat -ntpl


# 修改配置文件
vim /usr/local/elasticsearch-8.9.0/config/elasticsearch.yml
xpack.security.enabled: false
xpack.security.http.ssl:
  enabled: false
  keystore.path: certs/http.p12

# 重新启动
systemctl restart elasticsearch.service

```

访问：http://ip:9200/

![image-20230815092639132](README.assets/image-20230815092639132.png)



部署skywalking

```shell
# cd /usr/local/
# wget https://www.apache.org/dyn/closer.cgi/skywalking/9.5.0/apache-skywalking-apm-9.5.0.tar.gz
mv sky/apache-skywalking-apm-9.5.0.tar.gz /usr/local/
cd /usr/local/
tar -zxvf apache-skywalking-apm-9.5.0.tar.gz
rm -rf apache-skywalking-apm-9.5.0.tar.gz
cd apache-skywalking-apm-bin
vim config/application.yml

# 启动
/usr/local/apache-skywalking-apm-bin/bin/oapService.sh
/usr/local/apache-skywalking-apm-bin/bin/webappService.sh

```

访问:http://ip:8080/

![image-20230815103254276](README.assets/image-20230815103254276.png)



