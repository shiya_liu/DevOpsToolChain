# 进阶使用



## 自动化监控

自动监控的目的：面多大量机器添加监控时，如果人工手动去添加是十分浪费精力的事情，最好新加入的大量主机能够自动添加到zabbix监控中

实现方式：

1. 自动发现
2. 自动注册



自动发现：zabbix-server扫描某个指定网段，通过动作添加监控（链接什么模板）；添加主机

自动注册：zabbix-agent自动注册到zabbix-server；添加主机

低级自动发现：自动添加监控项、触发器、图形等zabbix资源的规则；添加具体的主机监控资源；

应用场景：每台机器的配置都不一样，比如有的一块网卡 有的多块网卡，网卡名称并不统一，此时如果监控这些指标时，需要创建好多套监控模板才行。

### 自动发现

1、新增加的机器正常安装zabbix-agent

```shell
yum install ca-certificates -y
update-ca-trust extract
rpm -ivh https://mirrors.tuna.tsinghua.edu.cn/zabbix/zabbix/4.0/rhel/7/x86_64/zabbix-agent-4.0.1-1.el7.x86_64.rpm
vim /etc/zabbix/zabbix_agentd.conf 
Server=10.0.0.15
systemctl enable zabbix-agent.service 
systemctl start zabbix-agent.service



mac地址：00:0C:29:4C:E3:84

```

2、zabbix-server web页面设置

需要设置：

1. 配置自动发现扫描的网段
2. 配置动作：添加指定网段的主机以及链接模板



配置扫描网段

![image-20230510132652452](assets/image-20230510132652452.png)

![image-20230510132711677](assets/image-20230510132711677.png)

配置动作

![image-20230510133237342](assets/image-20230510133237342.png)

![image-20230510133412058](assets/image-20230510133412058.png)

![image-20230510133428643](assets/image-20230510133428643.png)

效果

![image-20230510134323459](assets/image-20230510134323459.png)



### 自动注册

自动注册的目的与自动发现一致：都是自动将大量主机加入到zabbix监控中，区别在于：自动注册是agent端自动向zabbix-server端发送请求，不需要zabbix-server主动向网段发起添加请求，减轻zabbix-server压力，提高效率

需要设置：

1. 设置自动注册规则
2. 配置zabbix-agent客户端



![image-20230510135202635](assets/image-20230510135202635.png)

设置主机源数据

![image-20230510135326179](assets/image-20230510135326179.png)



配置zabbix-agent客户端

```shell
vim /etc/zabbix/zabbix_agentd.conf
Server=10.0.0.15
ServerActive=10.0.0.15
Hostname=10.0.0.16
HostMetadata=10.0.0.16
systemctl restart zabbix-agent.service
```

效果

![image-20230510140049242](assets/image-20230510140049242.png)



### 低级自动发现

自动发现的目的是：自动添加主机，然后链接现有的模板；而低级自动发现的作用是：根据不同配置的监控项原型自动添加监控项、触发器、图形等

低级自动发现的应用场景：适用于有规律又有差异的监控项

步骤：

1. 创建低级自动发现脚本（输出json格式）
2. zabbix-agent端配置创建发现规则（和自定义监控项操作一样，1&2步骤相当于创建了自定义发现规则）
3. zabbix-agent端配置自定义监控项原型（创建了参数化监控项，可以利用第1步脚本中设置的变量$DISKNAME作为参数进行设置监控项原型）
4. web页面中创建发现规则



1、创建低级自动发现脚本（要求：输出json格式）

```shell
cat /tmp/disk.sh
#!/bin/bash
port=($(iostat | awk 'NR>6{print $1}'|grep -v '^$'))
printf '{\n'
printf '\t"data":[\n'
for key in ${!port[@]}
    do
        if [[ "${#port[@]}" -gt 1 && "${key}" -ne "$((${#port[@]}-1))" ]];then
            printf '\t {\n'
            printf "\t\t\t\"{#DISKNAME}\":\"${port[${key}]}\"},\n"
        else [[ "${key}" -eq "((${#port[@]}-1))" ]]
            printf '\t {\n'
            printf "\t\t\t\" {#DISKNAME}\":\"${port[${key}]}\"}\n"
        fi
    done
            printf '\t ]\n'
            printf '}\n'
chmod +x /tmp/disk.sh
chown zabbix:zabbix /tmp/disk.sh

```



2、zabbix-agent端配置创建发现规则 (磁盘)

```shell
cat /etc/zabbix/zabbix_agentd.d/disk.conf
UserParameter=disk,/bin/bash /tmp/disk.sh


# zabbix-server测试取值
zabbix_get  -s 10.0.0.16 -k disk
{
        "data":[
         {
                        "{#DISKNAME}":"sdb"},
         {
                        "{#DISKNAME}":"sda"},
         {
                        "{#DISKNAME}":"scd0"},
         {
                        "{#DISKNAME}":"dm-0"},
         {
                        "{#DISKNAME}":"dm-1"},
         {
                        " {#DISKNAME}":"dm-2"}
         ]
}

```

3、zabbix-agent端配置自定义监控项

```shell
cat /etc/zabbix/zabbix_agentd.d/disk.conf
# 定义低级自动发现规则
UserParameter=disk,/bin/bash /tmp/disk.sh
# 定义监控项原型
UserParameter=tps[*],iostat |grep $1 |awk '{print $$2}'

systemctl restart zabbix-agent

# 测试取值
zabbix_get  -s 10.0.0.16 -k tps[sda]
0.12

```



4、web页面中创建发现规则

![image-20230511230450307](assets/image-20230511230450307.png)

![image-20230511230731784](assets/image-20230511230731784.png)

![image-20230511230823022](assets/image-20230511230823022.png)

4、web页面添加监控项原型

![image-20230511230905797](assets/image-20230511230905797.png)

![image-20230511233355652](assets/image-20230511233355652.png)

查看最新数据

![image-20230511233846385](assets/image-20230511233846385.png)















问题：创建监控项原型后 发现在最新数据中并没有看到监控原型创建的监控项














## 升级





## 分布式监控





## 高可用

规划：

10.0.0.15、10.0.0.16作为zabbix-server

这两台机器中分别安装zabbix-server nginx keepalived mysql



1、部署zabbix-server



























## zabbix-api

