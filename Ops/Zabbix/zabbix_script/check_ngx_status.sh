#!/bin/bash
status_code=$(ps -ef |grep nginx | wc -l)
if [ $status_code -lt 2 ]
then
  # 当nginx停止时echo 0
  echo 0
else
  # 当nginx启动时echo 1
  echo 1
fi
