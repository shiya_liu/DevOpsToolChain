#!/bin/bash
port=($(iostat | awk 'NR>6{print $1}'|grep -v '^$'))
printf '{\n'
printf '\t"data":[\n'
for key in ${!port[@]}
    do
        if [[ "${#port[@]}" -gt 1 && "${key}" -ne "$((${#port[@]}-1))" ]];then
            printf '\t {\n'
            printf "\t\t\t\"{#DISKNAME}\":\"${port[${key}]}\"},\n"
        else [[ "${key}" -eq "((${#port[@]}-1))" ]]
            printf '\t {\n'
            printf "\t\t\t\" {#DISKNAME}\":\"${port[${key}]}\"}\n"
        fi
    done
            printf '\t ]\n'
            printf '}\n'