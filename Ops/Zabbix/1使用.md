# 使用

## 安装zabbix-server

```shell
# 配置zabbix的yum源
yum install ca-certificates -y
update-ca-trust extract
rpm -ivh https://mirrors.tuna.tsinghua.edu.cn/zabbix/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-1.el7.noarch.rpm
# 安装zabbix服务端和zabbix-web前端
yum install -y zabbix-server-mysql zabbix-get zabbix-web zabbix-web-mysql zabbix-agent zabbix-sender
# 安装mariadb
yum install mariadb-server -y 
systemctl start mariadb 
systemctl enable mariadb 
# 初始化数据库设置：先回车 再n 然后一路y
mysql_secure_installation 
mysql
# 创建zabbix库以及授权zabbix用户
create database zabbix character set utf8 collate utf8_bin; 
grant all privileges on zabbix.* to zabbix@localhost identified by '123456';
# 导入zabbix表结构和初始数据
zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -uzabbix -p123456 zabbix
# 检查zabbix库是否导入成功
mysql -uroot  zabbix -e 'use zabbix; show tables'
# 配置启动zabbix-server
vi /etc/zabbix/zabbix_server.conf 
DBHost=localhost 
DBName=zabbix 
DBUser=zabbix 
DBPassword=123456

systemctl start zabbix-server 
systemctl enable zabbix-server
# 检查端口
netstat -lntup|grep 10051
# 修改Zabbix前端的PHP配置,并启动httpd
vi /etc/httpd/conf.d/zabbix.conf 
php_value date.timezone Asia/Shanghai

systemctl start httpd 
systemctl enable httpd
# 前端zabbix-web的安装
浏览器：http://${ip}/zabbix
# 登录的账号密码
Admin zabbix
```



![image-20230506145546223](assets/image-20230506145546223.png)

![image-20230506145635687](assets/image-20230506145635687.png)

此处告警是因为zabbix-server安装的机器没有安装agent，无法获取到自身数据



## 安装zabbix-agent

```shell
rpm -ivh https://mirrors.tuna.tsinghua.edu.cn/zabbix/zabbix/4.0/rhel/7/x86_64/zabbix-agent-4.0.1-1.el7.x86_64.rpm
vim /etc/zabbix/zabbix_agentd.conf 
Server=10.0.0.15
systemctl enable zabbix-agent.service 
systemctl start zabbix-agent.service

```



## 配置文件

**zabbix_server.conf**

```ini
# This is a configuration file for Zabbix server daemon
# To get more information about Zabbix, visit http://www.zabbix.com

############ 常规参数 #################

# 监听端口，默认10051，可以设置1024-32767
# ListenPort=10051

# 传出连接的源IP地址。
# SourceIP=

# 日志类型：system、file、console
# LogType=file

# 日志文件 如果LogType设置为file，则为yes，否则为no
LogFile=/var/log/zabbix/zabbix_server.log

# 日志文件最大大小（MB），范围可以指定0-1024，默认为1MB
LogFileSize=0

# 调试级别：0（关于Zabbix进程启动和停止的基本信息）；1（关键信息）；2（错误信息）；3（警告）；4（产生大量信息）；5（产生更多信息）
# DebugLevel=3

# pid文件路径
PidFile=/var/run/zabbix/zabbix_server.pid

# IPC套接字
SocketDir=/var/run/zabbix

# 数据库主机名
DBHost=localhost

# 数据库名称
DBName=zabbix

# 用于IBM DB2和PostgreSQL的Schema名称
# DBSchema=

# 数据库用户
DBUser=zabbix

# 数据库密码
DBPassword=123456

# MySQL套接字的路径
# DBSocket=

# 不使用本地套接字时的数据库端口
# DBPort=

# 历史存储数据URL
# HistoryStorageURL=

# 要发送到历史记录存储器的值类型的逗号分隔列表。
# HistoryStorageTypes=uint,dbl,str,log,text

# 启用历史存储中历史值的预处理，以便根据日期将值存储在不同的索引中，0禁用，1启用
# HistoryStorageDateIndex=0

# 用于以换行符分隔的JSON格式实时导出事件、历史记录和趋势的目录
# ExportDir=

# 每个导出文件的最大大小（以字节为单位），仅在设置了ExportDir的情况下用于旋转
# ExportFileSize=1G

############ 高级参数 ################

# 轮询器的预分叉实例数，范围0-1000
# StartPollers=5

# IPMI轮询器的预分叉实例数，当至少启动一个IPMI轮询器时，IPMI管理器进程将自动启动，范围0-1000
# StartIPMIPollers=0

# 预处理工作者的预分叉实例数，当预处理器工作程序启动时，预处理管理器进程将自动启动
# StartPreprocessors=3

# 无法访问的主机（包括IPMI和Java）的轮询器的预分叉实例数，如果是常规、IPMI或Java轮询器，则必须至少为无法访问的主机运行一个轮询器，范围0-1000
# StartPollersUnreachable=1

# 陷阱的预分叉实例数，陷阱接受来自Zabbix发送方、活动代理和活动代理的传入连接，必须至少运行一个trapper进程才能显示服务器可用性和查看队列，范围0-1000
# StartTrappers=5

# ICMP Ping器的预分叉实例数，范围0-1000
# StartPingers=1

# 发现者的预分叉实例数，范围0-250
# StartDiscoverers=1

# HTTP轮询器的预分叉实例数，范围0-1000
# StartHTTPPollers=1

# 定时器的预分叉实例数，计时器处理维护周期，只有第一个定时器进程处理主机维护更新。共享问题抑制更新，在所有定时器之间，范围1-1000
# StartTimers=1

# 自动扶梯的预分叉实例数，范围0-100
# StartEscalators=1

# 启动警报器，警报程序的预分叉实例数，警报器发送由操作操作创建的通知，范围0-100
# StartAlerters=3

# Zabbix Java网关的IP地址，只有在启动Java轮询器时才需要
# JavaGateway=

# Zabbix Java网关监听的端口，范围：1024-32767
# JavaGatewayPort=10052

# Java轮询器的预分叉实例数，范围0-1000
# StartJavaPollers=0

# 预分叉的vmware收集器实例数，范围0-250
# StartVMwareCollectors=0

# Zabbix连接到VMware服务以获取新数据的频率，范围：10-86400
# VMwareFrequency=60

# Zabbix连接到VMware服务以获取性能数据的频率，范围10-86400
# VMwarePerfFrequency=60

# VMware缓存的大小，以字节为单位，用于存储VMware数据的共享内存大小，仅在启动VMware收集器时使用
# VMwareCacheSize=8M

# 指定vmware收集器等待vmware服务响应的秒数
# VMwareTimeout=10

# 用于将数据从SNMP陷阱守护程序传递到服务器的临时文件，必须与zabbi_trap_receiver.pl或SNMPTT配置文件中的相同
SNMPTrapperFile=/var/log/snmptrap/snmptrap.log

# 如果为1，则启动SNMP陷阱进程
# StartSNMPTrapper=0

# 陷阱程序应侦听的逗号分隔的IP地址列表，如果缺少此参数，Trapper将侦听所有网络接口
# ListenIP=0.0.0.0


#Zabbix执行内务管理程序的频率（以小时为单位）。内务部正在从数据库中删除过时的信息。为防止管家超负荷工作，不超过管家频率的4倍 对于每个项目，在一个内务管理周期内删除数小时的过时信息。为了降低服务器启动时的负载，内务管理在服务器启动后推迟30分钟。HousekeepingFrequency=0时，管家只能使用运行时控制选项执行。在这种情况下，在一个内务处理周期中删除的过时信息的周期是自上次内务管理周期以来的一段时间，但不少于4小时且不超过4天。
# HousekeepingFrequency=1

#“管家”表包含管家程序的“任务”，格式如下：[housekeeperid]、[tablename]、[field]、[value]。不超过“MaxHousekeeperDelete”行（对应于[tablename]、[field]、[value]）将在一个内务处理周期内每个任务删除。如果设置为0，则根本不使用限制。在这种情况下，你必须知道自己在做什么！
# MaxHousekeeperDelete=5000

# 配置缓存的大小，以字节为单位，，用于存储主机、项和触发器数据的共享内存大小
# CacheSize=8M

# Zabbix执行配置缓存更新的频率，以秒为单位
# CacheUpdateFrequency=60

# 数据库同步程序的预分叉实例数
# StartDBSyncers=4

# 历史缓存的大小，以字节为单位，用于存储历史数据的共享内存大小
# HistoryCacheSize=16M

# 历史索引缓存的大小，以字节为单位，用于索引历史缓存的共享内存大小
# HistoryIndexCacheSize=4M

# 趋势缓存的大小，以字节为单位，用于存储趋势数据的共享内存大小
# TrendCacheSize=4M

# 历史值缓存的大小，以字节为单位，用于缓存项目历史数据请求的共享内存大小，设置为0将禁用值缓存
# ValueCacheSize=8M

# 指定等待代理、SNMP设备或外部检查的时间（以秒为单位）
Timeout=4

# 指定捕捉器处理新数据可能花费的秒数，可指定范围0-300
# TrapperTimeout=300

#在几秒钟的不可达性之后，将主机视为不可用，可指定范围：1-3600
# UnreachablePeriod=45

# 主机在不可用期间检查可用性的频率，以秒为单位，范围：1-3600
# UnavailableDelay=60

# 在无法访问期间检查主机可用性的频率，以秒为单位，范围：1-3600
# UnreachableDelay=15

# 自定义警报脚本位置的完整路径
AlertScriptsPath=/usr/lib/zabbix/alertscripts

# 外部脚本位置的完整路径
ExternalScripts=/usr/lib/zabbix/externalscripts

# 请确保fping二进制文件具有根所有权并设置了SUID标志
# FpingLocation=/usr/sbin/fping

# 请确保fping6二进制文件具有根所有权并设置了SUID标志
# Fping6Location=/usr/sbin/fping6

# SSH检查和操作的公钥和私钥的位置
# SSHKeyLocation=

# 数据库查询在被记录之前可能需要多长时间，只有当DebugLevel设置为3、4或5时才有效，0-不记录慢速查询；范围：1-3600000
LogSlowQueries=3000

# 临时目录
# TmpDir=/tmp

# 被动代理的轮询器的预分叉实例数
# StartProxyPollers=1

# Zabbix服务器向Zabbix代理发送配置数据的频率（秒），此参数仅用于被动模式下的代理；范围： 1-3600*24*7
# ProxyConfigFrequency=3600

# Zabbix服务器从Zabbix代理服务器请求历史数据的频率（以秒为单位），范围：1-3600
# ProxyDataFrequency=1

#允许服务器以“root”身份运行。如果禁用并且服务器由“root”启动，则服务器；将尝试切换到用户配置选项指定的用户。如果在常规用户下启动，则无效。0-不允许，1-允许
# AllowRoot=0

# 删除系统上特定现有用户的权限，只有在以“root”身份运行且禁用AllowRoot时才有效
# User=zabbix

# 您可以将单个文件或所有文件包含在配置文件的一个目录中，安装Zabbix将在/usr/local/etc中创建include目录，除非在编译时进行了修改
# Include=/usr/local/etc/zabbix_server.general.conf
# Include=/usr/local/etc/zabbix_server.conf.d/
# Include=/usr/local/etc/zabbix_server.conf.d/*.conf

#SSL客户端证书的位置。此参数仅用于web监控。默认值取决于编译选项。
# SSLCertLocation=${datadir}/zabbix/ssl/certs

#SSL客户端证书的私钥位置。此参数仅用于web监控。默认值取决于编译选项
# SSLKeyLocation=${datadir}/zabbix/ssl/keys

#覆盖SSL服务器证书验证的证书颁发机构（CA）文件的位置。如果未设置，将使用系统范围的目录。此参数仅用于web监视和SMTP身份验证。
# SSLCALocation=

# 逗号分隔的IP地址列表，可选CIDR表示法，或外部Zabbix实例的DNS名称，只有来自此处列出的地址才会接受Stats请求。如果未设置此参数，则无统计请求将被接受
# StatsAllowedIP=

####### 加载模块 #######

# 服务器模块位置的完整路径
# LoadModulePath=${libdir}/modules

# 要在服务器启动时加载的模块。模块用于扩展服务器的功能。
# LoadModule=

####### TLS相关参数 #######

# 包含的顶级CA证书的文件的完整路径名
# TLSCAFile=

# 包含已吊销证书的文件的完整路径名。
# TLSCRLFile=

# 包含服务器证书或证书链的文件的完整路径名。
# TLSCertFile=

# 包含服务器私钥的文件的完整路径名
# TLSKeyFile=

####### 对于高级用户-TLS密码套件选择标准 #######

# TLS 1.3中OpenSSL 1.1.1或更新版本的密码字符串，覆盖基于证书的加密的默认密码套件选择标准
# TLSCipherCert13=

# 覆盖基于证书的加密的默认密码套件选择标准
# TLSCipherCert=

#TLS 1.3中OpenSSL 1.1.1或更新版本的密码字符串
# TLSCipherPSK13=

# 覆盖基于PSK的加密的默认密码套件选择标准
# TLSCipherPSK=

# TLS 1.3中OpenSSL 1.1.1或更新版本的密码字符串。
# TLSCipherAll13=

# 覆盖基于证书和PSK的加密的默认密码套件选择标准。
# TLSCipherAll=

```



**zabbix_agentd.conf**

```shell
# This is a configuration file for Zabbix agent daemon (Unix)
# To get more information about Zabbix, visit http://www.zabbix.com

############ 常规参数 #################

#PID文件的名称
PidFile=/var/run/zabbix/zabbix_agentd.pid

# 日志类型：system、file、console
# LogType=file

# LogType“file”参数的日志文件名
LogFile=/var/log/zabbix/zabbix_agentd.log

# 日志文件的最大大小（MB），0-禁用日志自动旋转；范围：0-1024
LogFileSize=0

# 调试级别：0（关于Zabbix进程启动和停止的基本信息）；1（关键信息）；2（错误信息）；3（警告）；4（产生大量信息）；5（产生更多信息）
# DebugLevel=3

# 传出连接的源IP地址
# SourceIP=

# 是否允许来自Zabbix服务器的远程命令；0-不允许；1-允许
# EnableRemoteCommands=0

# 启用将已执行的shell命令记录为警告，0-已禁用，1-已启用
# LogRemoteCommands=0

##### 被动检查相关

# zabbix-serverIP地址
Server=10.0.0.15

# 代理将在此端口上侦听来自服务器的连接。
# ListenPort=10050

# 代理应侦听的逗号分隔的IP地址列表
# ListenIP=0.0.0.0

# 处理被动检查的zabbix_agent的预分叉实例数
# StartAgents=3

##### 主动检查相关

# 用于活动检查的Zabbix服务器和Zabbix代理的逗号分隔IP:port（或DNS名称：port）对列表
ServerActive=127.0.0.1

# 唯一的、区分大小写的主机名，活动检查需要，并且必须与服务器上配置的主机名相匹配，如果未定义，则从HostnameItem获取值
Hostname=Zabbix server

# 用于生成主机名的项（如果未定义）。如果定义了主机名，则忽略
# HostnameItem=system.hostname

# 定义主机元数据的可选参数，主机元数据用于主机自动注册过程。如果该值超过255个字符的限制，则代理将发出错误并不会启动
# HostMetadata=

# 可选参数，用于定义用于获取主机元数据的项。主机元数据用于主机自动注册过程。在自动注册请求期间，如果指定项目返回的值超过了255个字符的限制。
# HostMetadataItem=

# 活动检查列表刷新的频率，以秒为单位。范围：60-3600
# RefreshActiveChecks=120

# 冲区中的数据保存时间不要超过N秒。范围：1-3600
# BufferSend=5

# 内存缓冲区中的最大值数。代理将发送，如果缓冲区已满，则将所有收集的数据发送到Zabbix Server或Proxy
# BufferSize=100


# 代理每秒将发送到Zabbix服务器的最大新行数，或代理处理“log”和“logrt”活动检查，提供的值将被参数“maxlines”覆盖在“log”或“logrt”项密钥中提供。
# MaxLinesPerSecond=20

############ 高级参数 #################
# 设置项关键字的别名。它可以用来用一个更小更简单的项密钥来代替长而复杂的项密钥。
#Alias=zabbix.userid:vfs.file.regexp[/etc/passwd,^zabbix:.:([0-9]+),,,,\1]

# 处理时间不超过Timeout秒
# Timeout=3

# 允许代理以“root”身份运行。如果禁用并且代理由“root”启动，则代理将尝试切换到用户配置选项指定的用户。如果在常规用户下启动，则无效。0-不允许；1-允许
# AllowRoot=0

# 删除系统上特定现有用户的权限。只有在以“root”身份运行且禁用AllowRoot时才有效。
# User=zabbix

# 您可以将单个文件或所有文件包含在配置文件的一个目录中。
Include=/etc/zabbix/zabbix_agentd.d/*.conf

####### 用户定义的监控参数 #######

# 允许将参数中的所有字符传递给用户定义的参数，不允许使用以下字符：\ ' " ` * ? [ ] { } ~ $ ! & ; ( ) < > | # @；此外，不允许使用换行符。0-不允许；1-允许
# UnsafeUserParameters=0

# 要监视的用户定义参数。可以有几个用户定义的参数。
# UserParameter=

####### 加载模块 #######

# 代理模块位置的完整路径
# LoadModulePath=${libdir}/modules

# 要在代理启动时加载的模块。模块用于扩展代理的功能
# LoadModule=

####### TLS相关参数 #######

#代理应如何连接到服务器或代理。用于主动检查。只能指定一个值：未加密-不加密连接；psk-使用TLS和预共享密钥进行连接；cert-使用TLS和证书进行连接
# TLSConnect=unencrypted

#要接受的传入连接。可以指定多个值，用逗号分隔：未加密-接受不加密的连接；psk-接受使用TLS和预共享密钥保护的连接；cert-接受使用TLS和证书保护的连接
# TLSAccept=unencrypted

# 包含的顶级CA证书的文件的完整路径名
# TLSCAFile=

# 包含已吊销证书的文件的完整路径名
# TLSCRLFile=

# 允许的服务器证书颁发者。
# TLSServerCertIssuer=

# 允许的服务器证书使用者
# TLSServerCertSubject=

# 包含代理证书或证书链的文件的完整路径名。
# TLSCertFile=

# 包含代理私钥的文件的完整路径名。
# TLSKeyFile=

# 用于标识预共享密钥的唯一、区分大小写的字符串。
# TLSPSKIdentity=

# 包含预共享密钥的文件的完整路径名。
# TLSPSKFile=

####### 对于高级用户-TLS密码套件选择标准 #######

# #TLS 1.3中OpenSSL 1.1.1或更新版本的密码字符串。覆盖基于证书的加密的默认密码套件选择标准。
# TLSCipherCert13=

# GnuTLS优先级字符串或OpenSSL（TLS 1.2）密码字符串。
# TLSCipherCert=

# TLS 1.3中OpenSSL 1.1.1或更新版本的密码字符串。
# TLSCipherPSK13=

# GnuTLS优先级字符串或OpenSSL（TLS 1.2）密码字符串。
# TLSCipherPSK=

# TLS 1.3中OpenSSL 1.1.1或更新版本的密码字符串。
# TLSCipherAll13=

# GnuTLS优先级字符串或OpenSSL（TLS 1.2）密码字符串。
# TLSCipherAll=
```



## 监控主机

```shell
配置--->主机--->填写主机名称--->填写群组--->填写agentIP地址--->选择模板--->点击添加--->zbx变绿
```



![image-20230506153400304](assets/image-20230506153400304.png)

![image-20230506153741942](assets/image-20230506153741942.png)

![image-20230506153846956](assets/image-20230506153846956.png)

检查是否添加成功

![image-20230506153939501](assets/image-20230506153939501.png)



## 自定义监控项

自定义监控项UserParameter有三种方式：zabbix_agentd.conf中添加UserParameter

1. 命令行方式取值
2. 脚本方式取值
3. 单独将脚本放到具体路径
4. 参数化取值



```shell
zabbix_agentd.conf中添加UserParameter web页面添加监控项,debug:在zabbix-server中zabbix_get  -s 10.0.0.16 -k nginx_status测试取值

方式一：直接命令行取值（获取磁盘tps为例）
客户端操作：
# 1、添加UserParameter，以值，命令行的方式（注意web界面上需要和值的名称保持一致，都叫tps）
yum install  -y  sysstat.x86_64
cat /etc/zabbix/zabbix_agentd.conf |grep tps
UserParameter=tps,iostat|awk '/^sda/{print $2}'
# 2、重启zabbix-agent
systemctl restart zabbix-agent
# 3、web页面上添加监控项
配置--->主机--->监控项--->创建监控项 

方式二：脚本方式取值（以判断nginx是否存活为主）
# 1、准备脚本安装ngixn
yum install -y nginx
cat check_ngx_status.sh
#!/bin/bash
status_code=$(ps -ef |grep nginx | wc -l)
if [ $status_code -lt 2 ]
then
  # 当nginx停止时echo 0
  echo 0
else
  # 当nginx启动时echo 1
  echo 1
fi

chmod +x check_ngx_status.sh
chown zabbix:zabbix  /tmp/check_ngx_status.sh
# 2、添加UserParameter
cat /etc/zabbix/zabbix_agentd.conf |grep nginx
UserParameter=nginx_status,/bin/bash /tmp/check_ngx_status.sh

systemctl restart zabbix-agent
# 3、web页面上添加监控项
配置--->主机--->监控项--->创建监控项 


方式三：脚本取值并放到/etc/zabbix/zabbix_agentd.d下
# 1、将自定义监控项命名成文件存放到/etc/zabbix/zabbix_agentd.d目录下，注意监控项名称和文件名保持一致：nginx_status
cat /etc/zabbix/zabbix_agentd.d/nginx_status.conf
UserParameter=nginx_status,/bin/bash /tmp/check_ngx_status.sh
# 2、编写脚本
cat /tmp/check_ngx_status.sh
#!/bin/bash
status_code=$(ps -ef |grep nginx | wc -l)
if [ $status_code -lt 2 ]
then
  # 当nginx停止时echo 0
  echo 0
else
  # 当nginx启动时echo 1
  echo 1
fi
systemctl restart zabbix-agent
# 3、web页面添加监控项
配置--->主机--->监控项--->创建监控项 

方式四：参数化取值
#1、参数化
cat tps.conf
UserParameter=tps[*],iostat|grep $1|awk '{print $$2}'
#2、测试取值
zabbix_get -s 10.0.0.16 -k tps[sda]
89.97
zabbix_get -s 10.0.0.16 -k tps[sdb]
3.46
#3、在页面上进行设置
配置--->主机--->监控项--->创建监控项，将监控项的名字修改：tps --->tps[sda]或tps[sdb]

```

**命令行方式磁盘tps**

![image-20230506160424033](assets/image-20230506160424033.png)

![image-20230506160446261](assets/image-20230506160446261.png)

![image-20230506161445632](assets/image-20230506161445632.png)

检查是否添加成功

![image-20230506161249645](assets/image-20230506161249645.png)

**脚本方式监控nginx**

![image-20230506173354768](assets/image-20230506173354768.png)

![image-20230506173821759](assets/image-20230506173821759.png)



**参数化方式监控tps**

![image-20230506202737213](assets/image-20230506202737213.png)

![image-20230506202712460](assets/image-20230506202712460.png)





## 自定义值映射

```shell
配置--->主机--->创建监控项--->展示值映射--->创建值映射 
```

![image-20230506172143284](assets/image-20230506172143284.png)

![image-20230506172159686](assets/image-20230506172159686.png)

![image-20230506172218496](assets/image-20230506172218496.png)

![image-20230506172304271](assets/image-20230506172304271.png)

**更简单的方式**

![image-20230506174400999](assets/image-20230506174400999.png)



## 自定义触发器

![image-20230506221214207](assets/image-20230506221214207.png)

![image-20230506221250583](assets/image-20230506221250583.png)





## 邮件告警

```shell
步骤：
    配置发件人
    配置收件人 
    启动发送邮件的动作
```

**配置发件人**

![image-20230506215512751](assets/image-20230506215512751.png)

**配置收件人** 

![image-20230506215739820](assets/image-20230506215739820.png)

**启动发送邮件的动作**

![image-20230506220116404](assets/image-20230506220116404.png)

![image-20230506221122500](assets/image-20230506221122500.png)







## 企业微信机器人告警

总体步骤：

在zabbix-server中创建告警脚本

1、在zabbix-server中创建告警脚本

```shell
vim  /usr/lib/zabbix/alertscripts/wxrobot.py
#!/usr/bin/python
#-*- coding: utf-8 -*-
import requests
import json
import sys
import os

headers = {'Content-Type': 'application/json;charset=utf-8'}
api_url = "换成你自己的机器人token"
def msg(text):
    json_text= {
     "msgtype": "text",
        "text": {
            "content": text
        },
    }
    print requests.post(api_url,json.dumps(json_text),headers=headers).content
if __name__ == '__main__':
    text = sys.argv[1]

chmod +x /usr/lib/zabbix/alertscriptswxrobot.py
chown zabbix:zabbix /usr/lib/zabbix/alertscriptswxrobot.py
```

2、安装python的requests包

```shell
curl -o get-pip.py https://bootstrap.pypa.io/pip/2.7/get-pip.py
python2 get-pip.py
pip install requests
[root@localhost externalscripts]# ./wxrobot.py  test
{"errcode":0,"errmsg":"ok"}
```

![image-20230506224630836](assets/image-20230506224630836.png)

3、新增告警媒介

![image-20230507103113394](assets/image-20230507103113394.png)

![image-20230507103209494](assets/image-20230507103209494.png)

4、创建动作

![image-20230507103319663](assets/image-20230507103319663.png)

![image-20230507103341981](assets/image-20230507103341981.png)

![image-20230507103408112](assets/image-20230507103408112.png)

![image-20230507103428883](assets/image-20230507103428883.png)

```shell
问题：{EVENT.NAME}
告警信息：{TRIGGER.NAME}
告警地址：{HOST.NAME}
监控项目：{ITEM.NAME}
监控取值：{EVENT.VALUE}
告警严重性：{EVENT.SEVERITY}
当前状态：{EVENT.STATUS}
告警时间：{EVENT.DATE} {EVENT.TIME}
事件ID：{EVENT.ID}
告警信息：{TRIGGER.NAME}
告警地址：{HOST.NAME}
监控项目：{ITEM.NAME}
监控取值：{EVENT.RECOVERY.VALUE}
告警严重性：{EVENT.SEVERITY}
当前状态：{EVENT.RECOVERY.STATUS}
告警时间：{EVENT.DATE} {EVENT.TIME}
恢复时间：{EVENT.RECOVERY.TIME}
持续时间：{EVENT.AGE}
事件ID：{EVENT.RECOVERY.ID}
```

![image-20230507103801927](assets/image-20230507103801927.png)

## 钉钉告警

1、创建群聊和钉钉机器人

![image-20230507105041864](assets/image-20230507105041864.png)

![image-20230507113038283](assets/image-20230507113038283.png)



注意：因为选择了自定义关键词，所以必须要在信息中带有该字段才可以发送成功

2、编写钉钉告警脚本

```shell
cat /usr/lib/zabbix/alertscripts/dingding.py
# !/usr/bin/python
# -*- coding: utf-8 -*-
# zabbix钉钉报警

import requests, json, sys, os, datetime
# 说明：这里改为自己创建的机器人的webhook的值
webhook = "https://oapi.dingtalk.com/robot/send?access_token=eca6aa375741e88bdaca5d98bdf6185c223570c609da63da60ec348b0e5b3bcf"

user = sys.argv[1]
# 发给钉钉群中哪个用户
text = sys.argv[3]
# 发送的报警内容
data = {
    "msgtype": "text",
    "text": {
        "content": text
    },
    "at": {
        "atMobiles": [
            user
        ],
        "isAtAll": False
    }
}

# 钉钉API固定数据格式
headers = {'Content-Type': 'application/json'}
x = requests.post(url=webhook, data=json.dumps(data), headers=headers)

# 将发送的告警信息写入本地日志/var/log/zabbix/dingding.log中
if os.path.exists("/var/log/zabbix/dingding.log"):
    f = open("/var/log/zabbix/dingding.log", "a+")
else:
    f = open("/var/log/zabbix/dingding.log", "w+")
f.write("\n" + "--" * 30)
if x.json()["errcode"] == 0:
    f.write("\n" + str(datetime.datetime.now()) + "    " + str(user) + "    " + "发送成功" + "\n" + str(text))
    f.close()
else:
    f.write("\n" + str(datetime.datetime.now()) + "    " + str(user) + "    " + "发送失败" + "\n" + str(text))
    f.close()
# 授权并创建日志
chmod +x /usr/lib/zabbix/alertscripts/dingding.py
chown zabbix:zabbix /usr/lib/zabbix/alertscripts/dingding.py
touch /var/log/zabbix/dingding.log
chown zabbix.zabbix /var/log/zabbix/dingding.log

```

3、测试脚本

```shell
python dingding.py  "这是条告警测试信息，请忽略哦"
```



![image-20230507110308021](assets/image-20230507110308021.png)

4、web界面设置

创建报警媒介

![image-20230507110634659](assets/image-20230507110634659.png)

创建动作

![image-20230507111216807](assets/image-20230507111216807.png)

![image-20230507111441346](assets/image-20230507111441346.png)

![image-20230507113540118](assets/image-20230507113540118.png)

创建报警媒介

![image-20230507111340246](assets/image-20230507111340246.png)

效果

![image-20230507113654495](assets/image-20230507113654495.png)



## grafana出图

[Grafana: The open observability platform | Grafana Labs](https://grafana.com/)

```shell
步骤：
    安装启动grafana
    在grafana安装zabbix插件
    创建一个连接zabbix数据源
    dashboard展示面板
```

1、安装启动grafana

```shell
wget https://mirrors.tuna.tsinghua.edu.cn/grafana/yum/rpm/Packages/grafana-8.3.6-1.x86_64.rpm
yum localinstall -y  grafana-8.3.6-1.x86_64.rpm
systemctl start grafana-server
systemctl enable grafana-server
netstat -ntpl|grep 3000
登录地址：http://${IP}:3000/login
初始账号密码：admin/admin
```

![image-20230507153801866](assets/image-20230507153801866.png)

2、在grafana安装zabbix插件

```shell
 grafana-cli  plugins install alexanderzobnin-zabbix-app
 systemctl restart grafana-server
```

启用插件

![image-20230507154426688](assets/image-20230507154426688.png)

![image-20230507154446333](assets/image-20230507154446333.png)

3、创建一个连接zabbix数据源

![image-20230507154551728](assets/image-20230507154551728.png)

http://10.0.0.15/zabbix/api_jsonrpc.php

![image-20230507154827825](assets/image-20230507154827825.png)

![image-20230507163653218](assets/image-20230507163653218.png)

4、dashboard展示面板

![image-20230507163559363](assets/image-20230507163559363.png)

`用zabbix4.0版本 grafana9.5版本 无法使用zabbix模板，更换为grafana8.x之后可以使用`

## 自定义监控模板

以TCP十一种状态为例

```shell
ESTABLISHED
SYN_SENT
SYN_RECV
FIN_WAIT1
FIN_WAIT2
TIME_WAIT
CLOSE
CLOSE_WAIT
LAST_ACK
LISTEN
CLOSING
```

1、参数形式自定义监控项

```shell
cat tcp_status.conf
UserParameter=tcp_status[*],netstat -ant|grep -c $1

systemctl restart zabbix-agent
```



2、测试取值

```shell
zabbix_get  -s 10.0.0.16 -k tcp_status[TIME_WAIT]
85
```

3、web页面上新建模板

步骤：

1. 将十一种状态设置为自定义监控项
2. 创建空模板
3. 复制自定义监控到模板中
4. 为模板添加触发器
5. 导出模板



![image-20230508101719916](assets/image-20230508101719916.png)

![image-20230508102121637](assets/image-20230508102121637.png)

![image-20230508102145774](assets/image-20230508102145774.png)

创建模板

思路是：先创建一个空的模板，然后通过复制监控项的方式来填充模板

![image-20230508102657210](assets/image-20230508102657210.png)

![image-20230508102736395](assets/image-20230508102736395.png)

![image-20230508102924428](assets/image-20230508102924428.png)

![image-20230508102943985](assets/image-20230508102943985.png)

可以看到已经有监控项了 

![image-20230508103015615](assets/image-20230508103015615.png)

会发现复制到模板的监控项是没有应用集合的，按照以下方式添加应用集合

![image-20230508103430135](assets/image-20230508103430135.png)

![image-20230508103445768](assets/image-20230508103445768.png)

创建触发器

![image-20230508103816663](assets/image-20230508103816663.png)

![image-20230508103843359](assets/image-20230508103843359.png)

![image-20230508103730503](assets/image-20230508103730503.png)



导出模板

![image-20230508104127178](assets/image-20230508104127178.png)



## web监测

![image-20230509160842393](assets/image-20230509160842393.png)

![image-20230509160919052](assets/image-20230509160919052.png)

![image-20230509160936547](assets/image-20230509160936547.png)

![image-20230509160949189](assets/image-20230509160949189.png)



![image-20230509161048963](assets/image-20230509161048963.png)



## snmp方式监控

利用centos机器模拟支持snmp协议的网络设备

1、安装并配置snmp

```shell
yum install -y net-snmp
vim /etc/snmp/snmpd.conf
# line41
com2sec notConfigUser  default       zabbix
# 新增一行 line55
view    systemview    included   .1

systemctl start snmpd
systemctl enable snmpd
# 检查是否启动成功
netstat -ntpul|grep 161
udp        0      0 0.0.0.0:161             0.0.0.0:*                           33191/snmpd

```

2、zabbix-server端安装net-snmp-utils

```shell
yum install -y net-snmp-utils
# 安装完成后就可以使用snmpwalk,获取内存大小
snmpwalk -c zabbix（在第一步line41中设置的密码） -v 2c 10.0.0.16 .1.3.6.1.2.1.25.2.2.0
HOST-RESOURCES-MIB::hrMemorySize.0 = INTEGER: 1868688 KBytes
```

3、在zabbix-server web页面中添加主机

创建主机群组

![image-20230510130536653](assets/image-20230510130536653.png)

创建主机

![image-20230510130608829](assets/image-20230510130608829.png)

链接模板

![image-20230510130625399](assets/image-20230510130625399.png)

修改宏

![image-20230510130643778](assets/image-20230510130643778.png)

等待若干分钟后查看是否监控成功

![image-20230510130721412](assets/image-20230510130721412.png)



## agent主动模式

主动模式的作用：减轻zabbix-server的压力，由原来的取值方式：zabbix-server向zabbix-agent获取数据--->zabbix-agent主动向zabbix-server提供数据。在监控的主机数量庞大时，可以做到减轻zabbix-server服务器的压力

```
主动模式：
    agent主动向server端获取需要监控的项目列表,并主动将监控项内需要检测的数据提交给serer/proxy
zabbix agent首先向serverActive的IP请求获取监控项 获取并提交active items数据提交至srever/proxy
 
被动模式：
    server向agent请求获取监控项的数据,agent返回数据,server会打开一个TCP连接,
server发送请求agent ping ,agent接收到请求并响应,server处理收到的数据
```

步骤：

1. 修改zabbix-agent配置文件
2. zabbix-server上修改模板获取数据方式为zabbix-agent主动模式



1、修改zabbix-agent配置文件

```shell
vim /etc/zabbix/zabbix_agentd.conf
Server=10.0.0.15
ServerActive=10.0.0.15

systemctl restart zabbix-agent.service
```

2、克隆linux模板后选中全部监控项更新为zabbix-agent主动模式

![image-20230510142332071](assets/image-20230510142332071.png)



注意如果监控模板中一个被动监控项都没有的话 zbx容易不绿

![image-20230510142458171](assets/image-20230510142458171.png)





