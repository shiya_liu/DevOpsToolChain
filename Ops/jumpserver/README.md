





## 安装jumpserver

```shell
# msyql创建用户
create database jumpserver default charset 'utf8';
grant all privileges on *.* to jumpserver@'%' identified by '123456';
FLUSH PRIVILEGES;
#登录测试
mysql -ujumpserver -p123456 -h 127.0.0.1
# 安装docker
yum remove -y docker \
  docker-client \
  docker-client-latest \
  docker-common \
  docker-latest \
  docker-latest-logrotate \
  docker-logrotate \
  docker-selinux \
  docker-engine-selinux \
  docker-engine
# yum-util 提供 yum-config-manager 功能
# 另外两个是 devicemapper 驱动依赖的
yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
sudo yum makecache fast
yum install -y docker-ce
systemctl enable docker.service

# 修改数据存储目录
mkdir /home/docker
cat >/etc/docker/daemon.json<<EOF
{"data-root": "/home/docker"}
EOF
# 启动docker
systemctl restart docker.service
docker info|grep 'Root Dir'
# 检查docker网络
docker run -d -it --name=busybox  busybox  sh
docker exec -it busybox sh
ping baidu.com

# 下载离线安装包
wget https://cdn0-download-offline-installer.fit2cloud.com/jumpserver/jumpserver-offline-installer-v3.3.1-amd64-322.tar.gz?Expires=1689144072&OSSAccessKeyId=LTAI5tNm6eCXpZo6cgoJet2h&Signature=FMrLMhNHFPoAbzc%2F9eK5Kj6b7mA%3D
或
浏览器输入 https://community.fit2cloud.com/#/products/jumpserver/downloads
tar zvxf jumpserver-offline-installer-v3.3.1-amd64-322.tar.gz -C /home/data/
cd jumpserver-offline-installer-v3.3.1-amd64-322/
#  生成SECRET_KEY和BOOTSTRAP_TOKEN
vim get_key.sh
if [ ! "$SECRET_KEY" ]; then
  SECRET_KEY=`cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 50`;
  echo "SECRET_KEY=$SECRET_KEY" >> ~/.bashrc;
  echo $SECRET_KEY;
else
  echo $SECRET_KEY;
fi  
if [ ! "$BOOTSTRAP_TOKEN" ]; then
  BOOTSTRAP_TOKEN=`cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 16`;
  echo "BOOTSTRAP_TOKEN=$BOOTSTRAP_TOKEN" >> ~/.bashrc;
  echo $BOOTSTRAP_TOKEN;
else
  echo $BOOTSTRAP_TOKEN;
fi

source get_key.sh
mkdir /data/jumpserver
 
# 修改配置
vim config-example.txt


# 安装
./jmsctl.sh install
错误：django.db.utils.OperationalError: (2002, "Can't connect to MySQL server on '10.0.0.34' (115)")
issue：https://github.com/jumpserver/jumpserver/issues/8694

# 启动
./jmsctl.sh start
```





## 遇到问题

安装jumpserver报错

[[错误\] v3.0.1-amd64-244 离线安装初始化数据库报错 ·问题 #10688 ·跳转服务器/跳转服务器 (github.com)](https://github.com/jumpserver/jumpserver/issues/10688)

```shell
错误：django.db.utils.OperationalError: (2002, "Can't connect to MySQL server on '10.0.0.34' (115)")
issue：https://github.com/jumpserver/jumpserver/issues/8694
根本原因是：容器运行后无法访问外界网络。连接不上数据库导致。可以使用以下命令测试
docker run -d -it --name=busybox  busybox  sh
docker exec -it busybox sh
ping baidu.com
```