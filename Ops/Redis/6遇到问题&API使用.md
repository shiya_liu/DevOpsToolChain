# 遇到问题&api的使用

启动问题

redis.service start operation timed out. terminating.

解决：

[(4条消息) 【Redis】centos7 systemctl 启动 Redis 失败_centos redis启动不起来_姜太小白的博客-CSDN博客](https://blog.csdn.net/yxzone/article/details/114861927)



显示问题

redis显示中文乱码

[(4条消息) Redis中存储value中文显示问题解决_redis value中文_ArvinWoo的博客-CSDN博客](https://blog.csdn.net/qq_37595946/article/details/77488955#:~:text=解决方法： 启动redis-cli时，在其后面加上,–raw 即可，汉字即可显示正常。)



配置密码后无法启动



主从同步问题

Opening the temp file needed for MASTER <-> SLAVE synchronization: Permission denied

从库没有权限打开临时文件，使用root权限启动redis从库、或者改redis安装目录为启动用户可写





搭建集群问题

[ERR] Node xxx is not empty. Either the node already knows other no...

解决方案： [(4条消息) redis创建集群时显示错误： [ERR\] Node xxx is not empty. Either the node already knows other no..._樊先知樊先知的博客-CSDN博客](https://blog.csdn.net/XIANZHIXIANZHIXIAN/article/details/82777767)



我在操作redis集群时将某个master节点的slot全部迁移到其它节点后我发现该master节点自动变成了slave，请问这是为什么？

```shell
Redis 集群中，每个节点都会根据一定的规则将自己的部分数据分片（slot）迁移到其他节点，以保证数据的均衡。当一个节点的数据量小于其他节点时，该节点会从其他节点获取多余的数据，以保证集群数据的均衡。

在您将某个 master 节点的所有 slot 迁移到其他节点后，该 master 节点的数据量变得很少，因此它会从其他节点获取多余的数据。由于该节点原本是 master 节点，因此当它从其他节点获取数据时，会自动降级为 slave 节点，以保证集群的可靠性。

因此，您所描述的情况是正常的行为，并不需要担心。如果您想将该节点重新升级为 master 节点，您需要手动增加该节点的数据量，以便它能够满足升级为 master 节点的条件。
```



