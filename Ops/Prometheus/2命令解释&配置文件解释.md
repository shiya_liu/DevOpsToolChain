# 命令解释&配置文件解释

prometheus的命令只有两个

### prometheus

```shell
usage: prometheus [<flags>]

The Prometheus monitoring server


Flags:
  -h, --[no-]help                显示上下文相关的帮助（也可以尝试 --help-long 和 --help-man）。
      --[no-]version             显示应用程序版本。
      --config.file="prometheus.yml"
                                 普罗米修斯配置文件路径。
      --web.listen-address="0.0.0.0:9090"
                                 要侦听 UI、API 和遥测的地址。
      --web.config.file=""       [实验性]可以启用 TLS 或身份验证的配置文件的路径。
      --web.read-timeout=5m      读取请求超时和关闭空闲连接之前的最长持续时间。
      --web.max-connections=512  最大同时连接数。
      --web.external-url=<URL>   可从外部访问普罗米修斯的 URL（例如，如果通过反向代理提供普罗米修斯）。用于
                                 生成与普罗米修斯本身的相对和绝对链接。如果 URL 有路径部分，它将用于前缀
                                 Prometheus 提供的所有 HTTP 端点。如果省略，将自动派生相关的 URL 组件。
      --web.route-prefix=<path>  Web 终结点的内部路由的前缀。默认为 --web.external-url 的路径。
      --web.user-assets=<path>   静态资产目录的路径，位于 /user。
      --[no-]web.enable-lifecycle
                                 通过 HTTP 请求启用关机和重新加载。
      --[no-]web.enable-admin-api
                                 为管理员控制操作启用 API 终结点。
      --[no-]web.enable-remote-write-receiver
                                 启用接受远程写入请求的 API 端点。
      --web.console.templates="consoles"
                                 控制台模板目录的路径，可在 /consoles 中找到。
      --web.console.libraries="console_libraries"
                                 控制台库目录的路径。
      --web.page-title="Prometheus Time Series Collection and Processing Server"
                                 普罗米修斯实例的文档标题。
      --web.cors.origin=".*"     CORS起源的正则表达式。它是完全锚定的。例：'https?://(domain1|domain2)\.com'
      --storage.tsdb.path="data/"
                                 指标存储的基本路径。仅与服务器模式一起使用。
      --storage.tsdb.retention=STORAGE.TSDB.RETENTION
                                 [已弃用]样品储存多长时间。此标志已被弃用，请改用“storage.tsdb.retention.time”。
                                 仅与服务器模式一起使用。
      --storage.tsdb.retention.time=STORAGE.TSDB.RETENTION.TIME
                                 样品储存多长时间。设置此标志时，它将覆盖“storage.tsdb.retention”。如果既不是这个标志也不是设置“storage.tsdb.retention”或“storage.tsdb.retention.size”，保留时间默认为15d。 支持的单位： y， w，D， H， M， S， MS.仅与服务器模式一起使用。
      --storage.tsdb.retention.size=STORAGE.TSDB.RETENTION.SIZE
                                可以为块存储的最大字节数。单位为必填项，支持的单位为B、KB、MB、GB、TB、PB、EB。前任：
                                 “512MB”。基于 2 的幂，因此 1KB 是 1024B。仅与服务器模式一起使用。
      --[no-]storage.tsdb.no-lockfile
                                 不要在数据目录中创建锁定文件。仅与服务器模式一起使用。 
      --storage.tsdb.head-chunks-write-queue-size=0
                                 将头块写入要进行 m 映射的磁盘的队列大小为 0 表示完全禁用队列。
                                 实验的。仅与服务器模式一起使用。
      --storage.agent.path="data-agent/"
                                 指标存储的基本路径。仅与代理模式一起使用。
      --[no-]storage.agent.wal-compression
                                 压缩代理 WAL。仅与代理模式一起使用。
      --storage.agent.retention.min-time=STORAGE.AGENT.RETENTION.MIN-TIME
                                 当 WAL 被截断时，在考虑删除之前，最低年龄样本可能仅在代理模式下使用。
      --storage.agent.retention.max-time=STORAGE.AGENT.RETENTION.MAX-TIME
                                 当 WAL 被截断时，最大年龄样本可能在被强制删除之前 仅与代理模式一起使用。
      --[no-]storage.agent.no-lockfile
                                 不要在数据目录中创建锁定文件。仅与代理模式一起使用。
      --storage.remote.flush-deadline=<duration>
                                 关闭或重新加载配置时等待刷新样本的时间。
      --storage.remote.read-sample-limit=5e7
                                 单个查询中通过远程读取接口返回的最大样本总数。0表示没有限制。此限制为
已忽略流式响应类型。仅与服务器模式一起使用。
      --storage.remote.read-concurrent-limit=10
                                 并发远程读取调用的最大数目。0表示没有限制。仅与服务器模式一起使用。
      --storage.remote.read-max-bytes-in-frame=1048576
                                 封送前流式传输远程读取响应类型的单个帧中的最大字节数。请注意，客户端可能
                                 对框架大小也有限制。默认情况下，根据protobuf的建议为1MB。仅与服务器模式一起使用。
      --rules.alert.for-outage-tolerance=1h
                                 允许普罗米修斯中断以恢复“for”警报状态的最长时间。仅与服务器模式一起使用。
      --rules.alert.for-grace-period=10m
                                 警报和恢复的“for”状态之间的最短持续时间。这仅针对配置了“for”时间的警报进行维护
                                 大于宽限期。仅与服务器模式一起使用。
      --rules.alert.resend-delay=1m
                                 向Alertmanager重新发送警报之前等待的最短时间。仅与服务器模式一起使用。
      --alertmanager.notification-queue-capacity=10000
                                 挂起Alertmanager通知的队列容量。仅与服务器模式一起使用。
      --query.lookback-delta=5m  在表达式求值和联合过程中检索度量的最大回溯持续时间。仅与服务器模式一起使用。
      --query.timeout=2m         中止查询之前可能需要的最长时间。仅与服务器模式一起使用。
      --query.max-concurrency=20
                                 同时执行的最大查询数。仅与服务器模式一起使用。
      --query.max-samples=50000000
                                单个查询可以加载到内存中的最大样本数。请注意，如果查询尝试加载更多样本，则查询将失败
因此这也限制了查询可以返回的样本数量。仅与服务器模式一起使用。
      --enable-feature= ...      要启用的以逗号分隔的功能名称。有效选项：代理，示例存储，扩展外部标签，
关闭时的内存快照，promql在修饰符，promql负偏移，promql每步统计，远程写入接收器
                                 （已弃用），额外的刮取指标，新的服务发现管理器，自动gomaxprocs，没有默认刮取端口，
                                 原生直方图。看见https://prometheus.io/docs/prometheus/latest/feature_flags/了解更多详细信息。
      --log.level=info           仅记录具有给定严重性或更高严重性的消息。其中之一：[debug，info，warn，error]
      --log.format=logfmt        日志消息的输出格式。其中之一：[logfmt，json]

```



### promtool

```shell
usage: promtool [<flags>] <command> [<args> ...]

普罗米修斯监控系统的工具。


Flags:
  -h, --[no-]help            Show context-sensitive help (also try --help-long and --help-man).
      --[no-]version         Show application version.
      --enable-feature= ...  要启用的以逗号分隔的功能名称（仅与PromQL相关，没有默认的刮取端口）。看见https://prometheus.io/docs/prometheus/latest/feature_flags/有关选项和更多详细信息。

Commands:
help [<command>...]
    Show help.

check service-discovery [<flags>] <config-file> <job>
    对给定作业名称执行服务发现并报告结果，包括重新标记。

check config [<flags>] <config-files>...
    检查配置文件是否有效。

check web-config <web-config-files>...
    检查web配置文件是否有效。

check healthy [<flags>]
    检查普罗米修斯服务器是否正常。

check ready [<flags>]
    检查普罗米修斯服务器是否准备就绪。

check rules [<flags>] <rule-files>...
    检查规则文件是否有效。

check metrics
    通过stdin传递Prometheus度量，以使它们保持一致性和正确性。

    examples:

    $ cat metrics.prom | promtool check metrics

    $ curl -s http://localhost:9090/metrics | promtool check metrics

query instant [<flags>] <server> <expr>
    运行即时查询。

query range [<flags>] <server> <expr>
    运行范围查询。

query series --match=MATCH [<flags>] <server>
    运行序列查询。

query labels [<flags>] <server> <name>
    运行标签查询。

debug pprof <server>
    获取分析调试信息。

debug metrics <server>
    获取度量调试信息。

debug all <server>
    获取所有调试信息。

test rules <test-rule-file>...
    规则的单元测试。

tsdb bench write [<flags>] [<file>]
    运行写性能基准测试。

tsdb analyze [<flags>] [<db path>] [<block id>]
    分析流失、标签对基数和压缩效率。

tsdb list [<flags>] [<db path>]
    列出tsdb块。

tsdb dump [<flags>] [<db path>]
    从TSDB转储样本。

tsdb create-blocks-from openmetrics <input file> [<output directory>]
    从OpenMetrics输入导入样本并生成TSDB块。有关更多详细信息，请参阅存储文档。

tsdb create-blocks-from rules --start=START [<flags>] <rule-files>...
    为新的录制规则创建数据块。

```





## 配置文件解释

参考：[prometheus 配置文件详解 - liwenchao1995 - 博客园 (cnblogs.com)](https://www.cnblogs.com/liwenchao1995/p/16895710.html)

```shell
Prometheus配置方式有两种：

	命令行:用来配置不可变命令参数，主要是Prometheus运行参数，比如数据存储位置
	配置文件:用来配置Prometheus应用参数，比如数据采集，报警对接
不重启进程配置生效方式有两种:
	对进程发送信号SIGHUP
	HTTP POST请求:需要开启–web.enable-lifecycle选项curl -X POST http://192.168.66.112:9091/-/reload


＃全局配置 （如果有内部单独设定，会覆盖这个参数）
global:

＃告警插件定义。这里会设定alertmanager这个报警插件。
alerting:

＃告警规则。 按照设定参数进行扫描加载，用于自定义报警规则，其报警媒介和route路由由alertmanager插件实现。
rule_files:

＃采集配置。配置数据源，包含分组job_name以及具体target。又分为静态配置和服务发现
scrape_configs:

＃用于远程存储写配置
remote_write:

＃用于远程读配置
remote_read:	


```





