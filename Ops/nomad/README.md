

**需求场景**：

1. IDC+混合云进行容器编排
2. 需要支持容器方式以及os install方式
3. 需要可以对某些服务集群进行负载均衡
4. 需要支持容器持久化挂载配置

**Nomad**

1. 多master选主 node架构，支持规划区域
2. 声明示资源文件进行部署
3. 支持shell、docker



**期望的跨区域资源调度方式**

IDC-A 与IDC-B之间只有一个公网暴露，用于调度资源







# 问题

```shell
通过学习nomad的官网教程 对于nomad的个人理解：
轻量版K8S，类似产品有docker swarm

1、如何部署docker应用 done
2、如何实现不停机的滚动更新
3、如何部署有状态服务
4、如何负载均衡
5、如何自动修复应用的期望值
6、如何指定节点进行调度 方式1：设置datacenters = ["node01"] 指定它node的datacenter
7、如何固定部署服务的静态端口：done
8、如何部署集群
9、如何自定义适用于当前环境的“包”
10、只能使用docker作为引擎进行部署嘛？ 支持那些引擎？ 如何部署服务的？
11、如果选nomad作为公司的基础设施管理平台，如何部署集群？
12、如何编写自己的docker应用进行部署？done
13、当下线一个节点时，容器是如何自主调度的？ 支持那些策略？ 
14、除docker外的其他引擎如何进行自主调度？
15、如何进行权限控制？
16、支持的驱动类型有哪些？ docker、qemu、java、exec、raw_exec
17、如何编写hcl：参考https://developer.hashicorp.com/nomad/docs/job-specification
18、如何监控nomad集群？
19、如何自动扩容节点？ 当job的内存使用到80%时自动扩容Job应对流量突增
20、job的类型有那些？ 作用以及应用场景分别是什么？
system、service和batch，它们决定Nomad将用于此job中task的调度器。
service 调度器被设计用来调度永远不会宕机的长寿命服务。
batch作业对短期性能波动的敏感性要小得多，寿命也很短，几分钟到几天就可以完成。
system 调度器用于注册应该在满足作业约束的所有nomad client上运行的作业。当某个client加入到nomad集群或转换到就绪状态时也会调用它。
parameterized: 作业作为参数化作业，以便可以对其进行分派。
periodic: 允许作业以固定的时间、日期或间隔运行。
system batch:

21、如何使用exec模式安装一个redis三节点的集群

22、exec 和raw_exec 有什么区别？ 
raw_exec: 驱动程序用于执行任务的命令，而无需任何 隔离。此外，该任务以与 Nomad 进程相同的用户身份启动。 因此，应格外小心地使用它，默认情况下处于禁用状态。raw_exec

exec:驱动程序用于简单地执行任务的特定命令。 但是，与raw_exec不同的是，它使用基础隔离 操作系统的基元，用于限制任务对资源的访问。
参考： 
https://developer.hashicorp.com/nomad/docs/drivers/exec
https://developer.hashicorp.com/nomad/docs/drivers/raw_exec

23、task的先后顺序？ 通过lifecycle字段可以解决
24、批量安装环境如何做？ 比如昨天的那种批量配置jvm以及大数据软件平台
25、nomad的日志在哪里？ 直到看到了日志的级别，但是日志文件是存放在那里？
26、nomad node meta 是什么作用？ 应该是和hcl中的亲和性联合使用
27、nomad的自动扩容是怎么设置的？
28、nomad和 packer consul 等组件的结合使用？
29、参数化job是怎么写的？ 怎么用的？ 什么场景下会使用参数化job
30、对于集群类型的应用是否可以使用packer+nomad的方式来部署？
31、如果不采用packer的方式部署，如何将集群服务的配置文件拷贝到目标主机？
```



# 简介

参考文档：

[使用nomad实现集群管理和微服务部署调度_wx60ddb84bd9bf9的技术博客_51CTO博客](https://blog.51cto.com/tonybai/3311490)

[nomad简明教程 - 简书 (jianshu.com)](https://www.jianshu.com/p/b822ceda022c)

[nomad集群搭建 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/70078923)

[Nomad | HashiCorp Developer](https://developer.hashicorp.com/nomad)

[Nomad jobs with raw_exec driver and consul service discovery - Nomad - HashiCorp Discuss](https://discuss.hashicorp.com/t/nomad-jobs-with-raw-exec-driver-and-consul-service-discovery/52859)

[使用nomad实现集群管理和微服务部署调度 |白旭东 (tonybai.com)](https://tonybai.com/2019/03/30/cluster-management-and-microservice-deployment-and-scheduled-by-nomad/)



nomad的作用

```shell
简单灵活的调度程序和编排器，可跨本地和云大规模部署和管理容器和非容器化应用程序
```













# 使用

## 一master三node节点部署集群

| IP地址    | 角色   | 备注                                   |
| --------- | ------ | -------------------------------------- |
| 10.0.0.40 | master | master节点不会安排job运行，请区别于K8S |
| 10.0.0.41 | node01 |                                        |
| 10.0.0.42 | node02 |                                        |
| 10.0.0.43 | node03 |                                        |







**server端**

```shell
二进制包地址：https://developer.hashicorp.com/nomad/downloads
# 下载nomad二进制命令
wget https://releases.hashicorp.com/nomad/1.5.5/nomad_1.5.5_linux_amd64.zip
yum install -y unzip
unzip nomad_1.5.5_linux_amd64.zip
mv nomad /usr/local/bin/nomad
rm -rf nomad_1.5.5_linux_amd64.zip

# 安装docker驱动支持
yum install -y docker
systemctl start docker
systemctl enable docker
# 安装Java驱动支持
yum install  -y java-1.8.0-openjdk  java-1.8.0-openjdk-devel

# 创建system管理文件
cat > /etc/systemd/system/nomad-server.service << EOF
[Unit]
Description=Nomad
Documentation=https://nomadproject.io/docs/
Wants=network-online.target
After=network-online.target

# When using Nomad with Consul it is not necessary to start Consul first. These
# lines start Consul before Nomad as an optimization to avoid Nomad logging
# that Consul is unavailable at startup.
#Wants=consul.service
#After=consul.service

[Service]
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/nomad agent -config /etc/nomad.d
KillMode=process
KillSignal=SIGINT
LimitNOFILE=infinity
LimitNPROC=infinity
Restart=on-failure
RestartSec=2
StartLimitBurst=3
StartLimitIntervalSec=10
TasksMax=infinity

[Install]
WantedBy=multi-user.target
EOF

# 创建nomad用于存储数据用的文件夹
mkdir -p /opt/nomad/
# 创建nomad用于存储配置用的文件夹
mkdir -p /etc/nomad.d/



# 创建server配置文件
mkdir -p  /etc/nomad.d/
cat > /etc/nomad.d/server.hcl << EOF
name = "server"

log_level = "DEBUG"

data_dir = "/opt/nomad/"

server {
    enabled = true
    bootstrap_expect = 1
}

ports {
  http = 4646
  rpc  = 4647
  serf = 4648
}
EOF
# 启动服务
systemctl start nomad-server.service
systemctl enable nomad-server.service



# 查看server节点数量
nomad server members
Name           Address    Port  Status  Leader  Raft Version  Build  Datacenter  Region
server.global  10.0.0.40  4648  alive   true    3             1.5.5  dc1         global


```



**自动补全命令**

```shell
nomad -autocomplete-install
complete -C /usr/local/bin/nomad nomad
[root@nomad-server ~]# nomad
acl             deployment      init            logs            plan            scaling         stop            var
agent           eval            inspect         monitor         plugin          sentinel        system          version
agent-info      exec            job             namespace       quota           server          tls             volume
alloc           fmt             license         node            recommendation  service         ui
config          fs              login           operator        run             status          validate

```





**node端**

```shell
mkdir  -p /etc/nomad.d/
cat > /etc/nomad.d/node_config.hcl <<EOF
name = "node01"
# Increase log verbosity
log_level = "DEBUG"
# Setup data dir
data_dir = "/opt/nomad/"
# Enable the client
client {
enabled = true
# For demo assume we are talking to server1. For production,
# this should be like "nomad.service.consul:4647" and a system
# like Consul used for service discovery.
servers = ["10.0.0.40:4647"]
options = {
    "driver.allowlist" = "docker,exec,java"
  }
}
# Modify our port to avoid a collision with server1
ports {
http = 5656
}
datacenter = "container"
EOF

# 配置system管理服务
cat > /etc/systemd/system/nomad-node.service << EOF
[Unit]
Description=Nomad
Documentation=https://nomadproject.io/docs/
Wants=network-online.target
After=network-online.target

# When using Nomad with Consul it is not necessary to start Consul first. These
# lines start Consul before Nomad as an optimization to avoid Nomad logging
# that Consul is unavailable at startup.
#Wants=consul.service
#After=consul.service

[Service]
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/nomad agent -config /etc/nomad.d/node_config.hcl
KillMode=process
KillSignal=SIGINT
LimitNOFILE=infinity
LimitNPROC=infinity
Restart=on-failure
RestartSec=2
StartLimitBurst=3
StartLimitIntervalSec=10
TasksMax=infinity

[Install]
WantedBy=multi-user.target
EOF
# 启动服务
systemctl start nomad-node
systemctl enable nomad-node
# server端验证
nomad node status
ID        DC         Name    Class   Drain  Eligibility  Status
7b0e3357  container  node01  <none>  false  eligible     ready
c9d0eae1  container  node02  <none>  false  eligible     ready
503a5df6  container  node03  <none>  false  eligible     ready

# 访问：
http://10.0.0.40:4646/ui/topology
```

![image-20230523131023296](assets/image-20230523131023296.png)





## 命令解释

请参照[command](./command)







## 学习hcl语法

官方说明：[Job Specification | Nomad | HashiCorp Developer](https://developer.hashicorp.com/nomad/docs/job-specification)

示例

```shell
#这声明了一个名为“docs”的作业。可能只有一个
#每个作业文件的作业声明。
job "docs" {
#指定此作业应在名为“us”的区域中运行。地区由Nomad服务器的配置定义。
  region = "us"

#将此作业中的任务分散在我们的测试-1和测试-1之间。
  datacenters = ["us-west-1", "us-east-1"]

#将此作业作为“服务”类型运行。每种工作类型都不同
#属性。有关更多示例，请参阅下面的文档。
  type = "service"

#指定此作业进行滚动更新，每次两次间隔30秒。
  update {
    stagger      = "30s"
    max_parallel = 2
  }


#组定义了一系列任务，这些任务应位于同一客户端（主机）上。组中的所有任务都将放置在同一主机上。
  group "webs" {
#指定我们想要的这些任务的数量。
    count = 5

    network {
    #这请求一个名为“http”的动态端口。这将
    #类似于“46283”，但我们通过
    #标签“http”。
      port "http" {}

#这将请求主机上443上的静态端口。这
#将限制此任务在每个主机上运行一次，因为
#在每个主机上只有一个端口443。
      port "https" {
        static = 443
      }
    }

#服务块告诉Nomad如何注册此服务与Consul合作进行服务发现和监控。
    service {
#这告诉告诉监控港口的服务
#标签为“http”。由于Nomad分配高动态端口
#数字，我们用标签来指代它们。
      port = "http"

      check {
        type     = "http"
        path     = "/health"
        interval = "10s"
        timeout  = "2s"
      }
    }

#创建一个单独的任务（工作单元）。这个特别的任务使用Docker容器来引导web应用程序。
    task "frontend" {
#将驱动程序指定为“docker”。Nomad支持多个驱动程序。
      driver = "docker"

#配置针对每个驱动程序。
      config {
        image = "hashicorp/web-frontend"
        ports = ["http", "https"]
      }

#可以设置环境变量
#任务运行时可用。
      env {
        DB_HOST = "db01.example.com"
        DB_USER = "web"
        DB_PASS = "loremipsum"
      }

#指定运行任务所需的最大资源，
#包括CPU和存储器。
      resources {
        cpu    = 500 # MHz
        memory = 128 # MB
      }
    }
  }
}

```



示例2

```shell
job "pytechco-redis" {
  type = "service"

  group "ptc-redis" {
    count = 1
    network {
      port "redis" {
        to = 6379
      }
    }

    service {
      name     = "redis-svc"
      port     = "redis"
      provider = "nomad"
    }

    task "redis-task" {
      driver = "docker"

      config {
        image = "redis:7.0.7-alpine"
        ports = ["redis"]
      }
    }
  }
}
```



示例3：

```shell
job "pytechco-web" {
  type = "service"

  group "ptc-web" {
    count = 1
    network {
      port "web" {
        static = 5000
      }
    }

    service {
      name     = "ptc-web-svc"
      port     = "web"
      provider = "nomad"
    }

    task "ptc-web-task" {
      template {
        data        = <<EOH
{{ range nomadService "redis-svc" }}
REDIS_HOST={{ .Address }}
REDIS_PORT={{ .Port }}
FLASK_HOST=0.0.0.0
REFRESH_INTERVAL=500
{{ end }}
EOH
        destination = "local/env.txt"
        env         = true
      }

      driver = "docker"

      config {
        image = "ghcr.io/hashicorp-education/learn-nomad-getting-started/ptc-web:1.0"
        ports = ["web"]
      }
    }
  }
}
```

总结

```shell
job -> group -> task -> service
1、每个hcl中都要有job：定义任务名称，job内可以配置全局设置，比如工作类型、工作区域、分配到那些node中
2、job中还会有group组， 组是和节点node进行绑定的，每组都会在一个节点上。组内可以设置数量，可以设置service以及具体执行任务的task
3、task中的config是根据驱动程序driver的不同而具体设置不同的。具体内容为驱动程序的详细设置
4、service是什么作用？ service好像是为了和consul等外部服务发现工具连接的
5、port "https" {static = 443}是设置静态端口，也就是固定端口；port "http" {}是动态分配端口？ 完全由nomad来决定-port x：x嘛？      port "redis"{to = 6379} 是tcp将容器内的6379端口随机映射到宿主机上

```









## 配置文件

关于配置文件的官方说明：

[Agent Configuration | Nomad | HashiCorp Developer](https://developer.hashicorp.com/nomad/docs/configuration)















## 发布任务

### docker类型

```shell
#官网示例
git clone https://gitee.com/shiya_liu/learn-nomad-getting-started.git
cd learn-nomad-getting-started/jobs
yum install -y docker
systemctl  start docker
cat >/etc/docker/daemon.json<< EOF
{
    "registry-mirrors": ["https://yo3sdl2l.mirror.aliyuncs.com","https://registry.docker-cn.com","http://hub-mirror.c.163.com","https://docker.mirrors.ustc.edu.cn"]
}
EOF
systemctl  restart docker

nomad job run pytechco-redis.nomad.hcl
```









### java类型

```shell
# 获取Java程序代码


```



### shell类型

```shell
# 使用raw_exec可以避免被隔离，直接使用操作系统层面的操作，需要在node上格外设置启用raw_exec插件
[root@node01 ~]# cat /etc/nomad.d/node_config.hcl
plugin "raw_exec" {
  config {
    enabled = true
  }
}

systemctl status nomad-node.service


# 编写hcl
job "install_redis" {
  type = "batch"
  datacenters = ["node01"]
  group "redis" {
    count = 1
    task "repolist" {
      driver = "raw_exec"
      user = "root"
      config {
        command = "/usr/bin/yum"
        args    = ["repolist"]
      }
    }
  }
}


# 完整示例：安装nginx
job "nginx" {
  type = "batch"
  datacenters = ["node01"]
  group "nginx" {
    count = 1

    task "install" {
        lifecycle {
        hook = "prestart"
        sidecar = false
        }
      driver = "raw_exec"
      user = "root"
      config {
        command = "/usr/bin/yum"
        args    = ["install", "-y", "nginx"]
      }
    }
    task "enable" {
      driver = "raw_exec"
      user = "root"
      config {
        command = "/usr/bin/systemctl"
        args    = ["enable", "nginx"]
      }
    }
    task "start" {
        lifecycle {
        hook = "poststart"
        sidecar = false
        }
      driver = "raw_exec"
      user = "root"
      config {
        command = "/usr/bin/systemctl"
        args    = ["start", "nginx"]
      }
    }
    task "netstat" {
        lifecycle {
        hook = "poststop"
        sidecar = false
        }
      driver = "raw_exec"
      user = "root"
      config {
        command = "/usr/bin/netstat"
        args    = ["-ntpl"]
      }
    }
  }
}



```

















docker方式部署服务

```shell
#官网示例
git clone https://gitee.com/shiya_liu/learn-nomad-getting-started.git
cd learn-nomad-getting-started/jobs
yum install -y docker
systemctl  start docker
cat >/etc/docker/daemon.json<< EOF
{
    "registry-mirrors": ["https://yo3sdl2l.mirror.aliyuncs.com","https://registry.docker-cn.com","http://hub-mirror.c.163.com","https://docker.mirrors.ustc.edu.cn"]
}
EOF
systemctl  restart docker

nomad job run pytechco-redis.nomad.hcl
```





## 负载均衡

疑惑：某个部署一个redis 所有节点的ip地址都可以对它进行访问嘛？ 每次重新部署 都会进行端口重新生成嘛? 要怎么处理呢？

官网示例：[Load Balancing with NGINX | Nomad | HashiCorp Developer](https://developer.hashicorp.com/nomad/tutorials/load-balancing/load-balancing-nginx)

实验🆗：[Load Balancing with HAProxy | Nomad | HashiCorp Developer](https://developer.hashicorp.com/nomad/tutorials/load-balancing/load-balancing-haproxy#check-the-haproxy-statistics-page)

疑惑：

1、必须要和consul进行结合嘛？ nomad本身没有这个功能嘛？

2、没有在hcl配置中看到如何与consul结合的

3、如果我在10.0.0.10~10.0.0.13 部署了consul集群，如何在10.0.0.50~10.0.0.60部署的集群中使用consul？











## 持久化







## 包管理

```shell
git clone https://github.com/hashicorp/nomad-pack
cd ./nomad-pack && make dev
export PATH="./bin:$PATH"
nomad-pack registry list

nomad-pack run hello_world
nomad-pack info hello_world

```







启用acl--服务端配置

```shell
# 1、配置启用acl
[root@master nomad.d]# cat /etc/nomad.d/server.hcl
acl {
  enabled = true
}

region = "global"

name = "master"

log_level = "DEBUG"

data_dir = "/opt/nomad/"

server {
    enabled = true
    bootstrap_expect = 1
}

ports {
  http = 4646
  rpc  = 4647
  serf = 4648
}
#2、重启服务
systemctl restart nomad-server
# 此时再次执行命令会没有权限
[root@master ~]# nomad server members
Error querying servers: Unexpected response code: 403 (Permission denied)

# 3、运行引导程序的命令
[root@master nomad.d]# nomad acl bootstrap
Accessor ID  = 235b7e6e-e4f3-994e-e482-aba77b05f3d3
Secret ID    = 87a8d2c9-9f63-6c79-e829-b4d4efe4adbe
Name         = Bootstrap Token
Type         = management
Global       = true
Create Time  = 2023-05-23 13:29:44.992079576 +0000 UTC
Expiry Time  = <none>
Create Index = 822
Modify Index = 822
Policies     = n/a
Roles        = n/a

# 4、临时配置
export NOMAD_TOKEN="87a8d2c9-9f63-6c79-e829-b4d4efe4adbe"
# 验证
[root@master nomad.d]# nomad server members
Name               Address    Port  Status  Leader  Raft Version  Build  Datacenter  Region
region-a-master.a  10.0.0.31  4648  alive   true    3             1.5.5  dc1         a
region-b-master.b  10.0.0.32  4648  alive   true    3             1.5.5  dc1         b
master.global      10.0.0.33  4648  alive   true    3             1.5.5  dc1         global

# 5、设置匿名访问权限
[root@master nomad.d]# cat ./anonymous.policy.hcl
namespace "*" {
  policy       = "write"
  capabilities = ["alloc-node-exec"]
}

agent {
  policy = "write"
}

operator {
  policy = "write"
}

quota {
  policy = "write"
}

node {
  policy = "write"
}

host_volume "*" {
  policy = "write"
}
# 部署匿名策略
nomad acl policy apply -description "Anonymous policy (full-access)" anonymous anonymous.policy.hcl

# 验证
[root@master nomad.d]# nomad server members
Name               Address    Port  Status  Leader  Raft Version  Build  Datacenter  Region
region-a-master.a  10.0.0.31  4648  alive   true    3             1.5.5  dc1         a
region-b-master.b  10.0.0.32  4648  alive   true    3             1.5.5  dc1         b
master.global      10.0.0.33  4648  alive   true    3             1.5.5  dc1         global



```





示例hcl编写

```hcl
job "pytechco-redis" {
  type = "service"

  group "ptc-redis" {
    count = 1
    network {
      port "redis" {
        to = 6379
      }
    }

    service {
      name     = "redis-svc"
      port     = "redis"
      provider = "nomad"
    }

    task "redis-task" {
      driver = "docker"

      config {
        image = "redis:7.0.7-alpine"
        ports = ["redis"]
      }
    }
  }
}
```









批量清理job

```shell
for i in $(nomad  job status|awk '{print $1}'|grep -v 'ID'); do nomad job stop -purge $i ; done
```











shell方式部署一套MySQL主从



部署容器无法启动时，查看原因

![image-20230522150402040](assets/image-20230522150402040.png)

![image-20230522150421647](assets/image-20230522150421647.png)







# 案例



## 多区域部署

```shell
# region-a-node
[root@region-a-node ~]# cat /etc/nomad.d/node_config.hcl
region = "a"
bind_addr = "172.168.1.2"
name = "region-a-node"
log_level = "DEBUG"
data_dir = "/opt/nomad/"
client {
enabled = true
servers = ["172.168.1.3:4647"]
network_interface = "ens36"
options = {
    "driver.allowlist" = "docker"
  }
}
ports {
http = 5656
}
datacenter = "region-a-node"

[root@region-a-node ~]# cat /etc/sysconfig/network-scripts/route-ens36
10.0.0.0/24 via 172.168.1.3 dev ens36


# region-b-node
[root@region-b-node ~]# cat /etc/nomad.d/node_config.hcl
region = "b"
bind_addr = "172.168.2.4"
name = "region-b-node"
log_level = "DEBUG"
data_dir = "/opt/nomad/"
client {
enabled = true
network_interface = "ens36"
servers = ["172.168.2.3:4647"]
options = {
    "driver.allowlist" = "docker"
  }
}
ports {
http = 5656
}
datacenter = "region-b-node"

[root@region-b-node ~]# cat /etc/sysconfig/network-scripts/route-ens36
10.0.0.0/24 via 172.168.2.3 dev ens36

# region-a-master
[root@region-a-master ~]# cat /etc/nomad.d/server.hcl
name = "region-a-master"

region = "a"

log_level = "DEBUG"

data_dir = "/opt/nomad/"

server {
    enabled = true
    bootstrap_expect = 1
}

ports {
  http = 4646
  rpc  = 4647
  serf = 4648
}

# region-b-master
[root@region-b-master ~]# cat /etc/nomad.d/server.hcl
name = "region-b-master"

region = "b"

log_level = "DEBUG"

data_dir = "/opt/nomad/"

server {
    enabled = true
    bootstrap_expect = 1
}

ports {
  http = 4646
  rpc  = 4647
  serf = 4648
}

# master
[root@master jobs]# cat /etc/nomad.d/server.hcl
region = "global"

name = "master"

log_level = "DEBUG"

data_dir = "/opt/nomad/"

server {
    enabled = true
    bootstrap_expect = 1
}

ports {
  http = 4646
  rpc  = 4647
  serf = 4648
}


# 加入集群
nomad server join "10.0.0.31:4648"
```











# 学习过程中遇到的问题



问题：[(11条消息) Centos 7 出现2个 inet 解决方法_minchowang的博客-CSDN博客](https://blog.csdn.net/qq_39261894/article/details/112135554)

配置网络：[[iptables\] 基于iptables实现的跨网络通信 - 一介布衣·GZ - 博客园 (cnblogs.com)](https://www.cnblogs.com/acommoners/p/15963002.html)



问题：如何删除node节点？

```shell
您还可以使用nomad system gc 命令以触发垃圾回收器。这将删除所有不再处于活动状态的作业、分配和节点。
```

问题：多区域部署时的回显

```shell
Error submitting job: Unexpected response code: 500 (rpc error: 1 error occurred:
        * Multiregion jobs are unlicensed.)

解答： 是因为多区域部署的功能只是在企业版中拥有，没有授权许可证，所以报错。
```



问题：

```shell
Error starting agent: client setup failed: fingerprinting failed: Error while detecting network interface  during fingerprinting: No default interface found
```

想法：
```shell
编写python脚本解析hcl文件，按照不同的区域来执行脚本，实现开源版本nomad 在master统一管理集群的需求。
```



问题：使用root用户exec模式安装redis报错：validate: running as user "root" is disallowed

解决方案：

```shell
options = {
    "driver.allowlist" = "docker,exec"
    "user.denylist" = ""
  }
```



验证：

[root@node03 ~]# ps -ef |grep sleep
root       3520   3491  0 22:30 ?        00:00:00 /usr/bin/sleep 100
root       3599   2605  0 22:31 pts/0    00:00:00 grep --color=auto sleep

![image-20230525223157501](assets/image-20230525223157501.png)

参考：[client Block - Agent Configuration | Nomad | HashiCorp Developer](https://developer.hashicorp.com/nomad/docs/configuration/client#user-denylist)





问题：在使用root用户进行yum install -y redis时报错：This system is not registered with an entitlement server. You can use subscription-manager to register

解决方法：

```shell
vim /etc/yum/pluginconf.d/subscription-manager.conf

[main]
enabled=0           #将它禁用掉
```



参考：[(1条消息) yum安装时提示：This system is not registered with an entitlement server. You can use subscription-manager_文艺倾年的博客-CSDN博客](https://blog.csdn.net/m0_51517236/article/details/123060376)



问题：编写的exec模式部署redis,明明已经清理了job 新建job时仍然提醒Cancelled because job is stopped

参考：[无法停止作业 ·问题 #9722 ·哈希科/游牧民族 ·GitHub](https://github.com/hashicorp/nomad/issues/9722)





**task的执行顺序**

问题描述：

nomad在执行多个task时，并不按照hcl中定义的顺序进行执行

我在hcl中定义 install、start、enable、 netstat,但是执行顺序如下图：

![image-20230528230010772](assets/image-20230528230010772.png)

众所周知，有些任务是需要依赖上一步骤的操作结果而进行的。



参考解答：[Multiple Tasks in a Single Job with order - Nomad - HashiCorp Discuss](https://discuss.hashicorp.com/t/multiple-tasks-in-a-single-job-with-order/24263)

通过lifecycle字段可以解决

```shell
job "nginx" {
  type = "batch"
  datacenters = ["node01"]
  group "nginx" {
    count = 1

    task "install" {
        lifecycle {
        hook = "prestart"
        sidecar = false
        }
      driver = "raw_exec"
      user = "root"
      config {
        command = "/usr/bin/yum"
        args    = ["install", "-y", "nginx"]
      }
    }
    task "enable" {
      driver = "raw_exec"
      user = "root"
      config {
        command = "/usr/bin/systemctl"
        args    = ["enable", "nginx"]
      }
    }
    task "start" {
        lifecycle {
        hook = "poststart"
        sidecar = false
        }
      driver = "raw_exec"
      user = "root"
      config {
        command = "/usr/bin/systemctl"
        args    = ["start", "nginx"]
      }
    }
    task "netstat" {
        lifecycle {
        hook = "poststop"
        sidecar = false
        }
      driver = "raw_exec"
      user = "root"
      config {
        command = "/usr/bin/netstat"
        args    = ["-ntpl"]
      }
    }
  }
}

```

