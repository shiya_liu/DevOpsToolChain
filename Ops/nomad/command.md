## 命令解释

关于命令解释的说明：[Commands: acl | Nomad | HashiCorp Developer](https://developer.hashicorp.com/nomad/docs/commands/acl)

```shell
Usage: nomad [-version] [-help] [-autocomplete-(un)install] <command> [args]

Common commands:


Other commands:
    acl                 与ACL策略和令牌交互
    agent-info          显示有关本地代理的状态信息
    config              与配置交互
    deployment          与部署交互
    eval                与评估互动
    exec                在任务中执行命令
    fmt                 将Nomad配置和作业文件重写为规范格式
    license             与Nomad Enterprise License交互
    login               使用身份验证方法登录Nomad
    monitor             来自Nomad代理的流日志
    namespace           与命名空间交互
    operator            为Nomad运营商提供集群级工具
    plugin              检查插件
    quota               与配额交互
    recommendation      与Nomad推荐端点交互
    scaling             与Nomad缩放端点交互
    sentinel            与Sentinel政策互动
    server              与服务器交互
    service             与注册服务互动
    system              与系统API交互
    tls                 为Nomad生成自签名TLS证书
    ui                  打开Nomad Web UI
    var                 与变量交互
    version             打印Nomad版本
    volume              与卷交互

```

### agent

```shell
Usage: nomad agent [options]

启动Nomad代理并运行，直到收到中断为止。代理可以是客户端和/或服务器。

Nomad代理的配置主要来自配置文件，但也可以直接传递选项的子集作为CLI参数，如下所示。

常规选项（客户端和服务器）：

  -bind=<addr>
    代理将为其所有不同网络绑定到的地址
    服务。运行的单个服务绑定到单个
    此地址上的端口。默认为环回127.0.0.1。

  -config=<path>
    单个配置文件或配置目录的路径
    用于配置Nomad代理的文件。此选项可能是
    多次指定。如果使用了多个配置文件
    每个值将合并在一起。合并期间，值
    从列表中稍后找到的文件将合并到的值上
    以前解析的文件。

  -data-dir=<path>
    用于存储状态和其他持久数据的数据目录。
    在客户端机器上，这用于存放分配数据，例如
    驱动程序使用的已下载工件。在服务器节点上，数据
    dir还用于存储复制的日志。

  -plugin-dir=<path>
    插件目录用于发现Nomad插件。如果没有指定，
    插件目录默认为<data-dir>/plugins/的目录。

  -dc=<datacenter>
    此Nomad代理所属数据中心的名称。
    默认设置为“dc1”。

  -log-level=<level>
    指定Nomad日志的详细程度。有效值包括
    DEBUG、INFO和WARN，按详细程度降序排列。这个
    默认值为INFO。

  -log-json
    以JSON格式输出日志。默认值为false。

  -node=<name>
    本地代理的名称。此名称用于标识节点
    在集群中。每个区域的名称必须唯一。默认值为
    机器的当前主机名。

  -region=<region>
    Nomad代理商所属地区的名称。默认情况下
    该值设置为“全局”

  -dev
    以开发模式启动代理。这使预配置
    双重角色代理（客户端+服务器），这对开发非常有用
    或测试Nomad。无需其他配置即可启动
    代理，但您可以传递一个可选的逗号分隔
    模式配置列表：

  -dev-connect
    以开发模式启动代理，但绑定到公共网络
    接口，而不是localhost。这
    模式仅在Linux上作为root支持。

Server Options:

  -server
    为代理启用服务器模式。服务器模式下的代理包括
    聚集在一起并处理
    领导人选举、数据复制和将工作安排到
    符合条件的客户端节点。

  -bootstrap-expect=<num>
    配置之前要等待的服务器节点的预期数量
    引导集群。一旦＜num＞服务器已经彼此加入，
    Nomad启动引导程序。

  -encrypt=<key>
    提供八卦加密密钥

  -join=<address>
    开始时要加入的代理的地址。可以指定
    多次。

  -raft-protocol=<num>
    要使用的Raft协议版本。用于启用某些自动驾驶仪
    功能。默认值为2。

  -retry-join=<address>
    要在启动时加入并启用重试的代理的地址。
    可以指定多次。

  -retry-max=<num>
    连接尝试的最大次数。默认为0，将重试
    无限期地
    
  -retry-interval=<dur>
    两次加入尝试之间等待的时间。

  -rejoin
    忽略以前的休假并尝试重新加入集群。

Client Options:

  -client
    为代理启用客户端模式。客户端模式使给定节点能够
    评估分配情况。如果未启用客户端模式，则不会执行任何工作
    安排给代理。

  -state-dir
    用于存储状态和其他持久数据的目录。如果没有
    指定将使用“-data-dir”下的子目录。

  -alloc-dir
    用于存储分配数据（例如下载的工件）的目录
    以及任务产生的数据。如果未指定，则在
    将使用“-data dir”。

  -servers
    要连接到的已知服务器地址的列表，给定为“host:port”和
    用逗号分隔。

  -node-class
    将此节点标记为节点类的成员。这可用于标记
    类似的节点类型。

  -meta
    要与节点关联的用户指定的元数据。-meta的每个实例
    解析单个KEY=VALUE对。对每个键/值对重复元标志
    待添加。

  -network-interface
    强制网络指纹打印机使用指定的网络接口。

  -network-speed
    网络接口的默认速度（以 MB 为单位）如果链路速度不能动态确定。

ACL Options:

  -acl-enabled
    指定代理是否应启用 ACL。

  -acl-replication-token
	服务器从权威地区。令牌必须是来自权威地区。

Consul Options:

  -consul-address=<addr>
	指定本地 Consul 代理的地址，格式为 host：port。
    支持格式为 unix:///tmp/consul/consul.sock 的 Unix 套接字

  -consul-auth=<auth>
	指定用于访问领事代理，以用户名：密码格式给出。

  -consul-auto-advertise
	指定 Nomad 是否应在 Consul 中宣传其服务。服务内容
    根据server_service_name和client_service_name命名。游牧民族
    服务器和客户端通告各自的服务，每个都标记
    适当地使用 HTTP 或 RPC 标记。游牧服务器还通告
    农奴标记的服务。

  -consul-ca-file=<path>
	指定用于 Consul 通信的 CA 证书的可选路径。
    如果未指定，则默认为系统捆绑包。

  -consul-cert-file=<path>
    指定用于 Consul 通信的证书的路径。如果这个
    设置，那么您还需要设置key_file。

  -consul-checks-use-advertise
    指定领事运行状况检查是否应绑定到播发地址。由
    默认值，这是绑定地址。

  -consul-client-auto-join
    指定 Nomad 客户端是否应自动发现
    通过搜索 中定义的领事服务名称来获取同一区域
    server_service_name选项。

  -consul-client-service-name=<name>
    指定 Consul 中 Nomad 客户端的服务名称。

  -consul-client-http-check-name=<name>
    在 Consul 中为 Nomad 客户端指定 HTTP 运行状况检查名称。

  -consul-key-file=<path>
    指定用于 Consul 通信的私钥的路径。如果这个
    设置，那么您还需要设置cert_file。

  -consul-server-service-name=<name>
    在 Consul 中为 Nomad 服务器指定服务的名称。

  -consul-server-http-check-name=<name>
    在 Consul 中为 Nomad 服务器指定 HTTP 运行状况检查名称。

  -consul-server-serf-check-name=<name>
    在 Consul 中为 Nomad 服务器指定 Serf 运行状况检查名称。

  -consul-server-rpc-check-name=<name>
    在 Consul 中为 Nomad 服务器指定 RPC 运行状况检查名称。
    
  -consul-server-auto-join
    指定 Nomad 服务器是否应自动发现并加入其他服务器
    通过搜索 Consul 服务名称来搜索
    server_service_name选项。仅当服务器不进行此搜索时，才会发生此搜索
    有一个领导者。

  -consul-ssl
    指定传输方案是否应使用 HTTPS 与
    领事代理。

  -consul-token=<token>
    指定用于提供每个请求的 ACL 令牌的令牌。

  -consul-verify-ssl
    指定在与通过 HTTPS 的 Consul API 客户端。

Vault Options:

  -vault-enabled
    是启用还是禁用保管库集成。

  -vault-address=<addr>
    与保险柜通信的地址。这应该与 http:// 一起提供或 https:// 前缀。

  -vault-token=<token>
    用于代表客户端从保管库派生令牌的保管库令牌。
    这只需要在服务器上设置。覆盖从中读取的保管库令牌
    VAULT_TOKEN环境变量。

  -vault-create-from-role=<role>
    要从中创建任务令牌的角色名称。

  -vault-allow-unauthenticated
    是否允许提交请求保管库令牌但未提交的作业
    认证。该标志仅适用于服务器。

  -vault-ca-file=<path>
    用于验证保险柜服务器 SSL 的 PEM 编码 CA 证书文件的路径
    证书。

  -vault-ca-path=<path>
    用于验证保管库服务器的 PEM 编码 CA 证书文件目录的路径
    证书。

  -vault-cert-file=<token>
    保管库通信的证书路径。

  -vault-key-file=<addr>
    保管库通信的私钥路径。

  -vault-tls-skip-verify=<token>
    启用或禁用 SSL 证书验证。

  -vault-tls-server-name=<token>
    用于在通过 TLS 连接时设置 SNI 主机。

```



### node

```shell
Usage: nomad node <subcommand> [options] [args]

  此命令对用于与节点交互的子命令进行分组。游牧民族中的节点是
  可以运行已提交工作负载的代理。此命令可用于检查
  节点并在节点上运行，例如耗尽其中的工作负载。

  检查节点的状态：

      $ nomad node status <node-id>

将节点标记为不符合运行工作负载的条件。这在节点上很有用,预计会被删除或升级，因此不会在其上放置新的分配：

      $ nomad node eligibility -disable <node-id>

	将节点标记为已清空，允许批处理作业在四个小时之前完成,强制它们离开节点：

      $ nomad node drain -enable -deadline 4h <node-id>

  有关详细的使用信息，请参阅各个子命令帮助。

Subcommands:
    config         查看或修改客户端配置详细信息
    drain          在给定节点上切换排水模式
    eligibility    切换给定节点的计划资格
    meta           与节点元数据交互
    status         显示有关节点的状态信息

```

#### nomad node config

```shell
Usage: nomad node config [options]

  查看或修改客户机节点的配置详细信息。此命令仅有效
  在客户机节点上，并可用于更新正在运行的客户机配置
  它支持。

  参数的行为因给出的标志而异。查看每个
  标志对其特定要求的描述。

General Options:

  -address=<addr>
	Nomad 服务器的地址。
    覆盖NOMAD_ADDR环境变量（如果已设置）。
    默认值 = http://127.0.0.1:4646

  -region=<region>
	要将命令转发到的 Nomad 服务器的区域。
    覆盖NOMAD_REGION环境变量（如果已设置）。
    默认为代理的本地区域。

  -no-color
	禁用彩色命令输出。或者，NOMAD_CLI_NO_COLOR可能是
    设置。此选项优先于 -force-color。

  -force-color
	强制彩色命令输出。这可以在通常
    终端检测失败。或者，可以设置NOMAD_CLI_FORCE_COLOR。
    如果还使用 -no-color，则此选项不起作用。

  -ca-cert=<path>
	用于验证 PEM 编码的 CA 证书文件的路径
    游牧服务器 SSL 证书。覆盖NOMAD_CACERT
    环境变量（如果已设置）。

  -ca-path=<path>
	要验证的 PEM 编码 CA 证书文件目录的路径
    Nomad 服务器 SSL 证书。如果同时 -ca-cert 和
    指定了 -ca-path，则使用 -ca-cert。覆盖
    NOMAD_CAPATH环境变量（如果已设置）。

  -client-cert=<path>
	用于 TLS 身份验证的 PEM 编码客户端证书的路径
    到游牧服务器。还必须指定 -客户端密钥。重写
    NOMAD_CLIENT_CERT环境变量（如果已设置）。

  -client-key=<path>
	与来自 -客户端证书的客户端证书。覆盖
    NOMAD_CLIENT_KEY环境变量（如果已设置）。

  -tls-server-name=<value>
	通过 连接时用作 SNI 主机的服务器名称
    啰嗦。覆盖NOMAD_TLS_SERVER_NAME环境变量（如果已设置）。

  -tls-skip-verify
	不要验证 TLS 证书。强烈建议不要这样做。验证
    如果设置了NOMAD_SKIP_VERIFY，也将跳过

  -token
	用于对 API 请求进行身份验证的 ACL 令牌的机密 ID。
    覆盖NOMAD_TOKEN环境变量（如果已设置）。

Client Config Options:

  -servers
	列出客户机节点的已知服务器地址。客户机节点不
    参与八卦池，并在这些服务器上注册
    定期通过网络。

	如果启用了 ACL，则此选项需要具有“代理：读取”的令牌
    能力。

  -update-servers
	使用提供的参数更新客户端的服务器列表。倍数
    可以使用多个参数传递服务器地址。重要提示：何时
    更新服务器列表，您必须指定所有服务器节点
    希望配置。该集以原子方式更新。

	如果启用了 ACL，则此选项需要具有“代理：写入”的令牌
    能力。

    Example:
      $ nomad node config -update-servers foo:4647 bar:4647
```



#### nomad node drain

```shell
Usage: nomad node drain [options] <node>

  切换指定节点上的节点排出。要求指定了 -enable 或 -禁用，但不能同时指定两者。 -self 标志可用于
  清空本地节点。

  如果启用了 ACL，则此选项需要具有“node：write”的令牌
  能力。

General Options:

  -address=<addr>
	Nomad 服务器的地址。
    覆盖NOMAD_ADDR环境变量（如果已设置）。
    默认值 = http://127.0.0.1:4646

  -region=<region>
	要将命令转发到的 Nomad 服务器的区域。
    覆盖NOMAD_REGION环境变量（如果已设置）。
    默认为代理的本地区域。

  -no-color
	禁用彩色命令输出。或者，NOMAD_CLI_NO_COLOR可能是
    设置。此选项优先于 -force-color。

  -force-color
	强制彩色命令输出。这可以在通常
    终端检测失败。或者，可以设置NOMAD_CLI_FORCE_COLOR。
    如果还使用 -no-color，则此选项不起作用。

  -ca-cert=<path>
	用于验证 PEM 编码的 CA 证书文件的路径
    游牧服务器 SSL 证书。覆盖NOMAD_CACERT
    环境变量（如果已设置）。

  -ca-path=<path>
	要验证的 PEM 编码 CA 证书文件目录的路径
    Nomad 服务器 SSL 证书。如果同时 -ca-cert 和
    指定了 -ca-path，则使用 -ca-cert。覆盖
    NOMAD_CAPATH环境变量（如果已设置）。

  -client-cert=<path>
	用于 TLS 身份验证的 PEM 编码客户端证书的路径
    到游牧服务器。还必须指定 -客户端密钥。重写
    NOMAD_CLIENT_CERT环境变量（如果已设置）。

  -client-key=<path>
	与来自 -客户端证书的客户端证书。覆盖
    NOMAD_CLIENT_KEY环境变量（如果已设置）。

  -tls-server-name=<value>
    通过 连接时用作 SNI 主机的服务器名称
    啰嗦。覆盖NOMAD_TLS_SERVER_NAME环境变量（如果已设置）。

  -tls-skip-verify
    不要验证 TLS 证书。强烈建议不要这样做。验证
    如果设置了NOMAD_SKIP_VERIFY，也将跳过。

  -token
    用于对 API 请求进行身份验证的 ACL 令牌的机密 ID。
    覆盖NOMAD_TOKEN环境变量（如果已设置）。

Node Drain Options:

  -disable
    禁用指定节点的排出。

  -enable
    为指定节点启用排出。

  -deadline <duration>
    设置必须将所有分配移出节点的截止时间。
    截止时间之后的剩余分配将被强制从节点中删除。
    如果未指定，则应用一小时的默认截止时间。

  -detach
    立即返回，而不是进入监视模式。

  -monitor
    直接进入监控模式，无需修改排水状态。

  -force
    立即强制删除节点上的分配。

  -no-deadline
    没有截止日期允许分配在不强制的情况下耗尽节点
    在某个截止日期后停止。

  -ignore-system
    忽略系统允许在不停止系统作业的情况下完成排水
    分配。默认情况下，系统作业最后停止。

  -keep-ineligible
    保持不合格将保持节点的调度不合格性，即使
    正在禁用排水管。当现有排水管
    已取消，但不需要在节点上进行其他调度。

  -m
    排出更新操作的消息。在排水元数据中注册为
    漏极启用期间的“消息”和停用漏极期间的“cancel_message”。

  -meta <key>=<value>
    自定义元数据存储在排水操作上，可以多次使用。

  -self
    设置本地节点的排出状态。

  -yes
    对提示自动“是”。

```

#### nomad node eligibility

```shell
Usage: nomad node eligibility [options] <node>

	切换节点计划资格。当节点被标记为不合格时，
  不会对其进行新的分配，但保留现有分配。
  若要删除现有分配，请使用节点 drain 命令。

	需要指定 -enable 或 -禁用，但不能同时指定两者。
  -self 标志可用于设置本地节点的调度资格。

	如果启用了 ACL，则此选项需要具有“node：write”的令牌
  能力。

General Options:

  -address=<addr>
    The address of the Nomad server.
    Overrides the NOMAD_ADDR environment variable if set.
    Default = http://127.0.0.1:4646

  -region=<region>
    The region of the Nomad servers to forward commands to.
    Overrides the NOMAD_REGION environment variable if set.
    Defaults to the Agent's local region.

  -no-color
    Disables colored command output. Alternatively, NOMAD_CLI_NO_COLOR may be
    set. This option takes precedence over -force-color.

  -force-color
    Forces colored command output. This can be used in cases where the usual
    terminal detection fails. Alternatively, NOMAD_CLI_FORCE_COLOR may be set.
    This option has no effect if -no-color is also used.

  -ca-cert=<path>
    Path to a PEM encoded CA cert file to use to verify the
    Nomad server SSL certificate. Overrides the NOMAD_CACERT
    environment variable if set.

  -ca-path=<path>
    Path to a directory of PEM encoded CA cert files to verify
    the Nomad server SSL certificate. If both -ca-cert and
    -ca-path are specified, -ca-cert is used. Overrides the
    NOMAD_CAPATH environment variable if set.

  -client-cert=<path>
    Path to a PEM encoded client certificate for TLS authentication
    to the Nomad server. Must also specify -client-key. Overrides
    the NOMAD_CLIENT_CERT environment variable if set.

  -client-key=<path>
    Path to an unencrypted PEM encoded private key matching the
    client certificate from -client-cert. Overrides the
    NOMAD_CLIENT_KEY environment variable if set.

  -tls-server-name=<value>
    The server name to use as the SNI host when connecting via
    TLS. Overrides the NOMAD_TLS_SERVER_NAME environment variable if set.

  -tls-skip-verify
    Do not verify TLS certificate. This is highly not recommended. Verification
    will also be skipped if NOMAD_SKIP_VERIFY is set.

  -token
    The SecretID of an ACL token to use to authenticate API requests with.
    Overrides the NOMAD_TOKEN environment variable if set.

Node Eligibility Options:

  -disable
    将指定的节点标记为不符合新分配的条件。

  -enable
    将指定的节点标记为符合新分配条件。

  -self
    设置本地节点的资格。

```



#### nomad node meta

```shell
Usage: nomad node meta [subcommand]

		与节点的元数据交互。应用子命令允许动态
        更新节点元数据。read 子命令允许读取所有
        客户端上设置的元数据。所有命令都直接与客户端交互，并且
        允许使用 -node-id 选项设置自定义目标。

有关详细的使用信息，请参阅各个子命令帮助。

Subcommands:
    apply    修改节点元数据
    read     读取节点元数据


Usage: nomad node meta apply [-node-id ...] [-unset ...] key1=value1 ... kN=vN

		修改节点的元数据。此命令仅适用于客户端代理，并且可以
        用于更新节点注册的计划元数据。

	更改是批处理的，最多可能需要 10 秒才能传播到
  服务器并影响调度。

General Options:

  -address=<addr>
    The address of the Nomad server.
    Overrides the NOMAD_ADDR environment variable if set.
    Default = http://127.0.0.1:4646

  -region=<region>
    The region of the Nomad servers to forward commands to.
    Overrides the NOMAD_REGION environment variable if set.
    Defaults to the Agent's local region.

  -no-color
    Disables colored command output. Alternatively, NOMAD_CLI_NO_COLOR may be
    set. This option takes precedence over -force-color.

  -force-color
    Forces colored command output. This can be used in cases where the usual
    terminal detection fails. Alternatively, NOMAD_CLI_FORCE_COLOR may be set.
    This option has no effect if -no-color is also used.

  -ca-cert=<path>
    Path to a PEM encoded CA cert file to use to verify the
    Nomad server SSL certificate. Overrides the NOMAD_CACERT
    environment variable if set.

  -ca-path=<path>
    Path to a directory of PEM encoded CA cert files to verify
    the Nomad server SSL certificate. If both -ca-cert and
    -ca-path are specified, -ca-cert is used. Overrides the
    NOMAD_CAPATH environment variable if set.

  -client-cert=<path>
    Path to a PEM encoded client certificate for TLS authentication
    to the Nomad server. Must also specify -client-key. Overrides
    the NOMAD_CLIENT_CERT environment variable if set.

  -client-key=<path>
    Path to an unencrypted PEM encoded private key matching the
    client certificate from -client-cert. Overrides the
    NOMAD_CLIENT_KEY environment variable if set.

  -tls-server-name=<value>
    The server name to use as the SNI host when connecting via
    TLS. Overrides the NOMAD_TLS_SERVER_NAME environment variable if set.

  -tls-skip-verify
    Do not verify TLS certificate. This is highly not recommended. Verification
    will also be skipped if NOMAD_SKIP_VERIFY is set.

  -token
    The SecretID of an ACL token to use to authenticate API requests with.
    Overrides the NOMAD_TOKEN environment variable if set.

Node Meta Apply Options:

  -node-id
    更新指定节点上的元数据。如果未指定接收的节点
    默认情况下将使用该请求。

  -unset key1,...,keyN
    取消设置命令分隔的键列表。

    Example:
      $ nomad node meta apply -unset testing,tempvar ready=1 role=preinit-db




```

#### nomad node status

```shell
Usage: nomad node status [options] <node>

  显示有关给定节点的状态信息。节点列表
  返回的仅包括作业可能调度到的节点，以及
  包括状态和其他高级信息。

  如果传递了节点 ID，将显示该特定节点的信息，
  包括资源使用情况统计信息。如果未传递节点 ID，则
  将显示所有节点的简写列表。-self 标志可用于
  快速访问本地节点的状态。

  如果启用了 ACL，则此选项需要具有“节点：读取”的令牌
  能力。

General Options:

  -address=<addr>
    The address of the Nomad server.
    Overrides the NOMAD_ADDR environment variable if set.
    Default = http://127.0.0.1:4646

  -region=<region>
    The region of the Nomad servers to forward commands to.
    Overrides the NOMAD_REGION environment variable if set.
    Defaults to the Agent's local region.

  -no-color
    Disables colored command output. Alternatively, NOMAD_CLI_NO_COLOR may be
    set. This option takes precedence over -force-color.

  -force-color
    Forces colored command output. This can be used in cases where the usual
    terminal detection fails. Alternatively, NOMAD_CLI_FORCE_COLOR may be set.
    This option has no effect if -no-color is also used.

  -ca-cert=<path>
    Path to a PEM encoded CA cert file to use to verify the
    Nomad server SSL certificate. Overrides the NOMAD_CACERT
    environment variable if set.

  -ca-path=<path>
    Path to a directory of PEM encoded CA cert files to verify
    the Nomad server SSL certificate. If both -ca-cert and
    -ca-path are specified, -ca-cert is used. Overrides the
    NOMAD_CAPATH environment variable if set.

  -client-cert=<path>
    Path to a PEM encoded client certificate for TLS authentication
    to the Nomad server. Must also specify -client-key. Overrides
    the NOMAD_CLIENT_CERT environment variable if set.

  -client-key=<path>
    Path to an unencrypted PEM encoded private key matching the
    client certificate from -client-cert. Overrides the
    NOMAD_CLIENT_KEY environment variable if set.

  -tls-server-name=<value>
    The server name to use as the SNI host when connecting via
    TLS. Overrides the NOMAD_TLS_SERVER_NAME environment variable if set.

  -tls-skip-verify
    Do not verify TLS certificate. This is highly not recommended. Verification
    will also be skipped if NOMAD_SKIP_VERIFY is set.

  -token
    The SecretID of an ACL token to use to authenticate API requests with.
    Overrides the NOMAD_TOKEN environment variable if set.

Node Status Options:

  -self
    查询本地节点的状态。

  -stats
    显示详细的资源使用情况统计信息。

  -allocs
    显示每个节点的运行分配计数。

  -short
    显示短输出。仅在单个节点存在时使用
    已查询，并删除有关节点分配的详细输出。

  -verbose
    显示完整信息。

  -per-page
    每页要显示的结果数。

  -page-token
    从哪里开始分页。

  -filter
    指定用于筛选查询结果的表达式。

  -os
    显示操作系统名称。

  -quiet
    仅显示节点 ID。

  -json
    以 JSON 格式输出节点。

  -t
    使用 Go 模板格式化和显示节点。

```

### job

```shell
Usage: nomad job <subcommand> [options] [args]

 此命令对用于与作业交互的子命令进行分组。

  运行新作业或更新现有作业：

      $ nomad job run <path>

  规划作业的运行以确定将发生的更改：

      $ nomad job plan <path>

  停止正在运行的作业：

      $ nomad job stop <name>

  检查正在运行的作业的状态：

      $ nomad job status <name>

  有关详细的使用信息，请参阅各个子命令帮助。

Subcommands:
    allocs            列出作业的分配
    deployments       列出作业的部署
    dispatch          调度参数化作业的实例
    eval              强制评估作业
    history           显示作业的所有跟踪版本
    init              创建示例作业文件
    inspect           检查提交的作业
    periodic          与定期作业交互
    plan              试运行作业更新以确定其效果
    promote           提升作业的金丝雀
    restart           重新启动或重新计划作业的分配
    revert            还原到作业的先前版本
    run               运行新作业或更新现有作业
    scale             更改 Nomad 作业组的计数
    scaling-events    显示作业的最新扩展事件
    status            显示有关作业的状态信息
    stop              停止正在运行的作业
    validate          检查给定作业规范是否有效

```

#### nomad job allocs

```shell
Usage: nomad job allocs [options] <job>

  显示特定作业的分配。

  启用 ACL 后，此命令需要具有“读取作业”的令牌
  作业命名空间的功能。需要“列表作业”功能来
  使用作业前缀而不是确切的作业 ID 运行命令。

Allocs Options:

  -all
    显示与作业 ID 匹配的所有分配，甚至包括来自较旧作业 ID 的分配
    作业的实例。

  -json
    以 JSON 格式输出分配。

  -t
    使用 Go 模板格式化和显示分配。

  -verbose
    显示完整信息。

```

#### nomad job deployments

```shell
Usage: nomad job deployments [options] <job>

 
  部署用于显示特定作业的部署。

  启用 ACL 后，此命令需要具有“读取作业”的令牌
  作业命名空间的功能。需要“列表作业”功能来
  使用作业前缀而不是确切的作业 ID 运行命令。

Deployments Options:

  -json
    以 JSON 格式输出部署。

  -t
    使用 Go 模板设置和显示部署的格式。
    
  -latest
    Display the latest deployment only.

  -verbose
    显示完整信息。

  -all
    显示与作业 ID 匹配的所有部署，包括那些
    来自作业的较旧实例。


```

#### nomad job dispatch

```shell
Usage: nomad job dispatch [options] <parameterized job> [input source]

  调度创建参数化作业的实例。数据有效负载到
  可以通过 stdin 使用“-”或指定
  文件的路径。元数据可以通过使用一个或多个元标志来提供
  次。

  可选的幂等性令牌可用于防止多个实例
  要调度的作业。如果已具有相同令牌的实例
  存在，则命令返回而不执行任何操作。

  创建成功后，将打印已调度的作业 ID，并
  将监控触发的评估。这可以通过提供
  分离标志。

  启用 ACL 后，此命令需要带有“调度作业”的令牌
  作业命名空间的功能。需要“列表作业”功能来
  使用作业前缀而不是确切的作业 ID 运行命令。“读取作业”
  当 -detach 为
  未使用

Dispatch Options:

  -meta <key>=<value>
	Meta 采用由“=”分隔的键/值对。元数据键将是
    合并到作业的元数据中。作业可能会为
    调度时被覆盖的密钥。该标志可以提供超过
    一次注入多个元数据键/值对。任意键不是
    允许。参数化作业必须允许合并密钥。

  -detach
    立即返回，而不是进入监视模式。作业调度后，
    评估 ID 将打印到屏幕上，可用于
    使用 eval-status 命令检查评估。

  -idempotency-token
    可选标识符，用于防止作业的多个实例
    正在派遣。

  -id-prefix-template
    已调度作业 ID 的可选前缀模板。

  -verbose
    显示完整信息。


```

#### nomad job eval

```shell
Usage: nomad job eval [options] <job_id>


  强制评估提供的作业 ID。强制评估将
  触发计划程序以重新评估作业。力标志允许
  运算符强制调度程序在某些条件下创建新的分配
  场景。

  启用 ACL 后，此命令需要具有“读取作业”的令牌
  作业命名空间的功能。需要“列表作业”功能来
  使用作业前缀而不是确切的作业 ID 运行命令。
  
Eval Options:

  -force-reschedule
	强制重新安排失败的分配，即使它们当前不是
    符合重新安排的条件。

  -detach
	立即返回，而不是进入监视模式。标识
    的评估将打印到屏幕上，可以是
    用于使用 eval-status 命令检查评估。

  -verbose
    显示完整信息。



```

#### nomad job history

```shell
Usage: nomad job history [options] <job>

  历史记录用于显示特定作业的已知版本。命令
  可以显示作业版本之间的差异，并且有助于理解
  对作业发生的更改以及决定要还原的作业版本
  自。

  启用 ACL 后，此命令需要具有“读取作业”的令牌
  作业命名空间的功能。需要“列表作业”功能来
  使用作业前缀而不是确切的作业 ID 运行命令。
  
History Options:

  -p
    显示每个作业与其前置作业之间的差异。

  -full
    显示每个版本的完整作业定义。

  -version <job version>
    仅显示给定作业版本的历史记录。

  -json
    以 JSON 格式输出作业版本。

  -t
    使用 Go 模板格式化和显示作业版本。


```

#### nomad job init

```shell
Alias: nomad init <filename>

  创建可用作自定义的起点的示例作业文件
  进一步。如果未给出文件名，则将使用默认值“example.nomad.hcl”。

Init Options:

  -short
    如果设置了短标志，则会发出不带注释的最小作业规范。

  -connect
    如果设置了连接标志，则作业规范包括 Consul Connect 集成。

  -template
    指定要初始化的预定义模板。必须是游牧变量
    住在游牧民族/工作模板/<template>

  -list-templates
    显示要传递给 -template 的可能作业模板的列表。读取自
    所有变量路径在游牧/作业模板/<template>


```
#### nomad job inspect

```shell
Usage: nomad job inspect [options] <job>
Alias: nomad inspect

“检查”用于查看已提交作业的规范。

  启用 ACL 后，此命令需要具有“读取作业”的令牌
  作业命名空间的功能。需要“列表作业”功能来
  使用作业前缀而不是确切的作业 ID 运行命令。

Inspect Options:

  -version <job version>
    在给定作业版本显示作业。

  -json
    以 JSON 格式输出作业。

  -t
    使用 Go 模板格式化和显示作业。

```



#### nomad job periodic

```shell
Usage: nomad job periodic <subcommand> [options] [args]

  此命令对用于与定期作业交互的子命令进行分组。

  Force a periodic job:

      $ nomad job periodic force <job_id>

  有关详细的使用信息，请参阅各个子命令帮助。

Subcommands:
    force    强制启动定期作业

```



#### nomad job periodic force

```shell
Usage: nomad job periodic force <job id>

  此命令用于强制创建定期作业的新实例。
  这用于立即运行定期作业，即使它违反了作业的
  prohibit_overlap设置。

  启用 ACL 后，此命令需要具有“提交作业”的令牌
  作业命名空间的功能。需要“列表作业”功能来
  使用作业前缀而不是确切的作业 ID 运行命令。“读取作业”
  当 -detach 为
  未使用。

Periodic Force Options:

  -detach
    立即返回，而不是进入监视模式。受力后，
    评估 ID 将打印到屏幕上，可用于
    使用 eval-status 命令检查评估。

  -verbose
    显示完整信息。

```





#### nomad job plan

```shell
Usage: nomad job plan [options] <path>
Alias: nomad plan

计划调用调度程序的试运行以确定提交的效果
  作业的新版本或更新版本。该计划不会产生任何结果
  对群集的更改，但可以深入了解是否可以运行作业
  成功以及它将如何影响现有分配。

如果提供的路径为“-”，则从标准输入读取作业文件。否则
  它从提供的路径的文件读取或下载，并且
  从指定的 URL 读取。

作业修改索引随计划一起返回。此值可用于以下情况
  使用“nomad run -check-index”提交作业，这将检查作业是否在调用
  调度。这可确保自计划以来未修改作业。
  多区域作业不返回作业修改索引。

本地作业和远程作业之间的结构化差异将显示给
  深入了解调度程序将尝试执行的操作及其原因。

如果作业指定了区域，则 -region 标志和NOMAD_REGION
  环境变量将被覆盖，并使用作业的区域。

计划将返回以下退出代码之一：
    * 0：未创建或销毁分配。
    * 1：创建或销毁的分配。
    * 255：确定计划结果时出错。

计划命令将根据以下内容设置作业的vault_token
  优先级，从最高到最低：-vault-token 标志，即
  $VAULT_TOKEN 环境变量，最后是作业文件中的值。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  作业命名空间的功能。


Plan Options:

  -diff
    确定是否显示远程作业和计划作业之间的差异。
    默认值为 true。

  -json
    将作业文件解析为 JSON。如果外部对象具有作业字段，例如
    从“Nomad Job Inspect”或“Nomad Run -output”中，该字段的值为
    用作作业。

  -hcl1
    将作业文件解析为 HCLv1。优先于 “-hcl2-strict”。

  -hcl2-strict
    是否应从 HCL2 解析器生成错误，其中变量
    已提供未在根变量中定义的。违约
    为 true，但如果还定义了“-hcl1”，则忽略。

  -policy-override
    设置标志以强制覆盖任何软性强制 Sentinel 策略。

  -vault-token
    用于验证提交作业的用户是否有权运行作业
    根据其保险柜政策。如果保管库
    块allow_unauthenticated在 Nomad 服务器配置中被禁用。
    如果设置了 -vault-token 标志，则传递的 Vault 令牌将添加到作业规范中
    在发送到Nomad服务器之前。这允许传递保管库令牌
    而不将其存储在作业文件中。这将覆盖在
    作业文件中的 $VAULT_TOKEN 环境变量和vault_token字段。
    此令牌在验证后从作业中清除，不能在
    作业执行环境。在作业中模板化时使用保管库块
    使用保险柜令牌

  -vault-namespace
    如果设置，则传递的 Vault 命名空间将存储在作业中，然后再发送到
    游牧服务器。

  -var 'key=value'
    模板的变量，可以多次使用。

  -var-file=path
    包含用户变量的 HCL2 文件的路径。

  -verbose
    增加差异详细程度。


```



#### nomad job promote

```shell
Usage: nomad job promote [options] <job id>

  升级用于升级最新部署中的任务组
  给定的工作。当部署已将金丝雀放置到
  任务组和那些金丝雀被认为是健康的。当任务组
  提升后，剩余分配的滚动升级畅通无阻。如果
  发现 Canary 运行状况不佳，使用
  “nomad 部署失败”命令，可以通过提交来转发作业失败
  新版本或通过使用恢复到旧版本向后失败
  “游牧作业还原”命令。

  启用 ACL 后，此命令需要一个带有“提交作业”的令牌，
  以及作业命名空间的“读取作业”功能。“列出工作”
  需要使用作业前缀而不是
  确切的作业 ID。

Promote Options:

  -group
    组可以多次指定，并用于提升该特定
    群。如果未指定特定组，则会升级所有组。

  -detach
    立即返回，而不是进入监视模式。部署后
    简历，评估ID将打印到屏幕上，可以使用
    以使用 eval-status 命令检查评估。

  -verbose
    显示完整信息。



```

#### nomad job restart

```shell
Usage: nomad job restart [options] <job>

重新启动或重新计划特定作业的分配。

重新启动作业将调用“重新启动分配”API 端点以重新启动
  分配内的任务，因此分配本身不会被修改，而是
  而是就地重新启动。

重新计划作业使用“停止分配”API 端点来停止
  分配并触发 Nomad 调度程序来计算新的展示位置。这
  可能会导致在与
  原件。

此命令可以批量运行，并等待直到所有重新启动或
  重新安排的分配将再次运行，然后继续下一个分配
  批。也可以指定额外的等待时间
  批次。

分配可以就地重新启动或重新计划。重新启动时
  就地命令可能针对分配中的特定任务，重新启动
  仅当前正在运行的任务，或重新启动所有任务，甚至是那些任务
  已经运行了。分配也可以按组作为目标。当两者兼而有之时
  组和任务仅定义用于分配这些的任务
  组将重新启动。

重新调度时，当前分配停止触发游牧民族
  调度程序，用于创建可能放置在不同位置的替换分配
  客户。该命令将等待，直到新分配具有客户端状态
  “就绪”，然后再继续处理剩余批次。服务运行状况检查
  不考虑在内。

默认情况下，该命令使用一个命令就地重新启动所有正在运行的任务
  每批分配。


启用 ACL 后，此命令需要一个令牌，其中包含
  作业命名空间的“分配生命周期”和“读取作业”功能。这
  需要“列出作业”功能才能使用作业前缀运行命令
  而不是确切的作业 ID。


Restart Options:

  -all-tasks
    如果设置，将重新启动分配中的所有任务，即使是那些
    已经运行，例如非挎斗任务。任务将在下面重新启动
    他们的生命周期顺序。此选项不能与“-task”一起使用。

  -batch-size=<n|n%>
    要一次重新启动的分配数。它可以定义为百分比
    当前正在运行的分配数的值。百分比值为
    向上舍入以增加并行度。默认值为 1。

  -batch-wait=<duration|'ask'>
    在重新启动批处理之间等待的时间。如果设置为“询问”，命令将停止
    在批处理之间，并等待用户输入有关如何继续操作。如果答案
    是所有剩余批次将使用此新值的持续时间。违约
    到 0。

  -group=<group-name>
    仅重新启动给定组的分配。可以指定多个
    次。如果未设置组，则会重新启动作业的所有分配。

  -no-shutdown-delay
    忽略组和任务“shutdown_delay”配置，因此没有
    服务取消注册与任务关闭或重新启动之间的延迟。注意
    使用此标志将导致与
    正在重新启动分配。

  -on-error=<'ask'|'fail'>
    确定在重新启动期间发生错误时要执行的操作
    批。如果“ask”命令停止并等待用户确认如何
    进行。如果“失败”，命令将立即退出。默认为“询问”。

  -reschedule
    如果设置，分配将停止并重新计划，而不是重新启动
    就地。由于未修改组，因此重新启动不会创建新的
    部署，因此在“更新”块中定义的值，例如
    “max_parallel”，不考虑在内。此选项不能与
    “-任务”。

  -task=<task-name>
    指定要重新启动的任务。可以多次指定。如果组是
    还指定任务必须至少存在于其中一个中。如果没有任务
    仅设置重新启动当前正在运行的任务。例如
    除非“-all-tasks”为
    改用。此选项不能与“-所有任务”或
    “-重新安排”。

  -yes
    对提示自动“是”。如果设置，命令将自动重新启动
    多区域作业仅在命令目标区域中，忽略批处理
    错误，并自动继续处理剩余批次，而无需
    等待。使用“出错时”和“批处理等待”来调整这些行为。

  -verbose
    显示完整信息。


```



#### nomad job revert

```shell
Usage: nomad job revert [options] <job> <version>

  还原用于将作业还原到作业的先前版本。可用的
  可以使用“nomad 作业历史记录”命令找到要还原的版本。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  作业命名空间的功能。需要“列表作业”功能来
  使用作业前缀而不是确切的作业 ID 运行命令。“读取作业”
  当 -detach 为
  未使用。
Revert Options:

  -detach
    立即返回，而不是进入监视模式。作业还原后，
    评估 ID 将打印到屏幕上，可用于
    使用 eval-status 命令检查评估。

  -consul-token
   用于验证调用方是否有权访问服务的 Consul 令牌
   在作业的目标版本中关联的标识策略。

  -vault-token
   用于验证调用方是否有权访问保管库的保管库令牌
   作业的目标版本中的策略。

  -verbose
    显示完整信息。


```



#### nomad job run

```shell
Usage: nomad job run [options] <path>
Alias: nomad run

  开始使用新作业或更新现有作业
  规范位于 <path>。这是主命令
  曾经与游牧民族互动。

如果提供的路径为“-”，则从标准输入读取作业文件。否则
  它从提供的路径的文件读取或下载，并且
  从指定的 URL 读取。

作业提交成功后，此命令将立即
  进入交互式监视器。这对于观看游牧民族的
  内部人员做出调度决策并放置提交的工作
  到节点上。工作安置完成后，监视器将结束。它
  使用 Ctrl+C 提前退出监视器是安全的。

成功提交作业和计划后，退出代码 0 将为
  返回。如果遇到工作安置问题
  （无法满足的约束、资源耗尽等），然后
  退出代码将为 2。任何其他错误，包括客户端连接
  问题或内部错误由退出代码 1 指示。

  如果作业指定了区域，则 -region 标志和NOMAD_REGION
  环境变量将被覆盖，并使用作业的区域。

run 命令将根据以下内容设置作业的consul_token
  优先级，从最高到最低：-consul-token 标志，即
  $CONSUL_HTTP_TOKEN 环境变量，最后是作业文件中的值。

run 命令将根据以下内容设置作业的vault_token
  优先级，从最高到最低：-vault-token 标志，即
  $VAULT_TOKEN 环境变量，最后是作业文件中的值。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  作业命名空间的功能。装载 CSI 卷的作业需要
  具有“CSI 装载卷”功能的令牌，适用于卷的
  命名空间。装载主机卷的作业需要具有
  该卷的“host_volume”功能。
Run Options:

  -check-index
    如果设置，则仅当通过时，才注册或更新作业
    作业修改索引与服务器端版本匹配。如果校验索引值
    零被传递，仅当作业尚不存在时才注册。如果
    传递非零值，它确保作业是从
    已知状态。此标志最常与计划结合使用
    命令。

  -detach
    立即返回，而不是进入监视模式。作业提交后，
    评估 ID 将打印到屏幕上，可用于
    使用 eval-status 命令检查评估。

  -eval-priority
    覆盖由此作业生成的评估的优先级
    提交。默认情况下，此值设置为作业的优先级。

  -json
    将作业文件解析为 JSON。如果外部对象具有作业字段，例如
    从“Nomad Job Inspect”或“Nomad Run -output”中，该字段的值为
    用作作业。

  -hcl1
    将作业文件解析为 HCLv1。优先于 “-hcl2-strict”。

  -hcl2-strict
    是否应从 HCL2 解析器生成错误，其中变量
    已提供未在根变量中定义的。违约
    为 true，但如果还定义了“-hcl1”，则忽略。

  -output
    输出将提交到 HTTP API 而不提交的 JSON
    工作。

  -policy-override
    设置标志以强制覆盖任何软性强制 Sentinel 策略。

  -preserve-counts
    如果设置，则在更新作业时将保留现有任务组计数。

  -consul-token
    如果设置，则传递的 Consul 令牌将存储在作业中，然后再发送到
    游牧服务器。这允许传递 Consul 令牌而不将其存储在
    作业文件。这将覆盖在 $CONSUL_HTTP_TOKEN 环境中找到的令牌
    变量和在作业中找到的变量。

  -vault-token
    用于验证提交作业的用户是否有权运行作业
    根据其保险柜政策。如果保管库
    块allow_unauthenticated在 Nomad 服务器配置中被禁用。
    如果设置了 -vault-token 标志，则传递的 Vault 令牌将添加到作业规范中
    在发送到Nomad服务器之前。这允许传递保管库令牌
    而不将其存储在作业文件中。这将覆盖在
    作业文件中的 $VAULT_TOKEN 环境变量和vault_token字段。
    此令牌在验证后从作业中清除，不能在
    作业执行环境。在作业中模板化时使用保管库块
    使用保管库令牌。

  -vault-namespace
    如果设置，则传递的 Vault 命名空间将存储在作业中，然后再发送到
    游牧服务器。

  -var 'key=value'
    模板的变量，可以多次使用。

  -var-file=path
    包含用户变量的 HCL2 文件的路径。

  -verbose
    显示完整信息。


```



#### nomad job scale

```shell
Usage: nomad job scale [options] <job> [<group>] <count>

  通过更改作业组中的计数来执行扩展操作。

作业提交成功后，此命令将立即
  进入交互式监视器。这对于观看游牧民族的
  内部人员做出调度决策并放置提交的工作
  到节点上。工作安置完成后，监视器将结束。它
  使用 Ctrl+C 提前退出监视器是安全的。

启用 ACL 后，此命令需要一个令牌，其中包含
  “读取作业缩放”和“缩放作业”或“提交作业”功能
  作为作业的命名空间。需要“列表作业”功能才能运行
  命令，而不是作业前缀而不是确切的作业 ID。“读取作业”
  当 -detach 为
  未使用。

Scale Options:

  -detach
    立即返回，而不是进入监视模式。作业扩展后，
    评估 ID 将打印到屏幕上，可用于
    使用 eval-status 命令检查评估。

  -verbose
    显示完整信息。

```



#### nomad job scaling-events

 ```shell
Usage: nomad job scaling-events [options] <args>

  列出指定作业的扩展事件。

启用 ACL 后，此命令需要一个令牌，其中包含
  作业命名空间的“读取作业”或“读取作业缩放”功能。这
  需要“列出作业”功能才能使用作业前缀运行命令
  而不是确切的作业 ID。
  
Scaling-Events Options:

  -verbose
    显示完整信息。



 ```





#### nomad job status

```shell
Usage: nomad status [options] <job>

  显示有关作业的状态信息。如果未提供作业 ID，则列出所有
  将显示已知作业。

启用 ACL 后，此命令需要具有“读取作业”的令牌
  作业命名空间的功能。需要“列表作业”功能来
  使用作业前缀而不是确切的作业 ID 运行命令。

Status Options:

  -short
    显示短输出。仅在单个作业时使用
    已查询，并删除有关分配的详细信息。

  -evals
    显示与作业关联的评估。

  -all-allocs
    显示与作业 ID 匹配的所有分配，包括来自较旧作业 ID 的分配
    作业的实例。

  -verbose
    显示完整信息。


```



#### nomad job stop

```shell
Usage: nomad job stop [options] <job>
Alias: nomad stop

  
停止现有作业。此命令用于向分配发出关闭信号
  对于给定的作业 ID，向下。成功注销后，互动式
  监视器会话将在作业展开其时开始显示日志行
  分配和完成关闭。退出显示器是安全的
  早期使用 Ctrl+C。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  以及作业命名空间的“读取作业”功能。“列出工作”
  需要功能才能使用作业前缀而不是精确前缀运行命令
  作业 ID。

Stop Options:

  -detach
    立即返回，而不是进入监视模式。之后
    提交取消注册命令，将新的评估 ID 打印到
    屏幕，可用于使用评估状态检查评估
    命令。

  -eval-priority
    覆盖由此作业生成的评估的优先级
    注销。默认情况下，此值设置为作业的优先级。

  -global
    停止所有区域中的多区域作业。默认情况下，作业停止将停止
    一次只能有一个区域。对于单区域作业，已忽略。

  -no-shutdown-delay
        忽略组和任务shutdown_delay配置，以便没有
    服务取消注册和任务关闭之间的延迟。请注意，使用
    此标志将导致与分配的网络连接失败
    被阻止。

  -purge
    清除用于停止作业并将其从系统中清除。如果未设置，则
    作业仍将是可查询的，并且将由垃圾回收器清除。

  -yes
    对提示自动“是”。

  -verbose
    显示完整信息。


```



#### nomad job validate

```shell
Usage: nomad job validate [options] <path>
Alias: nomad validate

检查给定的 HCL 作业文件是否具有有效的规范。这可用于
  检查作业是否存在任何语法错误或验证问题。

如果提供的路径为“-”，则从标准输入读取作业文件。否则
  它从提供的路径的文件读取或下载，并且
  从指定的 URL 读取。

run 命令将根据以下内容设置作业的vault_token
  优先级，从最高到最低：-vault-token 标志，即
  $VAULT_TOKEN 环境变量，最后是作业文件中的值。

启用 ACL 后，此命令需要具有“读取作业”的令牌
  作业命名空间的功能。
  
alidate Options:

  -json
    将作业文件解析为 JSON。如果外部对象具有作业字段，例如
    从“Nomad Job Inspect”或“Nomad Run -output”中，该字段的值为
    用作作业。

  -hcl1
    将作业文件解析为 HCLv1。优先于 “-hcl2-strict”。
    
  -hcl2-strict
    是否应从 HCL2 解析器生成错误，其中变量
    已提供未在根变量中定义的。违约
    为 true，但如果还定义了“-hcl1”，则忽略。

  -vault-token
    用于验证提交作业的用户是否有权运行作业
    根据其保险柜政策。如果保管库
    块allow_unauthenticated在 Nomad 服务器配置中被禁用。
    如果设置了 -vault-token 标志，则传递的 Vault 令牌将添加到作业规范中
    在发送到Nomad服务器之前。这允许传递保管库令牌
    而不将其存储在作业文件中。这将覆盖在
    作业文件中的 $VAULT_TOKEN 环境变量和vault_token字段。
    此令牌在验证后从作业中清除，不能在
    作业执行环境。在作业中模板化时使用保管库块
    使用保管库令牌。

  -vault-namespace
    如果设置，则传递的 Vault 命名空间将存储在作业中，然后再发送到
    游牧服务器。

  -var 'key=value'
    模板的变量，可以多次使用。

  -var-file=path
    包含用户变量的 HCL2 文件的路径。




```



### alloc

```shell
Usage: nomad alloc <subcommand> [options] [args]

  此命令对用于与分配交互的子命令进行分组。用户可以
  检查状态，检查文件系统或分配日志。

  检查分配状态：

      $ nomad alloc status <alloc-id>

  流式传输任务的日志：

      $ nomad alloc logs -f <alloc-id> <task>

  有关详细的使用信息，请参阅各个子命令帮助。

Subcommands:
    checks     输出服务运行状况检查状态信息。
    exec       在任务中执行命令
    fs         检查分配目录的内容
    logs       流式传输任务的日志。
    restart    重新启动正在运行的分配
    signal     发出正在运行的分配信号
    status     显示分配状态信息和元数据
    stop       停止并重新安排正在运行的分配

```

#### nomad  alloc checks

```shell
Usage: nomad alloc checks [options] <allocation>
Alias: nomad checks


输出分配中服务的最新运行状况检查状态信息
  使用 Nomad 服务发现提供程序。
  
Checks Specific Options:

  -verbose
    显示完整信息。

  -json
    以JSON格式输出最新的健康检查状态信息。

  -t
    使用 Go 模板格式化和显示最新的运行状况检查状态信息。
```



#### nomad  alloc exec

```shell
Usage: nomad alloc exec [options] <allocation> <command>

  在给定分配和任务的环境中运行命令。

启用 ACL 后，此命令需要一个带有“alloc-exec”的令牌，
  分配命名空间的“读取作业”和“列出作业”功能。如果
  任务驱动程序没有文件系统隔离（与“raw_exec”一样），
  此命令需要“alloc-node-exec”、“read-job”和“list-jobs”
  分配命名空间的功能。
  
Exec Specific Options:

  -task <task-name>
    将任务设置为 exec 命令

  -job
    使用指定作业 ID 中的随机分配。

  -i
    将 stdin 传递给容器，默认为 true。 传递 -i=false 以禁用。

  -t
    分配伪 tty，如果检测到 stdin 是 tty 会话，则默认为 true。
    传递 -t=false 以显式禁用。

  -e <escape_char>
    为带有 pty 的会话设置转义字符（默认值：“~”）。 逃亡
    字符仅在行首识别。 转义字符
    后跟一个点 （'.） 表示关闭连接。 将字符设置为
    “none”禁用任何转义并使会话完全透明。



```



#### nomad  alloc fs

```shell
Usage: nomad alloc fs [options] <allocation> <path>
Alias: nomad fs

  FS 显示传递的分配目录的内容
  分配，或在给定路径处显示文件。路径相对于
  分配目录的根，如果未指定，则默认为根。

  启用 ACL 后，此命令需要一个带有“read-fs”的令牌，
  分配命名空间的“读取作业”和“列出作业”功能。

FS Specific Options:

  -H
    机器友好的输出。

  -verbose
    显示完整信息。

  -job <job-id>
    使用指定作业 ID 中的随机分配。

  -stat
    显示文件统计信息，而不是显示文件或列出目录。
    
  -f
    使输出在到达文件末尾时不会停止，而是
    等待其他输出。

  -tail
    显示文件内容，并相对于文件末尾偏移。如果没有
    给出偏移量，-n 默认为 10。

  -n
    以相对于尾部的最大行数设置尾部位置
    的文件。

  -c
    设置相对于文件末尾的尾部位置（以字节数为单位）。
```



#### nomad  alloc logs

```shell
Usage: nomad alloc logs [options] <allocation> <task>
Alias: nomad logs

  流式传输给定分配和任务的 stdout/stderr。

  启用 ACL 后，此命令需要一个带有“读取日志”的令牌，
  分配命名空间的“读取作业”和“列出作业”功能。

Logs Specific Options:

  -stdout
    显示标准输出日志。这在所有命令中用作默认值
    除非使用“-f”标志，其中标准输出和标准derr都用作
    违约。

  -stderr
    显示标准日志。

  -verbose
    显示完整信息。


  -task <task-name>
    设置任务以查看日志。如果任务名称同时带有参数
        和“-任务”选项，优先选择“-任务”选项。

  -job <job-id>
    使用指定作业 ID 中的随机分配。


  -f
    使输出在到达日志末尾时不会停止，但
    而是等待额外的输出。未提供其他标志时
    除了可选的“-job”和“-task”之外，标准输出和标准输出日志都将是
    跟着。

  -tail
    显示日志内容，并相对于日志末尾进行偏移。如果没有
    给出偏移量，-n 默认为 10。

  -n
    以相对于尾部的最大行数设置尾部位置
    的日志。

  -c
    设置相对于日志末尾的尾部位置（以字节数为单位）。

  请注意，-no-color 选项适用于 Nomad 自己的输出。如果任务的
  日志包括颜色代码的终端转义序列，Nomad不会
  删除它们。


```



#### nomad  alloc restart

```shell
Usage: nomad alloc restart [options] <allocation> <task>

  重新启动现有分配。此命令用于重新启动特定的分配
  及其任务。如果未提供任何任务，则分配的所有任务
  当前正在运行将重新启动。

使用选项“-all-tasks”重新启动已运行的任务，例如
  非挎斗预启动和启动后任务。

启用 ACL 后，此命令需要一个令牌，其中包含
  “分配生命周期”、“读取作业”和“列出作业”功能
  分配的命名空间。

Restart Specific Options:

  -all-tasks
    如果设置，分配中的所有任务都将重新启动，即使是那些
    已经跑了。此选项不能与“-task”<task>或“”一起使用
    论点。

  -task <task-name>
    指定要重新启动的单个任务。如果任务名称同时带有
    参数和“-任务”选项，优先选择“-任务”选项。
    此选项不能与“-所有任务”一起使用。

  -verbose
    显示完整信息。


```



#### nomad  alloc signal

```shell
Usage: nomad alloc signal [options] <allocation> <task>

  发出现有分配的信号。此命令用于发出特定分配的信号
  及其子任务。如果未提供任务，则所有分配子任务
  将接收信号。

启用 ACL 后，此命令需要一个令牌，其中包含
  “分配生命周期”、“读取作业”和“列出作业”功能
  分配的命名空间。

Signal Specific Options:

  -s
    指定所选任务应接收的信号。默认为 SIGKILL。

  -task <task-name>
        指定将接收信号的单个任务。如果给定了任务名称
        使用参数和“-task”选项，优先选择“-task”
        选择。

  -verbose
    显示完整信息。

```



#### nomad  alloc status

```shell
Usage: nomad alloc status [options] <allocation>

  显示有关现有分配及其任务的信息。此命令可以
  用于检查分配的当前状态，包括其运行情况
  内部报告的状态、元数据和详细故障消息
  子系统。

启用 ACL 后，此命令需要一个带有“读取作业”和
  分配命名空间的“列表作业”功能。

Alloc Status Options:

  -short
    显示短输出。仅显示最近的任务事件。

  -stats
    显示详细的资源使用情况统计信息。

  -verbose
    显示完整信息。

  -json
    以 JSON 格式输出分配。

  -t
    使用 Go 模板格式化和显示分配。



```



#### nomad  alloc stop

```shell
Alias: nomad stop

  停止现有分配。此命令用于发出特定分配的信号
  以关闭。当分配被关闭时，它将
  改期。交互式监视会话会将日志行显示为
  分配完成关闭。提前退出显示器是安全的
  Ctrl-c.

启用 ACL 后，此命令需要一个令牌，其中包含
  “分配生命周期”、“读取作业”和“列出作业”功能
  分配的命名空间。
  
Stop Specific Options:

  -detach
    立即返回，而不是进入监视模式。之后
    提交停止命令，将新的评估 ID 打印到
    屏幕，可用于使用
    评估状态命令。

  -no-shutdown-delay
    忽略组和任务shutdown_delay配置，因此没有
    服务取消注册和任务关闭之间的延迟。请注意，使用
    此标志将导致与分配的网络连接失败
    被阻止。

  -verbose
    显示完整信息。

```

### status

```shell
Usage: nomad status [options] <identifier>

  显示任何给定资源的状态输出。该命令将
  检测要查询的资源类型并显示相应的
  状态输出。
  
  
Status Options:
  -verbose
    显示完整信息。


```



### stop

```shell
Usage: nomad job stop [options] <job>
Alias: nomad stop

停止现有作业。此命令用于向分配发出关闭信号
  对于给定的作业 ID，向下。成功注销后，互动式
  监视器会话将在作业展开其时开始显示日志行
  分配和完成关闭。退出显示器是安全的
  早期使用 Ctrl+C。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  以及作业命名空间的“读取作业”功能。“列出工作”
  需要功能才能使用作业前缀而不是精确前缀运行命令
  作业 ID。


Stop Options:

  -detach
    立即返回，而不是进入监视模式。之后
    提交取消注册命令，将新的评估 ID 打印到
    屏幕，可用于使用评估状态检查评估
    命令。

  -eval-priority
    覆盖由此作业生成的评估的优先级
    注销。默认情况下，此值设置为作业的优先级。

  -global
    停止所有区域中的多区域作业。默认情况下，作业停止将停止
    一次只能有一个区域。对于单区域作业，已忽略。

  -no-shutdown-delay
        忽略组和任务shutdown_delay配置，以便没有
    服务取消注册和任务关闭之间的延迟。请注意，使用
    此标志将导致与分配的网络连接失败
    被阻止。

  -purge
    清除用于停止作业并将其从系统中清除。如果未设置，则
    作业仍将是可查询的，并且将由垃圾回收器清除。

  -yes
    对提示自动“是”。

  -verbose
    显示完整信息。


```



### run

```shell
Usage: nomad job run [options] <path>
Alias: nomad run

  开始使用新作业或更新现有作业
  规范位于 <path>。这是主命令
  曾经与游牧民族互动。

如果提供的路径为“-”，则从标准输入读取作业文件。否则
  它从提供的路径的文件读取或下载，并且
  从指定的 URL 读取。

  作业提交成功后，此命令将立即
  进入交互式监视器。这对于观看游牧民族的
  内部人员做出调度决策并放置提交的工作
  到节点上。工作安置完成后，监视器将结束。它
  使用 Ctrl+C 提前退出监视器是安全的。

成功提交作业和计划后，退出代码 0 将为
  返回。如果遇到工作安置问题
  （无法满足的约束、资源耗尽等），然后
  退出代码将为 2。任何其他错误，包括客户端连接
  问题或内部错误由退出代码 1 指示。

  如果作业指定了区域，则 -region 标志和NOMAD_REGION
  环境变量将被覆盖，并使用作业的区域。

run 命令将根据以下内容设置作业的consul_token
  优先级，从最高到最低：-consul-token 标志，即
  $CONSUL_HTTP_TOKEN 环境变量，最后是作业文件中的值。

run 命令将根据以下内容设置作业的vault_token
  优先级，从最高到最低：-vault-token 标志，即
  $VAULT_TOKEN 环境变量，最后是作业文件中的值。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  作业命名空间的功能。装载 CSI 卷的作业需要
  具有“CSI 装载卷”功能的令牌，适用于卷的
  命名空间。装载主机卷的作业需要具有
  该卷的“host_volume”功能。


Run Options:

  -check-index
    如果设置，则仅当通过时，才注册或更新作业
    作业修改索引与服务器端版本匹配。如果校验索引值
    零被传递，仅当作业尚不存在时才注册。如果
    传递非零值，它确保作业是从
    已知状态。此标志最常与计划结合使用
    命令。

  -detach
    立即返回，而不是进入监视模式。作业提交后，
    评估 ID 将打印到屏幕上，可用于
    使用 eval-status 命令检查评估。
    
  -eval-priority
    覆盖由此作业生成的评估的优先级
    提交。默认情况下，此值设置为作业的优先级。

  -json
    将作业文件解析为 JSON。如果外部对象具有作业字段，例如
    从“Nomad Job Inspect”或“Nomad Run -output”中，该字段的值为
    用作作业。

  -hcl1
    将作业文件解析为 HCLv1。优先于 “-hcl2-strict”。

  -hcl2-strict
    是否应从 HCL2 解析器生成错误，其中变量
    已提供未在根变量中定义的。违约
    为 true，但如果还定义了“-hcl1”，则忽略。

  -output
    输出将提交到 HTTP API 而不提交的 JSON
    工作。

  -policy-override
    设置标志以强制覆盖任何软性强制 Sentinel 策略。

  -preserve-counts
    如果设置，则在更新作业时将保留现有任务组计数。

  -consul-token
    如果设置，则传递的 Consul 令牌将存储在作业中，然后再发送到
    游牧服务器。这允许传递 Consul 令牌而不将其存储在
    作业文件。这将覆盖在 $CONSUL_HTTP_TOKEN 环境中找到的令牌
    变量和在作业中找到的变量。

  -vault-token
    用于验证提交作业的用户是否有权运行作业
    根据其保险柜政策。如果保管库
    块allow_unauthenticated在 Nomad 服务器配置中被禁用。
    如果设置了 -vault-token 标志，则传递的 Vault 令牌将添加到作业规范中
    在发送到Nomad服务器之前。这允许传递保管库令牌
    而不将其存储在作业文件中。这将覆盖在
    作业文件中的 $VAULT_TOKEN 环境变量和vault_token字段。
    此令牌在验证后从作业中清除，不能在
    作业执行环境。在作业中模板化时使用保管库块
    使用保管库令牌。

  -vault-namespace
    如果设置，则传递的 Vault 命名空间将存储在作业中，然后再发送到
    游牧服务器。

  -var 'key=value'
    模板的变量，可以多次使用。

  -var-file=path
    包含用户变量的 HCL2 文件的路径。

  -verbose
    显示完整信息。


```





    acl                 与ACL策略和令牌交互
    agent-info          显示有关本地代理的状态信息
    config              与配置交互
    deployment          与部署交互
    eval                与评估互动
    exec                在任务中执行命令
    fmt                 将Nomad配置和作业文件重写为规范格式
    license             与Nomad Enterprise License交互
    login               使用身份验证方法登录Nomad
    monitor             来自Nomad代理的流日志
    namespace           与命名空间交互
    operator            为Nomad运营商提供集群级工具
    plugin              检查插件
    quota               与配额交互
    recommendation      与Nomad推荐端点交互
    scaling             与Nomad缩放端点交互
    sentinel            与Sentinel政策互动
    server              与服务器交互
    service             与注册服务互动
    system              与系统API交互
    tls                 为Nomad生成自签名TLS证书
    ui                  打开Nomad Web UI
    var                 与变量交互
    version             打印Nomad版本
    volume              与卷交互
### acl

```shell
Usage: nomad acl <subcommand> [options] [args]

  此命令对用于与 ACL 策略和令牌交互的子命令进行分组。
  用户可以引导Nomad的ACL系统，创建限制访问的策略，
  并从这些策略生成令牌。

  Bootstrap ACLs:

      $ nomad acl bootstrap

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    auth-method     与 ACL 身份验证方法交互
    binding-rule    与 ACL 绑定规则交互
    bootstrap       引导 ACL 系统获取初始令牌
    policy          与 ACL 策略交互
    role            与 ACL 角色交互
    token           与 ACL 令牌交互

```



#### nomad acl  auth-method

```shell

Usage: nomad acl auth-method <subcommand> [options] [args]

  此命令对用于与 ACL 身份验证方法交互的子命令进行分组。


  创建 ACL 身份验证方法：:

      $ nomad acl auth-method create -name="name" -type="OIDC" -max-token-ttl="3600s"

  列出所有 ACL 身份验证方法：

      $ nomad acl auth-method list

  查找特定的 ACL 身份验证方法：

      $ nomad acl auth-method info <acl_auth_method_name>

  更新 ACL 身份验证方法：

      $ nomad acl auth-method update -type="updated-type" <acl_auth_method_name>

  删除 ACL 身份验证方法：

      $ nomad acl auth-method delete <acl_auth_method_name>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    create    创建新的 ACL 身份验证方法
    delete    删除现有 ACL 身份验证方法
    info      获取有关现有 ACL 身份验证方法的信息
    list      列出 ACL 身份验证方法
    update    更新现有 ACL 身份验证方法

```



##### nomad acl  auth-method create

```shell
Usage: nomad acl auth-method create [options]

  创建用于创建新的 ACL 身份验证方法。使用需要管理令牌。

ACL Auth Method Create Options:

  -name
    设置 ACL 身份验证方法的人类可读名称。名称必须是
    介于 1-128 个字符之间，是必需参数。

  -type
    设置身份验证方法的类型。支持的类型是“OIDC”和“JWT”。

  -max-token-ttl
    设置此身份验证方法创建的所有令牌应为的持续时间
    有效。

  -token-locality
    定义此身份验证方法应生成的令牌类型。这可以是
    “本地”或“全球”。


  -default
    指定是否应将此身份验证方法视为默认身份验证方法
    案例 没有为登录命令显式指定身份验证方法。

  -config
    JSON 格式的身份验证方法配置。可以以“@”为前缀
    指示该值是要从中加载配置的文件路径。“-”也可以
    表示配置在标准定上可用。


  -json
    以 JSON 格式输出 ACL 身份验证方法。

  -t
    使用 Go 模板格式化并显示 ACL 身份验证方法。

```



##### nomad acl  auth-method delete

```shell
Usage: nomad acl auth-method delete <acl_method_name>

  删除用于删除现有的 ACL 身份验证方法。使用需要
  管理令牌。

```

##### nomad acl  auth-method info

```shell
Usage: nomad acl auth-method info [options] <acl_method_name>

  信息用于获取有关现有 ACL 身份验证方法的信息。需要
  管理令牌。
ACL Info Options:

  -json
    以 JSON 格式输出 ACL 身份验证方法。

  -t
    使用 Go 模板格式化和显示 ACL 身份验证方法。

```

##### nomad acl  auth-method list

```shell
Usage: nomad acl auth-method list [options]

  列表用于列出现有的 ACL 身份验证方法。


ACL List Options:

  -json
    以 JSON 格式输出 ACL 身份验证方法。

  -t
    使用 Go 模板格式化和显示 ACL 身份验证方法。

```

##### nomad acl  auth-method update

```shell
Usage: nomad acl auth-method update [options] <acl_auth_method_name>

  更新用于更新 ACL 身份验证方法。使用需要管理令牌。

ACL Auth Method Update Options:

  -type
    更新身份验证方法的类型。支持的类型是“OIDC”和“JWT”。


  -max-token-ttl
    更新此身份验证方法创建的所有令牌应为的持续时间
    有效。

  -token-locality
    更新此身份验证方法应生成的令牌类型。这可以是
    “本地”或“全球”。

  -default
    指定是否应将此身份验证方法视为默认身份验证方法
    案例 没有为登录命令显式指定身份验证方法。

  -config
    更新身份验证方法配置（JSON 格式）。可以以
    “@”表示该值是要从中加载配置的文件路径。'-'
    也可以给出以指示配置在标准定上可用。

  -json
    以 JSON 格式输出 ACL 身份验证方法。

  -t
    使用 Go 模板格式化和显示 ACL 身份验证方法。



```

#### nomad acl  binding-rule

```shell
Usage: nomad acl binding-rule <subcommand> [options] [args]

此命令对用于与 ACL 绑定规则交互的子命令进行分组。
  Nomad的ACL系统可用于控制对数据和API的访问。对于完整的
  指南请参阅：https://www.nomadproject.io/guides/acl.html

  创建 ACL 绑定规则:

      $ nomad acl binding-rule create \
          -auth-method=auth0 \
          -selector="nomad-engineering in list.groups" \
          -bind-type=role \
          -bind-name="custer-admin" \

  列出所有 ACL 绑定规则:

      $ nomad acl binding-rule list

  查找特定的 ACL 绑定规则:

      $ nomad acl binding-rule info <acl_binding_rule_id>

  更新 ACL 绑定规则:

      $ nomad acl binding-rule update \
          -description="nomad engineering team" \
          <acl_binding_rule_id>

  删除 ACL 绑定规则:

      $ nomad acl binding-rule delete <acl_binding_rule_id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    create    Create a new ACL binding rule
    delete    Delete an existing ACL binding rule
    info      Fetch information on an existing ACL binding rule
    list      List ACL binding rules
    update    Update an existing ACL binding rule

```



#### nomad acl bootstrap

```shell
Usage: nomad acl bootstrap [options]

  引导程序用于引导 ACL 系统并获取初始令牌。

Bootstrap Options:


  -json
    以 JSON 格式输出 ACL 身份验证方法。

  -t
    使用 Go 模板格式化和显示 ACL 身份验证方法。



```



#### nomad acl  policy

```shell
Usage: nomad acl policy <subcommand> [options] [args]

此命令对用于与 ACL 策略交互的子命令进行分组。游牧民族的ACL
  系统可用于控制对数据和 API 的访问。ACL 策略允许
  要授予或列入允许列表的功能或操作集。有关完整指南
  请参阅：https://www.nomadproject.io/guides/acl.html

  Create an ACL policy:

      $ nomad acl policy apply <name> <policy-file>

  List ACL policies:

      $ nomad acl policy list

  Inspect an ACL policy:

      $ nomad acl policy info <policy>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    apply     Create or update an ACL policy
    delete    Delete an existing ACL policy
    info      Fetch info on an existing ACL policy
    list      List ACL policies

```



#### nomad acl  role

```shell
Usage: nomad acl role <subcommand> [options] [args]

此命令对用于与 ACL 角色交互的子命令进行分组。游牧民族的ACL
  系统可用于控制对数据和 API 的访问。ACL 角色是
  与一个或多个授予特定功能的 ACL 策略相关联。
  有关完整指南，请参阅：https://www.nomadproject.io/guides/acl.html

  Create an ACL role:

      $ nomad acl role create -name="name" -policy-name="policy-name"

  List all ACL roles:

      $ nomad acl role list

  Lookup a specific ACL role:

      $ nomad acl role info <acl_role_id>

  Update an ACL role:

      $ nomad acl role update -name="updated-name" <acl_role_id>

  Delete an ACL role:

      $ nomad acl role delete <acl_role_id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    create    Create a new ACL role
    delete    Delete an existing ACL role
    info      Fetch information on an existing ACL role
    list      List ACL roles
    update    Update an existing ACL role

```



#### nomad acl  token

```shell
Usage: nomad acl token <subcommand> [options] [args]


此命令对用于与 ACL 令牌交互的子命令进行分组。游牧民族的ACL
  系统可用于控制对数据和 API 的访问。ACL 令牌是
  与一个或多个授予特定功能的 ACL 策略相关联。
  有关完整指南，请参阅：https://www.nomadproject.io/guides/acl.html

  Create an ACL token:

      $ nomad acl token create -name "my-token" -policy foo -policy bar

  Lookup a token and display its associated policies:

      $ nomad acl policy info <token_accessor_id>

  Revoke an ACL token:

      $ nomad acl policy delete <token_accessor_id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    create    Create a new ACL token
    delete    Delete an existing ACL token
    info      Fetch information on an existing ACL token
    list      List ACL tokens
    self      Lookup self ACL token
    update    Update an existing ACL token

```

### agent-info

```shell
Usage: nomad agent-info [options]

显示有关本地代理的状态信息。

启用 ACL 后，此命令需要一个带有“代理：读取”的令牌
  能力。
  
 Agent Info Options: 
  -json
    以 JSON 格式输出 ACL 身份验证方法。

  -t
    使用 Go 模板格式化和显示 ACL 身份验证方法。

```



### config

```shell
Usage: nomad config <subcommand> [options] [args]

此命令对用于与配置交互的子命令进行分组。
  用户可以验证 Nomad 代理的配置。

  Validate configuration:

      $ nomad config validate <config_path> [<config_path>...]

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    validate    验证配置文件/目录

```

#### nomad config validate

```shell
Usage: nomad config validate <config_path> [<config_path...>]

  对一组 Nomad 配置文件执行验证。这很有用
  在不启动代理的情况下测试 Nomad 配置。

接受单个配置文件或目录的路径
  用于配置 Nomad 代理的配置文件。此选项可能会
  多次指定。如果使用多个配置文件，则
  每个值将合并在一起。在合并过程中，值来自
  稍后在列表中找到的文件将合并到以前的值上
  解析的文件。

此命令无法对部分配置片段进行操作，因为
  这些不会通过完整的代理验证。此命令不
  需要 ACL 令牌。

如果配置有效，则返回 0;如果存在问题，则返回 1。

```

### deployment

```shell
Usage: nomad deployment <subcommand> [options] [args]

  此命令对用于与部署交互的子命令进行分组。部署
  用于管理两个版本的 Nomad 作业之间的转换。用户
  可以检查正在进行的部署，提升金丝雀分配，强制失败
  部署等。

  Examine a deployments status:

      $ nomad deployment status <deployment-id>

  提升金丝雀以允许在
  滚动部署方式：

      $ nomad deployment promote <deployment-id>

  将部署标记为失败。这将阻止放置新的分配
  如果作业的升级块指定了auto_revert，则会导致作业
  恢复到作业的上一个稳定版本：

      $ nomad deployment fail <deployment-id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    fail       手动使部署失败
    list       列出所有部署
    pause      暂停部署
    promote    在部署中升级金丝雀
    resume     恢复暂停的部署
    status     显示部署的状态
    unblock    取消阻止被阻止的部署

```

#### nomad deployment  fail

```shell
Usage: nomad deployment fail [options] <deployment id>

失败用于将部署标记为失败。部署失败将
  停止放置新分配作为滚动部署的一部分，以及
  如果作业配置为自动还原，则作业将尝试回滚到
  稳定版。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  以及部署命名空间的“读取作业”功能

Fail Options:

  -detach
    立即返回，而不是进入监视模式。部署后
    简历，评估ID将打印到屏幕上，可以使用
    以使用 eval-status 命令检查评估。

  -verbose
    显示完整信息。



```



#### nomad deployment  list

```shell
Usage: nomad deployment list [options]

  列表用于列出 Nomad 跟踪的部署集。

启用 ACL 后，此命令需要具有“读取作业”的令牌
  部署命名空间的功能。

List Options:

  -json
    以 JSON 格式输出部署。

  -filter
    指定用于筛选查询结果的表达式。

  -t
    使用 Go 模板设置和显示部署的格式。

  -verbose
    显示完整信息。


```

#### nomad deployment  pause

```shell
Usage: nomad deployment pause [options] <deployment id>

  暂停用于暂停部署。暂停部署将暂停
  在滚动部署过程中放置新分配。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  以及部署命名空间的“读取作业”功能。
  
  
Pause Options:

  -verbose
    显示完整信息



```

#### nomad deployment  promote

```shell
Usage: nomad deployment promote [options] <deployment id>

  升级用于升级部署中的任务组。应进行升级
  当部署为任务组放置了金丝雀并且这些金丝雀具有
  被认为是健康的。升级任务组时，滚动升级
  剩余的分配已解除阻止。如果发现金丝雀不健康，
  使用“nomad 部署失败”命令，部署可能会失败，
  作业可以通过提交新版本来向前失败，也可以通过提交向后失败
  使用“nomad 作业还原”命令还原到旧版本。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  以及部署命名空间的“读取作业”功能。

Promote Options:

  -group
    组可以多次指定，并用于提升该特定
    群。如果未指定特定组，则会升级所有组。

  -detach
    立即返回，而不是进入监视模式。部署后
    简历，评估ID将打印到屏幕上，可以使用
    以使用 eval-status 命令检查评估。

  -verbose
    显示完整信息。



```

#### nomad deployment  resume

```shell
Usage: nomad deployment resume [options] <deployment id>

  恢复用于取消暂停暂停的部署。恢复部署将
  作为滚动部署的一部分，恢复放置新分配。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  以及部署命名空间的“读取作业”功能。

Resume Options:

  -detach
    立即返回，而不是进入监视模式。部署后
    简历，评估ID将打印到屏幕上，可以使用
    以使用 eval-status 命令检查评估。

  -verbose
    显示完整信息。



```

#### nomad deployment  status

```shell
Usage: nomad deployment status [options] <deployment id>

  状态用于显示部署的状态。将显示状态
  所需更改的数量以及当前应用的更改。

启用 ACL 后，此命令需要具有“读取作业”的令牌
  部署命名空间的功能。

Status Options:

  -verbose
    显示完整信息。

  -json
    以 JSON 格式输出部署。

  -monitor
    进入监视模式以轮询部署状态的更新。

  -wait
    轮询更新之前要等待多长时间，与监视器结合使用
    模式。默认为 2 秒。

  -t
    使用 Go 模板格式化和显示部署


```

#### nomad deployment  unblock

```shell
Usage: nomad deployment unblock [options] <deployment id>

取消阻止用于取消阻止正在等待的多区域部署
  要完成的对等区域部署。

启用 ACL 后，此命令需要具有“提交作业”的令牌
  以及部署命名空间的“读取作业”功能。

Unblock Options:

  -detach
    立即返回，而不是进入监视模式。部署后
    解封，评估ID将打印到屏幕上，可以使用
    以使用 eval-status 命令检查评估。

  -verbose
    显示完整信息。

```

### eval

```shell
Usage: nomad eval <subcommand> [options] [args]

  此命令对用于与评估交互的子命令进行分组。评估
  用于触发调度事件。因此，评估是内部的
  详细信息，但可用于调试群集时的放置失败
  没有运行给定作业的资源。

  列出评估：

      $ nomad eval list

  检查评估状态：

      $ nomad eval status <eval-id>

  删除评估：

      $ nomad eval delete <eval-id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    delete    按 ID 或使用筛选器删除评估
    list      列出 Nomad 处理的评估集
    status    显示评估状态和放置失败原因

```

#### nomad eval delete

```shell
Usage: nomad eval delete [options] <evaluation>

  Delete an evaluation by ID. If the evaluation ID is omitted, this command
  will use the filter flag to identify and delete a set of evaluations. If ACLs
  are enabled, this command requires a management ACL token.

  This command should be used cautiously and only in outage situations where
  there is a large backlog of evaluations not being processed. During most
  normal and outage scenarios, Nomads reconciliation and state management will
  handle evaluations as needed.

  The eval broker is expected to be paused prior to running this command and
  un-paused after. This can be done using the following two commands:
    - nomad operator scheduler set-config -pause-eval-broker=true
    - nomad operator scheduler set-config -pause-eval-broker=false
    
    
Eval Delete Options:

  -filter
    Specifies an expression used to filter evaluations by for deletion. When
    using this flag, it is advisable to ensure the syntax is correct using the
    eval list command first. Note that deleting evals by filter is imprecise:
    for sets of evals larger than a single raft log batch, evals can be inserted
    behind the cursor and therefore be missed.

  -yes
    Bypass the confirmation prompt if an evaluation ID was not provided.


```

#### nomad eval list

```shell
Usage: nomad eval list [options]

  List is used to list the set of evaluations processed by Nomad.

Eval List Options:

  -verbose
    Show full information.

  -per-page
    How many results to show per page.

  -page-token
    Where to start pagination.

  -filter
    Specifies an expression used to filter query results.

  -job
    Only show evaluations for this job ID.

  -status
    Only show evaluations with this status.

  -json
    Output the evaluation in its JSON format.

  -t
    Format and display evaluation using a Go template.



```

#### nomad eval status

```shell
Usage: nomad eval status [options] <evaluation>

  Display information about evaluations. This command can be used to inspect the
  current status of an evaluation as well as determine the reason an evaluation
  did not place all allocations.


Eval Status Options:

  -monitor
    Monitor an outstanding evaluation

  -verbose
    Show full information.

  -json
    Output the evaluation in its JSON format.

  -t
    Format and display evaluation using a Go template.


```

### exec

```shell
Usage: nomad alloc exec [options] <allocation> <command>

  在给定分配和任务的环境中运行命令。

启用 ACL 后，此命令需要一个带有“alloc-exec”的令牌，
  分配命名空间的“读取作业”和“列出作业”功能。如果
  任务驱动程序没有文件系统隔离（与“raw_exec”一样），
  此命令需要“alloc-node-exec”、“read-job”和“list-jobs”
  分配命名空间的功能。

Exec Specific Options:

  -task <task-name>
    将任务设置为 exec 命令

  -job
    使用指定作业 ID 中的随机分配。

  -i
    将 stdin 传递给容器，默认为 true。 传递 -i=false 以禁用。

  -t
    分配伪 tty，如果检测到 stdin 是 tty 会话，则默认为 true。
    传递 -t=false 以显式禁用。

  -e <escape_char>
    为带有 pty 的会话设置转义字符（默认值：“~”）。 逃亡
    字符仅在行首识别。 转义字符
    后跟一个点 （'.） 表示关闭连接。 将字符设置为
    “none”禁用任何转义并使会话完全透明。


```



### fmt

```shell
Usage: nomad fmt [flags] paths ...

将 Nomad 代理配置和作业文件的格式设置为规范格式。
  如果路径是目录，它将递归格式化所有文件
  目录中有 .nomad 和 .hcl 扩展名。

如果您提供单个短划线 （-） 作为参数，fmt 将从标准读取
  输入（STDIN）并将处理后的输出输出到标准输出（STDOUT）。

Format Options:

  -check
        检查文件是否为有效的 HCL 文件。如果不是，则退出状态
        命令将为 1，并且不会格式化不正确的文件。这
    标志覆盖任何 -write 标志值。

  -list
        列出包含格式不一致的文件。违约
        到 -list=true。

  -recursive
        处理子目录中的文件。默认情况下，只有给定的（或当前的）
        目录已处理。

  -write
        覆盖输入文件。默认为 -write=true。如果输入，则忽略
    来自STDIN。

```

### license

```shell
Usage: nomad license <subcommand> [options] [args]

此命令包含用于管理 Nomad 企业许可证的子命令。
有关更详细的示例，请参阅：
https://www.nomadproject.io/docs/commands/license/

Retrieve the server's license:

        $ nomad license get

Subcommands:
    get    Retrieve the current Nomad Enterprise License

```

#### nomad license get

```shell
Usage: nomad license get [options]

  
  获取服务器加载的许可证。该命令不会转发到
  游牧领袖，并将从特定服务器返回许可证
  联系。

启用 ACL 后，此命令需要一个令牌，其中包含
“运算符：读取”功能。

```

### login

```shell
Usage: nomad login [options]

  登录命令将提供的第三方凭据与
  为新铸造的游牧 ACL 令牌请求身份验证方法。

Login Options:

  -method
    要登录的 ACL 身份验证方法的名称。如果群集管理员
    已配置默认值，此标志是可选的。

  -oidc-callback-addr
    用于本地 OIDC 回调服务器的地址。这应该给出
    采用 ： <IP><PORT> 的形式，默认为 “localhost：4649”。

  -login-token
    用于身份验证的登录令牌，将交换为Nomad ACL
    令 牌。仅当使用 OIDC 以外的身份验证方法类型时才需要。

  -json
    以 JSON 格式输出 ACL 令牌。

  -t
    使用 Go 模板格式化和显示 ACL 令牌。


```

### monitor

```shell
Usage: nomad monitor [options]

  流式传输游牧代理的日志消息。监视器命令可让您
  侦听可能从 Nomad 代理中筛选出的日志级别。为
  示例，您的代理可能仅在 INFO 级别进行日志记录，但使用监视器
  命令，你可以设置 -log-level DEBUG

启用 ACL 后，此命令需要一个带有“代理：读取”的令牌
  能力。

Monitor Specific Options:

  -log-level <level>
    设置要监视的日志级别（默认：INFO）

  -node-id <node-id>
    设置要监视的特定节点

  -server-id <server-id>
    设置要监视的特定服务器

  -json
    将日志输出设置为 JSON 格式

```

### namespace

```shell
Usage: nomad namespace <subcommand> [options] [args]

此命令对用于与命名空间交互的子命令进行分组。命名空间
  允许作业及其关联对象相互分段，并且
  群集的其他用户。有关命名空间的完整指南，请参阅：
  https://learn.hashicorp.com/tutorials/nomad/namespaces

  创建或更新命名空间：

      $ nomad namespace apply -description "My new namespace" <name>

  列出命名空间：

      $ nomad namespace list

 查看命名空间的状态：

      $ nomad namespace status <name>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    apply      创建或更新命名空间
    delete     删除命名空间
    inspect    检查命名空间
    list       列出命名空间
    status     显示命名空间的状态

```

#### nomad namespace apply

```shell
Usage: nomad namespace apply [options] <input>

  Apply is used to create or update a namespace. The specification file
  will be read from stdin by specifying "-", otherwise a path to the file is
  expected.

  Instead of a file, you may instead pass the namespace name to create
  or update as the only argument.

  If ACLs are enabled, this command requires a management ACL token.

Apply Options:

  -quota
    The quota to attach to the namespace.

  -description
    An optional description for the namespace.

  -json
    Parse the input as a JSON namespace specification.


```

#### nomad namespace delete

```shell
Usage: nomad namespace delete [options] <namespace>

  Delete is used to remove a namespace.

  If ACLs are enabled, this command requires a management ACL token.


```

#### nomad namespace inspect

```shell
Usage: nomad namespace inspect [options] <namespace>

  Inspect is used to view raw information about a particular namespace.

  If ACLs are enabled, this command requires a management ACL token or a token
  that has a capability associated with the namespace.

Inspect Options:

  -t
    Format and display the namespaces using a Go template.

```

#### nomad namespace list

```shell
Usage: nomad namespace list [options]

  List is used to list available namespaces.

  If ACLs are enabled, this command requires a management ACL token to view
  all namespaces. A non-management token can be used to list namespaces for
  which it has an associated capability.


List Options:

  -json
    Output the namespaces in a JSON format.

  -t
    Format and display the namespaces using a Go template.


```

#### nomad namespace status

```shell
Usage: nomad namespace status [options] <namespace>

  Status is used to view the status of a particular namespace.

  If ACLs are enabled, this command requires a management ACL token or a token
  that has a capability associated with the namespace.


Status Specific Options:

  -json
    Output the latest namespace status information in a JSON format.

  -t
    Format and display namespace status information using a Go template.


```


### operator

```shell
Usage: nomad operator <subcommand> [options]

  Provides cluster-level tools for Nomad operators, such as interacting with
  the Raft subsystem. NOTE: Use this command with extreme caution, as improper
  use could lead to a Nomad outage and even loss of data.

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    api             Query Nomad's HTTP API
    autopilot       Provides tools for modifying Autopilot configuration
    client-state    Dump the nomad client state
    debug           Build a debug archive
    gossip
    metrics         Retrieve Nomad metrics
    raft            Provides access to the Raft subsystem
    root
    scheduler       Provides access to the scheduler configuration
    snapshot        Saves and inspects snapshots of Nomad server state

```

#### nomad  operator api

```shell
Usage: nomad operator api [options] <path>

  api is a utility command for accessing Nomad's HTTP API and is inspired by
  the popular curl command line tool. Nomad's operator api command populates
  Nomad's standard environment variables into their appropriate HTTP headers.
  If the 'path' does not begin with "http" then $NOMAD_ADDR will be used.

  The 'path' can be in one of the following forms:

    /v1/allocations                       <- API Paths must start with a /
    localhost:4646/v1/allocations         <- Scheme will be inferred
    https://localhost:4646/v1/allocations <- Scheme will be https://

  Note that this command does not always match the popular curl program's
  behavior. Instead Nomad's operator api command is optimized for common Nomad
  HTTP API operations.

Operator API Specific Options:

  -dryrun
    Output equivalent curl command to stdout and exit.
    HTTP Basic Auth will never be output. If the $NOMAD_HTTP_AUTH environment
    variable is set, it will be referenced in the appropriate curl flag in the
    output.
    ACL tokens set via the $NOMAD_TOKEN environment variable will only be
    referenced by environment variable as with HTTP Basic Auth above. However
    if the -token flag is explicitly used, the token will also be included in
    the output.

  -filter <query>
    Specifies an expression used to filter query results.

  -H <Header>
    Adds an additional HTTP header to the request. May be specified more than
    once. These headers take precedence over automatically set ones such as
    X-Nomad-Token.

  -verbose
    Output extra information to stderr similar to curl's --verbose flag.

  -X <HTTP Method>
    HTTP method of request. If there is data piped to stdin, then the method
    defaults to POST. Otherwise the method defaults to GET.



```

#### nomad  operator autopilot

```shell
Usage: nomad operator autopilot <subcommand> [options]

  This command groups subcommands for interacting with Nomad's Autopilot
  subsystem. Autopilot provides automatic, operator-friendly management of Nomad
  servers. The command can be used to view or modify the current Autopilot
  configuration. For a full guide see: https://www.nomadproject.io/guides/autopilot.html

  Get the current Autopilot configuration:

      $ nomad operator autopilot get-config

  Set a new Autopilot configuration, enabling automatic dead server cleanup:

      $ nomad operator autopilot set-config -cleanup-dead-servers=true

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    get-config    Display the current Autopilot configuration
    set-config    Modify the current Autopilot configuration

```

##### nomad  operator autopilot get-config

```shell
Usage: nomad operator autopilot get-config [options]

  Displays the current Autopilot configuration.

  If ACLs are enabled, this command requires a token with the 'operator:read'
  capability.


```

##### nomad  operator autopilot set-config

```shell
Usage: nomad operator autopilot set-config [options]

  Modifies the current Autopilot configuration.

  If ACLs are enabled, this command requires a token with the 'operator:write'
  capability.


Set Config Options:

  -cleanup-dead-servers=[true|false]
     Controls whether Nomad will automatically remove dead servers when
     new ones are successfully added. Must be one of [true|false].

  -disable-upgrade-migration=[true|false]
     (Enterprise-only) Controls whether Nomad will avoid promoting
     new servers until it can perform a migration. Must be one of
     "true|false".

  -last-contact-threshold=200ms
     Controls the maximum amount of time a server can go without contact
     from the leader before being considered unhealthy. Must be a
     duration value such as "200ms".

  -max-trailing-logs=<value>
     Controls the maximum number of log entries that a server can trail
     the leader by before being considered unhealthy.

  -min-quorum=<value>
      Controls the minimum number of servers required in a cluster
      before autopilot can prune dead servers.

  -redundancy-zone-tag=<value>
     (Enterprise-only) Controls the node_meta tag name used for
     separating servers into different redundancy zones.

  -server-stabilization-time=<10s>
     Controls the minimum amount of time a server must be stable in
     the 'healthy' state before being added to the cluster. Only takes
     effect if all servers are running Raft protocol version 3 or
     higher. Must be a duration value such as "10s".

  -upgrade-version-tag=<value>
     (Enterprise-only) The node_meta tag to use for version info when
     performing upgrade migrations. If left blank, the Nomad version
     will be used.



```



#### nomad  operator client-state

```shell
Usage: nomad operator client-state <path_to_nomad_dir>

  Emits a representation of the stored client state in JSON format.

```

#### nomad  operator debug

```shell
Usage: nomad operator debug [options]

  Build an archive containing Nomad cluster configuration and state, and Consul
  and Vault status. Include logs and pprof profiles for selected servers and
  client nodes.

  If ACLs are enabled, this command will require a token with the 'node:read'
  capability to run. In order to collect information, the token will also
  require the 'agent:read' and 'operator:read' capabilities, as well as the
  'list-jobs' capability for all namespaces. To collect pprof profiles the
  token will also require 'agent:write', or enable_debug configuration set to
  true.

  If event stream capture is enabled, the Job, Allocation, Deployment,
  and Evaluation topics require 'namespace:read-job' capabilities, the Node
  topic requires 'node:read'.  A 'management' token is required to capture
  ACLToken, ACLPolicy, or all all events.

General Options:

  -address=<addr>
    The address of the Nomad server.
    Overrides the NOMAD_ADDR environment variable if set.
    Default = http://127.0.0.1:4646

  -region=<region>
    The region of the Nomad servers to forward commands to.
    Overrides the NOMAD_REGION environment variable if set.
    Defaults to the Agent's local region.

  -no-color
    Disables colored command output. Alternatively, NOMAD_CLI_NO_COLOR may be
    set. This option takes precedence over -force-color.

  -force-color
    Forces colored command output. This can be used in cases where the usual
    terminal detection fails. Alternatively, NOMAD_CLI_FORCE_COLOR may be set.
    This option has no effect if -no-color is also used.

  -ca-cert=<path>
    Path to a PEM encoded CA cert file to use to verify the
    Nomad server SSL certificate. Overrides the NOMAD_CACERT
    environment variable if set.

  -ca-path=<path>
    Path to a directory of PEM encoded CA cert files to verify
    the Nomad server SSL certificate. If both -ca-cert and
    -ca-path are specified, -ca-cert is used. Overrides the
    NOMAD_CAPATH environment variable if set.

  -client-cert=<path>
    Path to a PEM encoded client certificate for TLS authentication
    to the Nomad server. Must also specify -client-key. Overrides
    the NOMAD_CLIENT_CERT environment variable if set.

  -client-key=<path>
    Path to an unencrypted PEM encoded private key matching the
    client certificate from -client-cert. Overrides the
    NOMAD_CLIENT_KEY environment variable if set.

  -tls-server-name=<value>
    The server name to use as the SNI host when connecting via
    TLS. Overrides the NOMAD_TLS_SERVER_NAME environment variable if set.

  -tls-skip-verify
    Do not verify TLS certificate. This is highly not recommended. Verification
    will also be skipped if NOMAD_SKIP_VERIFY is set.

  -token
    The SecretID of an ACL token to use to authenticate API requests with.
    Overrides the NOMAD_TOKEN environment variable if set.

Consul Options:

  -consul-http-addr=<addr>
    The address and port of the Consul HTTP agent. Overrides the
    CONSUL_HTTP_ADDR environment variable.

  -consul-token=<token>
    Token used to query Consul. Overrides the CONSUL_HTTP_TOKEN environment
    variable and the Consul token file.

  -consul-token-file=<path>
    Path to the Consul token file. Overrides the CONSUL_HTTP_TOKEN_FILE
    environment variable.

  -consul-client-cert=<path>
    Path to the Consul client cert file. Overrides the CONSUL_CLIENT_CERT
    environment variable.

  -consul-client-key=<path>
    Path to the Consul client key file. Overrides the CONSUL_CLIENT_KEY
    environment variable.

  -consul-ca-cert=<path>
    Path to a CA file to use with Consul. Overrides the CONSUL_CACERT
    environment variable and the Consul CA path.

  -consul-ca-path=<path>
    Path to a directory of PEM encoded CA cert files to verify the Consul
    certificate. Overrides the CONSUL_CAPATH environment variable.

Vault Options:

  -vault-address=<addr>
    The address and port of the Vault HTTP agent. Overrides the VAULT_ADDR
    environment variable.

  -vault-token=<token>
    Token used to query Vault. Overrides the VAULT_TOKEN environment
    variable.

  -vault-client-cert=<path>
    Path to the Vault client cert file. Overrides the VAULT_CLIENT_CERT
    environment variable.

  -vault-client-key=<path>
    Path to the Vault client key file. Overrides the VAULT_CLIENT_KEY
    environment variable.

  -vault-ca-cert=<path>
    Path to a CA file to use with Vault. Overrides the VAULT_CACERT
    environment variable and the Vault CA path.

  -vault-ca-path=<path>
    Path to a directory of PEM encoded CA cert files to verify the Vault
    certificate. Overrides the VAULT_CAPATH environment variable.

Debug Options:

  -duration=<duration>
    Set the duration of the debug capture. Logs will be captured from specified servers and
    nodes at "log-level". Defaults to 2m.

  -event-index=<index>
    Specifies the index to start streaming events from. If the requested index is
    no longer in the buffer the stream will start at the next available index.
    Defaults to 0.

  -event-topic=<Allocation,Evaluation,Job,Node,*>:<filter>
    Enable event stream capture, filtered by comma delimited list of topic filters.
    Examples:
      "all" or "*:*" for all events
      "Evaluation" or "Evaluation:*" for all evaluation events
      "*:example" for all events related to the job "example"
    Defaults to "none" (disabled).

  -interval=<interval>
    The interval between snapshots of the Nomad state. Set interval equal to
    duration to capture a single snapshot. Defaults to 30s.

  -log-level=<level>
    The log level to monitor. Defaults to DEBUG.

  -max-nodes=<count>
    Cap the maximum number of client nodes included in the capture. Defaults
    to 10, set to 0 for unlimited.

  -node-id=<node1>,<node2>
    Comma separated list of Nomad client node ids to monitor for logs, API
    outputs, and pprof profiles. Accepts id prefixes, and "all" to select all
    nodes (up to count = max-nodes). Defaults to "all".

  -node-class=<node-class>
    Filter client nodes based on node class.

  -pprof-duration=<duration>
    Duration for pprof collection. Defaults to 1s or -duration, whichever is less.

  -pprof-interval=<pprof-interval>
    The interval between pprof collections. Set interval equal to
    duration to capture a single snapshot. Defaults to 250ms or
   -pprof-duration, whichever is less.

  -server-id=<server1>,<server2>
    Comma separated list of Nomad server names to monitor for logs, API
    outputs, and pprof profiles. Accepts server names, "leader", or "all".
    Defaults to "all".

  -stale=<true|false>
    If "false", the default, get membership data from the cluster leader. If
    the cluster is in an outage unable to establish leadership, it may be
    necessary to get the configuration from a non-leader server.

  -output=<path>
    Path to the parent directory of the output directory. If specified, no
    archive is built. Defaults to the current directory.

  -verbose
    Enable verbose output.

```

#### nomad  operator gossip

```shell
This command is accessed by using one of the subcommands below.

Subcommands:
    keyring    Manages gossip layer encryption keys

```

#### nomad  operator metrics

```shell
Usage: nomad operator metrics [options]

Get Nomad metrics

Metrics Specific Options

  -pretty
    Pretty prints the JSON output

  -format <format>
    Specify output format (prometheus)

  -json
    Output the allocation in its JSON format.

  -t
    Format and display allocation using a Go template.


```

#### nomad  operator raft

```shell
Usage: nomad operator raft <subcommand> [options]

  This command groups subcommands for interacting with Nomad's Raft subsystem.
  The command can be used to verify Raft peers or in rare cases to recover
  quorum by removing invalid peers.

  List Raft peers:

      $ nomad operator raft list-peers

  Remove a Raft peer:

      $ nomad operator raft remove-peer -peer-address "IP:Port"

  Display info about the raft logs in the data directory:

      $ nomad operator raft info /var/nomad/data

  Display the log entries persisted in data dir in JSON format.

      $ nomad operator raft logs /var/nomad/data

  Display the server state obtained by replaying raft log entries
  persisted in data dir in JSON format.

      $ nomad operator raft state /var/nomad/data

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    info           Display info of the raft log
    list-peers     Display the current Raft peer configuration
    logs           Display raft log content
    remove-peer    Remove a Nomad server from the Raft configuration
    state          Display raft server state

```



##### nomad  operator raft  info

```shell
Usage: nomad operator raft info <path to nomad data dir>

  Displays summary information about the raft logs in the data directory.

  This command requires file system permissions to access the data directory on
  disk. The Nomad server locks access to the data directory, so this command
  cannot be run on a data directory that is being used by a running Nomad server.

  This is a low-level debugging tool and not subject to Nomad's usual backward
  compatibility guarantees.

```

##### nomad  operator raft  list-peers

```shell
Usage: nomad operator raft list-peers [options]

  Displays the current Raft peer configuration.

  If ACLs are enabled, this command requires a management token.

List Peers Options:

  -stale=[true|false]
    The -stale argument defaults to "false" which means the leader provides the
    result. If the cluster is in an outage state without a leader, you may need
    to set -stale to "true" to get the configuration from a non-leader server.
[root@master ~]#

```

##### nomad  operator raft  logs 

```shell
Usage: nomad operator raft logs <path to nomad data dir>

  Display the log entries persisted in the Nomad data directory in JSON
  format.

  This command requires file system permissions to access the data directory on
  disk. The Nomad server locks access to the data directory, so this command
  cannot be run on a data directory that is being used by a running Nomad server.

  This is a low-level debugging tool and not subject to Nomad's usual backward
  compatibility guarantees.

Raft Logs Options:

  -pretty
    By default this command outputs newline delimited JSON. If the -pretty flag
    is passed, each entry will be pretty-printed.

```

##### nomad  operator raft   remove-peer

```shell
Usage: nomad operator raft remove-peer [options]

  Remove the Nomad server with given -peer-address from the Raft configuration.

  There are rare cases where a peer may be left behind in the Raft quorum even
  though the server is no longer present and known to the cluster. This command
  can be used to remove the failed server so that it is no longer affects the
  Raft quorum. If the server still shows in the output of the "nomad
  server-members" command, it is preferable to clean up by simply running "nomad
  server-force-leave" instead of this command.

  If ACLs are enabled, this command requires a management token.

Remove Peer Options:

  -peer-address="IP:port"
        Remove a Nomad server with given address from the Raft configuration.

  -peer-id="id"
        Remove a Nomad server with the given ID from the Raft configuration.


```

##### nomad  operator raft  state

```shell
Usage: nomad operator raft state <path to nomad data dir>

  Display the server state obtained by replaying raft log entries persisted in
  the Nomad data directory in JSON format.

  This command requires file system permissions to access the data directory on
  disk. The Nomad server locks access to the data directory, so this command
  cannot be run on a data directory that is being used by a running Nomad server.

  This is a low-level debugging tool and not subject to Nomad's usual backward
  compatibility guarantees.

Options:

  -last-index=<last_index>
    Set the last log index to be applied, to drop spurious log entries not
    properly committed. If passed last_index is zero or negative, it's perceived
    as an offset from the last index seen in raft.

```



#### nomad  operator root

```shell
This command is accessed by using one of the subcommands below.

Subcommands:
    keyring    Manages root encryption keys

```

##### nomad  operator root keyring

```shell
Usage: nomad operator root keyring [options]

  Manages encryption keys used for storing variables and signing workload
  identities. This command may be used to examine active encryption keys
  in the cluster, rotate keys, add new keys from backups, or remove unused keys.

  If ACLs are enabled, all subcommands requires a management token.

  Rotate the encryption key:

      $ nomad operator root keyring rotate

  List all encryption key metadata:

      $ nomad operator root keyring list

  Remove an encryption key from the keyring:

      $ nomad operator root keyring remove <key ID>

  Please see individual subcommand help for detailed usage information.

Subcommands:
    list      Lists the root encryption keys
    remove    Removes a root encryption key
    rotate    Rotates the root encryption key

```

###### nomad  operator root keyring list

```shell
Usage: nomad operator root keyring list [options]

  List the currently installed keys. This list returns key metadata and not
  sensitive key material.

  If ACLs are enabled, this command requires a management token.


Keyring Options:

  -verbose
    Show full information.

```

###### nomad  operator root keyring remove

```shell
Usage: nomad operator root keyring remove [options] <key ID>

  Remove an encryption key from the cluster. This operation may only be
  performed on keys that are not the active key.

  If ACLs are enabled, this command requires a management token.



```

###### nomad  operator root keyring rotate

```shell
Usage: nomad operator root keyring rotate [options]

  Generate a new encryption key for all future variables.

  If ACLs are enabled, this command requires a management token.


Keyring Options:

  -full
    Decrypt all existing variables and re-encrypt with the new key. This command
    will immediately return and the re-encryption process will run
    asynchronously on the leader.

  -verbose
    Show full information.

```

#### nomad  operator scheduler

```shell
Usage: nomad operator scheduler <subcommand> [options]

  This command groups subcommands for interacting with Nomad's scheduler
  subsystem.

  Get the scheduler configuration:

      $ nomad operator scheduler get-config

  Set the scheduler to use the spread algorithm:

      $ nomad operator scheduler set-config -scheduler-algorithm=spread

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    get-config    Display the current scheduler configuration
    set-config    Modify the current scheduler configuration

```

##### nomad  operator scheduler  get-config

```shell

```



##### nomad  operator scheduler  set-config

```shell

```







#### nomad  operator snapshot

```shell
Usage: nomad operator snapshot <subcommand> [options]

  This command has subcommands for saving and inspecting the state
  of the Nomad servers for disaster recovery. These are atomic, point-in-time
  snapshots which include jobs, nodes, allocations, periodic jobs, and ACLs.

  If ACLs are enabled, a management token must be supplied in order to perform
  snapshot operations.

  Create a snapshot:

      $ nomad operator snapshot save backup.snap

  Inspect a snapshot:

      $ nomad operator snapshot inspect backup.snap

  Run a daemon process that locally saves a snapshot every hour (available only in
  Nomad Enterprise) :

      $ nomad operator snapshot agent

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    inspect    Displays information about a Nomad snapshot file
    restore    Restore snapshot of Nomad server state
    save       Saves snapshot of Nomad server state
    state      Displays information about a Nomad snapshot file

```

##### nomad  operator snapshot  inspect

```shell

```

##### nomad  operator snapshot  restore 

```shell

```

##### nomad  operator snapshot  save

```shell

```

##### nomad  operator snapshot  state

```shell

```

### plugin

```shell
Usage nomad plugin status [options] [plugin]

    This command groups subcommands for interacting with plugins.


Subcommands:
    status    Display status information about a plugin

```

#### nomad plugin status

```shell
Usage nomad plugin status [options] <plugin>

  Display status information about a plugin. If no plugin id is given,
  a list of all plugins will be displayed.

  If ACLs are enabled, this command requires a token with the 'plugin:read'
  capability.


Status Options:

  -type <type>
    List only plugins of type <type>.

  -short
    Display short output.

  -verbose
    Display full information.

  -json
    Output the allocation in its JSON format.

  -t
    Format and display allocation using a Go template.

```

### quota

```shell
Usage: nomad quota <subcommand> [options] [args]

  This command groups subcommands for interacting with resource quotas. Resource
  quotas allow operators to restrict the aggregate resource usage of namespaces.
  Users can inspect existing quota specifications, create new quotas, delete and
  list existing quotas, and more. For a full guide on resource quotas see:
  https://www.nomadproject.io/guides/quotas.html

  Examine a quota's status:

      $ nomad quota status <name>

  List existing quotas:

      $ nomad quota list

  Create a new quota specification:

      $ nomad quota apply <path>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    apply      Create or update a quota specification
    delete     Delete a quota specification
    init       Create an example quota specification file
    inspect    Inspect a quota specification
    list       List quota specifications
    status     Display a quota's status and current usage

```

#### nomad quota apply

```shell

```



#### nomad quota delete

```shell

```

#### nomad quota init

```shell

```

#### nomad quota inspect

```shell

```

#### nomad quota list

```shell

```

#### nomad quota status

```shell

```

### recommendation

```shell
Usage: nomad recommendation <subcommand> [options]

  This command groups subcommands for interacting with the recommendation API.

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    apply      Apply one or more Nomad recommendations
    dismiss    Dismiss one or more Nomad recommendations
    info       Display an individual Nomad recommendation
    list       Display all Nomad recommendations

```

#### nomad recommendation  apply

```shell

```

#### nomad recommendation  dismiss

```shell

```

#### nomad recommendation  info

```shell

```

#### nomad recommendation  list

```shell

```



### scaling

```shell
Usage: nomad scaling <subcommand> [options]

  This command groups subcommands for interacting with the scaling API.

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    policy    Interact with Nomad scaling policies

```

#### nomad scaling policy

```shell
Usage: nomad scaling policy <subcommand> [options]

  This command groups subcommands for interacting with scaling policies. Scaling
  policies can be used by an external autoscaler to perform scaling actions on
  Nomad targets.

  List policies:

      $ nomad scaling policy list

  Detail an individual scaling policy:

      $ nomad scaling policy info <policy_id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    info    Display an individual Nomad scaling policy
    list    Display all Nomad scaling policies

```



##### nomad scaling policy info

```shell
Usage: nomad scaling policy info [options] <policy_id>

  Info is used to read the specified scaling policy.

  If ACLs are enabled, this command requires a token with the 'read-job' and
  'list-jobs' capabilities for the policy's namespace.


Policy Info Options:

  -verbose
    Display full information.

  -json
    Output the scaling policy in its JSON format.

  -t
    Format and display the scaling policy using a Go template.

```



#####  nomad scaling policy  list

```shell
Usage: nomad scaling policy list [options]

  List is used to list the currently configured scaling policies.

  If ACLs are enabled, this command requires a token with the 'read-job' and
  'list-jobs' capabilities for the namespace of all policies. Any namespaces
  that the token does not have access to will have its policies filtered from
  the results.

Policy Info Options:

  -job
    Specifies the job ID to filter the scaling policies list by.

  -type
    Filter scaling policies by type.

  -verbose
    Display full information.

  -json
    Output the scaling policy in its JSON format.

  -t
    Format and display the scaling policy using a Go template.

```



### sentinel

```shell
Usage: nomad sentinel <subcommand> [options] [args]

  This command groups subcommands for interacting with Sentinel policies.
  Sentinel policies allow operators to express fine-grained policies as code and
  have their policies automatically enforced. This allows operators to define a
  "sandbox" and restrict actions to only those compliant with policy. The
  Sentinel integration builds on the ACL System. Users can read existing
  Sentinel policies, create new policies, delete and list existing policies, and
  more. For a full guide on Sentinel policies see:
  https://www.nomadproject.io/guides/sentinel-policy.html

  Read an existing policy:

      $ nomad sentinel read <name>

  List existing policies:

      $ nomad sentinel list

  Create a new Sentinel policy:

      $ nomad sentinel apply <name> <path>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    apply     Create a new or update existing Sentinel policies
    delete    Delete an existing Sentinel policies
    list      Display all Sentinel policies
    read      Inspects an existing Sentinel policies

```



#### nomad  sentinel apply

```shell
Usage: nomad sentinel apply [options] <name> <file>

  Apply is used to write a new Sentinel policy or update an existing one.
  The name of the policy and file must be specified. The file will be read
  from stdin by specifying "-".

  Sentinel commands are only available when ACLs are enabled. This command
  requires a management token.

Apply Options:

  -description
    Sets a human readable description for the policy.

  -scope (default: submit-job)
    Sets the scope of the policy and when it should be enforced.

  -level (default: advisory)
    Sets the enforcement level of the policy. Must be one of advisory,
    soft-mandatory, hard-mandatory.

```



#### nomad  sentinel delete

```shell
Usage: nomad sentinel delete [options] <name>

  Delete is used to delete an existing Sentinel policy.

  Sentinel commands are only available when ACLs are enabled. This command
  requires a management token.



```

#### nomad sentinel list

```shell
Usage: nomad sentinel list [options]

  List is used to display all the installed Sentinel policies.

  Sentinel commands are only available when ACLs are enabled. This command
  requires a management token.


```

#### nomad sentinel read

```shell
Usage: nomad sentinel read [options] <name>

  Read is used to inspect a Sentinel policy.

  Sentinel commands are only available when ACLs are enabled. This command
  requires a management token.
  
Read Options:

  -raw
    Prints only the raw policy


```



### server 

```shell
Usage: nomad server <subcommand> [options] [args]

  This command groups subcommands for interacting with Nomad servers. Users can
  list Servers, join a server to the cluster, and force leave a server.

  List Nomad servers:

      $ nomad server members

  Join a new server to another:

      $ nomad server join "IP:Port"

  Force a server to leave:

      $ nomad server force-leave <name>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    force-leave    Force a server into the 'left' state
    join           Join server nodes together
    members        Display a list of known servers and their status

```

#### nomad server force-leave

```shell
Usage: nomad server force-leave [options] <node>

  Forces an server to enter the "left" state. This can be used to
  eject nodes which have failed and will not rejoin the cluster.
  Note that if the member is actually still alive, it will
  eventually rejoin the cluster again.

  If ACLs are enabled, this option requires a token with the 'agent:write'
  capability.

```

#### nomad server join

```shell
Usage: nomad server join [options] <addr> [<addr>...]

  Joins the local server to one or more Nomad servers. Joining is
  only required for server nodes, and only needs to succeed
  against one or more of the provided addresses. Once joined, the
  gossip layer will handle discovery of the other server nodes in
  the cluster.

```

#### nomad server members

```shell
Usage: nomad server members [options]

  Display a list of the known servers and their status. Only Nomad servers are
  able to service this command.

  If ACLs are enabled, this option requires a token with the 'node:read'
  capability.

Server Members Options:

  -verbose
    Show detailed information about each member. This dumps a raw set of tags
    which shows more information than the default output format.

 -json
    Output the latest information about each member in a JSON format.

  -t
    Format and display latest information about each member using a Go template.

```



### server

```shell
Usage: nomad service <subcommand> [options]

  This command groups subcommands for interacting with the services API.

  List services:

      $ nomad service list

  Detail an individual service:

      $ nomad service info <service_name>

  Delete an individual service registration:

      $ nomad service delete <service_name> <service_id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    delete    Deregister a registered service
    info      Display an individual Nomad service registration
    list      Display all registered Nomad services

```

#### nomad server delete

```shell
Usage: nomad server <subcommand> [options] [args]

  This command groups subcommands for interacting with Nomad servers. Users can
  list Servers, join a server to the cluster, and force leave a server.

  List Nomad servers:

      $ nomad server members

  Join a new server to another:

      $ nomad server join "IP:Port"

  Force a server to leave:

      $ nomad server force-leave <name>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    force-leave    Force a server into the 'left' state
    join           Join server nodes together
    members        Display a list of known servers and their status

```

#### nomad server info

```shell
Usage: nomad server <subcommand> [options] [args]

  This command groups subcommands for interacting with Nomad servers. Users can
  list Servers, join a server to the cluster, and force leave a server.

  List Nomad servers:

      $ nomad server members

  Join a new server to another:

      $ nomad server join "IP:Port"

  Force a server to leave:

      $ nomad server force-leave <name>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    force-leave    Force a server into the 'left' state
    join           Join server nodes together
    members        Display a list of known servers and their status

```

#### nomad server list

```shell
Usage: nomad server <subcommand> [options] [args]

  This command groups subcommands for interacting with Nomad servers. Users can
  list Servers, join a server to the cluster, and force leave a server.

  List Nomad servers:

      $ nomad server members

  Join a new server to another:

      $ nomad server join "IP:Port"

  Force a server to leave:

      $ nomad server force-leave <name>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    force-leave    Force a server into the 'left' state
    join           Join server nodes together
    members        Display a list of known servers and their status

```



### service

```shell
Usage: nomad service <subcommand> [options]

  This command groups subcommands for interacting with the services API.

  List services:

      $ nomad service list

  Detail an individual service:

      $ nomad service info <service_name>

  Delete an individual service registration:

      $ nomad service delete <service_name> <service_id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    delete    Deregister a registered service
    info      Display an individual Nomad service registration
    list      Display all registered Nomad services

```


#### nomad service delete

```shell
Usage: nomad service delete [options] <service_name> <service_id>

  Delete is used to deregister the specified service registration. It should be
  used with caution and can only remove a single registration, via the service
  name and service ID, at a time.

  When ACLs are enabled, this command requires a token with the 'submit-job'
  capability for the service registration namespace.

```

#### nomad service info

```shell
Usage: nomad service info [options] <service_name>

  Info is used to read the services registered to a single service name.

  When ACLs are enabled, this command requires a token with the 'read-job'
  capability for the service namespace.

Service Info Options:

  -verbose
    Display full information.

  -per-page
    How many results to show per page.

  -page-token
    Where to start pagination.

  -filter
    Specifies an expression used to filter query results.

  -json
    Output the service in JSON format.

  -t
    Format and display the service using a Go template.


```

#### nomad service list

```shell
Usage: nomad service list [options]

  List is used to list the currently registered services.

  If ACLs are enabled, this command requires a token with the 'read-job'
  capabilities for the namespace of all services. Any namespaces that the token
  does not have access to will have its services filtered from the results.

Service List Options:

  -json
    Output the services in JSON format.

  -t
    Format and display the services using a Go template.

```

### system

```shell
Usage: nomad system <subcommand> [options]

  This command groups subcommands for interacting with the system API. Users
  can perform system maintenance tasks such as trigger the garbage collector or
  perform job summary reconciliation.

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    gc           Run the system garbage collection process
    reconcile    Perform system reconciliation tasks

```

#### nomad system gc

```shell
Usage: nomad system gc [options]

  Initializes a garbage collection of jobs, evaluations, allocations, and nodes.

  If ACLs are enabled, this option requires a management token.



```



#### nomad system reconcile

```shell
Usage: nomad system reconcile <subcommand> [options]

  This command groups subcommands for interacting with the system reconcile API.

  Reconcile the summaries of all registered jobs:

      $ nomad system reconcile summaries

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    summaries    Reconciles the summaries of all registered jobs

```



##### nomad system reconcile  summaries

```shell
Usage: nomad system reconcile summaries [options]

  Reconciles the summaries of all registered jobs.

  If ACLs are enabled, this option requires a management token.

```



### tls

```shell
Usage: nomad tls <subcommand> <subcommand> [options]

This command groups subcommands for creating certificates for Nomad TLS configuration.
The TLS command allows operators to generate self signed certificates to use
when securing your Nomad cluster.

Some simple examples for creating certificates can be found here.
More detailed examples are available in the subcommands or the documentation.

Create a CA

    $ nomad tls ca create

Create a server certificate

    $ nomad tls cert create -server

Create a client certificate

    $ nomad tls cert create -client

Subcommands:
    ca      Helpers for managing certificate authorities
    cert    Helpers for managing certificates

```



#### nomad tls ca

```shell
Usage: nomad tls ca <subcommand> [options]

  This command groups subcommands for interacting with certificate authorities.
  For examples, see the documentation.

  Create a certificate authority.

      $ nomad tls ca create

  Show information about a certificate authority.

      $ nomad tls ca info

Subcommands:
    create    Create a certificate authority for Nomad
    info      Show certificate authority information

```

#### nomad tls cert

```shell
Usage: nomad tls cert <subcommand> [options]

  This command groups subcommands for interacting with certificates.
  For examples, see the documentation.

  Create a TLS certificate.

      $ nomad tls cert create

  Show information about a TLS certificate.

      $ nomad tls cert info

Subcommands:
    create    Create a new TLS certificate
    info      Show certificate information

```



### ui 

```shell
Usage: nomad ui [options] <identifier>

Open the Nomad Web UI in the default browser. An optional identifier may be
provided, in which case the UI will be opened to view the details for that
object. Supported identifiers are jobs, allocations and nodes.

UI Options

  -authenticate: Exchange your Nomad ACL token for a one-time token in the
    web UI, if ACLs are enabled.

  -show-url: Show the Nomad UI URL instead of opening with the default browser.

```

### var

```shell
Usage: nomad var <subcommand> [options] [args]

  This command groups subcommands for interacting with variables. Variables
  allow operators to provide credentials and otherwise sensitive material to
  Nomad jobs at runtime via the template block or directly through
  the Nomad API and CLI.

  Users can create new variables; list, inspect, and delete existing
  variables, and more. For a full guide on variables see:
  https://www.nomadproject.io/docs/concepts/variables

  Create a variable specification file:

      $ nomad var init

  Upsert a variable:

      $ nomad var put <path>

  Examine a variable:

      $ nomad var get <path>

  List existing variables:

      $ nomad var list <prefix>

  Purge a variable:

      $ nomad var purge <path>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    get      Read a variable
    init     Create an example variable specification file
    list     List variable metadata
    purge    Purge a variable
    put      Create or update a variable

```



#### nomad var get

```shell
Usage: nomad var get [options] <path>

  The 'var get' command is used to get the contents of an existing variable.

  If ACLs are enabled, this command requires a token with the 'variables:read'
  capability for the target variable's namespace and path.

Get Options:

  -item <item key>
     Print only the value of the given item. Specifying this option will
     take precedence over other formatting directives. The result will not
     have a trailing newline making it ideal for piping to other processes.

  -out ( go-template | hcl | json | none | table )
     Format to render the variable in. When using "go-template", you must
     provide the template content with the "-template" option. Defaults
     to "table" when stdout is a terminal and to "json" when stdout is
     redirected.

  -template
     Template to render output with. Required when output is "go-template".

```

#### nomad var init

```shell
Usage: nomad var init <filename>

  Creates an example variable specification file that can be used as a starting
  point to customize further. When no filename is supplied, a default filename
  of "spec.nv.hcl" or "spec.nv.json" will be used depending on the output
  format.

Init Options:

  -out (hcl | json)
    Format of generated variable specification. Defaults to "hcl".

```

#### nomad var list

```shell
Usage: nomad var list [options] <prefix>

  List is used to list available variables. Supplying an optional prefix,
  filters the list to variables having a path starting with the prefix.
  When using pagination, the next page token is provided in the JSON output
  or as a message to standard error to leave standard output for the listed
  variables from that page.

  If ACLs are enabled, this command will only return variables stored in
  namespaces and paths where the token has the 'variables:list' capability.

List Options:

  -per-page
    How many results to show per page.

  -page-token
    Where to start pagination.

  -filter
    Specifies an expression used to filter query results. Queries using this
    option are less efficient than using the prefix parameter; therefore,
    the prefix parameter should be used whenever possible.

  -out (go-template | json | table | terse )
    Format to render created or updated variable. Defaults to "none" when
    stdout is a terminal and "json" when the output is redirected. The "terse"
        format outputs as little information as possible to uniquely identify a
        variable depending on whether or not the wildcard namespace was passed.

 -template
    Template to render output with. Required when format is "go-template",
    invalid for other formats.


```

#### nomad var purge

```shell
Usage: nomad var purge [options] <path>

  Purge is used to permanently delete an existing variable.

  If ACLs are enabled, this command requires a token with the 'variables:destroy'
  capability for the target variable's namespace and path.

Purge Options:

  -check-index
    If set, the variable is only acted upon if the server side version's modify
    index matches the provided value.

```

#### nomad var put

```shell
Usage:
nomad var put [options] <variable spec file reference> [<key>=<value>]...
nomad var put [options] <path to store variable> [<variable spec file reference>] [<key>=<value>]...

  The 'var put' command is used to create or update an existing variable.
  Variable metadata and items can be supplied using a variable specification,
  by using command arguments, or by a combination of the two techniques.

  An entire variable specification can be provided to the command via standard
  input (stdin) by setting the first argument to "-" or from a file by using an
  @-prefixed path to a variable specification file. When providing variable
  data via stdin, you must provide the "-in" flag with the format of the
  specification, either "hcl" or "json"

  Items to be stored in the variable can be supplied using the specification,
  as a series of key-value pairs, or both. The value for a key-value pair can
  be a string, an @-prefixed file reference, or a '-' to get the value from
  stdin. Item values provided from file references or stdin are consumed as-is
  with no additional processing and do not require the input format to be
  specified.

  Values supplied as command line arguments supersede values provided in
  any variable specification piped into the command or loaded from file.

  If ACLs are enabled, this command requires the 'variables:write' capability
  for the destination namespace and path.


Apply Options:

  -check-index
     If set, the variable is only acted upon if the server-side version's index
     matches the provided value. When a variable specification contains
     a modify index, that modify index is used as the check-index for the
     check-and-set operation and can be overridden using this flag.

  -force
     Perform this operation regardless of the state or index of the variable
     on the server-side.

  -in (hcl | json)
     Parser to use for data supplied via standard input or when the variable
     specification's type can not be known using the file extension. Defaults
     to "json".

  -out (go-template | hcl | json | none | table)
     Format to render created or updated variable. Defaults to "none" when
     stdout is a terminal and "json" when the output is redirected.

  -template
     Template to render output with. Required when format is "go-template",
     invalid for other formats.

  -verbose
     Provides additional information via standard error to preserve standard
     output (stdout) for redirected output.

```



### volume

```shell
Usage: nomad volume <subcommand> [options]

  volume groups commands that interact with volumes.

  Register a new volume or update an existing volume:

      $ nomad volume register <input>

  Examine the status of a volume:

      $ nomad volume status <id>

  Deregister an unused volume:

      $ nomad volume deregister <id>

  Detach an unused volume:

      $ nomad volume detach <vol id> <node id>

  Create an external volume and register it:

      $ nomad volume create <input>

  Delete an external volume and deregister it:

      $ nomad volume delete <external id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    create        Create an external volume
    delete        Delete a volume
    deregister    Remove a volume
    detach        Detach a volume
    init          Create an example volume specification file
    register      Create or update a volume
    snapshot      Interact with volume snapshots
    status        Display status information about a volume

```

#### nomad volume create

```shell
Usage: nomad volume create [options] <input>

  Creates a volume in an external storage provider and registers it in Nomad.

  If the supplied path is "-" the volume file is read from stdin. Otherwise, it
  is read from the file at the supplied path.

  When ACLs are enabled, this command requires a token with the
  'csi-write-volume' capability for the volume's namespace.


```

#### nomad volume delete

```shell
Usage: nomad volume delete [options] <vol id>

  Delete a volume from an external storage provider. The volume must still be
  registered with Nomad in order to be deleted. Deleting will fail if the
  volume is still in use by an allocation or in the process of being
  unpublished. If the volume no longer exists, this command will silently
  return without an error.

  When ACLs are enabled, this command requires a token with the
  'csi-write-volume' and 'csi-read-volume' capabilities for the volume's
  namespace.

Delete Options:

  -secret
    Secrets to pass to the plugin to delete the snapshot. Accepts multiple
    flags in the form -secret key=value


```

#### nomad volume deregister

```shell
Usage: nomad volume deregister [options] <id>

  Remove an unused volume from Nomad.

  When ACLs are enabled, this command requires a token with the
  'csi-write-volume' capability for the volume's namespace.

Volume Deregister Options:

  -force
    Force deregistration of the volume and immediately drop claims for
    terminal allocations. Returns an error if the volume has running
    allocations. This does not detach the volume from client nodes.

```

#### nomad volume detach

```shell
Usage: nomad volume detach [options] <vol id> <node id>

  Detach a volume from a Nomad client.

  When ACLs are enabled, this command requires a token with the
  'csi-write-volume' and 'csi-read-volume' capabilities for the volume's
  namespace.



```

#### nomad volume init 

```shell
Usage: nomad volume init <filename>

  Creates an example volume specification file that can be used as a starting
  point to customize further. If no filename is give, the default "volume.json"
  or "volume.hcl" will be used.

Init Options:

  -json
    Create an example JSON volume specification.

```

#### nomad volume register

```shell
Usage: nomad volume register [options] <input>

  Creates or updates a volume in Nomad. The volume must exist on the remote
  storage provider before it can be used by a task.

  If the supplied path is "-" the volume file is read from stdin. Otherwise, it
  is read from the file at the supplied path.

  When ACLs are enabled, this command requires a token with the
  'csi-write-volume' capability for the volume's namespace.

```

#### nomad volume snapshot

```shell
Usage: nomad volume snapshot <subcommand> [options] [args]

  This command groups subcommands for interacting with CSI volume snapshots.

  Create a snapshot of an external storage volume:

      $ nomad volume snapshot create <volume id>

  Display a list of CSI volume snapshots along with their
  source volume ID as known to the external storage provider.

      $ nomad volume snapshot list -plugin <plugin id>

  Delete a snapshot of an external storage volume:

      $ nomad volume snapshot delete <snapshot id>

  Please see the individual subcommand help for detailed usage information.

Subcommands:
    create    Snapshot a volume
    delete    Delete a snapshot
    list      Display a list of volume snapshots for plugin

```

#### nomad volume status

```shell
Usage: nomad volume status [options] <id>

  Display status information about a CSI volume. If no volume id is given, a
  list of all volumes will be displayed.

  When ACLs are enabled, this command requires a token with the
  'csi-read-volume' and 'csi-list-volumes' capability for the volume's
  namespace.


Status Options:

  -type <type>
    List only volumes of type <type>.

  -short
    Display short output. Used only when a single volume is being
    queried, and drops verbose information about allocations.

  -verbose
    Display full allocation information.

  -json
    Output the allocation in its JSON format.

  -t
    Format and display allocation using a Go template.

```





