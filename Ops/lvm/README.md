

pv vg lv数量命令查看

[Linux LVM学习 查看pg,vg,LV的命令_51CTO博客_linux lv命令](https://blog.51cto.com/u_1821342/2420298#:~:text=命令：lsblk 最后还可以使用vgs命令查看相关卷组信息%2CPV1个，LV 共有5个.命令：vgs 2，查看物理卷，卷组等信息，命令%3APVS,1%2CPV是第一块硬盘第5个分区%2Fdev%2Fsda5 2%2Cvg的名字%3Ddebian-vg 3%2Cvg的容量大小约%3D50G 4，剩余空间大小%3D12.5G，这个剩于的容量可以直接给上面的5个分区直接增加容量的。)



[LVM——让Linux磁盘空间的弹性管理 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/67166867)

https://zhuanlan.zhihu.com/p/67166867

[(5条消息) 简述Linux删除LVM的过程_金陵大掌柜的博客-CSDN博客](https://blog.csdn.net/Gao068465/article/details/121466890#:~:text=删除LVM的过程 1.先卸载系统上面的 LVM 文件系统 (包括快照与所有 LV) 2.使用 lvremove,VGname 让 VGname 这个 VG 不具有 Active 的标志)

[Linux 扩容 / 根分区(LVM+非LVM) - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/83340525)

需求： /home 50g  取消挂载并将该lvm归入到/目录所属的lvm中，扩充/磁盘的lvm容量，最后df -Th 时要看到/磁盘已经增加了/home的50G大小。



思路：

```shell
#1、卸载/home
umount /home
#2、删除/home的lvm 将lv的大小回收到vg
umount -f /nas && lvremove  /dev/mapper/centos-home -y 
#3、扩容/的lvm
lvextend -l  +100%free /dev/mapper/centos-root
#4、刷新/的lvm
xfs_growfs /dev/mapper/centos-root
# 查看
df -Th
```

