文档：https://www.bookstack.cn/read/dianping-cat/HOME.md

github readme:https://github.com/dianping/cat/wiki/readme_server

# 单机部署

## 安装jdk

方式一：

```shell
yum install java-1.8.0-openjdk-devel -y
java -version
```

方式二：

```shell
tar xf jdk-8u381-linux-x64.tar.gz
rm -rf jdk-8u381-linux-x64.tar.gz
mv jdk1.8.0_381 /usr/local/
echo "JAVA_HOME=/usr/local/jdk1.8.0_381">> /etc/profile
echo 'PATH=$JAVA_HOME/bin:$PATH'>> /etc/profile
echo 'CLASSPATH=$JAVA_HOME/jre/lib/ext:$JAVA_HOME/lib/tools.jar'>> /etc/profile
echo "export PATH JAVA_HOME CLASSPATH">> /etc/profile
source /etc/profile
java -version

```



## 安装Apache Tomcat

方式一：

```shell
yum install -y tomcat
tomcat version
yum install -y tomcat-webapps tomcat-admin-webapps 
yum install -y tomcat-docs-webapp tomcat-javadoc
systemctl restart tomcat
systemctl status tomcat
vi /usr/share/tomcat/conf/tomcat-users.xml
<tomcat-users>
   <user username="admin" password="password" roles="manager-gui,admin-gui"/>
</tomcat-users>
```

方式二：

```shell
tar zxvf apache-tomcat-8.5.91.tar.gz -C /usr/local/
rm -rf apache-tomcat-8.5.91.tar.gz
ln -s /usr/local/apache-tomcat-8.5.91 /usr/local/tomcat 
/usr/local/tomcat/bin/startup.sh 
```

## 安装maven

```shell
tar xf  apache-maven-3.9.4-bin.tar.gz
rm -rf apache-maven-3.9.4-bin.tar.gz
mv apache-maven-3.9.4 /usr/local/
echo  'export PATH=/usr/local/apache-maven-3.9.4/bin:$PATH'>> /etc/profile
source /etc/profile
mvn -v
```



## 安装mysql5.7

```shell
rpm -qa|grep mysql
yum install -y libaio.x86_64 numactl-libs.x86_64
tar -zxvf mysql-5.7.41-linux-glibc2.12-x86_64.tar.gz -C /usr/local
rm -rf  mysql-5.7.41-linux-glibc2.12-x86_64.tar.gz
cd /usr/local/
mv mysql-5.7.41-linux-glibc2.12-x86_64/ mysql
groupadd mysql
useradd -g mysql -s /sbin/nologin mysql
echo "export PATH=$PATH:/usr/local/mysql/bin" >> /etc/profile
source /etc/profile
mkdir -p /usr/local/mysql/{log,etc,run}
mkdir -p /data/mysql/{data,binlogs}
ln -s /data/mysql/data  /usr/local/mysql/data
ln -s /data/mysql/binlogs   /usr/local/mysql/binlogs
chown -R mysql.mysql /usr/local/mysql/{data,binlogs,log,etc,run}
chown -R mysql.mysql /data/mysql
rm -f /etc/my.cnf
cat > /etc/my.cnf <<EOF
[client]
port = 3306
socket = /usr/local/mysql/run/mysql.sock

[mysqld]
log_bin
port = 3306
socket = /usr/local/mysql/run/mysql.sock
pid_file = /usr/local/mysql/run/mysql.pid
datadir = /usr/local/mysql/data
default_storage_engine = InnoDB
max_allowed_packet = 512M
max_connections = 2048
open_files_limit = 65535

skip-name-resolve
lower_case_table_names=1

character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
init_connect='SET NAMES utf8mb4'


innodb_buffer_pool_size = 1024M
innodb_log_file_size = 2048M
innodb_file_per_table = 1
innodb_flush_log_at_trx_commit = 0


key_buffer_size = 64M

log-error = /usr/local/mysql/log/mysql_error.log
log-bin = /usr/local/mysql/binlogs/mysql-bin
slow_query_log = 1
slow_query_log_file = /usr/local/mysql/log/mysql_slow_query.log
long_query_time = 5


tmp_table_size = 32M
max_heap_table_size = 32M
query_cache_type = 0
query_cache_size = 0

server-id=1
EOF

 # 初始化MySQL
mysqld --initialize --user=mysql --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data

# systemd管理
cat >/usr/lib/systemd/system/mysqld.service <<EOF
[Unit]
Description=MySQL Server
Documentation=man:mysqld(8)
Documentation=http://dev.mysql.com/doc/refman/en/using-systemd.html
After=network.target
After=syslog.target

[Install]
WantedBy=multi-user.target

[Service]
User=mysql
Group=mysql

Type=forking

PIDFile=/usr/local/mysql/run/mysqld.pid

# Disable service start and stop timeout logic of systemd for mysqld service.
TimeoutSec=0

# Execute pre and post scripts as root
PermissionsStartOnly=true

# Needed to create system tables
#ExecStartPre=/usr/bin/mysqld_pre_systemd

# Start main service
ExecStart=/usr/local/mysql/bin/mysqld --daemonize --pid-file=/usr/local/mysql/run/mysqld.pid $MYSQLD_OPTS

# Use this to switch malloc implementation
EnvironmentFile=-/etc/sysconfig/mysql

# Sets open_files_limit
LimitNOFILE = 65535

Restart=on-failure

RestartPreventExitStatus=1

PrivateTmp=false
EOF

# 启动MySQL
systemctl daemon-reload
systemctl enable mysqld.service
systemctl is-enabled mysqld
systemctl restart mysqld.service
# 密码
grep 'temporary password' /usr/local/mysql/log/mysql_error.log
```



```shell

# 重新授权root连接
alter user 'root'@'localhost' identified by '123456';
grant all privileges on *.* to 'root'@'%' identified by '123456';
flush privileges;
cd /root/CatSoftware
```





## 部署cat前环境准备

### 设置maven国内源

```shell
vim /usr/local/apache-maven-3.9.4/conf/settings.xml
<mirror>
      <id>alimaven</id>
      <mirrorOf>central</mirrorOf>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
</mirror>

# mvn clean install -Dmaven.test.skip=true
```

### 拷贝cat.war包到tomcat目录

```shell
mv cat-home.war cat.war
mv cat.war /usr/local/tomcat/webapps/
```

### 创建mysql库并导入表结构

```shell
tar xf 3.1.0.tar.gz
rm -rf 3.1.0.tar.gz
mysql -uroot -p123456
create database if not exists cat default charset utf8 collate utf8_general_ci;
use cat;
source /root/CatSoftware/cat-3.1.0/script/CatApplication.sql
```

### 创建cat目录以及配置文件

```shell
mkdir -p /data/appdatas/cat/
cat > /data/appdatas/cat/datasources.xml <<EOF
<?xml version="1.0" encoding="utf-8"?>
<data-sources>
    <data-source id="cat">
        <maximum-pool-size>3</maximum-pool-size>
        <connection-timeout>1s</connection-timeout>
        <idle-timeout>10m</idle-timeout>
        <statement-cache-size>1000</statement-cache-size>
        <properties>
            <driver>com.mysql.jdbc.Driver</driver>
            <url><![CDATA[jdbc:mysql://10.0.0.10:3306/cat]]></url>  <!-- 请替换为真实数据库URL及Port  -->
            <user>root</user>  <!-- 请替换为真实数据库用户名  -->
            <password>123456</password>  <!-- 请替换为真实数据库密码  -->
            <connectionProperties><![CDATA[useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&socketTimeout=120000]]></connectionProperties>
        </properties>
    </data-source>
</data-sources>
EOF
# 创建client.xml
cat > /data/appdatas/cat/client.xml <<EOF
  <?xml version="1.0" encoding="utf-8"?>
  <config mode="client">
      <servers>
          <server ip="10.0.0.10" port="2280" http-port="8080"/>
          <server ip="10.0.0.11" port="2280" http-port="8080"/>
          <server ip="10.0.0.12" port="2280" http-port="8080"/>
      </servers>
  </config>
EOF

chmod -R 777 /data/
```

### 页面访问

```shell
#/usr/local/tomcat/bin/shutdown.sh
/usr/local/tomcat/bin/startup.sh
http://10.0.0.10:8080/cat/s/config?op=routerConfigUpdate




wget https://ghproxy.com/https://github.com/dianping/cat/archive/refs/tags/3.1.0.tar.gz
wget https://ghproxy.com/https://github.com/dianping/cat/releases/download/3.1.0/cat-home.war


/usr/local/jdk1.8.0_381/bin/java -Dhost.ip=10.0.0.10 -Djava.net.preferIPv4Stack=true -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephemeralDHKeySize=2048 -Djava.protocol.handler.pkgs=org.apache.catalina.webresources -Dorg.apache.catalina.security.SecurityListener.UMASK=0027 -Dignore.endorsed.dirs= -classpath /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar -Dcatalina.base=/usr/local/tomcat -Dcatalina.home=/usr/local/tomcat -Djava.io.tmpdir=/usr/local/tomcat/temp org.apache.catalina.startup.Bootstrap start





cat > /data/appdatas/cat/server.xml <<EOF
<?xml version="1.0" encoding="utf-8"?>
  <!-- Configuration for development environment-->
  <config local-mode="false" hdfs-machine="false" job-machine="true" alert-machine="true">
        <storage  local-base-dir="/data/appdatas/cat/bucket/" max-hdfs-storage-time="15" local-report-storage-time="7" local-logivew-storage-time="7">
        </storage>
        <console default-domain="Cat" show-cat-domain="true">
         <!--将172.16.90.114修改为部署CAT的内网IP,请不要写127.0.0.1和外网IP -->
             <remote-servers>10.0.0.10:8080</remote-servers>     
        </console>
   </config>
EOF
```

更新配置示例如下：

```shell
<?xml version="1.0" encoding="utf-8"?>
<router-config backup-server="10.0.0.10" backup-server-port="2280">
   <default-server id="10.0.0.10" weight="1.0" port="2280" enable="true"/>
   <default-server id="10.0.0.11" weight="1.0" port="2280" enable="true"/>
   <default-server id="10.0.0.12" weight="1.0" port="2280" enable="true"/>
   <network-policy id="default" title="default" block="false" server-group="default_group">
   </network-policy>
   <server-group id="default_group" title="default-group">
      <group-server id="10.0.0.11"/>
      <group-server id="10.0.0.12"/>
   </server-group>
   <domain id="cat">
      <group id="default">
         <server id="10.0.0.11" port="2280" weight="1.0"/>
         <server id="10.0.0.12" port="2280" weight="1.0"/>
      </group>
   </domain>
</router-config>
```

















github上好像有类似的错误，但是我不清楚具体是怎么导致的。

https://github.com/dianping/cat/issues/2109

![image-20230808182429355](assets/image-20230808182429355.png)





```shell


# 下载cat包
wget https://ghproxy.com/https://github.com/dianping/cat/releases/download/v3.0.0/cat-3.0.0.war
mv cat-3.0.0.war cat.war
mv cat.war /usr/share/tomcat/webapps

# 下载源码包获取sql语句
wget https://ghproxy.com/https://github.com/dianping/cat/archive/refs/tags/v3.0.0.tar.gz
tar xf v3.0.0.tar.gz


# 导入sql语句
mysql -uroot -p123456
create database if not exists cat_schema default charset utf8 collate utf8_general_ci;
use cat_schema;
source /root/cat-3.0.0/script/CatApplication.sql
# 启动tomcat
systemctl restart tomcat

```



## issue

我安装部署完成之后 访问http://ip:8080/cat/s/config?op=routerConfigUpdate 出现这个错误
![image](https://github.com/dianping/cat/assets/55818804/2f06b909-f9fb-49e3-885e-41649632f58e)
日志如下：

```shell
08-09 15:23:44.032] [INFO] [DefaultClientConfigManager] client xml path /data/appdatas/cat/client.xml
[08-09 15:23:44.039] [ERROR] [HttpServlet] Error occured when handling uri: /cat/s/config
java.lang.RuntimeException: Unable to lookup component(org.unidal.web.mvc.lifecycle.InboundActionHandler:default).
	at org.unidal.web.mvc.lifecycle.DefaultRequestLifecycle.handleException(DefaultRequestLifecycle.java:89)
	at org.unidal.web.mvc.lifecycle.DefaultRequestLifecycle.handleRequest(DefaultRequestLifecycle.java:192)
	at org.unidal.web.mvc.lifecycle.DefaultRequestLifecycle.handle(DefaultRequestLifecycle.java:63)
	at org.unidal.web.MVC.service(MVC.java:90)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:583)
	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:212)
	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:156)
	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:51)
	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:181)
	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:156)
	at com.dianping.cat.servlet.CatFilter$Context.handle(CatFilter.java:439)
	at com.dianping.cat.servlet.CatFilter$CatHandler$3.handle(CatFilter.java:236)
	at com.dianping.cat.servlet.CatFilter$Context.handle(CatFilter.java:437)
	at com.dianping.cat.servlet.CatFilter$CatHandler$4.handle(CatFilter.java:329)
	at com.dianping.cat.servlet.CatFilter$Context.handle(CatFilter.java:437)
	at com.dianping.cat.servlet.CatFilter$CatHandler$2.handle(CatFilter.java:219)
	at com.dianping.cat.servlet.CatFilter$Context.handle(CatFilter.java:437)
	at com.dianping.cat.servlet.CatFilter$CatHandler$1.handle(CatFilter.java:127)
	at com.dianping.cat.servlet.CatFilter$Context.handle(CatFilter.java:437)
	at com.dianping.cat.servlet.CatFilter.doFilter(CatFilter.java:65)
	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:181)
	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:156)
	at com.dianping.cat.system.page.permission.PermissionFilter.doFilter(PermissionFilter.java:94)
	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:181)
	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:156)
	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:167)
	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:90)
	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:483)
	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:130)
	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:93)
	at org.apache.catalina.valves.AbstractAccessLogValve.invoke(AbstractAccessLogValve.java:682)
	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:74)
	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:343)
	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:617)
	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:63)
	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:932)
	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1694)
	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:52)
	at org.apache.tomcat.util.threads.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1191)
	at org.apache.tomcat.util.threads.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:659)
	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)
	at java.lang.Thread.run(Thread.java:750)
Caused by: org.unidal.lookup.LookupException: Unable to lookup component(org.unidal.web.mvc.lifecycle.InboundActionHandler:default).
	at org.unidal.lookup.ContainerHolder.lookup(ContainerHolder.java:42)
	at org.unidal.lookup.ContainerHolder.lookup(ContainerHolder.java:33)
	at org.unidal.web.mvc.lifecycle.DefaultActionHandlerManager.getInboundActionHandler(DefaultActionHandlerManager.java:33)
	at org.unidal.web.mvc.lifecycle.DefaultRequestLifecycle.handleRequest(DefaultRequestLifecycle.java:167)
	... 40 more
Caused by: org.codehaus.plexus.component.repository.exception.ComponentLookupException: Unable to lookup component 'org.unidal.web.mvc.lifecycle.InboundActionHandler', it could not be started.
      role: org.unidal.web.mvc.lifecycle.InboundActionHandler
  roleHint: default
classRealm: plexus.core
-----------------------------------------------------
realm =    plexus.core
strategy = org.codehaus.plexus.classworlds.strategy.SelfFirstStrategy
Number of foreign imports: 0

-----------------------------------------------------

	at org.codehaus.plexus.DefaultComponentRegistry.getComponent(DefaultComponentRegistry.java:373)
	at org.codehaus.plexus.DefaultComponentRegistry.lookup(DefaultComponentRegistry.java:178)
	at org.codehaus.plexus.DefaultPlexusContainer.lookup(DefaultPlexusContainer.java:388)
	at org.unidal.lookup.ContainerHolder.lookup(ContainerHolder.java:38)
	... 43 more
Caused by: org.codehaus.plexus.component.repository.exception.ComponentLifecycleException: Error constructing component role: 'org.unidal.web.mvc.lifecycle.InboundActionHandler', implementation: 'org.unidal.web.mvc.lifecycle.DefaultInboundActionHandler', role hint: 'default'
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.createComponentInstance(XBeanComponentBuilder.java:176)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.build(XBeanComponentBuilder.java:123)
	at org.codehaus.plexus.component.manager.AbstractComponentManager.createComponentInstance(AbstractComponentManager.java:181)
	at org.codehaus.plexus.component.manager.PerLookupComponentManager.getComponent(PerLookupComponentManager.java:51)
	at org.codehaus.plexus.DefaultComponentRegistry.getComponent(DefaultComponentRegistry.java:358)
	... 46 more
Caused by: org.apache.xbean.recipe.ConstructionException: Composition failed of field null in object of type org.unidal.web.mvc.lifecycle.DefaultInboundActionHandler because the requirement ComponentRequirement{role='com.dianping.cat.message.MessageProducer', roleHint='', fieldName='null'} was missing)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder$RequirementRecipe.internalCreate(XBeanComponentBuilder.java:413)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:96)
	at org.apache.xbean.recipe.RecipeHelper.convert(RecipeHelper.java:167)
	at org.apache.xbean.recipe.ObjectRecipe.setProperty(ObjectRecipe.java:517)
	at org.apache.xbean.recipe.ObjectRecipe.setProperties(ObjectRecipe.java:385)
	at org.apache.xbean.recipe.ObjectRecipe.internalCreate(ObjectRecipe.java:300)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:96)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:61)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:49)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.createComponentInstance(XBeanComponentBuilder.java:159)
	... 50 more
Caused by: org.codehaus.plexus.component.repository.exception.ComponentLookupException: Unable to lookup component 'com.dianping.cat.message.MessageProducer', it could not be started.
      role: com.dianping.cat.message.MessageProducer
  roleHint: default
classRealm: plexus.core
-----------------------------------------------------
realm =    plexus.core
strategy = org.codehaus.plexus.classworlds.strategy.SelfFirstStrategy
Number of foreign imports: 0

-----------------------------------------------------

	at org.codehaus.plexus.DefaultComponentRegistry.getComponent(DefaultComponentRegistry.java:373)
	at org.codehaus.plexus.DefaultComponentRegistry.lookup(DefaultComponentRegistry.java:178)
	at org.codehaus.plexus.DefaultPlexusContainer.lookup(DefaultPlexusContainer.java:393)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder$RequirementRecipe.internalCreate(XBeanComponentBuilder.java:402)
	... 59 more
Caused by: org.codehaus.plexus.component.repository.exception.ComponentLifecycleException: Error constructing component role: 'com.dianping.cat.message.MessageProducer', implementation: 'com.dianping.cat.message.internal.DefaultMessageProducer', role hint: 'default'
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.createComponentInstance(XBeanComponentBuilder.java:176)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.build(XBeanComponentBuilder.java:123)
	at org.codehaus.plexus.component.manager.AbstractComponentManager.createComponentInstance(AbstractComponentManager.java:181)
	at org.codehaus.plexus.component.manager.SingletonComponentManager.getComponent(SingletonComponentManager.java:67)
	at org.codehaus.plexus.DefaultComponentRegistry.getComponent(DefaultComponentRegistry.java:358)
	... 62 more
Caused by: org.apache.xbean.recipe.ConstructionException: Composition failed of field null in object of type com.dianping.cat.message.internal.DefaultMessageProducer because the requirement ComponentRequirement{role='com.dianping.cat.message.spi.MessageManager', roleHint='', fieldName='null'} was missing)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder$RequirementRecipe.internalCreate(XBeanComponentBuilder.java:413)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:96)
	at org.apache.xbean.recipe.RecipeHelper.convert(RecipeHelper.java:167)
	at org.apache.xbean.recipe.ObjectRecipe.setProperty(ObjectRecipe.java:517)
	at org.apache.xbean.recipe.ObjectRecipe.setProperties(ObjectRecipe.java:385)
	at org.apache.xbean.recipe.ObjectRecipe.internalCreate(ObjectRecipe.java:300)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:96)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:61)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:49)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.createComponentInstance(XBeanComponentBuilder.java:159)
	... 66 more
Caused by: org.codehaus.plexus.component.repository.exception.ComponentLookupException: Unable to lookup component 'com.dianping.cat.message.spi.MessageManager', it could not be started.
      role: com.dianping.cat.message.spi.MessageManager
  roleHint: default
classRealm: plexus.core
-----------------------------------------------------
realm =    plexus.core
strategy = org.codehaus.plexus.classworlds.strategy.SelfFirstStrategy
Number of foreign imports: 0

-----------------------------------------------------

	at org.codehaus.plexus.DefaultComponentRegistry.getComponent(DefaultComponentRegistry.java:373)
	at org.codehaus.plexus.DefaultComponentRegistry.lookup(DefaultComponentRegistry.java:178)
	at org.codehaus.plexus.DefaultPlexusContainer.lookup(DefaultPlexusContainer.java:393)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder$RequirementRecipe.internalCreate(XBeanComponentBuilder.java:402)
	... 75 more
Caused by: org.codehaus.plexus.component.repository.exception.ComponentLifecycleException: Error constructing component role: 'com.dianping.cat.message.spi.MessageManager', implementation: 'com.dianping.cat.message.internal.DefaultMessageManager', role hint: 'default'
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.createComponentInstance(XBeanComponentBuilder.java:176)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.build(XBeanComponentBuilder.java:123)
	at org.codehaus.plexus.component.manager.AbstractComponentManager.createComponentInstance(AbstractComponentManager.java:181)
	at org.codehaus.plexus.component.manager.SingletonComponentManager.getComponent(SingletonComponentManager.java:67)
	at org.codehaus.plexus.DefaultComponentRegistry.getComponent(DefaultComponentRegistry.java:358)
	... 78 more
Caused by: org.apache.xbean.recipe.ConstructionException: Composition failed of field null in object of type com.dianping.cat.message.internal.DefaultMessageManager because the requirement ComponentRequirement{role='com.dianping.cat.configuration.ClientConfigManager', roleHint='', fieldName='null'} was missing)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder$RequirementRecipe.internalCreate(XBeanComponentBuilder.java:413)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:96)
	at org.apache.xbean.recipe.RecipeHelper.convert(RecipeHelper.java:167)
	at org.apache.xbean.recipe.ObjectRecipe.setProperty(ObjectRecipe.java:517)
	at org.apache.xbean.recipe.ObjectRecipe.setProperties(ObjectRecipe.java:385)
	at org.apache.xbean.recipe.ObjectRecipe.internalCreate(ObjectRecipe.java:300)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:96)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:61)
	at org.apache.xbean.recipe.AbstractRecipe.create(AbstractRecipe.java:49)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.createComponentInstance(XBeanComponentBuilder.java:159)
	... 82 more
Caused by: org.codehaus.plexus.component.repository.exception.ComponentLookupException: Unable to lookup component 'com.dianping.cat.configuration.ClientConfigManager', it could not be started.
      role: com.dianping.cat.configuration.ClientConfigManager
  roleHint: default
classRealm: plexus.core
-----------------------------------------------------
realm =    plexus.core
strategy = org.codehaus.plexus.classworlds.strategy.SelfFirstStrategy
Number of foreign imports: 0

-----------------------------------------------------

	at org.codehaus.plexus.DefaultComponentRegistry.getComponent(DefaultComponentRegistry.java:373)
	at org.codehaus.plexus.DefaultComponentRegistry.lookup(DefaultComponentRegistry.java:178)
	at org.codehaus.plexus.DefaultPlexusContainer.lookup(DefaultPlexusContainer.java:393)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder$RequirementRecipe.internalCreate(XBeanComponentBuilder.java:402)
	... 91 more
Caused by: org.codehaus.plexus.component.repository.exception.ComponentLifecycleException: Error starting component
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.startComponentLifecycle(XBeanComponentBuilder.java:285)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.build(XBeanComponentBuilder.java:129)
	at org.codehaus.plexus.component.manager.AbstractComponentManager.createComponentInstance(AbstractComponentManager.java:181)
	at org.codehaus.plexus.component.manager.SingletonComponentManager.getComponent(SingletonComponentManager.java:67)
	at org.codehaus.plexus.DefaultComponentRegistry.getComponent(DefaultComponentRegistry.java:358)
	... 94 more
Caused by: org.codehaus.plexus.personality.plexus.lifecycle.phase.PhaseExecutionException: Error initialising component
	at org.codehaus.plexus.personality.plexus.lifecycle.phase.InitializePhase.execute(InitializePhase.java:37)
	at org.codehaus.plexus.lifecycle.AbstractLifecycleHandler.start(AbstractLifecycleHandler.java:96)
	at org.codehaus.plexus.component.manager.AbstractComponentManager.start(AbstractComponentManager.java:167)
	at org.codehaus.plexus.component.builder.XBeanComponentBuilder.startComponentLifecycle(XBeanComponentBuilder.java:283)
	... 98 more
Caused by: org.codehaus.plexus.personality.plexus.lifecycle.phase.InitializationException: The processing instruction target matching "[xX][mM][lL]" is not allowed.
	at com.dianping.cat.configuration.DefaultClientConfigManager.initialize(DefaultClientConfigManager.java:189)
	at com.dianping.cat.configuration.DefaultClientConfigManager.initialize(DefaultClientConfigManager.java:154)
	at org.codehaus.plexus.personality.plexus.lifecycle.phase.InitializePhase.execute(InitializePhase.java:33)
	... 101 more
Caused by: org.xml.sax.SAXParseException; lineNumber: 1; columnNumber: 8; The processing instruction target matching "[xX][mM][lL]" is not allowed.
	at com.sun.org.apache.xerces.internal.util.ErrorHandlerWrapper.createSAXParseException(ErrorHandlerWrapper.java:204)
	at com.sun.org.apache.xerces.internal.util.ErrorHandlerWrapper.fatalError(ErrorHandlerWrapper.java:178)
	at com.sun.org.apache.xerces.internal.impl.XMLErrorReporter.reportError(XMLErrorReporter.java:400)
	at com.sun.org.apache.xerces.internal.impl.XMLErrorReporter.reportError(XMLErrorReporter.java:327)
	at com.sun.org.apache.xerces.internal.impl.XMLScanner.reportFatalError(XMLScanner.java:1467)
	at com.sun.org.apache.xerces.internal.impl.XMLScanner.scanPIData(XMLScanner.java:747)
	at com.sun.org.apache.xerces.internal.impl.XMLDocumentFragmentScannerImpl.scanPIData(XMLDocumentFragmentScannerImpl.java:1016)
	at com.sun.org.apache.xerces.internal.impl.XMLScanner.scanPI(XMLScanner.java:715)
	at com.sun.org.apache.xerces.internal.impl.XMLDocumentScannerImpl$PrologDriver.next(XMLDocumentScannerImpl.java:922)
	at com.sun.org.apache.xerces.internal.impl.XMLDocumentScannerImpl.next(XMLDocumentScannerImpl.java:605)
	at com.sun.org.apache.xerces.internal.impl.XMLDocumentFragmentScannerImpl.scanDocument(XMLDocumentFragmentScannerImpl.java:507)
	at com.sun.org.apache.xerces.internal.parsers.XML11Configuration.parse(XML11Configuration.java:867)
	at com.sun.org.apache.xerces.internal.parsers.XML11Configuration.parse(XML11Configuration.java:796)
	at com.sun.org.apache.xerces.internal.parsers.XMLParser.parse(XMLParser.java:142)
	at com.sun.org.apache.xerces.internal.parsers.AbstractSAXParser.parse(AbstractSAXParser.java:1216)
	at com.sun.org.apache.xerces.internal.jaxp.SAXParserImpl$JAXPSAXParser.parse(SAXParserImpl.java:644)
	at com.sun.org.apache.xerces.internal.jaxp.SAXParserImpl.parse(SAXParserImpl.java:328)
	at com.dianping.cat.configuration.client.transform.DefaultSaxParser.parseEntity(DefaultSaxParser.java:71)
	at com.dianping.cat.configuration.client.transform.DefaultSaxParser.parse(DefaultSaxParser.java:59)
	at com.dianping.cat.configuration.DefaultClientConfigManager.initialize(DefaultClientConfigManager.java:167)
	... 103 more
[08-09 15:24:00.552] [INFO] [DefaultModuleContext] Starting thread(Cat-ModelService-3) ...
[08-09 15:25:00.559] [INFO] [DefaultModuleContext] Starting thread(Cat-ModelService-4) ...
[08-09 15:26:00.536] [INFO] [DefaultModuleContext] Starting thread(Cat-ModelService-5) ...
[08-09 15:27:00.576] [INFO] [DefaultModuleContext] Starting thread(Cat-ModelService-6) ...
[08-09 15:28:00.599] [INFO] [DefaultModuleContext] Starting thread(Cat-ModelService-7) ...
[08-09 15:29:02.294] [INFO] [DefaultModuleContext] Starting thread(Cat-ModelService-8) ...
[08-09 15:30:02.302] [INFO] [DefaultModuleContext] Starting thread(Cat-ModelService-9) ...
[08-09 15:31:01.357] [INFO] [DefaultModuleContext] Starting thread(Cat-ModelService-10) ...
[08-09 15:31:09.438] [INFO] [DefaultClientConfigManager] client xml path /data/appdatas/cat/client.xml
[08-09 15:31:10.656] [ERROR] [HttpServlet] Error occured when handling uri: /cat/s/config

```

我的环境信息以及启动命令：
```shell
[root@localhost cat]# /usr/local/jdk1.8.0_381/bin/java -Dhost.ip=10.0.0.10 -Djava.net.preferIPv4Stack=true -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephemeralDHKeySize=2048 -Djava.protocol.handler.pkgs=org.apache.catalina.webresources -Dorg.apache.catalina.security.SecurityListener.UMASK=0027 -Dignore.endorsed.dirs= -classpath /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar -Dcatalina.base=/usr/local/tomcat -Dcatalina.home=/usr/local/tomcat -Djava.io.tmpdir=/usr/local/tomcat/temp org.apache.catalina.startup.Bootstrap start
09-Aug-2023 15:50:49.037 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version name:   Apache Tomcat/8.5.91
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server built:          Jul 6 2023 14:43:48 UTC
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Server version number: 8.5.91.0
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Name:               Linux
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log OS Version:            3.10.0-957.el7.x86_64
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Architecture:          amd64
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Java Home:             /usr/local/jdk1.8.0_381/jre
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Version:           1.8.0_381-b09
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log JVM Vendor:            Oracle Corporation
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_BASE:         /usr/local/apache-tomcat-8.5.91
09-Aug-2023 15:50:49.050 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_HOME:         /usr/local/apache-tomcat-8.5.91
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dhost.ip=10.0.0.10
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.net.preferIPv4Stack=true
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djdk.tls.ephemeralDHKeySize=2048
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.protocol.handler.pkgs=org.apache.catalina.webresources
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dorg.apache.catalina.security.SecurityListener.UMASK=0027
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dignore.endorsed.dirs=
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.base=/usr/local/tomcat
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Dcatalina.home=/usr/local/tomcat
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log Command line argument: -Djava.io.tmpdir=/usr/local/tomcat/temp
09-Aug-2023 15:50:49.051 INFO [main] org.apache.catalina.core.AprLifecycleListener.lifecycleEvent The Apache Tomcat Native library which allows using OpenSSL was not found on the java.library.path: [/usr/java/packages/lib/amd64:/usr/lib64:/lib64:/lib:/usr/lib]
09-Aug-2023 15:50:49.380 INFO [main] org.apache.coyote.AbstractProtocol.init Initializing ProtocolHandler ["http-nio-8080"]
09-Aug-2023 15:50:49.411 INFO [main] org.apache.catalina.startup.Catalina.load Initialization processed in 1203 ms
09-Aug-2023 15:50:49.493 INFO [main] org.apache.catalina.core.StandardService.startInternal Starting service [Catalina]
09-Aug-2023 15:50:49.494 INFO [main] org.apache.catalina.core.StandardEngine.startInternal Starting Servlet engine: [Apache Tomcat/8.5.91]
09-Aug-2023 15:50:49.522 INFO [localhost-startStop-1] org.apache.catalina.startup.HostConfig.deployWAR Deploying web application archive [/usr/local/apache-tomcat-8.5.91/webapps/cat.war]
```
我部署的war包版本：cat-3.0.0.war（从cat的github release处下载的）



页面访问









集群部署



监控系统日志



监控业务日志



web页面查询日志



