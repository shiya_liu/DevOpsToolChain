

https://www.mysqlzh.com/

## 二进制部署MySQL5.7

```shell
# 检查环境
rpm -qa|grep mysql
# 安装依赖
yum install -y libaio.x86_64 numactl-libs.x86_64
# 下载MySQL
https://dev.mysql.com/downloads/mysql/5.7.html#downloads
or
wget https://downloads.mysql.com/archives/get/p/23/file/mysql-5.7.41-linux-glibc2.12-x86_64.tar.gz
# 安装MySQL
tar -zxvf mysql-5.7.41-linux-glibc2.12-x86_64.tar.gz -C /usr/local
cd /usr/local/
mv mysql-5.7.41-linux-glibc2.12-x86_64/ mysql
或者
ln -s mysql-5.7.41-linux-glibc2.12-x86_64 mysql
 # 创建MySQL用户
groupadd mysql
useradd -g mysql -s /sbin/nologin mysql
 # 配置PATH
echo "export PATH=$PATH:/usr/local/mysql/bin" >> /etc/profile
source /etc/profile
# 数据库目录规划
mkdir -p /usr/local/mysql/{log,etc,run}
mkdir -p /data/mysql/{data,binlogs}
ln -s /data/mysql/data  /usr/local/mysql/data
ln -s /data/mysql/binlogs   /usr/local/mysql/binlogs
chown -R mysql.mysql /usr/local/mysql/{data,binlogs,log,etc,run}
chown -R mysql.mysql /data/mysql
# 配置my.cnf参数文件
rm -f /etc/my.cnf
cat > /etc/my.cnf <<EOF
[client]
port = 3306
socket = /usr/local/mysql/run/mysql.sock

[mysqld]
log_bin
port = 3306
socket = /usr/local/mysql/run/mysql.sock
pid_file = /usr/local/mysql/run/mysql.pid
datadir = /usr/local/mysql/data
default_storage_engine = InnoDB
max_allowed_packet = 512M
max_connections = 2048
open_files_limit = 65535

skip-name-resolve
lower_case_table_names=1

character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
init_connect='SET NAMES utf8mb4'


innodb_buffer_pool_size = 1024M
innodb_log_file_size = 2048M
innodb_file_per_table = 1
innodb_flush_log_at_trx_commit = 0


key_buffer_size = 64M

log-error = /usr/local/mysql/log/mysql_error.log
log-bin = /usr/local/mysql/binlogs/mysql-bin
slow_query_log = 1
slow_query_log_file = /usr/local/mysql/log/mysql_slow_query.log
long_query_time = 5


tmp_table_size = 32M
max_heap_table_size = 32M
query_cache_type = 0
query_cache_size = 0

server-id=1
EOF

 # 初始化MySQL
mysqld --initialize --user=mysql --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data

# systemd管理
cat >/usr/lib/systemd/system/mysqld.service <<EOF
[Unit]
Description=MySQL Server
Documentation=man:mysqld(8)
Documentation=http://dev.mysql.com/doc/refman/en/using-systemd.html
After=network.target
After=syslog.target

[Install]
WantedBy=multi-user.target

[Service]
User=mysql
Group=mysql

Type=forking

PIDFile=/usr/local/mysql/run/mysqld.pid

# Disable service start and stop timeout logic of systemd for mysqld service.
TimeoutSec=0

# Execute pre and post scripts as root
PermissionsStartOnly=true

# Needed to create system tables
#ExecStartPre=/usr/bin/mysqld_pre_systemd

# Start main service
ExecStart=/usr/local/mysql/bin/mysqld --daemonize --pid-file=/usr/local/mysql/run/mysqld.pid $MYSQLD_OPTS

# Use this to switch malloc implementation
EnvironmentFile=-/etc/sysconfig/mysql

# Sets open_files_limit
LimitNOFILE = 65535

Restart=on-failure

RestartPreventExitStatus=1

PrivateTmp=false
EOF
# 启动MySQL
systemctl daemon-reload
systemctl enable mysqld.service
systemctl is-enabled mysqld
systemctl restart mysqld.service
# 密码
grep 'temporary password' /usr/local/mysql/log/mysql_error.log
# 修改root密码
 alter user 'root'@'localhost' identified by 'rH>ygi,9ylTa';
```



## 搭建主从同步

```shell
# 关闭防火墙
systemctl status firewalld
# 主从数据库都需要开启log_bin
cat /etc/my.cnf
[mysqld]
log_bin
# 主从数据库的server-id保证不一样
cat /etc/my.cnf|grep server-id
server-id=2
cat /etc/my.cnf |grep server-id
server-id=1

# 主数据库授权一个用户
grant replication slave on *.* to 'test'@'%' identified by '123456';
flush privileges;
# 主数据库查看binlog信息
mysql> show master status;
+------------------+----------+--------------+------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+------------------+----------+--------------+------------------+-------------------+
| mysql-bin.000001 |      154 |              |                  |                   |
+------------------+----------+--------------+------------------+-------------------+

# 从数据库机器 登录验证是否可以登录主数据库
mysql -utest -p -h<master_ip>
# 从数据库中设置主从关系
change master to master_host='xxx', master_user='xxx', master_password='xxx', master_log_file='mysql-bin.000174', master_log_pos=628609930, master_connect_retry=30;
start slave;
show slave status\G; 
# 如果设置失败需要清空重新设置 （从节点操作）
stop slave;
reset slave all;
show slave status\G
```













```shell
思路：
1、主库停止写入 done
flush tables with read lock;
2、备份主库的数据库  done
mysqldump -uhzaxb -p -h xxx -p xxx > cc_detection.sql
3、导入从库 todo
mysql -uhzaxb -p -h xxx -p cc_detection < cc_detection.sql
4、设置主从复制 done
change master to master_host='xxx', master_user='xxx', master_password='xxx', master_log_file='mysql-bin.000174', master_log_pos=628609930, master_connect_retry=30;
start slave;
show slave status\G; 
5、解锁主库
unlock tables;
6、主库导入TIDB数据 
myloader -xxx -p xxx  -h xxx -o -d /home/axb/20230516_tidb_backup/tidb_backup -B  dx_privc
```





常用的sql语句
```shell
#统计表条数
select count(*) from call_record;
# 授权远程登录
grant all privileges on *.* to 'root'@'%' identified by 'xxx';
flush privileges;

# mysqldump方式导出导入表

# mysqldump的方式 单表单独备份 导入
mysqldump  -u root -p -P 4000 -h xxx -t dx_privc --tables company_call_stat_day company_call_stat_day_detail company_call_stat_month  company_call_stat_month_detail > buchong.sql
# 导入
mysql -u hzaxb -pxxx  -h xxx-D dx_privc <  buchong.sql

#查看已经授权的用户：
select user,host from mysql.user;

#查看用户的授权权限：
select * from information_schema.user_privileges;




```









问题记录：

```shell
1、webshell长时间不操作导致关闭，并且msyql -uroot -pxxx -p cc_detection < cc_detection.sql 命令不支持免密，很奇怪。
解决方案：
yum install -y screen 
screen -xRR 开启一个新的进程
执行命令： msyql -uroot -pxxx -p cc_detection < cc_detection.sql 输入密码后
CRTL +A, D 就放到后台运行了
参考：[linux 在终端打开程序后关闭终端，程序也跟着关闭了怎么办？ - 知乎 (zhihu.com)](https://www.zhihu.com/question/442188249#:~:text=1 按下Ctrl-z，让程序进入suspend（这个词中文是——挂起？ ）状态。 2 这个时候你应该会看到 [1]%2B Stopped xxxx,） 4 然后再执行 disown ，解除你现在的shell跟刚才这个进程的所属关系。 这个时候再执行jobs，就看不到那个进程了。 5 现在就可以关掉终端，下班回家了。)


2、mysql免密
参考：https://blog.csdn.net/w1054230914/article/details/116749730
第四种方法：expect

3、myloader将输入导入到主从同步的库中时需要指定-e参数开启binlog日志
myloader -u xxx -p xxxx -h xxxx -e -o -d /home/axb/20230516_tidb_backup/tidb_backup -B dx_privc

4、A库和B库做主从同步，A库作为主库并且存在一些数据，如何让A库和B库做主从同步？
首先要将A库中的数据进行导出并导入到B库中，然后再开始做主从同步；

```
按照日期导出库中的某个表
```shell
# 按照日期导出数据
mysqldump  -u root -p -P 4000 -h xxxx -t dx_privc --tables call_record -w "createTime >= '2023-05-16 00:00:00' " >call_record-20230516.sql
```


导出mysql中某张表特定的字段
```shell
参考文章：
https://blog.csdn.net/happyitlife/article/details/9704899
出现了提示：
ERROR 1290 (HY000): The MySQL server is running with the --secure-file-priv option so it cannot execute this statement
文章参考：https://www.cnblogs.com/index01/articles/12761609.html

mysql -u username -ppassword -h ipaddress  databasename -e "select number from tel_display_number where status = '1' INTO OUTFILE './子自己定义名字.sql'"

```

mysql压测
https://zhuanlan.zhihu.com/p/340527565



学习到了:

1. [ ] TIDB的备份和还原：[MySQL备份迁移之mydumper - immaxfang - 博客园 (cnblogs.com)](https://www.cnblogs.com/immaxfang/p/16188555.html#:~:text=%23 个人实际中最常用的导入语句 myloader -h 192.168.0.192 -P 33306 -u,-d %2Fhome%2Fmydumper%2Fdata%2Fdb1%2F0%2F %23 导入特定的某几张表 %23%23 先将 metadata 文件和需要单独导入的表的结构文件和数据文件导入到单独的文件夹中。)
2. [ ] 数据库主从搭建GTID：https://www.shuzhiduo.com/A/VGzlKr7xdb/
3. [ ] 容器形式主从搭建：https://zhuanlan.zhihu.com/p/86966579
4. [ ] 二进制安装mysql：https://blog.csdn.net/weixin_45947267/article/details/106817576







mysql VS tidb

数据库备份工具选型
逻辑备份：
mysqldump
mydumper myloader

物理备份
cp
xtrabackup


### 问题

[mysql用root用户给其他用户授权报错1044 access denied for user root - 千年寒冰火 - 博客园 (cnblogs.com)](https://www.cnblogs.com/qumogu/p/13504082.html)

```shell
grant all privileges on dx_privc.* to 'hzaxb'@'172.22.143.%' identified by 'xxxx';
flush privileges;
```




### mysql机器cpu占用率很高的排查方式

前提： top查看cpu的负载，1分钟负载在1.0以上就可以认为cpu负载有些压力了

我遇到的场景是一分钟负载2.0左右

排查方式：

方式一：

1、查看当前运行的线程状态

```shell
SHOW FULL PROCESSLIST;
```

2、查看正在执行的查询语句：

```shell
SELECT * FROM INFORMATION_SCHEMA.PROCESSLIST WHERE COMMAND != 'Sleep';
```

这里提醒一下：

如果在程序中使用的mysql连接池，执行这条语句时会出现很多sleep的线程，这个并不会导致cpu很高，因为他们是由线程池进行的连接，并没有执行语句，只是sleep而已，会占用mysql以及机器的打开文件数，并不会导致cpu负载变大

如果研发同意的话，可以使用下面的语句暂时kill掉线程，查看是否可以解决问题。

```sql
KILL QUERY query_id;
```



3、查看慢查询日志是否开启：

```shell
SHOW VARIABLES LIKE 'slow_query_log';
```

4、查看慢查询的日志路径和文件名，进而分析慢日志

```shell
SHOW VARIABLES LIKE 'slow_query_log_file';
```



注意： 单次执行可能并不会很慢，比如我这次遇到的场景，单语句select查询0.52秒（手动执行了一下），表是60万条数据

```shell
# 查看某表有多少条数据
SELECT COUNT(*) FROM table_name;
```

但是如果程序一直在发送select，不停的select，也会导致cpu标高，并且使用prometheus、或者登录mysql查询慢语句也无法查到（因为单条执行是还可以的0.52s钟）。

尝试和研发沟通，将该查询的程序进行关闭，进行测试下，发现果真是这个语句导致的。





























































