本系列文档主要参考

- MongoDB权威指南（第三版）![image-20230712143121106](assets/image-20230712143121106.png)
- [MongoDB官方文档](https://www.mongodb.com/docs/manual/tutorial/getting-started/)
- 疑惑解答来源于网络以及文心一言
- [MongoDB中文文档](https://mongodb.net.cn/manual/)



# 内容

[开始前的疑惑](https://gitee.com/shiya_liu/DevOpsToolChain/blob/main/OperatingSystem/mongodb/0%E5%BC%80%E5%A7%8B%E5%89%8D%E7%9A%84%E7%96%91%E6%83%91.md)

[安装部署](https://gitee.com/shiya_liu/DevOpsToolChain/blob/main/OperatingSystem/mongodb/1%E5%AE%89%E8%A3%85%E9%83%A8%E7%BD%B2.md)

简介

[增删改查](https://gitee.com/shiya_liu/DevOpsToolChain/blob/main/OperatingSystem/mongodb/2%E5%A2%9E%E5%88%A0%E6%94%B9%E6%9F%A5.md)

[索引&聚合&事务](https://gitee.com/shiya_liu/DevOpsToolChain/blob/main/OperatingSystem/mongodb/3%E7%B4%A2%E5%BC%95&%E8%81%9A%E5%90%88&%E4%BA%8B%E5%8A%A1.md)

[导入&导出&备份&恢复](https://gitee.com/shiya_liu/DevOpsToolChain/blob/main/OperatingSystem/mongodb/4%E5%AF%BC%E5%85%A5&%E5%AF%BC%E5%87%BA&%E5%A4%87%E4%BB%BD&%E6%81%A2%E5%A4%8D.md)

[副本集](https://gitee.com/shiya_liu/DevOpsToolChain/blob/main/OperatingSystem/mongodb/5%E5%89%AF%E6%9C%AC%E9%9B%86.md)

[分片集群](https://gitee.com/shiya_liu/DevOpsToolChain/blob/main/OperatingSystem/mongodb/6%E5%88%86%E7%89%87%E9%9B%86%E7%BE%A4.md)

[相关脚本](https://gitee.com/shiya_liu/DevOpsToolChain/tree/main/OperatingSystem/mongodb/script)

命令&配置文件详解

[问题记录](https://gitee.com/shiya_liu/DevOpsToolChain/blob/main/OperatingSystem/mongodb/%E9%97%AE%E9%A2%98%E8%AE%B0%E5%BD%95.md)






## 索引



## 视图









## 聚合







## 事务







## mongosh









## 导入数据

mongoimport







## MongoDB备份









## 恢复MongoDB中误删误更改的数据







## 用户认证










### 创建副本集时设置优先级

```shell
优先级用于表示一个成员“渴望”成为主节点的程度。它的取值范围是 0 到 100，默认是 1。将 "priority" 设置为 0 有特殊含义：优先级为 0 的成员永远不能成为主节点。这样的成员称为被动（passive）成员。
拥有最高优先级的成员总是会被选举为主节点（只要它能连接到副本集中的大多数成员，并且拥有最新的数据）
> rs.add({"host" : "server-4:27017", "priority" : 1.5})

假设其他成员的优先级都是 1，只要 server-4 拥有最新的数据，当前的主节点就会自动退位，并且 server-4 会被选举为新的主节点。如果 server-4 由于某些原因未能拥有最新的数据，那么当前主节点就会保持不变。设置优先级并不会导致副本集中无法选举出主节点，也不会使在数据同步中落后的成员成为主节点（一直到它的数据更新到最新）
"priority" 的绝对值只与它是否大于或小于副本集中的其他优先级有关：优先级为100、1 和 1 的一组成员与优先级为 2、1 和 1 的另一组成员行为方式相同。
```

### 隐藏成员

```shell
客户端不会向隐藏成员发送请求，隐藏成员也不会优先作为副本集的数据源（尽管当其他复制源不可用时隐藏成员也会被使用）。
因此，很多人会将性能较弱的服务器或者备份服务器隐藏起来。
假设有一个副本集如下所示：
> rs.isMaster()
{
    ...
    "hosts" : [
        "server-1:27107",
        "server-2:27017",
        "server-3:27017"
    ],
    ...
}
要隐藏 server-3，可以在它的配置中指定 hidden: true。只有优先级为 0 的成员才能被隐藏（不能隐藏主节点）：
> var config = rs.config()
> config.members[2].hidden = true
0
> config.members[2].priority = 0
0
> rs.reconfig(config)
现在，运行 isMaster 可以看到如下结果：
> rs.isMaster()
{
    ...
    "hosts" : [
        "server-1:27107",
        "server-2:27017"
    ],
    ...
}


使用 rs.status() 和 rs.config() 能够看到隐藏成员，隐藏成员只对 isMaster 不可见。
当客户端连接到副本集时，会调用 isMaster 来查看副本集中的成员。
因此，隐藏成员永远不会收到客户端的读请求。
要将隐藏成员设为非隐藏，只需将配置中的 hidden 设为 false，或者彻底删除此选项。
```

### 选举仲裁者

```shell
对于大多数需求来说，两节点副本集具有明显的缺点。然而，许多小型部署不希望保存 3 份数据副本集，觉得两份副本集就足够了，而保存第三份副本集会付出不必要的管理、操作和财务成本。
对于这种部署，MongoDB 支持一种特殊类型的成员，称为仲裁者（arbiter），其唯一作用就是参与选举。
仲裁者并不保存数据，也不会为客户端提供服务：它只是为了帮助具有两个成员的副本集满足“大多数”这个条件。通常来说，最好使用没有仲裁者的部署。
可以使用 rs.addArb() 辅助函数将其添加到副本集中
> rs.addArb("server-5:27017")
成员一旦以仲裁者的身份被添加到副本集中，它就永远只能是仲裁者：无法将仲裁者重新配置为非仲裁者，反之亦然。

在使用仲裁者时需要考虑以下几件事情:
`最多只能使用一个仲裁者`
在不知道将一个成员作为数据节点还是仲裁者时，应该将其作为数据节点。
在小副本集中使用仲裁者而不是数据节点会导致一些操作性的任务变困难。
假设有一个副本集有两个“普通”成员和一个仲裁者，其中一个数据成员停止运行了。
如果这个数据成员真的停止运行（数据无法恢复），那么就需要一个新的从节点，并且将主节点的数据副本复制到从节点。
复制数据会对服务器造成很大的压力，从而降低应用程序的速度。（通常将几 GB 的数据复制到新服务器很简单，但是如果要复制100GB 以上的数据就不太现实了。）相反，如果拥有 3 个数据成员，在一台服务器彻底停止运行时，副本集就会有更多的“喘息空间”。
这时可以用剩余的那个从节点来启动一个新的服务器端，而不必依赖于主节点。
在两个数据成员加一个仲裁者成员的情景中，主节点是仅剩的一份完好的数据，它不仅要处理应用程序请求，还要将数据复制到另一台新的服务器上。
因此，如果可能，尽可能在副本集中使用奇数个数据成员，而不要使用仲裁者。
```

### 副本集中某些场景下可能不需要创建索引

```shell
有时从节点不需要具有与主节点上相同的索引，甚至可以没有索引。如果仅使用从节点备份数据或脱机批量处理作业，则可以在成员配置中指定 "buildIndexes" :false。
此选项可防止从节点创建任何索引。这是一个永久性设置：拥有 "buildIndexes" : false 的成员再也不能重新配置为“普通”的创建索引的成员。
如果要将非创建索引成员更改为创建索引成员，则必须将其从副本集中删除，然后删除所有数据，最后再将其添加到副本集中，并允许其重新同步。
与隐藏成员一样，此选项要求成员的优先级为 0
```



### 副本集之间如何同步的？

```shell
复制是指在多台服务器上保持相同的数据副本。MongoDB 实现此功能的方式是保存操作日志（oplog），其中包含了主节点执行的每一次写操作。oplog 是存在于主节点 local 数据库中的一个固定集合。从节点通过查询此集合以获取需要复制的操作。

每个从节点都维护着自己的 oplog，用来记录它从主节点复制的每个操作。这使得每个成员都可以被用作其他成员的同步源;
从节点从同步源中获取操作，将其应用到自己的数据集上，然后再写入 oplog 中。如果应用某个操作失败（只有在基础数据已损坏或数据与主节点不一致时才会发生这种情况），则从节点会停止从当前数据源复制数据。

如果一个从节点由于某种原因而停止运行，那么当它重新启动后，就会从 oplog 中的最后一个操作开始同步。由于这些操作是先应用到数据上然后再写入 oplog，因此从节点可能会重复已经应用到其数据上的操作。MongoDB 在设计时就考虑到了这种情况：将 oplog 中的同一个操作执行多次与只执行一次效果是一样的。oplog 中的每个操作都是幂等的。也就是说，无论对目标数据集应用一次还是多次，oplog 操作都会产生相同的结果。

由于 oplog 的大小是固定的，因此它只能容纳一定数量的操作。通常来说，oplog 使用空间的速度与系统写入的速度差不多：如果在主节点上每分钟写入 1KB 的数据，那么 oplog 就会以每分钟 1KB 的速度被填满。不过，也有一些例外：如果一个操作会影响多个文档，比如删除多个文档或导致多文档更新，那么这个操作将被分解为许多 oplog 条目。主节点上的单个操作将为每个受影响的文档分解一个oplog 操作。因此，如果使用 db.coll. remove() 从集合中删除 1 000 000 个文档，那么 oplog 中就会有 1 000 000 条操作日志，每条日志对应一个被删除的文档。如果进行大量的批量操作，那么 oplog 可能会比你预期的更快被填满。
```



**mongosh**

MongoDB 自带 JavaScript shell，允许使用命令行与 MongoDB 实例进行交互。shell 在很多场景中非常有用，包括执行管理功能、检查正在运行的实例或仅仅是探索 MongoDB。mongo shell 是 MongoDB 的关键工具

操作mongodb

```shell
test> help

  Shell Help:

    use                                        Set current database
    show                                       'show databases'/'show dbs': Print a list of all available databases.
                                               'show collections'/'show tables': Print a list of all collections for current database.
                                               'show profile': Prints system.profile information.
                                               'show users': Print a list of all users for current database.
                                               'show roles': Print a list of all roles for current database.
                                               'show log <type>': log for current connection, if type is not set uses 'global'
                                               'show logs': Print all logs.

    exit                                       Quit the MongoDB shell with exit/exit()/.exit
    quit                                       Quit the MongoDB shell with quit/quit()
    Mongo                                      Create a new connection and return the Mongo object. Usage: new Mongo(URI, options [optional])
    connect                                    Create a new connection and return the Database object. Usage: connect(URI, username [optional], password [optional])
    it                                         result of the last line evaluated; use to further iterate
    version                                    Shell version
    load                                       Loads and runs a JavaScript file into the current shell environment
    enableTelemetry                            Enables collection of anonymous usage data to improve the mongosh CLI
    disableTelemetry                           Disables collection of anonymous usage data to improve the mongosh CLI
    passwordPrompt                             Prompts the user for a password
    sleep                                      Sleep for the specified number of milliseconds
    print                                      Prints the contents of an object to the output
    printjson                                  Alias for print()
    cls                                        Clears the screen like console.clear()
    isInteractive                              Returns whether the shell will enter or has entered interactive mode

  For more information on usage: https://docs.MongoDB.com/manual/reference/method
```

```shell
# 登录本地mongodb
mongosh
# 登录远程mongodb

# mongodb设置账户密码（mongodb和redis一样，默认没有密码）

# 授权用户

# 列出数据库
show databases;
# 切换数据库(如果数据库不存在，则创建数据库，否则切换到指定数据库。)
use test
# 查看当前所在数据库
db
# 删除数据库
lsy> use lsy
already on db lsy
lsy> db.dropDatabase()
{ ok: 1, dropped: 'lsy' }
# 增加集合（mongodb中的库、表默认不存在，直接执行insert语句会直接创建的）
db.集合名.insertOne({"name":"111"})
# 删除集合
db.集合名.drop()
# 删除文档数据
方式一：删除所有满足匹配条件的数据
db.集合名.deleteMany({})
方式二：删除第一条满足匹配条件的数据
db.集合名.deleteOne( { status: "D" } )
# 更新文档数据
方式一：update()方法
db.集合名.update({条件},{$set:{'title':'MongoDB'}})
方式二：save()方法
save() 方法通过传入的文档来替换已有文档，_id 主键存在就更新，不存在就插入。
db.集合名.save(
   <document>,
   {
     writeConcern: <document>
   }
)

# 查看数据
db.集合.find()
```

### 数据类型

null

布尔类型

数值类型

整数

字符串

日期

正则表达式

数组类型

内嵌文档

Object ID

代码

二进制

### 查询条件操作符

条件操作符用于比较两个表达式并从mongoDB集合中获取数据。

在本章节中，我们将讨论如何在MongoDB中使用条件操作符。

MongoDB中条件操作符有：

- (>) 大于 - $gt
- (<) 小于 - $lt
- (>=) 大于等于 - $gte
- (<= ) 小于等于 - $lte

```shell
#获取 "col" 集合中 "likes" 大于 100 的数据
db.col.find({likes : {$gt : 100}})
#获取"col"集合中 "likes" 大于等于 100 的数据
db.col.find({likes : {$gte : 100}})
#获取"col"集合中 "likes" 小于 150 的数据
db.col.find({likes : {$lt : 150}})
#获取"col"集合中 "likes" 小于等于 150 的数据
db.col.find({likes : {$lte : 150}})
#获取"col"集合中 "likes" 大于100，小于 200 的数据
db.col.find({likes : {$lt :200, $gt : 100}})
#获取 "col" 集合中 title 为 String 的数据
db.col.find({"title" : {$type : 2}})
```

limit 限制返回数量

```shell
video> db.col.find({},{"title":true,_id:false}).limit(2)
[ { title: 'PHP 教程' }, { title: 'Java 教程' } ]
video>

# 解释
{}： 表示查询条件参数 为空，将查询出所有的数据
{"title":1,_id:0}： 表示要求返回title信息，不要输出object_id信息


```

skip()跳过某数据

```shell
video> db.col.find({},{"title":1,_id:0}).limit(1).skip(1)
[ { title: 'Java 教程' } ]
video>
```



**默认的数据库**

```shell
admin　　admin 数据库会在身份验证和授权时被使用。
local　　特定于单个服务器的数据会存储在此数据库中。在副本集中，local 用于存储复制过程中所使用的数据，而 local 数据库本身不会被复制。
config　　MongoDB 的分片集群会使用 config 数据库存储关于每个分片的信息。
```



通过将数据库名称与该库中的集合名称连接起来，可以获得一个完全限定的集合名称，称为命名空间。如果你要使用 cms 数据库中的 blog.posts 集合，则该集合的命名空间为 cms.blog.posts。





# 问题记录

1、部署了一套mongodb副本集群，通过mongoshell的方式登录到mongodb的从节点，是不可以写入数据的，但是我通过程序连接该从节点，发现可以写入数据，这是什么鬼？

文心一言回答：

```shell
你好！这是一个比较常见的问题。在MongoDB副本集中，从节点（Slave）通常只用于读取数据，而不用于写入数据。这是为了确保数据的一致性和高可用性。

当你使用mongoshell连接到MongoDB从节点时，默认情况下是不允许写入操作的。这是因为mongoshell通常用于管理数据库和执行写入操作，而从节点上的数据可能不是最新的，因此写入操作可能会影响数据的正确性。

然而，如果你通过程序（例如Python、Node.js等）连接到MongoDB从节点，并执行写入操作，可能会发现写入操作是成功的。这是因为程序在执行写入操作时，通常会同时指定要写入的文档的完整标识符（包括集合名、文档ID等），从而避免了从节点的数据不一致性问题。

但是，为了确保数据的一致性和高可用性，建议在写入数据时，始终将数据写入MongoDB的主节点（Master）或仲裁节点（Arbiter），而不是从节点。
```

2、在部署mongodb分片集群时产生疑惑： config server 是怎么发现 shard集群的？ 我并没有告诉config server shard集群的地址，它可以自己发现shard集群的地址吗？

文心一言的回答：

```shell
MongoDB的Config Server是通过集群中的元数据来了解Shard集群的信息的，而不需要手动指定Shard集群的地址。

在MongoDB分片集群中，每个Shard节点都会将自己注册到Config Server节点上，包括自己的地址、端口号和分片信息等。这些信息会被Config Server节点记录在集群元数据中，并用于维护集群的状态和路由。

当Config Server需要查询某个分片的信息时，它会通过元数据中的信息来获取Shard节点的地址和端口号，并直接与Shard节点通信，以获取分片的信息。因此，即使你没有手动告诉Config ServerShard集群的地址，Config Server也可以通过元数据自动发现Shard集群的信息。

需要注意的是，如果Shard集群中的节点发生故障或者新的Shard节点加入集群，Config Server需要重新加载元数据来更新集群状态。此时，Config Server会向所有的Shard节点发送通知，要求它们重新注册自己，以确保元数据是最新的。

那么我想要查看shard副本集向config server副本集注册的元数据信息 应该如何查看呢？

你可以通过以下步骤查看MongoDB分片集群中Shard副本集向Config Server副本集注册的元数据信息：

连接到MongoDB分片集群的Config Server节点，可以使用以下命令：

mongo --host <config-server-host> --port <config-server-port>
其中，<config-server-host>是Config Server节点的主机名或IP地址，<config-server-port>是Config Server节点的端口号，默认为27017。

进入admin数据库，可以使用以下命令：

use admin
运行以下命令来查看所有分片的元数据信息，包括分片的名称、状态、副本集等：
db.shards.find()
这个命令会返回一个包含所有分片信息的游标。你可以逐个查看每个分片的元数据信息，包括分片的名称、状态、副本集等。

运行以下命令来查看特定分片的元数据信息：
db.shards.findOne({_id:<shard-id>})
其中，<shard-id>是你要查看的分片的名称或ID。这个命令会返回包含特定分片信息的文档，包括分片的名称、状态、副本集等。

通过以上步骤，你可以查看MongoDB分片集群中Shard副本集向Config Server副本集注册的元数据信息，以及特定分片的元数据信息。

```

3、mongodb分片集群 横向扩展的问题

```shell
问题：我发现mongodb分片集群中的shard 服务都是要组成副本使用的，那么我搭建一个mongodb分片集群的目的是本是 提高负载均衡、防止单点故障导致服务故障，横向扩展节点数量，提高容量的，但是我需要6台机器组成两个shard副本集 其实对于整个分片集群来说，每增加3台机器 才能增加一台机器的容量，我感觉不太合理呢

文心一言：
你提到的MongoDB分片集群的目的是提高负载均衡、防止单点故障导致服务故障，以及横向扩展节点数量，这是正确的。MongoDB分片集群可以将数据划分为多个部分，并在多个节点上存储和管理这些数据，从而实现负载均衡和容错。

在MongoDB分片集群中，组成Shard服务的副本集确实是为了提高可用性和容错性。每个Shard服务的副本集都包含多个节点，这些节点之间相互复制和备份数据。如果一个节点发生故障，其他节点可以继续提供服务，从而避免了单点故障导致整个服务故障的情况。

对于你的疑问，为什么每增加3台机器只能增加一台机器的容量，这是因为MongoDB的分片集群是通过将数据划分为多个片段来实现负载均衡和容错的。每个Shard服务的副本集都包含多个节点，这些节点之间相互复制和备份数据。当你在一个Shard服务的副本集中增加3台机器时，实际上是增加了该副本集的容量，因为这些机器可以共同承担数据的存储和管理任务，从而提高了整个分片集群的容量。

然而，由于每个Shard服务的副本集都需要保持数据的同步和一致性，因此增加机器的数量也会受到一定的限制。当一个Shard服务的副本集中的机器数量增加时，可能会带来额外的网络开销和数据同步的延迟，这可能会影响集群的性能和可扩展性。

因此，在设计和搭建MongoDB分片集群时，需要考虑到数据量、访问频率、可用性和容错性等方面的需求，并选择适当的机器数量和配置来满足这些需求。同时，还需要进行适当的监控和管理，以确保集群的稳定性和性能。

问题：那么 我想提问： mongodb的副本集 作用是什么？ 假如 我有三台100G磁盘的机器 组成了一个Shard副本集，对于整个mongodb分片集群来说 是横向扩展了多少容量？ 是300G还是100G?

MongoDB的副本集主要是为了提供数据的可用性和容错性。MongoDB的副本集是通过将数据复制到多个节点来实现的，这样可以在一个节点发生故障时，其他节点可以继续提供服务，从而避免了数据的丢失和服务的中断。

对于整个MongoDB分片集群来说，组成一个Shard副本集可以增加一定的容量。由于MongoDB的分片集群是通过将数据划分为多个片段来实现负载均衡和容错的，因此每个Shard副本集都可以承担一部分数据存储和管理的任务，从而增加了整个集群的容量。

具体到你提到的情况，如果你有三个100G磁盘的机器组成一个Shard副本集，那么整个Shard副本集的容量是300G还是100G，取决于你如何配置MongoDB的分片集群。如果每个机器都只存储了100G的数据，那么整个Shard副本集的容量就是300G；如果每个机器都存储了超过100G的数据，那么整个Shard副本集的容量就是超过300G。

需要注意的是，组成Shard副本集的机器数量和容量并不是唯一的考虑因素。在设计和搭建MongoDB分片集群时，还需要考虑到数据量、访问频率、可用性和容错性等方面的需求，并选择适当的机器数量和配置来满足这些需求。同时，还需要进行适当的监控和管理，以确保集群的稳定性和性能。

问题：看了你上述的回答，我总结以下： 在mongodb分片集群中的组成shard副本集的三台机器并不只是相互复制提高容错率的作用，他们还可以负载均衡 对吗？
文心一言：
是的，组成Shard副本集的机器不仅仅是为了相互复制数据以提高容错率，它们还可以实现负载均衡。

在MongoDB分片集群中，数据被划分为多个片段，每个片段都会被分配到不同的节点上。组成Shard副本集的机器会共同承担存储和管理数据的任务，从而分担负载，实现负载均衡。

当一个请求到达MongoDB分片集群时，请求会被路由到其中一个Shard服务的节点上，该节点会根据一定的负载均衡策略将请求转发到具体的片段所在的节点上。这样，每个节点都可以处理来自不同Shard服务的请求，从而实现负载均衡。

需要注意的是，负载均衡的实现需要考虑多个因素，例如数据量、访问频率、数据分布等。在设计MongoDB分片集群时，需要进行适当的规划和配置，以确保负载均衡的效果和性能。

综上所述，组成Shard副本集的机器不仅为了提高容错率，还可以实现负载均衡，从而提高了整个MongoDB分片集群的性能和可靠性。

我： 好的 我明白了 ，这样的设计是合理的！ 我可能被副本集的概念给迷惑了
文心一言：
是的，副本集的概念是MongoDB中一个非常重要的概念，它确实会让人有些迷惑。

副本集的作用主要是为了保证数据的可用性和容错性。通过将数据复制到多个节点上，即使其中一个节点发生故障，其他节点仍然可以继续提供服务，从而提高了整个系统的可用性。

但是，副本集并不是为了实现负载均衡而设计的。在MongoDB分片集群中，负载均衡是通过将数据划分为多个片段来实现的。每个片段会被分配到不同的节点上，从而分担了整个系统的负载。

副本集和负载均衡是两个不同的概念，但在MongoDB分片集群中，它们是相互配合的。通过组成副本集，可以提供数据的可用性和容错性，同时也可以实现负载均衡，从而提高了整个系统的性能和可靠性。

希望这样的解释能够帮助你更好地理解MongoDB分片集群中副本集和负载均衡的概念和作用。
```

为何查询不出y等于null的数据？

```shell
test>  db.c.find()
[
  {
    '0': { _id: ObjectId("4ba0f0dfd22aa494fd523621"), y: null },
    '1': { _id: ObjectId("4ba0f0dfd22aa494fd523622"), y: 1 },
    '2': { _id: ObjectId("4ba0f148d22aa494fd523623"), y: 2 },
    _id: ObjectId("64b8a679556c612f9c102727")
  }
]

test> db.c.find({"y" : {"$eq" : null, "$exists" : true}})

test> 
```













