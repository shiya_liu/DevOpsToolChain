# 环境说明



| IP地址    | 主机名     | 机器资源 | 操作系统                                         |
| --------- | ---------- | -------- | ------------------------------------------------ |
| 10.0.0.10 | controller | 2C4G     | Ubuntu 20.04.4 LTS Linux 5.4.0-100-generic arm64 |
| 10.0.0.11 | compute    | 2C4G     | Ubuntu 20.04.4 LTS Linux 5.4.0-100-generic arm64 |



# 基础设置

默认只在控制节点执行，当控制节点和计算节点都需要执行时会标明

`控制节点和计算节点都执行`

```shell
# 修改主机名
hostnamectl set-hostname controller
hostnamectl set-hostname computer

# 设置时区
sudo cp /usr/share/zoneinfo/Asia/Shanghai  /etc/localtime -r

# 关闭swap  # 临时    # 永久 
swapoff -a  
sed -ri 's/.*swap.*/#&/' /etc/fstab  

# 设置网络配置文件
cp /etc/netplan/00-installer-config.yaml /etc/netplan/10-installer-config.yaml
vim /etc/netplan/10-installer-config.yaml
network:
  version: 2
  ethernets:
    ens256:
      dhcp4: no
      activation-mode: manual
# 生效
netplan apply

# 解析IP地址
echo "10.0.0.10       controller" >> /etc/hosts
echo "10.0.0.11       computer" >> /etc/hosts

# 时间同步
apt-get install chrony -y 
vim /etc/chrony/chrony.conf
server ntp.aliyun.com iburst
service chrony restart
chronyc sources

# 启用OpenStack库
add-apt-repository cloud-archive:yoga

# 安装 OpenStack 客户端
apt install python3-openstackclient -y 
```

# SQL数据库

```shell
apt install mariadb-server python3-pymysql -y 

cat > /etc/mysql/mariadb.conf.d/99-openstack.cnf << EOF
[mysqld]
bind-address = 0.0.0.0
default-storage-engine = innodb
innodb_file_per_table = on
max_connections = 4096
collation-server = utf8_general_ci
character-set-server = utf8
EOF

service mysql restart
mysql_secure_installation
```

# NoSQL 数据库

```shell
apt install -y mongodb-server mongodb-clients python3-pymongo

vim /etc/mongodb.conf
bind_ip = 0.0.0.0
smallfiles = true


service mongodb stop
rm /var/lib/mongodb/journal/prealloc.*
service mongodb start

```



# 消息队列

```shell
apt install -y rabbitmq-server
rabbitmqctl add_user openstack pwd
rabbitmqctl set_permissions openstack ".*" ".*" ".*"
```

# 内存缓存

```shell
apt install -y memcached python3-memcache
vim /etc/memcached.conf 
-l 0.0.0.0
service memcached restart
```

# 键值存储

```shell
apt install -y  etcd
cat > /etc/default/etcd << EOF
ETCD_NAME="controller"
ETCD_DATA_DIR="/var/lib/etcd"
ETCD_INITIAL_CLUSTER_STATE="new"
ETCD_INITIAL_CLUSTER_TOKEN="etcd-cluster-01"
ETCD_INITIAL_CLUSTER="controller=http://10.0.0.10:2380"
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://10.0.0.10:2380"
ETCD_ADVERTISE_CLIENT_URLS="http://10.0.0.10:2379"
ETCD_LISTEN_PEER_URLS="http://0.0.0.0:2380"
ETCD_LISTEN_CLIENT_URLS="http://10.0.0.10:2379"
ETCD_UNSUPPORTED_ARCH="arm64"
EOF
systemctl enable etcd
systemctl restart etcd
```

# 认证服务keystone

```shell
mysql -u root -ppwd
CREATE DATABASE keystone;
GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'localhost' \
IDENTIFIED BY 'pwd';
GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'%' \
IDENTIFIED BY 'pwd';

#openssl rand -hex 10
apt install -y keystone
 
vim /etc/keystone/keystone.conf

[database]
connection = mysql+pymysql://keystone:pwd@controller/keystone

[token]
provider = fernet

# 初始化身份认证服务的数据库
su -s /bin/sh -c "keystone-manage db_sync" keystone

# 初始化Fernet keys
keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
keystone-manage credential_setup --keystone-user keystone --keystone-group keystone

# 引导标识服务
keystone-manage bootstrap --bootstrap-password pwd \
  --bootstrap-admin-url http://controller:5000/v3/ \
  --bootstrap-internal-url http://controller:5000/v3/ \
  --bootstrap-public-url http://controller:5000/v3/ \
  --bootstrap-region-id RegionOne


# 编辑/etc/apache2/apache2.conf文件，为控制节点配置ServerName选项：
vim /etc/apache2/apache2.conf
ServerName controller

service apache2 restart

# 创建服务实体和API端点
export OS_USERNAME=admin
export OS_PASSWORD=pwd
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://controller:5000/v3
export OS_IDENTITY_API_VERSION=3

openstack domain create --description "An Example Domain" example

openstack project create --domain default \
  --description "Service Project" service


$ openstack endpoint create --region RegionOne \
  identity public http://controller:5000/v3
+--------------+----------------------------------+
| Field        | Value                            |
+--------------+----------------------------------+
| enabled      | True                             |
| id           | 30fff543e7dc4b7d9a0fb13791b78bf4 |
| interface    | public                           |
| region       | RegionOne                        |
| region_id    | RegionOne                        |
| service_id   | 8c8c0927262a45ad9066cfe70d46892c |
| service_name | keystone                         |
| service_type | identity                         |
| url          | http://controller:5000/v3        |
+--------------+----------------------------------+

$ openstack endpoint create --region RegionOne \
  identity internal http://controller:5000/v3
+--------------+----------------------------------+
| Field        | Value                            |
+--------------+----------------------------------+
| enabled      | True                             |
| id           | 57cfa543e7dc4b712c0ab137911bc4fe |
| interface    | internal                         |
| region       | RegionOne                        |
| region_id    | RegionOne                        |
| service_id   | 6f8de927262ac12f6066cfe70d99ac51 |
| service_name | keystone                         |
| service_type | identity                         |
| url          | http://controller:5000/v3        |
+--------------+----------------------------------+

$ openstack endpoint create --region RegionOne \
  identity admin http://controller:35357/v3
+--------------+----------------------------------+
| Field        | Value                            |
+--------------+----------------------------------+
| enabled      | True                             |
| id           | 78c3dfa3e7dc44c98ab1b1379122ecb1 |
| interface    | admin                            |
| region       | RegionOne                        |
| region_id    | RegionOne                        |
| service_id   | 34ab3d27262ac449cba6cfe704dbc11f |
| service_name | keystone                         |
| service_type | identity                         |
| url          | http://controller:35357/v3       |
+--------------+----------------------------------+
```



# 镜像服务Glance

```shell

# 创建数据库
mysql
CREATE DATABASE glance;
GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'localhost' \
  IDENTIFIED BY 'pwd';
GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'%' \
  IDENTIFIED BY 'pwd';

# 加载变量
. admin-openrc

# 创建服务凭据
openstack user create --domain default --password-prompt glance
# 将角色添加到用户和项目
openstack role add --project service --user glance admin
# 创建服务实体
openstack service create --name glance \
  --description "OpenStack Image" image
# 创建影像服务 API 端点
openstack endpoint create --region RegionOne \
  image public http://controller:9292
openstack endpoint create --region RegionOne \
  image internal http://controller:9292
openstack endpoint create --region RegionOne \
  image admin http://controller:9292

# 安装软件包
apt install glance -y
vim /etc/glance/glance-api.conf
# 配置数据库访问
[database]
connection = mysql+pymysql://glance:pwd@controller/glance
# 配置身份认证服务
[keystone_authtoken]
www_authenticate_uri = http://controller:5000
auth_url = http://controller:5000
memcached_servers = controller:11211
auth_type = password
project_domain_name = Default
user_domain_name = Default
project_name = service
username = glance
password = pwd
service_token_roles_required = true
#配置本地文件 系统存储和映像文件的位置
[glance_store]
enabled_backends = file_back:file
default_backend = file_back
[file_back]
filesystem_store_datadir = /var/lib/glance/images/
store_description = "File storage backend"
# 访问权限
[oslo_limit]
auth_url = http://controller:5000
auth_type = password
user_domain_id = default
username = MY_SERVICE
system_scope = all
password = MY_PASSWORD
endpoint_id = ENDPOINT_ID
region_name = RegionOne

# 配置文件示例
root@controller:~# grep -Ev '^$|^#' /etc/glance/glance-api.conf
[DEFAULT]
use_keystone_quotas = True
[oslo_limit]
auth_url = http://controller:5000
auth_type = password
user_domain_id = default
username = MY_SERVICE
system_scope = all
password = MY_PASSWORD
endpoint_id = ENDPOINT_ID
region_name = RegionOne
[barbican]
[barbican_service_user]
[cinder]
[cors]
[database]
connection = mysql+pymysql://glance:pwd@controller/glance
backend = sqlalchemy
[file]
[glance.store.http.store]
[glance.store.rbd.store]
[glance.store.s3.store]
[glance.store.swift.store]
[glance.store.vmware_datastore.store]
[glance_store]
enabled_backends = file_back:file
default_backend = file_back
[file_back]
filesystem_store_datadir = /var/lib/glance/images/
store_description = "File storage backend"
[healthcheck]
[image_format]
disk_formats = ami,ari,aki,vhd,vhdx,vmdk,raw,qcow2,vdi,iso,ploop.root-tar
[key_manager]
[keystone_authtoken]
www_authenticate_uri = http://controller:5000
auth_url = http://controller:5000
memcached_servers = controller:11211
auth_type = password
project_domain_name = Default
user_domain_name = Default
project_name = service
username = glance
password = pwd
service_token_roles_required = true
[oslo_concurrency]
[oslo_messaging_amqp]
[oslo_messaging_kafka]
[oslo_messaging_notifications]
[oslo_messaging_rabbit]
[oslo_middleware]
[oslo_policy]
[oslo_reports]
[paste_deploy]
flavor = keystone
[profiler]
[store_type_location_strategy]
[task]
[taskflow_executor]
[vault]
[wsgi]

# 下载依赖
apt install -y pip3
pip3 install boto3


# 同步数据库
su -s /bin/sh -c "glance-manage db_sync" glance
# 重启glance
service glance-api restart
```

日志warn问题

https://serverfault.com/questions/1054864/replaced-parameters-default-store-and-glance-store-breaks-cooperation-betwee

# 放置服务Placement

```shell
# 创建数据库
mysql
CREATE DATABASE placement;
GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'localhost' \
  IDENTIFIED BY 'pwd';
GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'%' \
  IDENTIFIED BY 'pwd';


# 配置用户和端点
. admin-openrc
openstack user create --domain default --password-prompt placement

openstack role add --project service --user placement admin
openstack service create --name placement \
  --description "Placement API" placement
openstack endpoint create --region RegionOne \
  placement public http://controller:8778
openstack endpoint create --region RegionOne \
  placement internal http://controller:8778
openstack endpoint create --region RegionOne \
  placement admin http://controller:8778

# 安装和配置组件
apt install placement-api -y
vim /etc/placement/placement.conf
[placement_database]
connection = mysql+pymysql://placement:pwd@controller/placement
[api]
auth_strategy = keystone

[keystone_authtoken]
auth_url = http://controller:5000/v3
memcached_servers = controller:11211
auth_type = password
project_domain_name = Default
user_domain_name = Default
project_name = service
username = placement
password = pwd

# 同步数据库
su -s /bin/sh -c "placement-manage db sync" placement
# 重启服务
service apache2 restart

# 检查
root@controller:~# . admin-openrc
root@controller:~# 
root@controller:~# 
root@controller:~# placement-status upgrade check
+-------------------------------------------+
| Upgrade Check Results                     |
+-------------------------------------------+
| Check: Missing Root Provider IDs          |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+
| Check: Incomplete Consumers               |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+
| Check: Policy File JSON to YAML Migration |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+

# 针对放置 API 运行一些命令
pip3 install osc-placement

openstack --os-placement-api-version 1.2 resource class list --sort-column name
openstack --os-placement-api-version 1.6 trait list --sort-column name
```

# 计算服务Nova

## 控制节点

```shell
# 要创建数据库
mysql
CREATE DATABASE nova_api;
CREATE DATABASE nova;
CREATE DATABASE nova_cell0;
GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'localhost' \
  IDENTIFIED BY 'pwd';
GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'%' \
  IDENTIFIED BY 'pwd';

GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'localhost' \
  IDENTIFIED BY 'pwd';
GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'%' \
  IDENTIFIED BY 'pwd';

GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'localhost' \
  IDENTIFIED BY 'pwd';
GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'%' \
  IDENTIFIED BY 'pwd';

# 创建计算服务凭据
. admin-openrc
openstack user create --domain default --password-prompt nova

openstack role add --project service --user nova admin
openstack service create --name nova \
  --description "OpenStack Compute" compute
openstack endpoint create --region RegionOne \
  compute public http://controller:8774/v2.1
openstack endpoint create --region RegionOne \
  compute internal http://controller:8774/v2.1
openstack endpoint create --region RegionOne \
  compute admin http://controller:8774/v2.1

# 安装和配置组件
apt install nova-api nova-conductor nova-novncproxy nova-scheduler -y 
vim /etc/nova/nova.conf
[api_database]
# ...
connection = mysql+pymysql://nova:pwd@controller/nova_api

[database]
# ...
connection = mysql+pymysql://nova:pwd@controller/nova
[DEFAULT]
# ...
transport_url = rabbit://openstack:pwd@controller:5672/
[api]
# ...
auth_strategy = keystone

[keystone_authtoken]
# ...
www_authenticate_uri = http://controller:5000/
auth_url = http://controller:5000/
memcached_servers = controller:11211
auth_type = password
project_domain_name = Default
user_domain_name = Default
project_name = service
username = nova
password = pwd

[service_user]
send_service_user_token = true
auth_url = https://controller/identity
auth_strategy = keystone
auth_type = password
project_domain_name = Default
project_name = service
user_domain_name = Default
username = nova
password = pwd

[DEFAULT]
# ...
my_ip = 10.0.0.11


[vnc]
enabled = true
# ...
server_listen = $my_ip
server_proxyclient_address = $my_ip

[glance]
# ...
api_servers = http://controller:9292

[oslo_concurrency]
# ...
lock_path = /var/lib/nova/tmp

[placement]
# ...
region_name = RegionOne
project_domain_name = Default
project_name = service
auth_type = password
user_domain_name = Default
auth_url = http://controller:5000/v3
username = placement
password = pwd

# 配置文件示例
root@controller:~# grep -Ev '^$|^#' /etc/nova/nova.conf
[DEFAULT]
log_dir = /var/log/nova
lock_path = /var/lock/nova
state_path = /var/lib/nova
my_ip = 10.0.0.10
transport_url = rabbit://openstack:pwd@controller:5672/
[api]
auth_strategy = keystone
[api_database]
connection = mysql+pymysql://nova:pwd@controller/nova_api
[barbican]
[barbican_service_user]
[cache]
[cinder]
[compute]
[conductor]
[console]
[consoleauth]
[cors]
[cyborg]
[database]
connection = mysql+pymysql://nova:pwd@controller/nova
[devices]
[ephemeral_storage_encryption]
[filter_scheduler]
[glance]
api_servers = http://controller:9292
[guestfs]
[healthcheck]
[hyperv]
[image_cache]
[ironic]
[key_manager]
[keystone]
[keystone_authtoken]
www_authenticate_uri = http://controller:5000/
auth_url = http://controller:5000/
memcached_servers = controller:11211
auth_type = password
project_domain_name = Default
user_domain_name = Default
project_name = service
username = nova
password = pwd
[libvirt]
[metrics]
[mks]
[neutron]
[notifications]
[oslo_concurrency]
lock_path = /var/lib/nova/tmp
[oslo_messaging_amqp]
[oslo_messaging_kafka]
[oslo_messaging_notifications]
[oslo_messaging_rabbit]
[oslo_middleware]
[oslo_policy]
[oslo_reports]
[pci]
[placement]
region_name = RegionOne
project_domain_name = Default
project_name = service
auth_type = password
user_domain_name = Default
auth_url = http://controller:5000/v3
username = placement
password = pwd
[powervm]
[privsep]
[profiler]
[quota]
[rdp]
[remote_debug]
[scheduler]
[serial_console]
[service_user]
send_service_user_token = true
auth_url = https://controller/identity
auth_strategy = keystone
auth_type = password
project_domain_name = Default
project_name = service
user_domain_name = Default
username = nova
password = pwd
[spice]
[upgrade_levels]
[vault]
[vendordata_dynamic_auth]
[vmware]
[vnc]
enabled = true
server_listen = $my_ip
server_proxyclient_address = $my_ip
[workarounds]
[wsgi]
[zvm]
[cells]
enable = False
[os_region_name]
openstack = 


#同步数据库
su -s /bin/sh -c "nova-manage api_db sync" nova
su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova
su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova
su -s /bin/sh -c "nova-manage db sync" nova
# 验证nova cell0 和 cell1 是否已正确注册
root@controller:~# su -s /bin/sh -c "nova-manage cell_v2 list_cells" nova
+-------+--------------------------------------+------------------------------------------+-------------------------------------------------+----------+
|  Name |                 UUID                 |              Transport URL               |               Database Connection               | Disabled |
+-------+--------------------------------------+------------------------------------------+-------------------------------------------------+----------+
| cell0 | 00000000-0000-0000-0000-000000000000 |                  none:/                  | mysql+pymysql://nova:****@controller/nova_cell0 |  False   |
| cell1 | 2b7661f1-12d6-49e3-8cc0-558103c21ef9 | rabbit://openstack:****@controller:5672/ |    mysql+pymysql://nova:****@controller/nova    |  False   |
+-------+--------------------------------------+------------------------------------------+-------------------------------------------------+----------+



# 重新启动计算服务
service nova-api restart
service nova-scheduler restart
service nova-conductor restart
service nova-novncproxy restart

```

## 计算节点

```shell
apt install -y  nova-compute
vim /etc/nova/nova.conf
[DEFAULT]
transport_url = rabbit://openstack:pwd@controller

[api]
auth_strategy = keystone

[keystone_authtoken]
www_authenticate_uri = http://controller:5000/
auth_url = http://controller:5000/
memcached_servers = controller:11211
auth_type = password
project_domain_name = Default
user_domain_name = Default
project_name = service
username = nova
password = pwd

[service_user]
send_service_user_token = true
auth_url = https://controller/identity
auth_strategy = keystone
auth_type = password
project_domain_name = Default
project_name = service
user_domain_name = Default
username = nova
password = pwd

[DEFAULT]
my_ip = 10.0.0.11


[vnc]
enabled = true
server_listen = 0.0.0.0
server_proxyclient_address = $my_ip
novncproxy_base_url = http://controller:6080/vnc_auto.html

[glance]
api_servers = http://controller:9292

[oslo_concurrency]
lock_path = /var/lib/nova/tmp

[placement]
region_name = RegionOne
project_domain_name = Default
project_name = service
auth_type = password
user_domain_name = Default
auth_url = http://controller:5000/v3
username = placement
password = pwd

[libvirt]
virt_type = qemu
# 删除[api_database]的默认连接配置
vim /etc/nova/nova.conf
connection = sqlite:////var/lib/nova/nova_api.sqlite

# 示例配置
root@computer:~# grep -Ev '^$|^#' /etc/nova/nova.conf
[DEFAULT]
my_ip = 10.0.0.11
log_dir = /var/log/nova
lock_path = /var/lock/nova
state_path = /var/lib/nova
transport_url = rabbit://openstack:pwd@controller
[api]
auth_strategy = keystone
[api_database]
[barbican]
[barbican_service_user]
[cache]
[cinder]
[compute]
[conductor]
[console]
[consoleauth]
[cors]
[cyborg]
[database]
connection = sqlite:////var/lib/nova/nova.sqlite
[devices]
[ephemeral_storage_encryption]
[filter_scheduler]
[glance]
api_servers = http://controller:9292
[guestfs]
[healthcheck]
[hyperv]
[image_cache]
[ironic]
[key_manager]
[keystone]
[keystone_authtoken]
www_authenticate_uri = http://controller:5000/
auth_url = http://controller:5000/
memcached_servers = controller:11211
auth_type = password
project_domain_name = Default
user_domain_name = Default
project_name = service
username = nova
password = pwd
[libvirt]
virt_type = qemu
[metrics]
[mks]
[neutron]
[notifications]
[oslo_concurrency]
lock_path = /var/lib/nova/tmp
[oslo_messaging_amqp]
[oslo_messaging_kafka]
[oslo_messaging_notifications]
[oslo_messaging_rabbit]
[oslo_middleware]
[oslo_policy]
[oslo_reports]
[pci]
[placement]
region_name = RegionOne
project_domain_name = Default
project_name = service
auth_type = password
user_domain_name = Default
auth_url = http://controller:5000/v3
username = placement
password = pwd
[powervm]
[privsep]
[profiler]
[quota]
[rdp]
[remote_debug]
[scheduler]
[serial_console]
[service_user]
send_service_user_token = true
auth_url = https://controller/identity
auth_strategy = keystone
auth_type = password
project_domain_name = Default
project_name = service
user_domain_name = Default
username = nova
password = pwd
[spice]
[upgrade_levels]
[vault]
[vendordata_dynamic_auth]
[vmware]
[vnc]
enabled = true
server_listen = 0.0.0.0
server_proxyclient_address = $my_ip
novncproxy_base_url = http://controller:6080/vnc_auto.html
[workarounds]
[wsgi]
[zvm]
[cells]
enable = False
[os_region_name]
openstack = 



# 安装插件
apt install -y pip3
apt install  python3-pymysql -y 

# 启动服务
service nova-compute restart
```



## 将计算节点添加到单元数据库

```shell
# 获取管理员凭据以启用仅限管理员的 CLI 命令，然后确认 数据库中有计算主机
root@controller:~# . admin-openrc
root@controller:~# openstack compute service list --service nova-compute
+--------------------------------------+--------------+----------+------+---------+-------+----------------------------+
| ID                                   | Binary       | Host     | Zone | Status  | State | Updated At                 |
+--------------------------------------+--------------+----------+------+---------+-------+----------------------------+
| fcddd276-9156-40fa-8274-e1fd1479fc55 | nova-compute | computer | nova | enabled | up    | 2023-09-27T08:53:55.000000 |
+--------------------------------------+--------------+----------+------+---------+-------+----------------------------+
# 发现计算主机
root@controller:~# su -s /bin/sh -c "nova-manage cell_v2 discover_hosts --verbose" nova
Found 2 cell mappings.
Skipping cell0 since it does not contain hosts.
Getting computes from cell 'cell1': 2b7661f1-12d6-49e3-8cc0-558103c21ef9
Checking host mapping for compute host 'computer': da3d28e2-aba2-4c9f-a763-99eac49c70c9
Creating host mapping for compute host 'computer': da3d28e2-aba2-4c9f-a763-99eac49c70c9
Found 1 unmapped computes in cell: 2b7661f1-12d6-49e3-8cc0-558103c21ef9

```

## 验证

```shell
# 获取凭据以获取对仅限管理员的 CLI 命令的访问权限
. admin-openrc
# 列出服务组件以验证每个组件的成功启动和注册 过程
root@controller:~# openstack compute service list
+--------------------------------------+----------------+------------+----------+---------+-------+----------------------------+
| ID                                   | Binary         | Host       | Zone     | Status  | State | Updated At                 |
+--------------------------------------+----------------+------------+----------+---------+-------+----------------------------+
| 13f418a4-5222-475a-aea2-1dc7703c093a | nova-scheduler | controller | internal | enabled | up    | 2023-09-27T08:58:29.000000 |
| 9f2fbbae-80eb-4b8f-9352-88bb56082626 | nova-conductor | controller | internal | enabled | up    | 2023-09-27T08:58:30.000000 |
| fcddd276-9156-40fa-8274-e1fd1479fc55 | nova-compute   | computer   | nova     | enabled | up    | 2023-09-27T08:58:25.000000 |
+--------------------------------------+----------------+------------+----------+---------+-------+----------------------------+

# 列出标识服务中的 API 终结点，以验证与 身份服务
root@controller:~# openstack catalog list
+-----------+-----------+-----------------------------------------+
| Name      | Type      | Endpoints                               |
+-----------+-----------+-----------------------------------------+
| glance    | image     | RegionOne                               |
|           |           |   internal: http://controller:9292      |
|           |           | RegionOne                               |
|           |           |   admin: http://controller:9292         |
|           |           | RegionOne                               |
|           |           |   public: http://controller:9292        |
|           |           |                                         |
| placement | placement | RegionOne                               |
|           |           |   admin: http://controller:8778         |
|           |           | RegionOne                               |
|           |           |   internal: http://controller:8778      |
|           |           | RegionOne                               |
|           |           |   public: http://controller:8778        |
|           |           |                                         |
| nova      | compute   | RegionOne                               |
|           |           |   internal: http://controller:8774/v2.1 |
|           |           | RegionOne                               |
|           |           |   admin: http://controller:8774/v2.1    |
|           |           | RegionOne                               |
|           |           |   public: http://controller:8774/v2.1   |
|           |           |                                         |
| keystone  | identity  | RegionOne                               |
|           |           |   public: http://controller:5000/v3/    |
|           |           | RegionOne                               |
|           |           |   internal: http://controller:5000/v3/  |
|           |           | RegionOne                               |
|           |           |   admin: http://controller:5000/v3/     |
|           |           |                                         |
+-----------+-----------+-----------------------------------------+

# 检查单元格和放置 API 是否正常工作，以及其他 必要的先决条件已到位
root@controller:~# nova-status upgrade check
+-------------------------------------------+
| Upgrade Check Results                     |
+-------------------------------------------+
| Check: Cells v2                           |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+
| Check: Placement API                      |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+
| Check: Cinder API                         |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+
| Check: Policy Scope-based Defaults        |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+
| Check: Policy File JSON to YAML Migration |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+
| Check: Older than N-1 computes            |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+
| Check: hw_machine_type unset              |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+
| Check: Service User Token Configuration   |
| Result: Success                           |
| Details: None                             |
+-------------------------------------------+

```

# 网络服务Neutron

## 控制节点

```shell
# 创建数据库
 mysql
 CREATE DATABASE neutron;
 GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'localhost' \
  IDENTIFIED BY 'pwd';
 GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'%' \
  IDENTIFIED BY 'pwd';
# 要创建服务凭据
. admin-openrc
openstack user create --domain default --password-prompt neutron

openstack role add --project service --user neutron admin
openstack service create --name neutron \
  --description "OpenStack Networking" network
openstack endpoint create --region RegionOne \
  network public http://controller:9696
openstack endpoint create --region RegionOne \
  network internal http://controller:9696
openstack endpoint create --region RegionOne \
  network admin http://controller:9696
 
# 配置网络选项
apt install neutron-server neutron-plugin-ml2 \
  neutron-linuxbridge-agent neutron-dhcp-agent \
  neutron-metadata-agent

root@controller:~# grep -Ev '^$|^#' /etc/neutron/neutron.conf
[DEFAULT]
core_plugin = ml2
service_plugins =
transport_url = rabbit://openstack:pwd@controller
notify_nova_on_port_status_changes = true
notify_nova_on_port_data_changes = true
auth_strategy = keystone
[agent]
root_helper = "sudo /usr/bin/neutron-rootwrap /etc/neutron/rootwrap.conf"
[cache]
[cors]
[database]
connection = mysql+pymysql://neutron:pwd@controller/neutron
[healthcheck]
[ironic]
[keystone_authtoken]
www_authenticate_uri = http://controller:5000
auth_url = http://controller:5000
memcached_servers = controller:11211
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = neutron
password = pwd
[nova]
auth_url = http://controller:5000
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = RegionOne
project_name = service
username = nova
password = pwd
[oslo_concurrency]
lock_path = /var/lib/neutron/tmp
[oslo_messaging_amqp]
[oslo_messaging_kafka]
[oslo_messaging_notifications]
[oslo_messaging_rabbit]
[oslo_middleware]
[oslo_policy]
[oslo_reports]
[placement]
[privsep]
[profiler]
[quotas]
[ssl]


root@controller:~# grep -Ev '^$|^#' /etc/neutron/plugins/ml2/ml2_conf.ini
[DEFAULT]
[ml2]
type_drivers = flat,vlan
tenant_network_types =
mechanism_drivers = linuxbridge
extension_drivers = port_security
[ml2_type_flat]
flat_networks = provider
[ml2_type_geneve]
[ml2_type_gre]
[ml2_type_vlan]
[ml2_type_vxlan]
[ovs_driver]
[securitygroup]
enable_ipset = true
[sriov_driver]

root@controller:~# grep -Ev '^$|^#' /etc/neutron/plugins/ml2/linuxbridge_agent.ini
[DEFAULT]
[agent]
[linux_bridge]
physical_interface_mappings = provider:ens160
[network_log]
[securitygroup]
enable_security_group = true
firewall_driver = neutron.agent.linux.iptables_firewall.IptablesFirewallDriver
[vxlan]
enable_vxlan = false

# 确保您的 Linux 操作系统内核支持网桥过滤器
root@controller:~# sysctl net.bridge.bridge-nf-call-iptables
net.bridge.bridge-nf-call-iptables = 1
root@controller:~# sysctl net.bridge.bridge-nf-call-ip6tables
net.bridge.bridge-nf-call-ip6tables = 1

root@controller:~# grep -Ev '^$|^#'  /etc/neutron/dhcp_agent.ini
[DEFAULT]
interface_driver = linuxbridge
dhcp_driver = neutron.agent.linux.dhcp.Dnsmasq
enable_isolated_metadata = true
[agent]
[ovs]


# 配置元数据代理
head -c 32 /dev/urandom | base64
ulBuAvKMVU7BRJCQ0P5MiglGN9kmq6oyB/A8t+mAM6I=

vim /etc/neutron/metadata_agent.ini
[DEFAULT]
nova_metadata_host = controller
metadata_proxy_shared_secret = ulBuAvKMVU7BRJCQ0P5MiglGN9kmq6oyB/A8t+mAM6I=

# 将计算服务配置为使用网络服务
vim /etc/nova/nova.conf
[neutron]
auth_url = http://controller:5000
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = RegionOne
project_name = service
username = neutron
password = pwd
service_metadata_proxy = true
metadata_proxy_shared_secret = ulBuAvKMVU7BRJCQ0P5MiglGN9kmq6oyB/A8t+mAM6I=


# 导入数据表
su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf \
  --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron
service nova-api restart
service neutron-server restart
service neutron-linuxbridge-agent restart
service neutron-dhcp-agent restart
service neutron-metadata-agent restart




  








ovn没有安装








```

## 计算节点

```shell
apt install neutron-linuxbridge-agent

vim /etc/neutron/neutron.conf
[DEFAULT]
transport_url = rabbit://openstack:pwd@controller

[DEFAULT]
auth_strategy = keystone

[keystone_authtoken]
www_authenticate_uri = http://controller:5000
auth_url = http://controller:5000
memcached_servers = controller:11211
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = neutron
password = pwd


[oslo_concurrency]
lock_path = /var/lib/neutron/tmp



# 配置 Linux 网桥代理
vim /etc/neutron/plugins/ml2/linuxbridge_agent.ini
[linux_bridge]
physical_interface_mappings = provider:PROVIDER_INTERFACE_NAME
[vxlan]
enable_vxlan = false
[securitygroup]
enable_security_group = true
firewall_driver = neutron.agent.linux.iptables_firewall.IptablesFirewallDriver


# 确保您的 Linux 操作系统内核支持网桥过滤器
root@controller:~# sysctl net.bridge.bridge-nf-call-iptables
net.bridge.bridge-nf-call-iptables = 1
root@controller:~# sysctl net.bridge.bridge-nf-call-ip6tables
net.bridge.bridge-nf-call-ip6tables = 1


# 将计算服务配置为使用网络服务
vim /etc/nova/nova.conf
[neutron]
auth_url = http://controller:5000
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = RegionOne
project_name = service
username = neutron
password = pwd

# 重新启动计算服务&重新启动 Linux 网桥代理
service nova-compute restart
service neutron-linuxbridge-agent restart

```

## 验证

`控制节点`

```shell
root@controller:~# openstack network agent list
+--------------------------------------+--------------------+------------+-------------------+-------+-------+---------------------------+
| ID                                   | Agent Type         | Host       | Availability Zone | Alive | State | Binary                    |
+--------------------------------------+--------------------+------------+-------------------+-------+-------+---------------------------+
| 882eeb72-0a1b-4e43-bd55-9ce1e59af4b8 | Linux bridge agent | computer   | None              | :-)   | UP    | neutron-linuxbridge-agent |
| aba64957-e529-40a1-b8ca-cc093298ed96 | Metadata agent     | controller | None              | :-)   | UP    | neutron-metadata-agent    |
| dc7c5413-0f73-4f37-8f71-85d29b79cffa | Linux bridge agent | controller | None              | :-)   | UP    | neutron-linuxbridge-agent |
| ddb34434-8fcc-4730-8a35-7af593f89100 | DHCP agent         | controller | nova              | :-)   | UP    | neutron-dhcp-agent        |
+--------------------------------------+--------------------+------------+-------------------+-------+-------+---------------------------+
```





# 仪表盘Horizon

```shell
apt install openstack-dashboard -y
vim /etc/openstack-dashboard/local_settings.py
OPENSTACK_HOST = "controller"

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

CACHES = {
    'default': {
         'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
         'LOCATION': 'controller:11211',
    }
}

# 配置示例
root@controller:/var/log/apache2# grep -Ev '^$|^#' /etc/openstack-dashboard/local_settings.py 
import os
from django.utils.translation import gettext_lazy as _
from horizon.utils import secret_key
from openstack_dashboard.settings import HORIZON_CONFIG
DEBUG = False
LOCAL_PATH = os.path.dirname(os.path.abspath(__file__))
SECRET_KEY = secret_key.generate_or_read_from_file('/var/lib/openstack-dashboard/secret_key')
CACHES = {
    'default': {
         'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
         'LOCATION': 'controller:11211',
    },
}
SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
OPENSTACK_API_VERSIONS = {
    "identity": 3,
    "image": 2,
    "volume": 3,
}
OPENSTACK_KEYSTONE_DEFAULT_DOMAIN = "Default"
OPENSTACK_KEYSTONE_DEFAULT_ROLE = "user"
OPENSTACK_NEUTRON_NETWORK = {
    'enable_router': False,
    'enable_quotas': False,
    'enable_ipv6': False,
    'enable_distributed_router': False,
    'enable_ha_router': False,
    'enable_fip_topology_check': False,
}
OPENSTACK_HOST = "controller"
OPENSTACK_KEYSTONE_URL = "http://%s:5000/identity/v3" % OPENSTACK_HOST
OPENSTACK_KEYSTONE_MULTIDOMAIN_SUPPORT = True
TIME_ZONE = "UTC"
LOGGING = {
    'version': 1,
    # When set to True this will disable all logging except
    # for loggers specified in this configuration dictionary. Note that
    # if nothing is specified here and disable_existing_loggers is True,
    # django.db.backends will still log unless it is disabled explicitly.
    'disable_existing_loggers': False,
    # If apache2 mod_wsgi is used to deploy OpenStack dashboard
    # timestamp is output by mod_wsgi. If WSGI framework you use does not
    # output timestamp for logging, add %(asctime)s in the following
    # format definitions.
    'formatters': {
        'console': {
            'format': '%(levelname)s %(name)s %(message)s'
        },
        'operation': {
            # The format of "%(message)s" is defined by
            # OPERATION_LOG_OPTIONS['format']
            'format': '%(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            # Set the level to "DEBUG" for verbose output logging.
            'level': 'DEBUG' if DEBUG else 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'console',
        },
        'operation': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'operation',
        },
    },
    'loggers': {
        'horizon': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'horizon.operation_log': {
            'handlers': ['operation'],
            'level': 'INFO',
            'propagate': False,
        },
        'openstack_dashboard': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'novaclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'cinderclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'keystoneauth': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'keystoneclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'glanceclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'neutronclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'swiftclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'oslo_policy': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'openstack_auth': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'django': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        # Logging from django.db.backends is VERY verbose, send to null
        # by default.
        'django.db.backends': {
            'handlers': ['null'],
            'propagate': False,
        },
        'requests': {
            'handlers': ['null'],
            'propagate': False,
        },
        'urllib3': {
            'handlers': ['null'],
            'propagate': False,
        },
        'chardet.charsetprober': {
            'handlers': ['null'],
            'propagate': False,
        },
        'iso8601': {
            'handlers': ['null'],
            'propagate': False,
        },
        'scss': {
            'handlers': ['null'],
            'propagate': False,
        },
    },
}
SECURITY_GROUP_RULES = {
    'all_tcp': {
        'name': _('All TCP'),
        'ip_protocol': 'tcp',
        'from_port': '1',
        'to_port': '65535',
    },
    'all_udp': {
        'name': _('All UDP'),
        'ip_protocol': 'udp',
        'from_port': '1',
        'to_port': '65535',
    },
    'all_icmp': {
        'name': _('All ICMP'),
        'ip_protocol': 'icmp',
        'from_port': '-1',
        'to_port': '-1',
    },
    'ssh': {
        'name': 'SSH',
        'ip_protocol': 'tcp',
        'from_port': '22',
        'to_port': '22',
    },
    'smtp': {
        'name': 'SMTP',
        'ip_protocol': 'tcp',
        'from_port': '25',
        'to_port': '25',
    },
    'dns': {
        'name': 'DNS',
        'ip_protocol': 'tcp',
        'from_port': '53',
        'to_port': '53',
    },
    'http': {
        'name': 'HTTP',
        'ip_protocol': 'tcp',
        'from_port': '80',
        'to_port': '80',
    },
    'pop3': {
        'name': 'POP3',
        'ip_protocol': 'tcp',
        'from_port': '110',
        'to_port': '110',
    },
    'imap': {
        'name': 'IMAP',
        'ip_protocol': 'tcp',
        'from_port': '143',
        'to_port': '143',
    },
    'ldap': {
        'name': 'LDAP',
        'ip_protocol': 'tcp',
        'from_port': '389',
        'to_port': '389',
    },
    'https': {
        'name': 'HTTPS',
        'ip_protocol': 'tcp',
        'from_port': '443',
        'to_port': '443',
    },
    'smtps': {
        'name': 'SMTPS',
        'ip_protocol': 'tcp',
        'from_port': '465',
        'to_port': '465',
    },
    'imaps': {
        'name': 'IMAPS',
        'ip_protocol': 'tcp',
        'from_port': '993',
        'to_port': '993',
    },
    'pop3s': {
        'name': 'POP3S',
        'ip_protocol': 'tcp',
        'from_port': '995',
        'to_port': '995',
    },
    'ms_sql': {
        'name': 'MS SQL',
        'ip_protocol': 'tcp',
        'from_port': '1433',
        'to_port': '1433',
    },
    'mysql': {
        'name': 'MYSQL',
        'ip_protocol': 'tcp',
        'from_port': '3306',
        'to_port': '3306',
    },
    'rdp': {
        'name': 'RDP',
        'ip_protocol': 'tcp',
        'from_port': '3389',
        'to_port': '3389',
    },
}
DEFAULT_THEME = 'ubuntu'
WEBROOT='/horizon/'
ALLOWED_HOSTS = '*'
COMPRESS_OFFLINE = True


```



![image-20230927183933167](assets/image-20230927183933167.png)

官网中`OPENSTACK_KEYSTONE_URL = "http://%s/identity/v3" % OPENSTACK_HOST`错误

需要改为`OPENSTACK_KEYSTONE_URL = "http://%s:5000/identity/v3" % OPENSTACK_HOST`否则一直认证失败。

账户名密码可以通过`cat admin-openrc`获取

参考[ubuntu - 当试图潜入地平线时，我得到：客户端被服务器配置拒绝：/usr/bin/keystone-wsgi-public - 服务器故障 (serverfault.com)](https://serverfault.com/questions/1053437/when-trying-to-loing-to-horizon-i-get-client-denied-by-server-configuration-u)

![image-20230927224116467](assets/image-20230927224116467.png)







镜像下载地址：[指数 /0.6.2 (cirros-cloud.net)](http://download.cirros-cloud.net/0.6.2/)

`wget https://ghproxy.com/https://github.com/cirros-dev/cirros/releases/download/0.6.2/cirros-0.6.2-aarch64-disk.img`



