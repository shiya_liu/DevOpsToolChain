

## 安装

环境说明

6c12g  100G磁盘 一台ubuntu 20.04 arch架构

## 环境准备

更换国内源

```shell
sudo cp /etc/apt/sources.list sources_backup.list
sudo vim /etc/apt/sources.list
删除原有的内容，添加以下：
# 默认注释了源码仓库，如有需要可自行取消注释
deb https://mirrors.ustc.edu.cn/ubuntu-ports/ focal main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu-ports/ focal main main restricted universe multiverse
deb https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-updates main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-updates main restricted universe multiverse
deb https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-backports main restricted universe multiverse
# deb-src https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-backports main restricted universe multiverse
deb https://mirrors.ustc.edu.cn/ubuntu-ports/ focal-security main restricted universe multiverse

sudo apt-get update
sudo apt-get upgrade

```



下载java jdk

```shell
apt update --fix-missing
apt list --upgradable
apt install -y openjdk-8-jdk
java -version

vim /etc/profile
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-arm64/jre
export JRE_HOME=${JAVA_HOME}/jre
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH

source /etc/profile
echo $JAVA_HOME
echo $JRE_HOME
echo $CLASSPATH
echo $PATH
```

设置时区以及时间同步

```shell
# 24小时制
echo '/etc/default/locale' > /etc/default/locale
# 设置时区
timedatectl set-timezone Asia/Shanghai
# 时间同步
apt install -y  ntpdate

/usr/sbin/ntpdate ntp1.aliyun.com
crontab -l > crontab_conf ; echo "* * * * * /usr/sbin/ntpdate ntp1.aliyun.com >/dev/null 2>&1" >> crontab_conf && crontab crontab_conf && rm -f crontab_conf

```

关闭swap

```shell
swapoff -a  
sed -ri 's/.*swap.*/#&/' /etc/fstab
free -h
```

设置系统参数

```shell
vim /etc/sysctl.conf
vm.max_map_count=2000000

sysctl -p
sysctl -w vm.max_map_count=2000000

ulimit -n 65536
sudo tee -a /etc/security/limits.conf << EOF
*               hard    nofile          65536
*               soft    nofile          65536
root            hard    nofile          65536
root            soft    nofile          65536
*               soft    nproc           65536
*               hard    nproc           65536
root            soft    nproc           65536
root            hard    nproc           65536
*               soft    core            65536
*               hard    core            65536
root            soft    core            65536
root            hard    core            65536
EOF
```

## ilogtail

[采集配置 - iLogtail用户手册 (gitbook.io)](https://ilogtail.gitbook.io/ilogtail-docs/configuration/collection-config)



| 日志             | 格式处理 | 目标格式                         |
| ---------------- | -------- | -------------------------------- |
| Java程序日志     |          | time ip hostname logpath message |
| Java业务日志     |          |                                  |
| Linux journalctl | 无法处理 |                                  |
| Nginx 日志       |          |                                  |
| redis日志        |          |                                  |
| mysql日志        |          |                                  |



```shell
# 安装
wget https://ilogtail-community-edition.oss-cn-shanghai.aliyuncs.com/1.7.1/ilogtail-1.7.1.linux-arm64.tar.gz
wget https://ilogtail-community-edition.oss-cn-shanghai.aliyuncs.com/1.8.0/ilogtail-1.8.0.linux-arm64.tar.gz

tar zxvf ilogtail-1.7.1.linux-arm64.tar.gz
cd ilogtail-1.7.1
# 配置收集 journalctl 日志
vim user_yaml_config.d/journalctl.yaml
enable: true
inputs:
  - Type: service_journal
    JournalPaths:
      - "/var/log/journal"
processors:
  - Type: processor_add_fields
    Fields: 
      ip: 10.0.0.10
     	logPath: /var/log/journal
    IgnoreIfExist: false
flushers:
  - Type: flusher_kafka_v2
    Brokers: 
      - 127.0.0.1:9092
    Topic: journalctllog
输出：
2023-10-24 01:27:18  MESSAGE _HOSTNAME logPath ip

{"PRIORITY":"6","_PID":"1","_CMDLINE":"/sbin/init","CODE_FUNC":"unit_log_success","_SELINUX_CONTEXT":"unconfined\n","SYSLOG_FACILITY":"3","_UID":"0","CODE_FILE":"src/core/unit.c","_COMM":"systemd","_SYSTEMD_SLICE":"-.slice","SYSLOG_IDENTIFIER":"systemd","MESSAGE_ID":"7ad2d189f7e94e70a38c781354912448","_EXE":"/usr/lib/systemd/systemd","_SOURCE_REALTIME_TIMESTAMP":"1698110359282536","CATALOG_ENTRY":"Subject: Unit succeeded\nDefined-By: systemd\nSupport: http://www.ubuntu.com/support\n\nThe unit packagekit.service has successfully entered the 'dead' state.\n","INVOCATION_ID":"0a2bcf64b8b94e1ab1a930ca02a3e6cd","_MACHINE_ID":"8a73df84fb32467ea0a90f1ac3505b24","_GID":"0","_CAP_EFFECTIVE":"3fffffffff","CODE_LINE":"5870","UNIT":"packagekit.service","MESSAGE":"packagekit.service: Succeeded.","_HOSTNAME":"DorisLog","_SYSTEMD_CGROUP":"/init.scope","_TRANSPORT":"journal","_BOOT_ID":"745b0f69d62743e7b505190f6fdf2b38","_SYSTEMD_UNIT":"init.scope","_realtime_timestamp_":"1698110359282594","_monotonic_timestamp_":"3458025669","ip":"10.0.0.10","logPath":"/var/log/journal","__time__":"1698110835"}

# 配置收集nginx日志
vim user_yaml_config.d/nginx.yaml
enable: true
inputs:
  - Type: file_log          
    LogPath: /var/log/nginx/             
    FilePattern: "*.log"
processors:
  - Type: processor_add_fields
    Fields:
      ip: 10.0.0.10
      hostname: Doris
    IgnoreIfExist: false
flushers:
  - Type: flusher_kafka_v2
    Brokers: 
      - 127.0.0.1:9092
    Topic: nginxlog

输出信息
2023-10-24 01:23:15 {
"__tag__:__path__":"/var/log/nginx/access.log",
"content":"127.0.0.1 - - [24/Oct/2023:01:23:14 +0000] \"GET / HTTP/1.1\" 200 612 \"-\" \"curl/7.68.0\"",
"ip":"10.0.0.10",
"hostname":"Doris",
"__time__":"1698110594"
}


# 配置收集java日志
vim user_yaml_config.d/java.yaml
enable: true
inputs:
  - Type: file_log
    LogPath: /var/log/
    FilePattern: server.log
processors:
  - Type: processor_split_log_regex
    SplitRegex: \d+-\d+-\d+\s\d+:\d+:\d+.\d+\s.*
    SplitKey: content
    PreserveOthers: true
  - Type: processor_regex
    SourceKey: content
    Regex: (\d+-\d+-\d+\s\d+:\d+:\d+.\d+) (.*)
    Keys:
    	- logtime
    	- message
  - Type: processor_strptime
    SourceKey: "time"
    DestFormat: "%Y-%m-%d %H:%M:%S"
flushers:
  - Type: flusher_kafka_v2
    Brokers: 
      - 127.0.0.1:9092
    Topic: javalog

输出信息：



# 启动
nohup /root/ilogtail-1.7.1/ilogtail > stdout.log 2> stderr.log &

# 查看采集到的文件日志
cat stdout.log

# 输出到Kafka

flushers:
  - Type: flusher_kafka_v2
    Brokers: 
      - 127.0.0.1:9092
    Topic: demolog


```

社区问题讨论：

[ilogtail time是unix时间戳 是否支持改为yyyy-mm-dd格式 · alibaba/ilogtail · Discussion #1196 (github.com)](https://github.com/alibaba/ilogtail/discussions/1196)

[ilogtail-1.8.0 启动报错：Segmentation fault (core dumped) · alibaba/ilogtail · Discussion #1195 (github.com)](https://github.com/alibaba/ilogtail/discussions/1195)

```shell
wget http://logtail-release-cn-hangzhou.oss-cn-hangzhou.aliyuncs.com/linux64/logtail.sh
wget http://logtail-release-cn-hangzhou.oss-cn-hangzhou.aliyuncs.com/linux64/aarch64/logtail-linux64.tar.gz
chmod +x logtail.sh; ./logtail.sh install-local cn-hangzhou
```





## kafka

```shell
wget http://mirrors.aliyun.com/apache/kafka/3.5.1/kafka_2.13-3.5.1.tgz
tar zxvf kafka_2.13-3.5.1.tgz
cd kafka_2.13-3.5.1
sudo bin/zookeeper-server-start.sh -daemon  config/zookeeper.properties 
sudo bin/kafka-server-start.sh -daemon config/server.properties 

# 创建topic
bin/kafka-topics.sh --bootstrap-server localhost:9092 --create --topic demolog --partitions 4 --replication-factor 1
# 查询topic内容
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic demolog --from-beginning


# 安装Kafka的UI
mkdir /root/kafka_ui/
cd /root/kafka_ui/
# wget https://ghproxy.com/https://github.com/provectus/kafka-ui/releases/download/v0.7.1/kafka-ui-api-v0.7.1.jar
vim config.yml
kafka:
  clusters:
    - name: kafka
      bootstrapServers: 127.0.0.1:9092
      metrics:
        port: 9094
        type: JMX

    - name: OTHER_KAFKA_CLUSTER_NAME
      bootstrapServers: 127.0.0.1:9092
      metrics:
        port: 9094
        type: JMX

spring:
  jmx:
    enabled: true
  security:
    user:
      name: admin
      password: admin

auth:
  type: DISABLED #LOGIN_FORM # DISABLED

server:
  port: 8080

logging:
  level:
    root: INFO
    com.provectus: INFO
    reactor.netty.http.server.AccessLog: INFO

management:
  endpoint:
    info:
      enabled: true
    health:
      enabled: true
  endpoints:
    web:
      exposure:
        include: "info,health"

# 安装docker docker-compose
apt install -y docker.io docker-compose
# 配置docker-compose.yml
cat docker-compose.yml 
version: '3'
services:
  kafka-ui:
    network_mode: host
    container_name: kafka-ui
    image: provectuslabs/kafka-ui:latest
    ports:
      - 8080:8080
    environment:
      DYNAMIC_CONFIG_ENABLED: 'true'
    volumes:
      - ./config.yml:/etc/kafkaui/dynamic_config.yaml
# 启动容器
docker-compose up -d
```

安装nginx

```shell
apt install -y nginx apache2-utils
ab -n1000 -c10 http://127.0.0.1/
```



安装filebeat

```shell
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo apt install apt-transport-https -y

echo "deb https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list
apt update  -y

apt install -y filebeat lrzsz net-tools

root@DorisDemo:~# grep -Ev '#|^$' /etc/filebeat/filebeat.yml  
filebeat.inputs:
- type: log
  enabled: true
  paths:
    - /var/log/server.log
  fields_under_root: true
  multiline.pattern: '^[0-9]{4}-[0-9]{2}-[0-9]{2}'
  multiline.negate: true
  multiline.match: "after"
output.kafka:
  codec.format:
    string: '%{[hostip]}\\001%{[agent.hostname]}\\001%{[log.file.path]}\\001%{[message]}'
  hosts: ["127.0.0.1:9092"]
  topic: demolog1
  enabled: true
  partition.round_robin:
    reachable_only: true
filebeat.config.modules:
  path: ${path.config}/modules.d/*.yml
  reload.enabled: false
setup.template.settings:
  index.number_of_shards: 1
setup.kibana:
processors:
  - add_fields:
      target: ''
      fields:
        hostip: '10.0.0.10'
  - add_host_metadata:
      when.not.contains.tags: forwarded
  - add_cloud_metadata: ~
  - add_docker_metadata: ~
  - add_kubernetes_metadata: ~
  

sudo systemctl enable filebeat
sudo systemctl restart filebeat
sudo systemctl status filebeat

```



## Doris

下载Doris二进制包

```shell
wget https://apache-doris-releases.oss-accelerate.aliyuncs.com/apache-doris-2.0.1.1-bin-arm64.tar.gz
tar zxvf apache-doris-2.0.1.1-bin-arm64.tar.gz
```

配置 FE

```shell
cd apache-doris-2.0.1.1-bin-arm64/fe
mkdir /fe
vim conf/fe.conf
priority_networks=10.0.0.0/24
meta_dir=/fe
```

启动 FE

```shell
./bin/start_fe.sh --daemon

curl http://127.0.0.1:8030/api/bootstrap
页面访问：http://fe_ip:8030
账户 root 密码空
```

![image-20231008100525529](assets/image-20231008100525529.png)

使用mysql- client登录

```shell
apt install -y mysql-client
mysql -uroot -P9030 -h127.0.0.1
#查看 FE 运行状态
show frontends\G;
```

配置 BE

```shell
cd apache-doris-2.0.1.1-bin-arm64/be
# 创建BE数据存储目录
mkdir /be

vim conf/be.conf
priority_networks=10.0.0.0/24
storage_root_path=/be
# JAVA_HOME=/usr/lib/jvm/java-8-openjdk-arm64
# 如果Java没有添加到系统环境变量则需要这一步 启动脚本第一行添加
vim bin/start_be.sh
export JAVA_HOME=your_java_home_path

```

启动 BE

```shell
./bin/start_be.sh --daemon
```

添加 BE 节点到集群

```shell
mysql -uroot -P9030 -h127.0.0.1
ALTER SYSTEM ADD BACKEND "10.0.0.10:9050";
SHOW BACKENDS\G
```

停止服务

```shell
# 停止FE
/root/apache-doris-2.0.1.1-bin-arm64/fe/bin/stop_fe.sh
# 停止BE
/root/apache-doris-2.0.1.1-bin-arm64/be/bin/stop_be.sh
```

## grafana

```shell
apt install -y  musl
sudo dpkg --install  grafana-enterprise_10.1.4_arm64.deb  

```



Kafka数据导入到Doris 可以使用flink

[Flink 使用 SQL 读取 Kafka 写入到Doris表中-阿里云开发者社区 (aliyun.com)](https://developer.aliyun.com/article/920301)

[订阅 Kafka 日志 - Apache Doris](https://doris.apache.org/zh-CN/docs/dev/data-operate/import/import-scenes/kafka-load)

```shell
mysql -uroot -P9030 -h127.0.0.1
# 创建表
create database kafka_demo;
use kafka_demo;

CREATE TABLE kafka_demo.demolog
(
    `host_ip` VARCHAR(65530) COMMENT "主机地址",
    `hostname` VARCHAR(65530)  COMMENT "主机名",
    `logpath` string COMMENT "日志路径",
    `message` string COMMENT "日志信息"
)
DUPLICATE KEY(`host_ip`, `hostname`)
DISTRIBUTED BY HASH(`hostname`) BUCKETS 1
PROPERTIES (
    "replication_allocation" = "tag.location.default: 1"
);

# 创建案例导入
CREATE ROUTINE LOAD kafka_demo.demolog ON demolog
COLUMNS(hostip,hostname,logpath,message)
PROPERTIES
(
    "desired_concurrent_number"="3",
    "max_batch_interval" = "20",
    "max_batch_rows" = "300000",
    "max_batch_size" = "209715200",
    "strict_mode" = "false"
 )FROM KAFKA
(
    "kafka_broker_list" = "127.0.0.1:9092",
    "kafka_topic" = "demolog",
    "kafka_partitions" = "0,1,2",
    "kafka_offsets" = "0,0,0"
 );

#查看是否从kafka导入了数据 
select count(*) from demolog;

# 查看routine load任务
SHOW ALL ROUTINE LOAD;
# 暂停routine load
PAUSE ALL ROUTINE LOAD;
# 恢复routine load
RESUME ALL ROUTINE LOAD;
```











安装flink

国内镜像地址：[Index of /apache/flink/ (tencent.com)](http://mirrors.cloud.tencent.com/apache/flink/)

```shell
# 安装jdk 11
apt install -y openjdk-11-jdk
# 下载flink
wget http://mirrors.cloud.tencent.com/apache/flink/flink-1.17.1/flink-1.17.1-bin-scala_2.12.tgz
wget http://mirrors.cloud.tencent.com/apache/flink/flink-connector-kafka-3.0.0/flink-connector-kafka-3.0.0-src.tgz

tar zxvf flink-1.17.1-bin-scala_2.12.tgz
tar zxvf flink-connector-kafka-3.0.0-src.tgz

# 启动
cd flink-1.17.1
./bin/start-cluster.sh


# Flink 的 Releases 附带了许多的示例作业。你可以任意选择一个，快速部署到已运行的集群上。
./bin/flink run examples/streaming/WordCount.jar
tail log/flink-*-taskexecutor-*.out

# 停止集群
./bin/stop-cluster.sh
```





Grafana对接Doris

配置数据源

![image-20231010160519119](assets/image-20231010160519119.png)

![image-20231010160612238](assets/image-20231010160612238.png)



测试环境信息
```shell
机器：205

登录Doris：mysql -uroot -P9030  -h127.0.0.1

查看表数据：select count(*) from ods_logs.demolog;

Kafka查看topic:
/home/guofuyinan/kafka_2.13-3.4.0/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic ods-logs-demolog --from-beginning ods-logs-demolog

```





```sh
[root@server203 ~]# cat /etc/filebeat/filebeat.yml 
filebeat.inputs:
- type: log
  enabled: true
  id: "javalog"
  paths:
    - /var/log/server.log
  processors:
    - script:
        lang: javascript
        id: javalog
        source: >
          function process(event) {
            var logLine = event.Get("message");
            var regex = /\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/g;
            var match = logLine.match(regex).toString();
            var newMessage = logLine.substring(24);
            if (match) {
              event.Put("time", match);
              event.Put("newMessage", newMessage);
            }
          }
  fields_under_root: true
  multiline.pattern: '^[0-9]{4}-[0-9]{2}-[0-9]{2}'
  multiline.negate: true
  multiline.match: "after"

    - script:
        lang: javascript
        id: javalog
        source: >
          function process(event) {
            var logLine = event.Get("message");
            var regex = /\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/g;
            var match = logLine.match(regex).toString();
            var newMessage = logLine.substring(24);
            if (match) {
              event.Put("time", match);
              event.Put("newMessage", newMessage);
            }
          }
  fields_under_root: true
  multiline.pattern: '^[0-9]{4}-[0-9]{2}-[0-9]{2}'
  multiline.negate: true
  multiline.match: "after"




output.kafka:
#  codec.format:
#    string: '{\"createTime\":\"%{[time]}\",\"ip\":\"%{[hostip]}\",\"hostname\":\"%{[agent.hostname]}\",\"logPath\":\"%{[log.file.path]}\",\"message\":\"%{[newMessage]}\"}'
  hosts: ["192.168.0.205:9092"]
  topic: ods-logs-demolog
  enabled: true
  partition.round_robin:
    reachable_only: true
filebeat.config.modules:
  path: ${path.config}/modules.d/*.yml
  reload.enabled: false
setup.template.settings:
  index.number_of_shards: 1
setup.kibana:
processors:
  - add_fields:
      target: ''
      fields:
        hostip: '192.168.0.203'
  - add_host_metadata:
      when.not.contains.tags: forwarded
  - add_cloud_metadata: ~
  - add_docker_metadata: ~
  - add_kubernetes_metadata: ~
  - add_locale: 
      format: abbreviation
```

