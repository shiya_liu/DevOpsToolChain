

安装部署

参考文章：[Qemu-kvm虚拟机搭建 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/636162438)好用！



```shell
#命令执行结果如果返回0，表示CPU不支持虚拟化技术。
egrep -c '(svm|vmx)' /proc/cpuinfo
# 安装virt
yum install qemu-kvm virt-install libvirt vnc kvm libvirt python-virtinst qemu-kvm virt-viewer tunctl bridge-utils avahi dmidecode qemu-kvm-tools virt-manager qemu-img virt-install net-tools libguestfs-tools bridge-utils -y
systemctl start libvirtd.service
systemctl enable libvirtd
# 查看kvm服务是否正常
virsh -c qemu:///system list

# 创建网桥方式1
virsh iface-bridge eth0 br0
# 执行后会断开网络 需要reboot机器,并且这种方式创建br0需要手动添加dns
[root@localhost network-scripts]# cat ifcfg-br0
DEVICE="br0"
ONBOOT="yes"
TYPE="Bridge"
BOOTPROTO="none"
IPADDR="10.0.0.13"
NETMASK="255.255.255.0"
GATEWAY="10.0.0.254"
`DNS1=223.5.5.5`
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
DHCPV6C="no"
STP="on"
DELAY="0"


# 创建网桥方式二
[root@localhost network-scripts]# cat ifcfg-br0
DEVICE="br0"
NM_CONTROLLED="yes"
ONBOOT="yes"
TYPE="Bridge"
IPADDR=10.0.0.11
PREFIX=24
GATEWAY=10.0.0.254
DNS1=223.5.5.5
DNS2=8.8.8.8
[root@localhost network-scripts]# cat ifcfg-eth0
TYPE=Ethernet
`PROXY_METHOD=none`
`BROWSER_ONLY=no`
BOOTPROTO=static
DEFROUTE=yes
PEERDNS=yes
PEERROUTES=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_PEERDNS=yes
IPV6_PEERROUTES=yes
IPV6_FAILURE_FATAL=no
NAME=eth0
UUID=a8ca505e-7fbd-4b2b-baca-e171e62cd01b
DEVICE=eth0
ONBOOT=yes
`BRIDGE="br0"`
#IPADDR=10.0.0.11
#PREFIX=24
#GATEWAY=10.0.0.254
#DNS1=223.5.5.5
#DNS2=8.8.8.8

#重启网络
systemctl restart network 
[root@localhost network-scripts]# brctl  show
bridge name     bridge id               STP enabled     interfaces
br0             8000.000c29bb095b       no              eth0
docker0         8000.000000000000       no
virbr0          8000.000000000000       yes

# 创建虚拟机
virt-install \
--virt-type kvm \
--name ct7 \
--memory 2048 \
--vcpu 2 \
--cdrom /mnt/CentOS-7-x86_64-DVD-1511.iso \
--disk /mnt/ct7.qcow2,size=100 \
--network bridge=br0 \
--graphics vnc,password=123,port=5900,listen=0.0.0.0 \
--noautoconsole

#警告
WARNING  Graphics requested but DISPLAY is not set. Not running virt-viewer.
WARNING  No console to launch for the guest, defaulting to --wait -1

# 失败 参考文档：https://blog.csdn.net/spade_l/article/details/120739362
参考文档：https://www.cnblogs.com/aqicheng/p/13033790.html
ERROR    Cannot get interface MTU on 'br0': No such device
ERROR    Unable to add bridge eth0 port vnet0: Operation not supported
# 查看列表
[root@localhost ~]# virsh --connect qemu:///system list
 Id    Name                           State
----------------------------------------------------
 2     ct7                            running


# 创建winserver
virt-install \
--virt-type kvm \
--name winserver \
--memory 4048 \
--vcpu 2 \
--cdrom /winserver/windows_server_2019_x64_dvd_4de40f33.iso \
--disk /winserver/winserver.qcow2,size=150 \
--network bridge=br0 \
--graphics vnc,password=123,port=5900,listen=0.0.0.0 \
--noautoconsole
```

![image-20230627180110452](README.assets/image-20230627180110452.png)

![image-20230627181417274](README.assets/image-20230627181417274.png)

![image-20230627181400054](README.assets/image-20230627181400054.png)

![image-20230627183045148](README.assets/image-20230627183045148.png)

![image-20230627183207514](README.assets/image-20230627183207514.png)



winserver2019密钥

[windows server 2019永久激活码|winserver2019激活密钥|server2019产品密钥-博悦天下 (ibytx.com)](https://www.ibytx.com/101.html#:~:text=Windows Server 2019 Datacenter Retail： ：RC4VN-4GQBW-WYPTV-3BD66-FVXR6 Windows Server,Windows Server 2019 Essential Retail： [Key]：NMB98-MKJBC-FTVQ9-J3XK6-QYKTQ server2019永久密匙激活步骤：开始—设置—更新和安全—激活—更改产品密钥—输入密钥—激活。 或者桌面上右键此电脑—属性—底部点击"更改产品密钥"—输入密钥—激活。)





失败的编译安装，但是编译过程很消耗cpu，可以作为压测使用

```shell
参考文章：https://zhuanlan.zhihu.com/p/384173611

yum install -y python3 bzip2 glib2 glib2-devel pixman.i686 pixman.x86_64 pixman-devel.i686  pixman-devel.x86_64

wget  https://download.qemu.org/qemu-5.1.0.tar.xz
tar xvf qemu-5.1.0.tar.xz
cd qemu-5.1.0
mkdir build
cd build
../configure
time make -j16
time make install

wget https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/virtio-win-0.1.190-1/virtio-win-0.1.190.iso
wget https://releases.pagure.org/virt-viewer/virt-viewer-x86-2.0.msi



/root/qemu-5.1.0/build/qemu-img create -f qcow2 windows_server_2019_x64_dvd_4de40f33.qcow2 120G

/root/qemu-5.1.0/build/x86_64-softmmu/qemu-system-x86_64 \
  -enable-kvm \
  -smp 4 \
  -m 2G \
  -machine usb=on \
  -device usb-tablet \
  -display default \
  -vga virtio \
  -device e1000,netdev=net0 \
  -netdev user,id=net0,net=10.0.0.0/24,dhcpstart=10.0.0.5 \
  -drive file=/root/windows_server_2019_x64_dvd_4de40f33.qcow2,if=virtio \
  -drive file=/root/virtio-win.iso,index=1,media=cdrom \
  -drive file=/root/windows_server_2019_x64_dvd_4de40f33.iso,index=2,media=cdrom \
  -spice port=8891,addr=10.0.0.10,disable-ticketing

```


virsh 操作命令：https://www.cnblogs.com/lin1/p/5776280.html#:~:text=virsh%20auotstart%20--disable%20%2B%E5%9F%9F%E5%90%8D%2010%EF%BC%8C%E5%BD%BB%E5%BA%95%E5%88%A0%E9%99%A4%E8%99%9A%E6%8B%9F%E6%9C%BA%201%2C%20%E5%88%A0%E9%99%A4%E8%99%9A%E6%8B%9F%E6%9C%BA,virsh%20destroy%20%2B%E5%9F%9F%E5%90%8D%202%EF%BC%8C%E8%A7%A3%E9%99%A4%E6%A0%87%E8%AE%B0%20virsh%20undefine%20%2B%E5%9F%9F%E5%90%8D%203%EF%BC%8C%E5%88%A0%E9%99%A4%E8%99%9A%E6%8B%9F%E6%9C%BA%E6%96%87%E4%BB%B6



安装novnc

```shell
https://www.cnblogs.com/connect/p/linux-novnc.html
```

![image-20230629155307064](README.assets/image-20230629155307064.png)



novnc:

### nginx 反向代理 novnc



#### 错误配置

```shell
     location /vnc/ {
         proxy_pass  http://nas:6080/vnc.html;
       }
```



![image-20230629155103962](README.assets/image-20230629155103962.png)



#### 正确配置

```shell

    location /novnc/ {
      proxy_pass http://127.0.0.1:6080/;
    }

    location /novnc/websockify {
      proxy_pass http://127.0.0.1:6080/;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_set_header Host $host;
    }


```



[NGINX 代理背后的 Websockify & noVNC (datawookie.dev)](https://datawookie.dev/blog/2021/08/websockify-novnc-behind-an-nginx-proxy/)

![image-20230629154909454](README.assets/image-20230629154909454.png)

访问时需要输入特定链接

```shell
http://10.0.0.13/novnc/vnc.html?ressize=remote&path=novnc/websockify
```















