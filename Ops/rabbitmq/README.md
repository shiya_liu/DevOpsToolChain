rabbitmq的作用：https://blog.csdn.net/My_daidai/article/details/109360684


文档：https://blog.csdn.net/lzx1991610/article/details/102970854

官网：https://www.rabbitmq.com/

参考博客：

[(4条消息) python对RabbitMQ的简单使用_python rabbitmq_想个名字蒸难的博客-CSDN博客](https://blog.csdn.net/weixin_43810267/article/details/123914324)


[Python实战案例:python结合rabbitmq+redis实现秒杀系统](https://blog.csdn.net/play_big_knife/article/details/114219352)

文档：https://zq99299.github.io/mq-tutorial/rabbitmq-ac/

该文档摘抄自[MQ 系列教程（笔记） (zq99299.github.io)](https://zq99299.github.io/mq-tutorial/),原文中使用Java代码进行演示，本文档改为python代码演示



# 问题

```shell
1、rabbitmq可以抗住多少并发？
2、一个生产者 一个消费者 应该如何使用rabbitmq
3、一个生产者 多个消费者 应该如何使用rabbitmq
4、多个生产者 多个消费者 应该如何使用rabbitmq
5、rabbitmq 如何搭建高可用？
6、rabbitmq 有主从架构吗？
7、rabbitmq 的原理？ 是将信息存储在内存还是磁盘？ 
```





# 简介



RabbitMQ 是目前非常热门的一款 **消息中间件**，不管是互联网行业还是传统行业都在大量使用。它具有高可靠、易扩展、高可用及丰富的功能特性。



## 什么是消息中间件

**消息（Message）**：在应用层传送的数据。比如文本字符串、JSON 等。

**消息队列中间件（Message Queue Middleware，MQ）**：利用 **可靠的消息传递机制** 进行 **与平台无关的数据交流**，并基于数据通信来进行分布式系统的集成。通过消息排队模型，它可以在 **分布式环境下扩展进程间的通信**。

消息队列中间件一般有两种传递模式：

- 点对点（P2P，Point-to-Point）模式

  基于 **队列**，消息生产者发送消息到队列，消息消费者从队列中接收消息。

  队列的存在使得消息的 **异步传输**成 为可能。

- 发布/订阅（Pub/Sub）模式

  定义了如何向一个内容节点发布和订阅消息，内容节点称为 **主题（topic）**，消息发布者将消息 **发布到某个主题**，而消息订阅者则 **从主题中订阅消息**。

  该模式在 **消息的一对多广播时采用**

消息中间件适用于需要可靠的数据传送的分布式环境。发送者将消息发送给消息服务器，消息服务器将 **消息存放在若干队列中**，在合适的时候再将消息 **转发给接收者**。实现应用程序之间的协同，优点在于能够在客户和服务器之间提供同步和异步的链接，并且在任何时刻都可以将消息进行传送或存储转发。

![image-20230630094419659](assets/image-20230630094419659.png)



例如上图：A、B程序使用消息中间件进行通信。程序 A、B 可以不在同一台服务器上，A 发送消息给 B，消息中间件负责处理网络通信，如果网络连接不可用，会存储消息，直到连接变得可用，再将消息转发给 B。

灵活的体现在于：程序 B 可以不在线，A 也能投递消息，防止 A 等待 B 处理出现的阻塞。

## 消息中间件的作用

在不同的应用场景下有不同的作用，总的来说，可以概括如下：

- 解耦

  对于项目的变化，很难预测到未来的变动。消息中间件在处理过程中间插入了一个隐含、基于数据的接口层，两边的处理过程都要实现这一接口，但是两边都可以独立的扩展或则修改自己的处理过程，只要 **确保他们遵守同样的接口约束** 即可。

- 冗余（存储）

  某些情况下，处理数据的过程可能失败。消息中间件可以把数据持久化直到他们已经被完全处理，通过这一方式 **规避了数据丢失的风险**。

  在把一个消息从中间件删除时，需要你明确的指出该消息已经被处理完成。

- 扩展性

  因为 **解耦了应用程序的处理过程**，所以 **提高消息入队** 和 **处理效率** 就变得很容易了，只要增加额外的处理过程即可，不需要改变代码和参数

- 削峰

  在访问量剧增的场景下，应用需要继续发挥作用，但是这样的突然流量并不常见。如果以能处理这类峰值为标准而投入资源（比如硬件），无疑是巨大的浪费。使用消息中间件能够使关键组件支持突发访问压力，不会因为突发的超负荷请求而完全崩溃。类似于缓冲地带。

- 可恢复性

  当系统一部分组件失效时，不会影响到整个系统。当这部分组件被修复后，再连接上消息中间件，可以继续处理未处理完的消息。

- 顺序保证

  大多数场景下，数据处理的顺序很重要，大部分消息中间件支持一定程度上的顺序性。

- 异步通信

  在很多时候，应用不想也不需要立即处理消息。将消息放入消息中间件中，但并不立即处理它，在之后需要的时候再慢慢处理。

## RabbitMQ 的起源

RabbitMQ 是采用 Erlang 语言实现 **AMQP（Advanced Message Queuing Protocol）高级消息队列协议** 的消息中间件，最初起源于金融系统，用于在 **分布式系统中存储转发消息**。

商业 MQ 供应商想要解决 **应用互通** 的问题，而不是去创建标准来实现不同的 MQ 产品间的互通，或则运行应用程序更改 MQ 平台。价格高昂、还不通用。

为了解决这种困境，**JMS（Java Message Service）** 就应运而生。JMS 试图通过提供 **公共 Java API** 的方式，隐藏单独 MQ 产品供应商提供的时实际接口，解决了互通问题。从技术上讲：只要针对 JMS API 编程，选择合适的 MQ 驱动，JMS 会打理好其他的部分。**ActiveMQ** 就是 JMS 的一种实现。由于 JMS 使用单独标准化接口来胶合众多不同的接口，最终会暴露出问题，使得程序变得脆弱。急需一种新的消息通信标准化方案。

2006 年 6 月，由 Cisco、Redhat、iMatix 等联合制定了 AMQP 的公开标准，它是 **应用层协议的一个开放标准**，以解决众多消息中间件的需求和拓扑结构问题。它为 **面向消息的中间件** 设计，基于此协议的客户端与消息中间件可传递消息，并 **不受产品、开放语言等条件的限制**。

RabbitMQ 最初版本实现了 AMQP 的一个关键特性：使用协议本身就可以对队列和交换器（Exchange）这样的资源进行配置。而商业 MQ 则需要特定的管理终端。RabbitMQ 的资源配置能力使其成为构建分布式应用的最完美的通信总线。

Rabbit 英译为兔子，含义为：兔子行动非常迅速且繁殖起来非常疯狂。因此使用 RabbitMQ 来命名这个分布式软件。

RabbitMQ 发展到今天，被越来越多人认可，这和它在易用性、扩展性、可靠性和高可用性等方面的卓越表现是分不开的。具体特点可以概括为以下几点：

- 可靠性：使用一些机制来保证可靠性

  如：持久化、**传输确认**、**发布确认**等。

- 灵活的路由：在消息进入队列之前，通过 **交换器** 来 **路由** 消息

  对于典型的路由功能，提供了 **内置的交换器** 来实现。针对复杂的路由功能，可以 **将多个交换器绑定在一起**，也可以通过 **插件机制** 来实现自己的交换器

- 扩展性：多个 RabbitMQ 节点可以组成一个集群，也可以动态扩展集群节点。

- 高可用性：

  **队列** 可以在集群中的机器上设置 **镜像**，使得在部分节点出现问题的情况下，队列仍然可用。

- 多种协议：原生支持 AMQP 协议

  还支持 STOMP、MQTT 等多种消息中间件协议

- 多语言客户端：支持常用语言客户端

  如：Java、Python、Ruby、PHP、`C#`、JavaScript

- 管理界面

  提供了一个易用的用户界面，使得用户可以 **监控和管理消息、集群中的节点** 等

- 插件机制：

  提供了许多插件，以实现从多方面进行扩展，也可以自己编写插件。



## 安装部署

```shell
# 安装erlang
rpm --import https://packages.erlang-solutions.com/rpm/erlang_solutions.asc
vim /etc/yum.repos.d/erlang-solutions.repo
[erlang-solutions]
name=CentOS $releasever - Erlang Solutions
baseurl=https://mirrors.tuna.tsinghua.edu.cn/erlang-solutions/centos/$releasever/
gpgcheck=1
gpgkey=https://packages.erlang-solutions.com/rpm/erlang_solutions.asc
enabled=1

yum makecache
yum list erlang --showduplicates | sort -r
yum install erlang -y
erl -version

wget https://ghproxy.com/https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.10.0/rabbitmq-server-3.10.0-1.el8.noarch.rpm
yum localinstall rabbitmq-server-3.10.0-1.el8.noarch.rpm -y

# 开启管理界面
rabbitmq-plugins enable rabbitmq_management
systemctl start rabbitmq-server #启动服务
systemctl status rabbitmq-server #查看服务状态
systemctl stop rabbitmq-server #停止服务
systemctl enable rabbitmq-server #开启启动服务
systemctl restart rabbitmq-server 
```

### 新增账户

默认情况下 RabbitMQ 有一个 guest 账户，只允许通过 `localhost` 访问，远程网络访问受限。所以需要新添加一个账户

```shell
# 创建 admin 用户，密码为 root
[root@study ~]# rabbitmqctl add_user admin root
Creating user "admin"

# 设置用户拥有所有权限
[root@study ~]# rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
Setting permissions for user "admin" in vhost "/"

# 设置 admin 用户为 管理员角色
[root@study ~]# rabbitmqctl set_user_tags admin administrator
Setting tags for user "admin" to [administrator]
```

### 生产和消费



**生产者代码**

```python
# coding=utf-8
"""
生产者
"""

import pika
import time

# 用户名和密码
user_info = pika.PlainCredentials('admin', 'root')
# 连接服务器上的RabbitMQ服务
connection = pika.BlockingConnection(pika.ConnectionParameters('10.0.0.10', 5672, '/', user_info))

# 创建一个channel
channel = connection.channel()

# 如果指定的queue不存在，则会创建一个queue，如果已经存在 则不会做其他动作，官方推荐，每次使用时都可以加上这句
channel.queue_declare(queue='hello')

for i in range(0, 100):
    # 当前是一个简单模式，所以这里设置为空字符串就可以了
    channel.basic_publish(exchange='',
                          routing_key='hello',  # 指定消息要发送到哪个queue
                          body='{}'.format(i)  # 指定要发送的消息
                          )
    time.sleep(1)

# 关闭连接
connection.close()

```

**消费者代码**

```python
# coding=utf-8
"""
消费者
"""
import pika

user_info = pika.PlainCredentials('admin', 'root')
connection = pika.BlockingConnection(pika.ConnectionParameters('10.0.0.10', 5672, '/', user_info))
channel = connection.channel()

# 如果指定的queue不存在，则会创建一个queue，如果已经存在 则不会做其他动作，生产者和消费者都做这一步的好处是
# 这样生产者和消费者就没有必要的先后启动顺序了
channel.queue_declare(queue='hello')


# 回调函数
def callback(ch, method, properties, body):
    print('消费者收到:{}'.format(body))

# channel: 包含channel的一切属性和方法
# method: 包含 consumer_tag, delivery_tag, exchange, redelivered, routing_key
# properties: basic_publish 通过 properties 传入的参数
# body: basic_publish发送的消息


channel.basic_consume(queue='hello',  # 接收指定queue的消息
                      auto_ack=True,  # 指定为True，表示消息接收到后自动给消息发送方回复确认，已收到消息
                      on_message_callback=callback  # 设置收到消息的回调函数
                      )

print('Waiting for messages. To exit press CTRL+C')

# 一直处于等待接收消息的状态，如果没收到消息就一直处于阻塞状态，收到消息就调用上面的回调函数
channel.start_consuming()

```

# rabbitmq入门

## 相关概念介绍

RabbitMQ 整体上是一个 **生产者与消费者模型**，主要负责 **接收、存储和转发消息**。

![image-20230630100044348](assets/image-20230630100044348.png)

## 生产者和消费者

### 生产者

Producer：生产者，投递消息的一方

生产者创建消息，发布到 RabbitMQ 中。

消息一般可以包含 2 个部分：

- 消息体（payload）

  你的业务数据，比如一个 JSON 字符串

- 标签（label）：用来描述这条消息

  比如：一个交换器的名称和一个路由键。RabbitMQ 会根据标签把消息发送给感兴趣的消费者（Consumer）

### 消费者

Consumer：消费者，接收消息的一方

消费者链接到 RabbitMQ，并 **订阅到队列** 上。当消费者消费一条消息时，只是消费消息的 **消息体（payload）**，路由过程中，**消息的标签会丢弃**，**存入到队列中的消息只有消息体**。

消费者不知道生产者是谁。

### Broker

Broker：消息中间件的服务节点。

对于 RabbitMQ 来说，一个 Broker 可以简单看成一个 RabbitMQ 服务节点，或则 RabbitMQ 服务实例。

下图展示了：生产者将消息存入 Broker，以及消费者从 Broker 中消费数据的整个流程

![image-20230630100159403](assets/image-20230630100159403.png)

- 生产者将消息发送到 Broker 中

  发送：对应的 AMQP 协议命令为 `Basic.Publish`

- 消费者订阅并接收消息

  订阅并接收：对应的 AMQP 协议命令为 `Basic.Consume` 或 `Basic.Get`

## 队列

Queue：队列 ，是 RabbitMQ 的内部对象，用于存储消息。

RabbitMQ 中消息都 **只能存储在队列中**，kafka 将消息存储在 topic 这个逻辑层面中，而相对应的队列逻辑知识 topic 实际存储文件中的位置偏移标识。

**多个消费者可以订阅同一个队列**，这时队列中的消息会被 **平均分摊**（Round-Robin 轮询）给多个消费者进行处理。

![image-20230630100227337](assets/image-20230630100227337.png)

RabbitMQ **不支持队列层面的广播消息**，如果需要广播消费，需要进行二次开发，麻烦，不推荐。

## 交换器、路由键、绑定

### Exchange 交换器

在图 2-4 中，消息并不是直接到队列中的，而是消息先到 Exchange（交换器，通常用大写的 X 表示），**由交换器将消息路由到一个或多个队列中**。如果路由不到，或许会返回给生产者，或许会直接丢弃。

示意图如下

![image-20230630100256461](assets/image-20230630100256461.png)

RabbitMQ 中的交换器有 4 种类型，不同的类型有不同的策略，后续详细讲解。

### RoutingKey 路由键

生产者将消息发给交换器时，一般会指定一个 RoutingKey，用来指定这个消息的路由规则。

RoutingKey 需要与**交换器类型**和 **绑定键（BindingKey）** 联合使用才能最终生效

### Binding 绑定

通过 **绑定** 将 **交换器与队列关联** 起来，绑定时一般会指定一个 **绑定键（BindingKey）**，这样 RabbitMQ 就知道如何正确将消息路由到队列了，示意图如下：

![image-20230630100327581](assets/image-20230630100327581.png)

生产者将消息发送给交换器时，需要一个 RoutingKey，**当 BindingKey 和 RoutingKey 匹配时**，消息被路由到对应的队列中。

绑定多个队列到同一个交换器时，BindingKey 允许相同。BindingKey 只能在 **指定的交换器类型下生效**，比如 fanout 类型的交换器就不生效，它将消息路由到所有绑定到该交换器的队列中。



## 交换器类型

RabbitMQ 常用的交换器类型有：

- fanout：相当于广播消息，广播到绑定到交换器的队列
- direct：路由到 RoutingKey 与 BindingKey 相同的队列
- topic
- headers

AMQP 协议里还有两种类型：System 和 自定义。这里只讲解以上 4 种类型。

### fanout

把所有发送到该交换器的消息路由到所有与交换器绑定的队列中。**相当于广播消息**

### direct

把消息 **路由到 BindingKey 和 RoutingKey 完全匹配的队列中**。

![image-20230630103101923](assets/image-20230630103101923.png)

Queue 2 绑定了 3 个 BindingKey，当消息指定的 RoutingKey 为 warming 时，两个队列都能收到，为 info 时，就只有 Queue 2 能收到。

### topic

与 direct 类型的交换器相似，也是将消息路由到 RoutingKey 与 BindingKey 相匹配的队列中，但是这里的匹配规则不同，它约定：

- RoutingKey 为一个点号「`.`」分隔的字符串

  被「`.`」 分隔开的每一段独立的字符串成为一个单词。如 `com.rabbitmq.client`

- BindingKey 和 RoutingKey 一样也是一个点号「`.`」分隔的字符串

- BindingKey 中可以存在两种特殊字符串，用作模糊匹配

  - `#`：用于匹配一个单词
  - `*`：用于匹配多规格的单词，可以是零个

![image-20230630103134185](assets/image-20230630103134185.png)

### headers

headers 类型的交换器 **不依赖于路由键的匹配规则来路由消息**，根据发送的消息内容中的 **headers 属性进行匹配**。

在绑定队列和交换器时制定一组键值对，发送消息到交换器时，会获取该消息的 headers ，对比其中的键值对是否完全匹配队列和交换器绑定时指定的键值对，完全匹配则路由到该队列。

headers 类型的交换器性能会很差，不推荐使用。

## RabbitMQ 的运转流程

回顾下整个消息队列的使用过程，在最初状态下，生产者发送消息时：

1. 连接到 RabbitMQ Broker，建立一个连接 Connection，开启一个信道 Channel

2. 声明一个交换器，并设置相关属性

   比如：交换器类型、是否持久化等

3. 声明一个队列，并设置相关属性

   比如：是否排他、是否持久化、是否自动删除等

4. 通过路由键将交换器和队列绑定起来

5. 发送消息到 Broker

   其中包含：路由键、交换器等信息

6. 相应交换器根据接收到的路由键查找匹配的队列

7. 如果找到：则存入相应的队列中。

8. 如果没有找到：则根据生产者配置的属性，选择丢弃还是回退给生产者

9. 关闭信道和链接

消费者接收消息的过程：

1. 链接到 RabbitMQ Broker，建立一个链接 Connection，开启一个信道 Channel
2. 向 Broker 请求消费相应队列中的消息，可能会设置相应的回调函数，以及做一些准备工作
3. 等待 Broker 回应并投递相依队列中的消息，消费者接收消息
4. 消费者确认（ack）接收消息
5. RabbitMQ 从队列中删除相应以及被确认的消息
6. 关闭信道和链接

![image-20230630103209578](assets/image-20230630103209578.png)

无论生产者与消费者都要与 Broker 建立连接，该链接就是一条 TCP 链接（Connection）。客户端可以创建一个 AMQP 信道（Channel），**每个信道会被指派一个唯一的 ID**。

**信道是建立在 Connection 之上的虚拟连接**，RabbitMQ 处理的每条 AMQP 指令都是通过信道完成的。这个模式是类似 NIO（Non-blocking I/O）的做法，TCP 链接复用，减少性能开销，同时也便于管理。



NIO

NIO 也称非阻塞 I/O，包含三大核心部分：Channel 信道、Buffer 缓冲区、Selector 选择器。

NIO 基于 Channel 和 Buffer 进行操作，数据总是从信道读取到缓冲区中，或则从缓冲区写入到信道中。

Selector 用于监听多个信道的事件（如：打开链接、数据到达等）。因此，单线程可以监听多个数据的信道。

NIO 中有一个很有名的 [Reactor 模式](https://zq99299.github.io/note-book/mycat/frontend_connection_interaction.html#基础知识)。

所以你的程序具体需要几个 Connection ，这个需要根据你业务量来进行调节，当一个 Connection 上的信道太多时，可能会产生瓶颈，就需要考虑使用多个 Connection 了。

信道在 AMQP 中是一个很重要的概念，大多数操作都在信道这个层面展开的。在前面的代码演示中，也可以看到，很多操作都是在 Channel 对象上完成的。



## AMQP协议介绍



RabbitMQ 是 AMQP 协议的 Erlang 的实现（RabbitMQ 还支持 STOMP、MQTT 等协议）。

- STOPM：Simple/Streaming Text Oriented Messaging Protocol

  简单/流文本面向消息协议，提供了一个可互操作的连接格式，运行 STOMP 客户端与任意 STOMP 消息代理（Broker）进行交互。

  STOMP 协议由于设计简单，易于开发客户端，因此在多种遇上和平台上得到广泛应用

- MQTT：Message Queuing Telemetry Transport

  消息队列遥测传输。是 IBM 开发的一个 **及时通信协议**，有可能成为物联网的重要组成部分。

  该协议支持所有平台，几乎可以把所有物联网和外部连接起来，被用来当做传感器和制动器的通信协议。

AMQP 的模型架构和 RabbitMQ 的模型架构是一样的：

1. 生产者发送消息给交换器
2. 交换器和队列绑定
3. 当 RoutingKey 和 BindingKey 想匹配时，消息被存入相应的队列中
4. 消费者可以订阅相应的队列来获取消息。

RabbitMQ 中的交换器、交换器类型、队列、绑定、路由键等都遵循 AMQP 协议中相应的概念。本书讲解的 RabbitMQ 版本对应的是 [AMQP 0-9-1](https://www.cnblogs.com/xiaochengzi/p/6895126.html) 版本协议。如无特指，则以该协议为基准介绍。

AMQP 协议本身包括三层：

- Module Layer：

  位于协议最高层，主要定义了一些供客户端调用的命令，客户端可以利用这些命令实现自己的业务逻辑。

  例如：客户端可以使用 `Queue.Declare` 命令声明一个队列或使用 `Basic.Consume` 订阅消费一个队列中的消息。

- Session Layer：

  位于中间层，主要负责将客户端的命令发送给服务器，再将服务端的应答返回给客户端，主要为客户端与服务器之间的通信提供可靠性同步机制和错误处理。

- Transport Layer：

  位于最底层，主要传输二进制数据流，提供帧的处理、信道复用、错误检测和数据表示等。

AMQP 是一个通信协议，会涉及到 **报文交互**：

- 从 low-level 举例来说，AMQP 本身是应用层的协议，填充于 TCP 协议层的数据部分
- 从 high-level 来说，AMQP 是通过协议命令进行交互的。可以看成是一系列结构化命令的集合，这里的命令代表一种操作，类似于 HTTP 中的方法，GET、POST...

至于 AMPQ 协议与之代码部分对应的命令，笔者这里就不记录了，感兴趣的可以看原书；



# Python操作Rabbitmq

开始前我们先准备一个mysql，我们将会将对比两个实例来说明rabbitmq的作用

1、准备两个mysql实例

2、准备python脚本

3、准备一个rabbitmq实例

我们将会对比a(直接向mysql写入)，b(写入rabbitmq，通过消费rabbitmq中的消息再写入到mysql)

docker-compose启动mysql

```yaml
version: '3'
services:
  mysql:
    network_mode: "host"
    image: 'mysql/mysql-server:5.7'
    restart: always
    container_name: mysql
    environment:
      MYSQL_ROOT_PASSWORD: 123456
    command:
      --default-authentication-plugin=mysql_native_password
      --character-set-server=utf8mb4
      --collation-server=utf8mb4_general_ci
      --explicit_defaults_for_timestamp=true
      --lower_case_table_names=1
      --max_allowed_packet=128M;
    ports:
      - 3306:3306
```



授权mysql

```sql
grant all privileges on *.* to 'root'@'%' identified by '123456';
flush privileges;
```



创建表语句

```sql
CREATE TABLE xiaoshuo (
    title VARCHAR(255),
    con LONGTEXT
);
```

python脚本

```python
import pymysql
import requests
from fake_useragent import UserAgent
from lxml import etree

url = "https://www.17k.com/list/493239.html"
headers = {'User-Agent': UserAgent().random}
html = requests.get(url=url, headers=headers, timeout=30)
html.encoding = 'utf-8'

db = pymysql.connect(host='10.0.0.11', user='root', password='123456', database='xiaoshuo', charset='utf8')
cursor = db.cursor()


def get_title(html):
    p = etree.HTML(html)
    li_list = p.xpath('//div[@class="Main List"]/dl/dd/a[@target="_blank"]')

    for li in li_list:
        name = li.xpath('.//span/text()')
        print(name[0].strip())
        href = li.xpath('.//@href')
        con = get_con(url='https://www.17k.com' + href[0].strip())
        print(str(con))
        inster_mysql(title=name[0].strip(), con=con)


def get_con(url):
    html = requests.get(url=url, headers=headers, timeout=3)
    html.encoding = 'utf-8'
    info = etree.HTML(html.text)
    con = info.xpath('/html/body/div[4]/div[2]/div[2]/div[1]/div[2]/p/text()')
    return con


def inster_mysql(title, con):
    list = [title, str(con)]
    ins = 'insert into xiaoshuo values(%s, %s)'
    print(ins, list)
    cursor.execute(ins, list)
    db.commit()


for num in range(1000):
    get_title(html=html.text)

db.close()

```

压测MySQL

```shell
# 命令一
sysbench --db-driver=mysql --time=300 --threads=10 --report-interval=1 --mysql-host=10.0.0.11 --mysql-port=3306 --mysql-user=root --mysql-password=123456 --mysql-db=xiaoshuo --tables=20 --table_size=1000000 oltp_read_write --db-ps-mode=disable prepare
# 命令二
sysbench --db-driver=mysql --time=300 --threads=50 --report-interval=1 --mysql-host=10.0.0.11 --mysql-port=3306 --mysql-user=root --mysql-password=123456 --mysql-db=xiaoshuo --tables=20 --table_size=1000000 oltp_read_write --db-ps-mode=disable run
```









## 连接 RabbitMQ

```

```



## 使用交换器和队列



## 发送消息



## 消费消息



## 消费端的确认与拒绝



## 关闭连接

