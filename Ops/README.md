```text
如何快速学习？
首先携带两个问题：
那些场景下需要用到该软件？和该工具类似的工具有那些？
那些软件可以和该软件组成一套工具链？ 比如企业微信告警，邮件通知，jira ticket,git等等
1、什么是xxx?
2、xxx是如何工作的？
3、安装(os安装、docker安装、k8s安装)
4、集群安装（主从、主备、高可用？）
5、配置文件详解
6、基本使用
7、定制化使用（结合应用场景）
8、性能监控
9、日志分析
10、数据清理
11、升级
12、迁移
13、该工具在devops链中位于哪一环？ 上下游分别是什么？ 
14、该工具的api使用？
15、如何利用该工具输出周报
```

## 分类

操作系统安装

```shell
pxe
```

基础环境配置

```shell
Ansible
Vagrant
Salt
Chef
```

web

```shell
nginx 
tomcat
```

负载均衡

```shell
nginx
Neutrino
HAProxy
```

防火墙

```shell
iptables
firewalld
```

高可用

```shell
haproxy
nginx + keeplive
nginx + lvs

Heartbeat：一个高可用集群软件，可以实现心跳检测、故障转移和负载均衡等功能。
Pacemaker：一个开源的高可用集群管理软件，支持多种资源管理器和服务管理器。
Ginormous：一个高性能、可扩展、高可用集群系统，支持多种应用类型和存储类型。
Skydog：一个基于Redis的高可用集群系统，可以实现节点间的故障转移和负载均衡。

```

代码管理

```shell
git
Bitbucket
gitlab
```

代码审查

```shell
Review Board：一个开源的代码评审工具，提供Web界面和API，支持Git、SVN等版本控制系统，支持自定义评审流程和审批流程。
Crucible：一个基于Git的代码评审工具，提供Web界面和API，专注于代码审查、代码变更审核和代码质量分析。
Phabricator：一个开源的代码评审工具，支持Git、SVN等版本控制系统，提供Web界面和API，支持代码审查、任务管理、代码变更审核等功能。
CodeStriker：一个开源的代码评审工具，支持Git、SVN等版本控制系统，提供Web界面和API，支持代码审查、任务管理、代码变更审核等功能。
Gogs：一个基于Git的开源代码评审系统，提供Web界面和API，支持代码审查、代码变更审核、代码质量分析等功能。
Gitea：一个开源的Git桌面客户端和服务器，提供Web界面和API，支持代码审查、代码变更审核、代码质量分析等功能。

```



存储

```shell
nfs
FastDFS
Ceph
GlusterFS
Mfs
```

对象存储

```shell
MinIO
LakeFS
Ceph
OpenIO
```

非关系型数据库（缓存）

```shell
redis
memcached
MongoDB
HBase
```

消息队列

```shell
rabbitmq
ActiveMQ
ZeroMQ
Kafka
RocketMQ
Apollo
```

制品库

```shell
harbor
jforg
nexus
```

CICD

```shell
jenkins
gitlab-ci
```

数据库

```shell
tidb
mysql
pgsql
```

监控

```shell
zabbix
prometheus
```

日志系统

```shell
ELK
loki
Fluentd
Graylog
```

服务注册与发现

```shell
zookeeper
consul
Eureka
etcd
```

堡垒机

```shell
jumpserver
```

基础平台

```shell
dokcer
k8s
nomad
```

代码质量检测

```shell
DeepSource
embold
SonarQube
Veracode
Code Climate
DeepScan
CodeSonar
```

资产管理

```shell
暂时没有找到合适的资产管理平台，再找下GitHub或者自己开发
```

服务网格

```shell
Traefik
Istio
Envoy：Envoy是一个高性能的代理软件，被用于构建服务网格。它提供了数据平面和服务端点的代理，可以处理HTTP、HTTP/2和gRPC请求。

Linkerd：Linkerd是Buoyant公司的一个开源服务网格产品，它提供了数据平面和应用层代理，可以处理HTTP、HTTP/2和gRPC请求。

Consul Connect：Consul Connect是HashiCorp公司的一个服务网格产品，它基于Consul和 Envoy构建，提供了安全的服务间通信和自动的服务发现。

Pilot：Pilot是Istio控制平面的一部分，它负责管理服务网格中的代理，提供了路由规则、服务发现和负载均衡等功能。

Ambassador：Ambassador是一个云原生服务网格，它提供了数据平面和应用层代理，支持HTTP、HTTP/2和gRPC请求，并且具有易于配置和管理的高级功能。

Kuma：Kuma是一个开源的服务网格，它提供了数据平面和应用层代理，支持HTTP、HTTP/2和WebSocket请求，并且具有易于配置和管理的高级功能。
```

大数据平台

```shell
Apache Hadoop：Apache Hadoop是一个开源的分布式计算平台，可用于处理大规模数据集并支持多种数据处理任务，如数据挖掘、数据分析、日志处理等。
Apache Spark：Apache Spark是一个基于内存计算的大数据处理框架，可以快速处理大规模数据集，并支持多种数据处理任务，如数据清洗、转换和分析等。
Apache Flink：Apache Flink是一个高性能的大数据处理框架，支持流处理和批处理，并提供了多种数据分析和处理工具。
Google Cloud Dataflow：Google Cloud Dataflow是谷歌云平台中的一个大数据处理服务，支持批处理和流处理，并提供了一系列数据分析和可视化工具。
Microsoft Azure HDInsight：Microsoft Azure HDInsight是一个基于Hadoop的大数据平台，提供了分布式计算和存储能力，并支持多种数据处理和分析工具。
Cloudera Altus：Cloudera Altus是Cloudera公司的大数据平台服务，提供了高性能的分布式计算和存储能力，并支持多种数据处理和分析工具。
Databricks：Databricks是一个基于Apache Spark的大数据平台和服务，提供了分布式计算和存储能力，并支持多种数据处理和分析工具。
```

分布式链路追踪系统

```shell
SkyWalking
Dynatrace：Dynatrace是一种全栈式分布式追踪系统，用于跟踪微服务之间的调用链路。它支持多种编程语言和框架，并提供了一个可视化界面来查询和分析跟踪数据。
Zipkin：Zipkin是一个开源的分布式追踪系统，用于跟踪微服务之间的调用链路。它支持多种编程语言和框架，并提供了可视化界面和API来查询和分析跟踪数据。
Jaeger：Jaeger是一种开源的分布式追踪系统，用于跟踪微服务之间的调用链路。它支持多种编程语言和框架，并提供了可视化界面和API来查询和分析跟踪数据。
```

虚拟化

```shell
完全虚拟化：vmare workstation，kvm，xen(hvm)
半虚拟化：xen，uml
模拟虚拟化：qemu
用户空间虚拟化(容器)：lxc，openvz，Solaris Containers，FreeBSD jails


```



