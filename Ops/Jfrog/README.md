Jfrog官方文档：https://jfrog.com/help/r/jfrog-installation-setup-documentation/install-artifactory-single-node-with-docker



社区版安装

```shell
# 机器已经安装docker以及docker-compose
curl -L https://ghproxy.com/https://github.com/docker/compose/releases/download/1.27.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
# 创建映射目录
mkdir -p /opt/docker/jfrog/artifactory && cd /opt/docker/jfrog/artifactory
#创建data目录并赋权，不然会启动失败
mkdir data && chmod 777 data
#创建docker-compose配置
cat docker-compose.yml 
version: '3'
services:
    jfrog-oss:
      image: releases-docker.jfrog.io/jfrog/artifactory-oss:latest
      container_name: jfrog-oss 
      restart: always
      network_mode: host
      privileged: true #root权限
      volumes:
        - /opt/docker/jfrog/artifactory/data:/var/opt/jfrog/artifactory

docker-compose -f docker-compose.yaml up -d
# 浏览器访问
http://<ip>:8081/
```

jfrog启动中时访问界面

![image-20230804103638756](assets/image-20230804103638756.png)

账户名密码 admin/password

![image-20230804103733298](assets/image-20230804103733298.png)

社区版默认不支持docker

![image-20230804104106128](assets/image-20230804104106128.png)

jforg-jcr

```shell
# 机器已经安装docker以及docker-compose
curl -L https://ghproxy.com/https://github.com/docker/compose/releases/download/1.27.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
# 创建映射目录
mkdir -p /opt/docker/jfrog/artifactory && cd /opt/docker/jfrog/artifactory
#创建data目录并赋权，不然会启动失败
mkdir data && chmod 777 data_jcr
#创建docker-compose配置
cat docker-compose-jcr.yaml 
version: '3'
services:
    jfrog-oss-jcr:
      image: docker.bintray.io/jfrog/artifactory-jcr:latest
      container_name: jfrog-oss-jcr
      restart: always
      network_mode: host
      privileged: true
      volumes:
        - /opt/docker/jfrog/artifactory/data_jcr:/var/opt/jfrog/artifactory

docker-compose -f docker-compose-jcr.yaml up -d
# 浏览器访问
http://<ip>:8081/
```

已经可以创建docker仓库了

![image-20230804105800880](assets/image-20230804105800880.png)

