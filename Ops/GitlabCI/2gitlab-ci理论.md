### gitlab-ci的理论

#### gitlab-ci的工作流程

```
gitlab的ci流程的组成部分:
code:开发人员推送的代码
.gitlab-ci.yml 指定构建、测试和部署的脚本。
GitLab Runner 运行.gitlab-ci.yml脚本

gitlab-ci的工作流程：
①开发人员推送代码到指定分支
②触发gitlab-pipeline，自动检测运行.gitlab-ci.yml文件
③gitlab runner运行脚本，根据脚本内容进行build test deploy
```



#### gitlab-ci的工作原理

```
①将代码托管到git存储库
②在项目根目录创建ci文件 .gitlab-ci.yml，在文件中指定构建，测试和部署脚本。
③gitlab将检测到他并使用名为gitlab-runner的工具运行脚本
④脚本被分组为作业，他们共同组成一个pipeline
```



#### gitalb-ci的工作流程图

![image-20230403120332031](assets/image-20230403120332031.png)


#### gitlab-runner简介

```
1、gitlab-runner是一个开源项目，用于运行作业并将结果返回给gitlab
2、与gitlab-ci结合使用，gitlab-ci是gitlab随附的用于协调作业的开源持续集成服务。
3、gitlab-runner是使用go语言写的，可以在Linux macos Windows操作系统上进行运行
4、容器部署需要使用最新docker版本。gitlab-runner需要最少的docker v1.13版本
5、gitlab-runner版本应和gitlab版本进行同步（避免版本不一致导致差异化）
6、可以根据需要配置任意数量的runner
PS：ditlab-runner类似于Jenkins的agent，执行CI持续集成、构建的脚本任务。
```



#### gitlab-runner的特点

```
作业运行控制：同时执行多个作业
作业运行环境：
①在本地、使用docker容器、使用docker容器并通过SSH执行作业
②使用docker容器在不用的云和虚拟化管理程序上自动缩放
③连接到远程SSH服务器
支持bash、Windows batch和Windows powershell
允许自定义作业运行环境
自动重新加载配置，无需重启
易于安装，可作为Linux，macos和Windows的服务
```

#### gitlab-runner类型与状态

```
类型：
shared 共享类型，运行整个平台项目的作业（gitlab）
group 项目组类型，运行特定group下的所有项目的作业（group）
specific 项目类型，运行指定的项目作业（project）

状态：
locked 锁定状态，无法运行项目作业
pause的 暂停状态，暂时不会接受新的作业
```





#### job的运行流程

（根据job的输出日志进行分析）

```
Running with gitlab-runner 14.1.0 (8925d9a0) on c088e5ef43f3 RXcgCdUx #选择的gitlab-runner准备运行pipeline
Preparing the "shell" executor #准备“shell”执行器
Using Shell executor... #正在使用Shell执行器
Preparing environment #准备环境
Running on ea06ddae5852... #正在ea06ddae5852（container id）上运行
Getting source from Git repository #从Git存储库获取源代码
Fetching changes with git depth set to 50... #正在获取git深度设置为50的更改
Reinitialized existing Git repository in /home/gitlab-runner/builds/RXcgCdUx/0/root/shell/.git/ #在/home/gitlab runner/builds/rxgcdux/0/root/shell/.Git中重新初始化现有Git存储库/
Checking out 5b8e0163 as main... #查找对比
Skipping object checkout, Git LFS is not installed. #正在跳过对象签出未安装Git LFS。
Skipping Git submodules setup #正在跳过Git子模块安装程序
Executing "step_script" stage of the job script #执行作业脚本的“步骤脚本”阶段
$ ls
python-demo.py
test
$ ls
python-demo.py
test
$ sleep 10
$ echo "mvn clean"
mvn clean
$ sleep 3
$ touch /home/gitlab-runner/builds/RXcgCdUx/0/root/shell/test.txt
$ pwd
/home/gitlab-runner/builds/RXcgCdUx/0/root/shell
$ sleep 30
Job succeeded
```