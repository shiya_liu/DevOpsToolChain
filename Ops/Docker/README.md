# 简介

官网：[Get Docker](https://docs.docker.com/get-docker/)

```text
Docker 是一个开源的应用容器引擎，基于 Go 语言 并遵从 Apache2.0 协议开源。

Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）,更重要的是容器性能开销极低。

Docker 从 17.03 版本之后分为 CE（Community Edition: 社区版） 和 EE（Enterprise Edition: 企业版）
```



## 应用场景

```tex
Web 应用的自动化打包和发布。

自动化测试和持续集成、发布。

在服务型环境中部署和调整数据库或其他的后台应用。

从头编译或者扩展现有的 OpenShift 或 Cloud Foundry 平台来搭建自己的 PaaS 环境。
```

## 基本概念

### 镜像

```text
操作系统分为内核和用户空间。对于 Linux 而言，内核启动后，会挂载 root 文件系统为其提供用户空间支持。而 Docker 镜像（Image），就相当于是一个 root 文件系统。比如官方镜像 ubuntu:18.04 就包含了完整的一套 Ubuntu 18.04 最小系统的 root 文件系统。

Docker 镜像是一个特殊的文件系统，除了提供容器运行时所需的程序、库、资源、配置等文件外，还包含了一些为运行时准备的一些配置参数（如匿名卷、环境变量、用户等）。镜像不包含任何动态数据，其内容在构建之后也不会被改变。
```



### 容器

```text
镜像（Image）和容器（Container）的关系，就像是面向对象程序设计中的 类 和 实例 一样，镜像是静态的定义，容器是镜像运行时的实体。容器可以被创建、启动、停止、删除、暂停等。

容器的实质是进程，但与直接在宿主执行的进程不同，容器进程运行于属于自己的独立的 命名空间。因此容器可以拥有自己的 root 文件系统、自己的网络配置、自己的进程空间，甚至自己的用户 ID 空间。容器内的进程是运行在一个隔离的环境里，使用起来，就好像是在一个独立于宿主的系统下操作一样。这种特性使得容器封装的应用比直接在宿主运行更加安全。也因为这种隔离的特性，很多人初学 Docker 时常常会混淆容器和虚拟机。

前面讲过镜像使用的是分层存储，容器也是如此。每一个容器运行时，是以镜像为基础层，在其上创建一个当前容器的存储层，我们可以称这个为容器运行时读写而准备的存储层为 容器存储层。

容器存储层的生存周期和容器一样，容器消亡时，容器存储层也随之消亡。因此，任何保存于容器存储层的信息都会随容器删除而丢失。

按照 Docker 最佳实践的要求，容器不应该向其存储层内写入任何数据，容器存储层要保持无状态化。所有的文件写入操作，都应该使用 数据卷（Volume）、或者绑定宿主目录，在这些位置的读写会跳过容器存储层，直接对宿主（或网络存储）发生读写，其性能和稳定性更高。

数据卷的生存周期独立于容器，容器消亡，数据卷不会消亡。因此，使用数据卷后，容器删除或者重新运行之后，数据却不会丢失。
```

### 仓库

```text
镜像构建完成后，可以很容易的在当前宿主机上运行，但是，如果需要在其它服务器上使用这个镜像，我们就需要一个集中的存储、分发镜像的服务，Docker Registry 就是这样的服务。

一个 Docker Registry 中可以包含多个 仓库（Repository）；每个仓库可以包含多个 标签（Tag）；每个标签对应一个镜像。

通常，一个仓库会包含同一个软件不同版本的镜像，而标签就常用于对应该软件的各个版本。我们可以通过 <仓库名>:<标签> 的格式来指定具体是这个软件哪个版本的镜像。如果不给出标签，将以 latest 作为默认标签。

以 Ubuntu 镜像 为例，ubuntu 是仓库的名字，其内包含有不同的版本标签，如，16.04, 18.04。我们可以通过 ubuntu:16.04，或者 ubuntu:18.04 来具体指定所需哪个版本的镜像。如果忽略了标签，比如 ubuntu，那将视为 ubuntu:latest。

仓库名经常以 两段式路径 形式出现，比如 jwilder/nginx-proxy，前者往往意味着 Docker Registry 多用户环境下的用户名，后者则往往是对应的软件名。但这并非绝对，取决于所使用的具体 Docker Registry 的软件或服务。
```





## docker原理

```text
docker是利用Linux内核虚拟机化技术（LXC），提供轻量级的虚拟化，以便隔离进程和资源。LXC不是硬件的虚拟化，而是Linux内核的级别的虚拟机化，相对于传统的虚拟机，节省了很多硬件资源。

NameSpace:
LXC是利用内核namespace技术，进行进程隔离。其中pid, net, ipc, mnt, uts 等namespace将container的进程, 网络, 消息, 文件系统和hostname 隔离开。

Control Group:
LXC利用的宿主机共享的资源，虽然用namespace进行隔离，但是资源使用没有收到限制，这里就需要用到Control Group技术，对资源使用进行限制，设定优先级，资源控制等。

OverlayFS2:
通过 OverlayFS 数据存储技术, 实现容器镜像的物理存储与新建容器存储。
```



## docker架构

Docker 使用客户端-服务器 (C/S) 架构模式，使用远程API来管理和创建Docker容器。

```tex
镜像（Image）：Docker 镜像（Image），就相当于是一个 root 文件系统。比如官方镜像 ubuntu:16.04 就包含了完整的一套 Ubuntu16.04 最小系统的 root 文件系统。

容器（Container）：镜像（Image）和容器（Container）的关系，就像是面向对象程序设计中的类和实例一样，镜像是静态的定义，容器是镜像运行时的实体。容器可以被创建、启动、停止、删除、暂停等。

仓库（Repository）：仓库可看成一个代码控制中心，用来保存镜像。
```

![image-20230417162926528](assets/image-20230417162926528.png)



## 联合文件系统

```text
联合文件系统（UnionFS）是一种分层、轻量级并且高性能的文件系统，它支持对文件系统的修改作为一次提交来一层层的叠加，同时可以将不同目录挂载到同一个虚拟文件系统下(unite several directories into a single virtual filesystem)。

联合文件系统是 Docker 镜像的基础。镜像可以通过分层来进行继承，基于基础镜像（没有父镜像），可以制作各种具体的应用镜像。

另外，不同 Docker 容器就可以共享一些基础的文件系统层，同时再加上自己独有的改动层，大大提高了存储的效率。

Docker 中使用的 AUFS（AnotherUnionFS）就是一种联合文件系统。 AUFS 支持为每一个成员目录（类似 Git 的分支）设定只读（readonly）、读写（readwrite）和写出（whiteout-able）权限, 同时 AUFS 里有一个类似分层的概念, 对只读权限的分支可以逻辑上进行增量地修改(不影响只读部分的)。

Docker 目前支持的联合文件系统包括 OverlayFS, AUFS, Btrfs, VFS, ZFS 和 Device Mapper。
```



各 Linux 发行版 Docker 推荐使用的存储驱动如下表。

| Linux 发行版        | Docker 推荐使用的存储驱动                           |
| :------------------ | :-------------------------------------------------- |
| Docker CE on Ubuntu | `overlay2` (16.04 +)                                |
| Docker CE on Debian | `overlay2` (Debian Stretch), `aufs`, `devicemapper` |
| Docker CE on CentOS | `overlay2`                                          |
| Docker CE on Fedora | `overlay2`                                          |

在可能的情况下，推荐使用 `overlay2` 存储驱动，`overlay2` 是目前 Docker 默认的存储驱动，以前则是 `aufs`。你可以通过配置来使用以上提到的其他类型的存储驱动。



## 网络模式

```text
bridge  默认类型 NAT模式
host    host类型，使用宿主机网络，网络性能最高
container 容器类型。使用其他容器共用网络，k8s中使用
none    没有网络，上不了外网
```



## 存储方式

docker四种方式：默认、volumes数据卷、bind mounts挂载、tmpfs mount(仅在linux环境中提供)，其中volumes、bind mounts两种实现持久化容器数据；

```text
默认：数据保存在运行的容器中，容器删除后，数据也随之删除；
volumes：数据卷，数据存放在主机文件系统/var/lib/docker/volumes/目录下，该目录由docker管理，其它进程不允许修改，推荐该种方式持久化数据；
Bind mounts：直接挂载主机文件系统的任何目录或文件，类似主机和容器的共享目录，主机上任何进程都可以访问修改，容器中也可以看到修改，这种方式最简单。
tmpfs：数据暂存在主机内存中，不会写入文件系统，重启后，数据删除。
```

![image-20230417165920251](assets/image-20230417165920251.png)



## 容器和虚拟机的区别

虚拟机是基于硬体的多个客户操作系统，由虚拟机监视器实现。 容器是应用程序级构造，并模拟共享单个内核的多个虚拟环境。

![image-20230417163911185](assets/image-20230417163911185.png)



|            | 虚拟机                                                       | 容器                                                         |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 系统性能   | 对于使用虚拟机的传统虚拟化，每个虚拟机都有自己的完整操作系统，因此在运行内置于虚拟机的应用程序时，内存使用量可能会高于必要值，虚拟机可能会开始耗尽主机所需的资源。 | 与传统的容器化应用程序不同，共享操作系统环境（内核），因此它们比完整虚拟机使用更少的资源，并减轻主机内存的压力。 |
| 重量       | 传统虚拟机可占用大量磁盘空间：除了虚拟机托管的任何应用程序外，它们还包含完整的操作系统和相关工具。 | 容器相对较轻：它们仅包含使容器化应用程序运行所需的库和工具，因此它们比虚拟机更紧凑，并且启动速度更快。 |
| 维护和更新 | 在更新或修补操作系统时，必须逐个更新传统计算机：必须单独修补每个客户操作系统。 | 对于容器，只需更新容器主机（托管容器的机器）的操作系统。这显著简化了维护。 |



# 具体使用



## 安装

二进制安装脚本：[install_docker.sh · shiya.liu/ShellScript - 码云 - 开源中国 (gitee.com)](https://gitee.com/shiya_liu/ShellScript/blob/main/install_docker.sh)

系统要求
Docker CE 支持 64 位版本 CentOS 7，并且要求内核版本不低于 3.10。 CentOS 7 满足最低内核的要求，但由于内核版本比较低，部分功能（如 overlay2 存储层驱动）无法使用，并且部分功能可能不太稳定。

```shell
# 卸载旧版本:
yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine
                  
# 执行以下命令安装依赖包：
yum install -y yum-utils device-mapper-persistent-data lvm2

# 使用国内源
yum-config-manager --add-repo https://mirrors.ustc.edu.cn/docker-ce/linux/centos/docker-ce.repo
# 安装 Docker CE
 yum makecache fast
 yum install docker-ce -y
# 加内核配置参数
tee -a /etc/sysctl.conf <<-EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl -p
# 启动 Docker CE
systemctl enable docker
systemctl start docker        
```



## 配置文件

```json
{
  "allow-nondistributable-artifacts": [],
  "api-cors-header": "",
  "authorization-plugins": [],
  "bip": "",
  "bridge": "",
  "cgroup-parent": "",
  "containerd": "/run/containerd/containerd.sock",
  "containerd-namespace": "docker",
  "containerd-plugin-namespace": "docker-plugins",
  "data-root": "",
  "debug": true,
  "default-address-pools": [
    {
      "base": "172.30.0.0/16",
      "size": 24
    },
    {
      "base": "172.31.0.0/16",
      "size": 24
    }
  ],
  "default-cgroupns-mode": "private",
  "default-gateway": "",
  "default-gateway-v6": "",
  "default-runtime": "runc",
  "default-shm-size": "64M",
  "default-ulimits": {
    "nofile": {
      "Hard": 64000,
      "Name": "nofile",
      "Soft": 64000
    }
  },
  "dns": [],
  "dns-opts": [],
  "dns-search": [],
  "exec-opts": [],
  "exec-root": "",
  "experimental": false,
  "features": {},
  "fixed-cidr": "",
  "fixed-cidr-v6": "",
  "group": "",
  "hosts": [],
  "icc": false,
  "init": false,
  "init-path": "/usr/libexec/docker-init",
  "insecure-registries": [],
  "ip": "0.0.0.0",
  "ip-forward": false,
  "ip-masq": false,
  "iptables": false,
  "ip6tables": false,
  "ipv6": false,
  "labels": [],
  "live-restore": true,
  "log-driver": "json-file",
  "log-level": "",
  "log-opts": {
    "cache-disabled": "false",
    "cache-max-file": "5",
    "cache-max-size": "20m",
    "cache-compress": "true",
    "env": "os,customer",
    "labels": "somelabel",
    "max-file": "5",
    "max-size": "10m"
  },
  "max-concurrent-downloads": 3,
  "max-concurrent-uploads": 5,
  "max-download-attempts": 5,
  "mtu": 0,
  "no-new-privileges": false,
  "node-generic-resources": [
    "NVIDIA-GPU=UUID1",
    "NVIDIA-GPU=UUID2"
  ],
  "oom-score-adjust": -500,
  "pidfile": "",
  "raw-logs": false,
  "registry-mirrors": [],
  "runtimes": {
    "cc-runtime": {
      "path": "/usr/bin/cc-runtime"
    },
    "custom": {
      "path": "/usr/local/bin/my-runc-replacement",
      "runtimeArgs": [
        "--debug"
      ]
    }
  },
  "seccomp-profile": "",
  "selinux-enabled": false,
  "shutdown-timeout": 15,
  "storage-driver": "",
  "storage-opts": [],
  "swarm-default-advertise-addr": "",
  "tls": true,
  "tlscacert": "",
  "tlscert": "",
  "tlskey": "",
  "tlsverify": true,
  "userland-proxy": false,
  "userland-proxy-path": "/usr/libexec/docker-proxy",
  "userns-remap": ""
}
```

详细解释

```json
{
  "allow-nondistributable-artifacts": [], #不对外分发的产品提交的registry仓库
  "api-cors-header": "", #在引擎API中设置CORS标头
  "authorization-plugins": [], #要加载的授权插件
  "bip": "", #容器的IP网段 
  "bridge": "", #将容器附加到网桥
  "cgroup-parent": "", #为所有容器设置父cgroup
  "containerd": "/run/containerd/containerd.sock",
  "containerd-namespace": "docker",
  "containerd-plugin-namespace": "docker-plugins",
  "data-root": "", #Docker运行时使用的根路径，默认/var/lib/ docker
  "debug": true, #启用调试模式，启用后，可以看到很多的启动信息。默认false
  "default-address-pools": [
    {
      "base": "172.30.0.0/16",
      "size": 24
    },
    {
      "base": "172.31.0.0/16",
      "size": 24
    }
  ],
  "default-cgroupns-mode": "private",
  "default-gateway": "",  #容器默认网关IPv4地址
  "default-gateway-v6": "", #容器默认网关IPv6地址
  "default-runtime": "runc", #容器的默认OCI运行时（默认为“ runc”）
  "default-shm-size": "64M", 
  "default-ulimits": { #容器的默认ulimit（默认[]）
    "nofile": {
      "Hard": 64000,
      "Name": "nofile",
      "Soft": 64000
    }
  },
  "dns": [], #设定容器DNS的地址，在容器的 /etc/ resolv.conf文件中可查看。
  "dns-opts": [], #容器/etc/resolv.conf文件，其他设置
  "dns-search": [],  #设定容器的搜索域，当设定搜索域为 .example.com 时，在搜索一个名为host的主机时，DNS不仅搜索host，还会搜索host.example.com 。 注意：如果不设置，Docker会默认用主机上的  /etc/ resolv.conf 来配置容器。
  "exec-opts": [], #运行时执行选项
  "exec-root": "",#执行状态文件的根目录（默认为’/var/run/ docker‘）
  "experimental": false,
  "features": {},
  "fixed-cidr": "", #固定IP的IPv4子网
  "fixed-cidr-v6": "", #固定IP的IPv6子网
  "group": "", #UNIX套接字的组（默认为“docker”）
  "hosts": [], #设置容器hosts
  "icc": false, #启用容器间通信（默认为true）
  "init": false,
  "init-path": "/usr/libexec/docker-init",
  "insecure-registries": [], #设置私有仓库地址可以设为http
  "ip": "0.0.0.0", #绑定容器端口时的默认IP（默认0. 0.0 . 0 ）
  "ip-forward": false, #默认true, 启用 net.ipv4.ip_forward ,进入容器后使用 sysctl -a |  grepnet.ipv4.ip_forward 查看
  "ip-masq": false,  #启用IP伪装（默认为true）
  "iptables": false,
  "ip6tables": false,
  "ipv6": false,
  "labels": [], #docker主机的标签，很实用的功能,例如定义：–label nodeName=host- 121 
  "live-restore": true, #在容器仍在运行时启用docker的实时还原
  "log-driver": "json-file", #容器日志的默认驱动程序（默认为“ json- file ”）
  "log-level": "", #设置日志记录级别（“调试”，“信息”，“警告”，“错误”，“致命”）（默认为“信息”）
  "log-opts": {
    "cache-disabled": "false",
    "cache-max-file": "5",
    "cache-max-size": "20m",
    "cache-compress": "true",
    "env": "os,customer",
    "labels": "somelabel",
    "max-file": "5",
    "max-size": "10m"
  },
  "max-concurrent-downloads": 3, #设置每个请求的最大并发下载量（默认为3）
  "max-concurrent-uploads": 5, #设置每次推送的最大同时上传数（默认为5）
  "max-download-attempts": 5,
  "mtu": 0, #设置容器网络MTU
  "no-new-privileges": false,
  "node-generic-resources": [
    "NVIDIA-GPU=UUID1",
    "NVIDIA-GPU=UUID2"
  ],
  "oom-score-adjust": -500, #设置守护程序的oom_score_adj（默认值为- 500 ）
  "pidfile": "", #Docker守护进程的PID文件
  "raw-logs": false, #原始日志、全时间戳机制
  "registry-mirrors": [], #设置镜像加速地址
  "runtimes": {
    "cc-runtime": {
      "path": "/usr/bin/cc-runtime"
    },
    "custom": {
      "path": "/usr/local/bin/my-runc-replacement",
      "runtimeArgs": [
        "--debug"
      ]
    }
  },
  "seccomp-profile": "",
  "selinux-enabled": false,  #默认false ，启用selinux支持
  "shutdown-timeout": 15,
  "storage-driver": "", #要使用的存储驱动程序
  "storage-opts": [],
  "swarm-default-advertise-addr": "",  #设置默认地址或群集广告地址的接口
  "tls": true,  #默认false , 启动TLS认证开关
  "tlscacert": "", #默认~/.docker/ ca.pem，通过CA认证过的的certificate文件路径
  "tlscert": "",  #默认 ~/.docker/ cert.pem ，TLS的certificate文件路径
  "tlskey": "",  #默认 ~/.docker/ key.pem，TLS的key文件路径
  "tlsverify": true,  #默认false，使用TLS并做后台进程与客户端通讯的验证
  "userland-proxy": false, #使用userland代理进行环回流量（默认为true）
  "userland-proxy-path": "/usr/libexec/docker-proxy",
  "userns-remap": ""
}
```







## 常用命令

| 名字                              | 描述                                                       |
| :-------------------------------- | :--------------------------------------------------------- |
| [`attach`](./attach.html)         | 将本地标准输入、输出和错误流附加到正在运行的容器           |
| [`build`](./build.html)           | 从 Docker 文件构建映像                                     |
| [`builder`](./builder.html)       | 管理生成                                                   |
| [`checkpoint`](./checkpoint.html) | 管理检查点                                                 |
| [`commit`](./commit.html)         | 根据容器的更改创建新映像                                   |
| [`config`](./config.html)         | 管理群配置                                                 |
| [`container`](./container.html)   | 管理容器                                                   |
| [`context`](./context.html)       | 管理上下文                                                 |
| [`cp`](./cp.html)                 | 在容器和本地文件系统之间复制文件/文件夹                    |
| [`create`](./create.html)         | 创建新容器                                                 |
| [`diff`](./diff.html)             | 检查对容器文件系统上的文件或目录的更改                     |
| [`events`](./events.html)         | 从服务器获取实时事件                                       |
| [`exec`](./exec.html)             | 在正在运行的容器中执行命令                                 |
| [`export`](./export.html)         | 将容器的文件系统导出为 tar 归档                            |
| [`history`](./history.html)       | 显示图像的历史记录                                         |
| [`image`](./image.html)           | 管理图像                                                   |
| [`images`](./images.html)         | 列表图像                                                   |
| [`import`](./import.html)         | 从压缩包导入内容以创建文件系统映像                         |
| [`info`](./info.html)             | 显示系统范围的信息                                         |
| [`inspect`](./inspect.html)       | 返回有关 Docker 对象的低级信息                             |
| [`kill`](./kill.html)             | 终止一个或多个正在运行的容器                               |
| [`load`](./load.html)             | 从 tar 存档或 STDIN 加载图像                               |
| [`login`](./login.html)           | 登录到注册表                                               |
| [`logout`](./logout.html)         | 从注册表注销                                               |
| [`logs`](./logs.html)             | 获取容器的日志                                             |
| [`manifest`](./manifest.html)     | 管理 Docker 映像清单和清单列表                             |
| [`network`](./network.html)       | 管理网络                                                   |
| [`node`](./node.html)             | 管理群节点                                                 |
| [`pause`](./pause.html)           | 暂停一个或多个容器中的所有进程                             |
| [`plugin`](./plugin.html)         | 管理插件                                                   |
| [`port`](./port.html)             | 列出容器的端口映射或特定映射                               |
| [`ps`](./ps.html)                 | 列出容器                                                   |
| [`pull`](./pull.html)             | 从注册表下载映像                                           |
| [`push`](./push.html)             | 将映像上传到注册表                                         |
| [`rename`](./rename.html)         | 重命名容器                                                 |
| [`restart`](./restart.html)       | 重新启动一个或多个容器                                     |
| [`rm`](./rm.html)                 | 删除一个或多个容器                                         |
| [`rmi`](./rmi.html)               | 删除一个或多个图像                                         |
| [`run`](./run.html)               | 从映像创建并运行新容器                                     |
| [`save`](./save.html)             | 将一张或多张图像保存到 tar 存档（默认情况式传输到 STDOUT） |
| [`search`](./search.html)         | 在 Docker Hub 中搜索映像                                   |
| [`secret`](./secret.html)         | 管理群机密                                                 |
| [`service`](./service.html)       | 管理群服务                                                 |
| [`stack`](./stack.html)           | 管理群堆栈                                                 |
| [`start`](./start.html)           | 启动一个或多个已停止的容器                                 |
| [`stats`](./stats.html)           | 显示容器资源使用情况统计信息的实时流                       |
| [`stop`](./stop.html)             | 停止一个或多个正在运行的容器                               |
| [`swarm`](./swarm.html)           | 管理群                                                     |
| [`system`](./system.html)         | 管理码头工人                                               |
| [`tag`](./tag.html)               | 创建引用SOURCE_IMAGE的标记TARGET_IMAGE                     |
| [`top`](./top.html)               | 显示容器正在运行的进程                                     |
| [`trust`](./trust.html)           | 管理 Docker 映像上的信任                                   |
| [`unpause`](./unpause.html)       | 取消暂停一个或多个容器中的所有进程                         |
| [`update`](./update.html)         | 更新一个或多个容器的配置                                   |
| [`version`](./version.html)       | 显示 Docker 版本信息                                       |
| [`volume`](./volume.html)         | 管理卷                                                     |
| [`wait`](./wait.html)             | 阻止，直到一个或多个容器停止，然后打印其退出代码           |



### **attach** 

```shell
# 用于附加终端的标准输入、输出和错误 （或三者的任意组合）到使用容器的 身份证或姓名。这允许您查看其正在进行的输出或控制它 交互式，就像命令直接在终端中运行一样。
docker run -d --name topdemo ubuntu:22.04 /usr/bin/top -b
docker attach topdemo
# 对比attach和exec
# docker attach可以attach到一个已经运行的容器的stdin，然后进行命令执行的动作。但是需要注意的是，如果从这个stdin中exit，会导致容器的停止。
# 用docker exec -it命令进入容器如果输入exit命令直接退出container，但是不会使得container停止
```

### **build**

从Dockerfile构建镜像,该参数可以引用三种资源：Git 存储库、 预打包的压缩包上下文和纯文本文件。

```shell
 # 从git构建
 docker build https://github.com/docker/rootfs.git#container:docker
  # 从压缩包构建
 docker build http://server/context.tar.gz
  # 从Dockerfile构建
 docker build -f Dockerfile.debug .
```

更为详细的信息参照官网



### **builder**

```shell
# 清理build缓存
docker builder prune 
```



### **checkpoint**

检查点和还原是一项实验性功能，允许您冻结正在运行的 容器，通过检查点将其状态转换为文件集合 在磁盘上。稍后，容器可以从冻结点恢复。

```shell
# docker开启checkpoint: "experimental": true,
[root@jenkins ~]# cat /etc/docker/daemon.json
{
    "experimental": true,
    "data-root": "/home/docker_data",
    "log-driver": "json-file",
    "log-opts": {"max-size": "500m", "max-file": "3"},
    "insecure-registries": ["10.0.0.13"],
    "registry-mirrors": ["https://yo3sdl2l.mirror.aliyuncs.com","https://registry.docker-cn.com","http://hub-mirror.c.163.com","https://docker.mirrors.ustc.edu.cn"]
}
# centos7安装criu
yum install -y criu
criu --version
# 重启docker
systemctl restart docker

# 使用
docker checkpoint create（创建一个新的检查点）
docker checkpoint ls（列出现有检查点）
docker checkpoint rm（删除现有检查点）
# 实验
# 运行一个容器
docker run --security-opt=seccomp:unconfined --name cr -d busybox /bin/sh -c 'i=0; while true; do echo $i; i=$(expr $i + 1); sleep 1; done'
# 查看当前打印的数字
docker logs -f cr
# 创建检查点（创建检查点后，容器会退出）
docker checkpoint create cr checkpoint1

#  从检查点还原
docker start --checkpoint checkpoint1 cr
# 查看打印的数字 应该和创建检查点保持一致或者数字增加了一点
docker logs -f cr
```



### **commit**

根据容器的更改创建新映像；将容器的文件更改或设置提交到新的文件中可能很有用 图像。这允许您通过运行交互式 shell 来调试容器，或者 将工作数据集导出到另一台服务器。一般来说，最好使用 Dockerfile 以记录和可维护的方式管理您的映像。默认情况下，正在提交的容器及其进程将暂停 提交映像时。这降低了遇到数据的可能性 创建提交过程中的损坏。如果此行为是 不需要的，将选项设置为 false。

```shell
#提交容器
[root@jenkins ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
fd960e030c9a        busybox             "/bin/sh -c 'i=0; wh…"   19 minutes ago      Up 19 minutes                           cr
[root@jenkins ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
fd960e030c9a        busybox             "/bin/sh -c 'i=0; wh…"   19 minutes ago      Up 19 minutes                           cr
[root@jenkins ~]# docker commit fd960e030c9a  svendowideit/testimage:version3
sha256:8df7370238431f376645389a547551f5b0dc7abb2c3dab1fbfa01242f61ebe45
[root@jenkins ~]# docker images|grep svendowideit
svendowideit/testimage         version3            8df737023843        39 seconds ago      1.24MB
[root@jenkins ~]#

# 提交具有新配置的容器
[root@jenkins ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
fd960e030c9a        busybox             "/bin/sh -c 'i=0; wh…"   21 minutes ago      Up 20 minutes                           cr
[root@jenkins ~]# docker inspect -f "{{ .Config.Env }}" fd960e030c9a
[PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin]
[root@jenkins ~]# docker commit --change "ENV DEBUG=true" fd960e030c9a svendowideit/testimage:version4
sha256:2d67fed86352919ac4044d7892a7a4a426a21a608714afe7e9b8f405d12cfb52
[root@jenkins ~]# docker inspect -f "{{ .Config.Env }}" 2d67fed86352919ac404
[PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin DEBUG=true]

#提交带有新指令的容器
[root@jenkins ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
fd960e030c9a        busybox             "/bin/sh -c 'i=0; wh…"   23 minutes ago      Up 22 minutes                           cr
[root@jenkins ~]# docker commit --change='CMD ["apachectl", "-DFOREGROUND"]' -c "EXPOSE 80" fd960e030c9a  svendowideit/testimage:version5
sha256:92e9a7cfd1e138c2ede61b67c8027f1ca9348108b9ddeded16fee4435d09cd0a
[root@jenkins ~]# docker run -d svendowideit/testimage:version5
bcc953d338705fd52115b8e8046b9ae3d2a0a7a6b4f855f5a3131a241fde6d0d
[root@jenkins ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
fd960e030c9a        busybox             "/bin/sh -c 'i=0; wh…"   24 minutes ago      Up 24 minutes                           cr
[root@jenkins ~]# docker ps -a
CONTAINER ID        IMAGE                             COMMAND                  CREATED             STATUS                           PORTS               NAMES
bcc953d33870        svendowideit/testimage:version5   "apachectl -DFOREGRO…"   10 seconds ago      Created                          80/tcp              sharp_dirac
[root@jenkins ~]# docker ps -a|grep bcc953d33870
bcc953d33870        svendowideit/testimage:version5   "apachectl -DFOREGRO…"   36 seconds ago      Created                          80/tcp              sharp_dirac
[root@jenkins ~]#

```

### **config**

管理群配置,此命令适用于 Swarm 业务流程协调程序。

```shell
docker config create	#从文件或 STDIN 创建配置
docker config inspect	#显示一个或多个配置的详细信息
docker config ls	#列出配置
docker config rm	#删除一个或多个配置
```



### **context**

可以快速切换 cli 用于连接到不同群集或单个节点的配置



### **cp**

应用工具将 的内容复制到 . 您可以从容器的文件系统复制到本地计算机或 反向，从本地文件系统到容器。

```shell
# 将本地文件复制到容器中
docker cp ./some_file CONTAINER:/work
# 将文件从容器复制到本地路径
docker cp CONTAINER:/var/logs/ /tmp/app_logs
# 将文件从容器复制到标准输出。请注意命令产生 tar 流cp
 docker cp CONTAINER:/var/logs/app.log - | tar x -O | grep "ERROR"
```





### **diff**

列出容器创建后文件系统中改变的目录。跟踪三种不同类型的更改：

| 象征 | 描述             |
| :--- | :--------------- |
| `A`  | 添加了文件或目录 |
| `D`  | 已删除文件或目录 |
| `C`  | 文件或目录已更改 |

```shell
[root@jenkins ~]# docker diff busy_bridge
C /root
A /root/.ash_history

```

### **events**

从服务器获取实时事件;

用于从服务器获取实时事件。这些事件有所不同 每个 Docker 对象类型。不同的事件类型具有不同的范围。当地 作用域内事件仅在它们发生的节点上可见，并且群作用域内 在所有经理上都可以看到事件。仅返回最后 1000 个日志事件。您可以使用过滤器来进一步限制 返回的事件数。

shell1 侦听事件：

```shell
docker events
```

shell2  启动和停止容器：

```shell
docker create --name test alpine:latest top
docker start test
docker stop test
```

再次查看shell1:

```shell
[root@jenkins ~]# docker events
2023-04-18T07:59:40.349583968-04:00 container destroy 8e522c4f63d3ce893c68fcbe9ddb381a84f0ab35f0dd90c3d55dfe3b9a3a2b12 (image=alpine, name=test)
2023-04-18T07:59:41.870697978-04:00 container create 659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42 (image=alpine:latest, name=test)
2023-04-18T07:59:52.761667539-04:00 network connect f7745a71e72ced56874e169ce2c7c04f55a08221a70b261e9fd940f5b50544f6 (container=659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42, name=bridge, type=bridge)
2023-04-18T07:59:53.259079577-04:00 container start 659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42 (image=alpine:latest, name=test)
2023-04-18T07:59:56.772608953-04:00 container kill 659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42 (image=alpine:latest, name=test, signal=15)
2023-04-18T07:59:56.827030236-04:00 container die 659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42 (exitCode=143, image=alpine:latest, name=test)
2023-04-18T07:59:56.862998000-04:00 network disconnect f7745a71e72ced56874e169ce2c7c04f55a08221a70b261e9fd940f5b50544f6 (container=659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42, name=bridge, type=bridge)
2023-04-18T07:59:56.872122412-04:00 container stop 659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42 (image=alpine:latest, name=test)
 # 格式化输出
 docker events --filter 'type=container' --format 'Type={{.Type}}  Status={{.Status}}  ID={{.ID}}'
Type=container  Status=kill  ID=659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42
Type=container  Status=die  ID=659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42
Type=container  Status=stop  ID=659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42

# json格式化输出
 docker events --format '{{json .}}'
{"Type":"network","Action":"connect","Actor":{"ID":"f7745a71e72ced56874e169ce2c7c04f55a08221a70b261e9fd940f5b50544f6","Attributes":{"container":"659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42","name":"bridge","type":"bridge"}},"scope":"local","time":1681819502,"timeNano":1681819502589284936}
{"status":"start","id":"659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42","from":"alpine:latest","Type":"container","Action":"start","Actor":{"ID":"659fc3b77408a013cf2a2cb5b292254e028369f6fcd1b0baa158d88fff42bc42","Attributes":{"image":"alpine:latest","name":"test"}},"scope":"local","time":1681819503,"timeNano":1681819503271453899}
```

### **export**

该命令不导出关联卷的内容 与容器。如果卷装载在 容器将导出*基础*目录的内容，而不是卷的内容。

```shell
# 两种方式结果相同
docker export red_panda > latest.tar
docker export --output="latest.tar" red_panda
```

### **history**

显示图像的历史记录

```shell
[root@jenkins ~]# docker history   busybox
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
beae173ccac6        15 months ago       /bin/sh -c #(nop)  CMD ["sh"]                   0B
<missing>           15 months ago       /bin/sh -c #(nop) ADD file:6db446a57cbd2b7f4…   1.24MB

# 格式化输出
[root@jenkins ~]# docker history --format "{{.ID}}: {{.CreatedSince}}" busybox
beae173ccac6: 15 months ago
<missing>: 15 months ago

```

| 占 位 符        | 描述                                                         |
| :-------------- | :----------------------------------------------------------- |
| `.ID`           | 图像标识                                                     |
| `.CreatedSince` | 自创建映像以来经过的时间，如果 ，否则为创建映像时的时间戳`--human=true` |
| `.CreatedAt`    | 创建映像时的时间戳                                           |
| `.CreatedBy`    | 用于创建映像的命令                                           |
| `.Size`         | 映像磁盘大小                                                 |
| `.Comment`      | 图片注释                                                     |

### **import**

从压缩包导入内容以创建文件系统映像

```shell
#从远程位置导入
docker import https://example.com/exampleimage.tgz
# 从本地文件导入
 cat exampleimage.tgz | docker import - exampleimagelocal:new
 cat exampleimage.tgz | docker import --message "New image imported from tarball" - exampleimagelocal:new
 docker import /path/to/exampleimage.tgz
 # 从本地目录导入
sudo tar -c . | docker import - exampleimagedir
 # 使用新配置从本地目录导入
 sudo tar -c . | docker import --change "ENV DEBUG=true" - exampleimagedir
```

### **inspect**

Docker inspect 提供有关 Docker 控制的构造的详细信息。

默认情况下，将在 JSON 数组中呈现结果。

```shell
 docker inspect --size mycontainer
 # 获取实例的 IP 地址
 docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $INSTANCE_ID
 # 获取实例的 MAC 地址
 docker inspect --format='{{range .NetworkSettings.Networks}}{{.MacAddress}}{{end}}' $INSTANCE_ID
 # 获取实例的日志路径
 docker inspect --format='{{.LogPath}}' $INSTANCE_ID
 # 获取实例的镜像名称
 docker inspect --format='{{.Config.Image}}' $INSTANCE_ID
 # 列出所有端口绑定
  docker inspect --format='{{range $p, $conf := .NetworkSettings.Ports}} {{$p}} -> {{(index $conf 0).HostPort}} {{end}}' $INSTANCE_ID
 # 查找特定端口映射
 docker inspect --format='{{(index (index .NetworkSettings.Ports "8787/tcp") 0).HostPort}}' $INSTANCE_ID
 # 获取 JSON 格式的小节
  docker inspect --format='{{json .Config}}' $INSTANCE_ID
```

### **login**

```shell
docker login localhost:8080
 cat ~/my_password.txt | docker login --username foo --password-stdin
```



### **manifest**

构建多种系统架构支持的 Docker 镜像

docker manifest命令本身不执行任何操作。为了操作 在清单或清单列表中，必须使用其中一个子命令。

单个清单是有关映像的信息，例如层、大小和摘要。 docker 清单命令还为用户提供了其他信息，例如操作系统 以及构建映像的体系结构。

清单列表是通过指定一个或 多个（理想情况下多个）映像名称。然后可以以与 例如，和命令中的映像名称。docker pulldocker run

理想情况下，从功能相同的图像创建清单列表 不同的操作系统/架构组合。因此，经常引用清单列表 作为“多架构图像”。但是，用户可以创建一个清单列表，该列表指向 到两个图像 - 一个用于AMD64上的Windows，另一个用于AMD64上的Darwin。



### **network后续补充** 



```shell
docker network connect
docker network create
docker network disconnect
docker network inspect
docker network ls
docker network prune
docker network rm
```




### **plugin 后续补充**



### **ps**

列出容器

| 滤波器              | 描述                                                         |
| :------------------ | :----------------------------------------------------------- |
| `id`                | 集装箱的标识                                                 |
| `name`              | 容器的名称                                                   |
| `label`             | 表示键或键值对的任意字符串。表示为 或`<key>``<key>=<value>`  |
| `exited`            | 表示容器的退出代码的整数。仅对 有用。`--all`                 |
| `status`            | 、、、、、或 中的一种`created``restarting``running``removing``paused``exited``dead` |
| `ancestor`          | 筛选将给定映像共享为祖先的容器。表示为 、 或`<image-name>[:<tag>]``<image id>``<image@digest>` |
| `before`或`since`   | 筛选在给定容器 ID 或名称之前或之后创建的容器                 |
| `volume`            | 过滤器运行已装入给定卷或绑定装载的容器。                     |
| `network`           | 筛选运行连接到给定网络的容器。                               |
| `publish`或`expose` | 筛选发布或公开给定端口的容器。表示为 或`<port>[/<proto>]``<startport-endport>/[<proto>]` |
| `health`            | 根据容器的运行状况检查状态筛选容器。、 或 之一。`starting``healthy``unhealthy``none` |
| `isolation`         | 仅限 Windows 守护程序。、 或 之一。`default``process``hyperv` |
| `is-task`           | 筛选作为服务“任务”的容器。布尔选项（或`true``false`)         |



```shell
# 不要截断输出（--no-trunc）
docker ps --no-trunc
# 按容器显示磁盘使用情况
docker ps --size -s
# 筛选名称中的子字符串
docker ps --filter "name=nostalgic"
# 筛选器按存在状态代码匹配容器。例如，到 筛选已成功退出的容器
docker ps -a --filter 'exited=0'
```



### **pull**

从注册表下载映像

```shell
# 从其他注册表拉取
docker image pull myregistry.local:5000/testing/test-image
# 从存储库中提取所有映像：ubuntu
docker image pull --all-tags ubuntu
```



### **push**

将映像上传到注册表

```shell
# 推送图像的所有标签
docker image push --all-tags registry-host:5000/myname/myimage
```

### **rm**

```shell
# 删除所有停止的容器
docker rm $(docker ps --filter status=exited -q)
# 强制删除正在运行的容器
docker rm --force redis
```

### **run后续补充**

```shell

```

### save

将一张或多张图像保存到 tar 存档

```shell
# 使用 gzip 将图像保存到 tar.gz 文件
[root@jenkins ~]# docker save c059bfaa849c |gzip > 1.tar.gz
[root@jenkins ~]# docker save c059bfaa849c -o 2.tar.gz
[root@jenkins ~]# ls 1.tar.gz  2.tar.gz  -al
-rw-r--r-- 1 root root 2742785 Apr 18 11:48 1.tar.gz
-rw------- 1 root root 5874688 Apr 18 11:48 2.tar.gz
[root@jenkins ~]# docker load -i 1.tar.gz
Loaded image ID: sha256:c059bfaa849c4d8e4aecaeb3a10c2d9b3d85f5165c66ad3a4d937758128c4d18
```



### stats

显示容器资源使用情况统计信息的实时流

```shell

# 获取格式的输出
[root@jenkins ~]# docker stats cr --no-stream --format "{{ json . }}"
{"BlockIO":"0B / 0B","CPUPerc":"0.11%","Container":"cr","ID":"fd960e030c9a","MemPerc":"0.02%","MemUsage":"392KiB / 1.925GiB","Name":"cr","NetIO":"2kB / 0B","PIDs":"2"}
# 格式化输出
[root@jenkins ~]# docker stats --format "{{.Container}}: {{.CPUPerc}}"
fd960e030c9a: 0.09%
# 列出所有容器统计信息及其名称、CPU 百分比和内存 表格格式
[root@jenkins ~]# docker stats --format "table {{.Container}}\t{{.CPUPerc}}\t{{.MemUsage}}"
CONTAINER           CPU %               MEM USAGE / LIMIT
fd960e030c9a        0.09%               408KiB / 1.925GiB

```



### system

管理docker

```shell
# 显示 docker 磁盘使用情况
[root@jenkins ~]# docker system df
TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
Images              23                  4                   3.743GB             3.52GB (94%)
Containers          7                   1                   1.133kB             1.133kB (100%)
Local Volumes       0                   0                   0B                  0B
Build Cache         0                   0                   0B                  0B
# 更为详细的信息
[root@jenkins ~]# docker system df -v

# 显示系统范围的信息
 docker system info

# 删除所有未使用的容器、网络、图像（悬空和未引用）
 docker system prune
# 删除所有未使用的容器、网络、图像（悬空和未引用）以及卷。
 docker system prune -a --volumes
```



### update

更新一个或多个容器的配置

```sh
# 更新容器的 CPU 份额
docker update --cpu-shares 512 abebf7571666
# 更新 CPU 份额和内存
docker update --cpu-shares 512 -m 300M abebf7571666 hopeful_morse
#　更新容器的重启策略
docker update --restart=on-failure:3 abebf7571666 hopeful_morse
```

### volume

```sh
 
#　创建卷
docker volume create hello
#　某些卷驱动程序可能会采用选项来自定义卷创建。使用 or 标志传递驱动程序选项：-o--opt
docker volume create --driver fake \
    --opt tardis=blue \
    --opt timey=wimey \
    foo
#　下面创建一个名为 100兆字节和1000兆字节。tmpfsfoouid
docker volume create --driver local \
    --opt type=tmpfs \
    --opt device=tmpfs \
    --opt o=size=100m,uid=1000 \
    foo
#　另一个使用的示例：btrfs
docker volume create --driver local \
    --opt type=btrfs \
    --opt device=/dev/sda2 \
    foo
#　另一个用于从以下位置挂载 in 模式的示例：nfs/path/to/dirrw192.168.1.1
docker volume create --driver local \
    --opt type=nfs \
    --opt o=addr=192.168.1.1,rw \
    --opt device=:/path/to/dir \
    foo
```







## 构建镜像

**Dockerfile**

```text
FROM :指定基础镜像，通常是一个官方镜像或者一个已经存在的镜像。
RUN :指定构建过程中需要执行的命令，通常是一些基础命令，如 apt-get update、apt-get install 等。
COPY :该指令从中复制新文件或目录，并将它们添加到路径 的容器文件系统中。
ENV :用于设置环境变量，可以在构建过程中动态设置变量值。可以是字符串、数字、整数或者一个列表。
WORKDIR :用于指定构建过程中的工作目录，可以是一个目录或者一个路径。
ADD :该指令从中复制新文件、目录或远程文件 URL，并将它们添加到路径 中的映像文件系统中
EXPOSE:用于指定容器启动时需要暴露的端口，可以是一个端口或者一个路径。
LABEL:该指令将元数据添加到图像：LABEL version="1.0"
STOPSIGNAL:该指令设置将发送到 要退出的容器。
USER:用于指定容器运行时的用户名，可以是一个字符串或者一个整数。
VOLUME:用于指定容器启动时需要挂载的目录或者文件系统，可以是一个目录或者一个路径。
ONBUILD:用于指定在构建过程中执行的命令，通常是一些基础命令，如 apt-get update、apt-get install 等。
ARG  :该指令定义了一个变量，用户可以在构建时将该变量传递给 使用标志的命令的构建器。如果用户指定的生成参数不是 在 Dockerfile 中定义，构建会输出警告。
CMD:用于指定构建完成后运行的命令，通常是一些基础命令，如 ls 等。
HEALTHCHECK :该指令告诉 Docker 如何测试容器以检查 它仍在工作。
SHELL :该指令允许用于 shell 形式的默认 shell。
PORT：用于指定容器启动时需要暴露的端口，可以是一个端口或者一个路径。
GROUP 用于指定容器运行时的组名，可以是一个字符串或者一个整数。
```



**.dockerignore** 

在 docker CLI 将上下文发送到 docker 守护程序之前，它看起来 在上下文的根目录中命名的文件。 如果此文件存在，CLI 将修改上下文以排除文件和 与其中的模式匹配的目录。这有助于避免 不必要地将大型或敏感文件和目录发送到 守护程序，并可能使用 或 将它们添加到映像中。

CLI 将文件解释为换行符分隔 类似于 Unix shell 的文件 glob 的模式列表。对于 为了匹配，上下文的根被认为是两者 工作目录和根目录。例如，模式和两者都排除在 git 的子目录或根目录中命名的文件或目录 存储库位于 。两者都不排除其他任何内容。

```ini
# comment
*/temp*
*/*/temp*
temp?
```



## 镜像存储

### harbor

安装脚本：[install_harbor.sh · shiya.liu/ShellScript - 码云 - 开源中国 (gitee.com)](https://gitee.com/shiya_liu/ShellScript/blob/main/install_harbor.sh)

```shell
[root@jenkins ~]# cat /etc/docker/daemon.json
{
    "experimental": true,
    "data-root": "/home/docker_data",
    "log-driver": "json-file",
    "log-opts": {"max-size": "500m", "max-file": "3"},
    "insecure-registries": ["10.0.0.13"],
    "registry-mirrors": ["https://yo3sdl2l.mirror.aliyuncs.com","https://registry.docker-cn.com","http://hub-mirror.c.163.com","https://docker.mirrors.ustc.edu.cn"]
}
```




# 容器编排



## Docker Compose有待补充

### docker-compose.yaml的关键字有42左右，太多，暂时搁置



### 命令

```none
  build              构建或重建服务
  bundle             从Compose文件生成Docker捆绑包
  config             验证和查看docker-compose.yaml文件
  create             创建服务
  down               停止并删除容器、网络、映像和卷
  events             从容器接收实时事件
  exec               在运行的容器中执行命令
  help               获取有关命令的帮助
  images             列出镜像
  kill               杀死容器
  logs               查看容器的日志
  pause              挂起容器
  port               打印端口绑定的公共端口
  ps                 列出容器
  pull               拉取镜像
  push               推送镜像
  restart            重新启动服务
  rm                 删除容器
  run                运行一次性命令
  scale              设置服务的容器数
  start              启动服务
  stop               停止服务
  top               　显示正在运行的进程
  unpause            恢复挂起的服务
  up                 创建并启动服务
  version            显示版本
```













## Docker Swarm

### 简介

Docker 1.12 Swarm mode 已经内嵌入 Docker 引擎，成为了 docker 子命令 docker swarm。请注意与旧的 Docker Swarm 区分开来。Swarm mode 内置 kv 存储功能，提供了众多的新特性，比如：具有容错能力的去中心化设计、内置服务发现、负载均衡、路由网格、动态伸缩、滚动更新、安全传输等。使得 Docker 原生的 Swarm 集群具备与 Mesos、Kubernetes 竞争的实力。

#### 基本概念

Swarm 是使用 SwarmKit 构建的 Docker 引擎内置（原生）的集群管理和编排工具。使用 Swarm 集群之前需要了解以下几个概念。

##### 节点

运行 Docker 的主机可以主动初始化一个 Swarm 集群或者加入一个已存在的 Swarm 集群，这样这个运行 Docker 的主机就成为一个 Swarm 集群的节点 (node) 。

节点分为管理 (manager) 节点和工作 (worker) 节点。

管理节点用于 Swarm 集群的管理，docker swarm 命令基本只能在管理节点执行（节点退出集群命令 docker swarm leave 可以在工作节点执行）。一个 Swarm 集群可以有多个管理节点，但只有一个管理节点可以成为 leader，leader 通过 raft 协议实现。

工作节点是任务执行节点，管理节点将服务 (service) 下发至工作节点执行。管理节点默认也作为工作节点。你也可以通过配置让服务只运行在管理节点。

来自 Docker 官网的这张图片形象的展示了集群中管理节点与工作节点的关系。

![image-20230418180408998](assets/image-20230418180408998.png)

##### 服务和任务

任务 （Task）是 Swarm 中的最小的调度单位，目前来说就是一个单一的容器。

服务 （Services） 是指一组任务的集合，服务定义了任务的属性。服务有两种模式：

replicated services 按照一定规则在各个工作节点上运行指定个数的任务。

global services 每个工作节点上运行一个任务

两种模式通过 docker service create 的 --mode 参数指定。

来自 Docker 官网的这张图片形象的展示了容器、任务、服务的关系。

![image-20230418180442257](assets/image-20230418180442257.png)



### 命令

#### swarm

```shell
# 运行不带任何选项的命令以查看当前根 CA 证书 采用 PEM 格式。
[root@jenkins ~]# docker swarm ca
-----BEGIN CERTIFICATE-----
MIIBazCCARCgAwIBAgIUbfxIqegC/smAzbxv0ftoosRz5UEwCgYIKoZIzj0EAwIw
EzERMA8GA1UEAxMIc3dhcm0tY2EwHhcNMjMwNDE4MTgyMjAwWhcNNDMwNDEzMTgy
MjAwWjATMREwDwYDVQQDEwhzd2FybS1jYTBZMBMGByqGSM49AgEGCCqGSM49AwEH
A0IABNwJV/caRZltJ+idKXXq4iQqL6g5JxfCB/vDhcApr1xiFy108CncHaa/O0rw
riqNe26StwAGGDBXvoJQm43JSUmjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMB
Af8EBTADAQH/MB0GA1UdDgQWBBREEM0nqmnPHqOVquqbP7n1hnMZnDAKBggqhkjO
PQQDAgNJADBGAiEAwQj+lkYfrYLIlG4AEfpfpzXa6dywR8C6/oDs0xwCsLkCIQCM
9zbVaR/cona75vp40XEyjrdFco6d1xBSTrgbSoOAqQ==
-----END CERTIFICATE-----
# 轮换当前的群根 CA
[root@jenkins ~]# docker swarm ca --rotate
desired root digest: sha256:ab33f9a42a672263c2615612b25ab20344e77f35148710f218646dfef073d625
  rotated TLS certificates:  [==================================================>] 3/3 nodes
  rotated CA certificates:   [==================================================>] 3/3 nodes
-----BEGIN CERTIFICATE-----
MIIBazCCARCgAwIBAgIUS7NeLnFMezcoir92Pg0eVzmjB5QwCgYIKoZIzj0EAwIw
EzERMA8GA1UEAxMIc3dhcm0tY2EwHhcNMjMwNDE4MjMyNDAwWhcNNDMwNDEzMjMy
NDAwWjATMREwDwYDVQQDEwhzd2FybS1jYTBZMBMGByqGSM49AgEGCCqGSM49AwEH
A0IABCm2OEp2F3rgb0d353S9yjvndoXso1yTjAZ+q/HAPXk0sQ6XHv716x/SH056
oqXtG+fY3sMXAOBZEA16ANEbqyWjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMB
Af8EBTADAQH/MB0GA1UdDgQWBBTVJyw4ZFPNkIfMuvjEqKlY/6e+SjAKBggqhkjO
PQQDAgNJADBGAiEAyvKsaVVDPLmjxMCVWV2V1mLzBZCJVkQYmk5pgVSGlKgCIQCo
ZP6DhcVUtrtPvHCCZkWFLpoyzuMWaKOoct90do3Q0g==
-----END CERTIFICATE-----

# docker swarm init生成两个随机令牌：一个工作线程令牌和一个经理令牌。当您加入时 群的新节点，该节点根据您传递的令牌作为工作节点或管理器节点加入 蜂拥加入。
docker swarm init 


# 使用管理器令牌加入管理器节点。
docker swarm join --token SWMTKN-1-3pu6hszjas19xyp7ghgosyx9k8atbfcr8p2is99znpy26u2lkl-7p73s1dx5in4tatdymyhg9hu2 192.168.99.121:2377

# 使用工作线程令牌加入工作线程节点。
docker swarm join --token SWMTKN-1-3pu6hszjas19xyp7ghgosyx9k8atbfcr8p2is99znpy26u2lkl-1awxwuwd3z9j1z3puu7rcgdbx 192.168.99.121:2377

# 查看worker的token
[root@jenkins ~]# docker swarm join-token worker
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-49m3j1stuhujr8f5c0rcywkyfpxi6sjodpodvvwovmlbketw6d-blvys8u7l817nxecr0zwnx4bk 10.0.0.11:2377
# 查看manager的token
[root@jenkins ~]# docker swarm join-token manager
To add a manager to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-49m3j1stuhujr8f5c0rcywkyfpxi6sjodpodvvwovmlbketw6d-6q20ve4ky4vw5v3oci5fcv23x 10.0.0.11:2377
# 使用该标志为指定角色生成新的加入令牌：重新生成token后，只有新令牌才能使用指定角色加入。
docker swarm join-token --rotate worker


# 在worker节点上执行命令，离开集群
docker swarm leave
# 在manager上查看并删除节点
[root@jenkins ~]# docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
atomn5cwhh6q5cxwcjpjpgf9v     gitlab              Down                Active                                  19.03.6
savkzrlmmxgmke5icus307fcf     harbor              Ready               Active                                  19.03.6
jdt2yhcp05w3an27l5rvp98o2 *   jenkins             Ready               Active              Leader              19.03.6
[root@jenkins ~]# docker node rm gitlab
gitlab
[root@jenkins ~]# docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
savkzrlmmxgmke5icus307fcf     harbor              Ready               Active                                  19.03.6
jdt2yhcp05w3an27l5rvp98o2 *   jenkins             Ready               Active              Leader              19.03.6
# 使用用户提供的解锁密钥解锁锁定的管理器。此命令必须是 用于在其 Docker 守护程序重新启动后重新激活管理器（如果自动锁定） 设置已打开。解锁密钥在自动锁定时打印 已启用，也可从命令中获得。
docker swarm unlock

# 解锁密钥是在 Docker 守护程序之后解锁管理器所需的密钥 重新 启动。
docker swarm unlock-key

# 更新集群资源
 docker swarm update --cert-expiry 720h
```



| 名称，速记               | 违约        | 描述                                         |
| ------------------------ | ----------- | -------------------------------------------- |
| `--autolock`             |             | 更改管理器自动锁定设置（真\|假）             |
| `--cert-expiry`          | `2160h0m0s` | 节点证书的有效期 （ns\|us\|ms\|s\|m\|h）     |
| `--dispatcher-heartbeat` | `5s`        | 调度程序检测信号周期 （ns\|us\|ms\|s\|m\|h） |
| `--external-ca`          |             | 一个或多个证书签名终结点的规范               |
| `--max-snapshots`        |             | 要保留的其他 Raft 快照数                     |
| `--snapshot-interval`    | `10000`     | Raft 快照之间的日志条目数                    |
| `--task-history-limit`   | `5`         | 任务历史记录保留限制                         |
| `--help`                 |             | 打印使用情况                                 |





#### node

管理群节点

```shell
#取消现有经理的动机，使其不再是经理。
docker node demote <node name>
# 获取节点详细信息
docker node inspect swarm-manager
# 列出群中的节点
docker node ls
# 将一个或多个节点提升为群中的管理器
docker node promote <node name>
# 列出在一个或多个节点上运行的任务，默认为当前节点
docker node ps swarm-manager1
# 从群中删除已停止的节点
docker node rm swarm-node-02
# 从群中删除正在运行的节点
docker node rm --force swarm-node-03

# 将标签元数据添加到节点（--标签-添加）
docker node update --label-add foo worker1

```





#### config 

此命令适用于 Swarm 业务流程协调程序，用于创建配置文件，然后挂载到容器中去

```shell
# 将配置添加到 Docker。
echo "This is a config" | docker config create my-config -
# 创建服务并使用配置文件。
docker service create --name redis --config my-config redis:alpine
# 使用 验证任务是否正在运行且没有问题
docker service ps redis
# 使用 获取服务任务容器的 ID，以便 您可以使用 连接到容器并读取内容 的配置文件数据文件
docker ps --filter name=redis -q
docker container exec $(docker ps --filter name=redis -q) ls -l /my-config
docker container exec $(docker ps --filter name=redis -q) cat /my-config
# 尝试删除配置。删除失败，因为服务是 正在运行并有权访问配置
docker config ls
docker config rm my-config

Error response from daemon: rpc error: code = InvalidArgument desc = config 'my-config' is in use by the following service: redis
# 通过更新服务删除配置
docker service update --config-rm my-config redis
# 再次验证服务是否不再具有访问权限 到配置。
docker container exec -it $(docker ps --filter name=redis -q) cat /my-config
# 停止并删除服务，然后从 Docker 中删除配置。
docker service rm redis
docker config rm my-config
```

使用模板化配置

```shell
# 将以下内容保存到新文件中。index.html.tmpl
[root@jenkins ~]# cat index.html.tmpl
<html lang="en">
  <head><title>Hello Docker</title></head>
  <body>
    <p>Hello {{ env "HELLO" }}! I'm service {{ .Service.Name }}.</p>
  </body>
</html>

# 将文件存为群配置
docker config create --template-driver golang homepage index.html.tmpl
# 创建一个运行 Nginx 并有权访问环境变量的服务 你好，到配置。
docker service create \
     --name hello-template \
     --env HELLO="Docker" \
     --config source=homepage,target=/usr/share/nginx/html/index.html \
     --publish published=3000,target=80 \
     nginx:alpine
  
 # 验证服务是否正常运行：您可以访问 Nginx 服务器，并且 正在提供正确的输出。
 [root@jenkins ~]# docker service ps hello-template
ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
rucf8i44k0ox        hello-template.1    nginx:alpine        gitlab              Running             Running 15 seconds ago

[root@gitlab ~]# curl http://0.0.0.0:3000
<html lang="en">
  <head><title>Hello Docker</title></head>
  <body>
    <p>Hello Docker! I'm service hello-template.</p>
  </body>
</html>


```



#### secret

从文件或 STDIN 创建密钥作为内容

```shell
# 创建密钥
[root@jenkins ~]#  printf "my super secret password" | docker secret create my_secret -
bnp22kplnmlqhrvagz7d52ng6
[root@jenkins ~]# docker secret ls
ID                          NAME                DRIVER              CREATED             UPDATED
bnp22kplnmlqhrvagz7d52ng6   my_secret                               15 seconds ago      15 seconds ago
# 使用文件创建密钥
 docker secret create my_secret ./secret.json
 [root@jenkins ~]# docker secret ls
ID                          NAME                DRIVER              CREATED              UPDATED
bnp22kplnmlqhrvagz7d52ng6   my_secret                               About a minute ago   About a minute ago
tml36g346r18iio7mvhloyj1y   my_secret01                             14 seconds ago       14 seconds ago
#  使用标签创建密钥 （--label）
[root@jenkins ~]# docker secret ls
ID                          NAME                DRIVER              CREATED              UPDATED
bnp22kplnmlqhrvagz7d52ng6   my_secret                               About a minute ago   About a minute ago
tml36g346r18iio7mvhloyj1y   my_secret01                             48 seconds ago       48 seconds ago
6lkv5mrz0xm0fhhbcsqerwn4x   my_secret02                             6 seconds ago        6 seconds ago
[root@jenkins ~]#
[root@jenkins ~]#
[root@jenkins ~]# docker secret inspect my_secret02
[
    {
        "ID": "6lkv5mrz0xm0fhhbcsqerwn4x",
        "Version": {
            "Index": 245
        },
        "CreatedAt": "2023-04-18T21:16:22.054445543Z",
        "UpdatedAt": "2023-04-18T21:16:22.054445543Z",
        "Spec": {
            "Name": "my_secret02",
            "Labels": {
                "env": "dev",
                "rev": "20170324"
            }
        }
    }
]

# 删除secret
[root@jenkins ~]# docker secret rm my_secret my_secret01  my_secret02
my_secret
my_secret01
my_secret02

```



#### service 

创建新服务

```shell
# 创建服务
docker service create --name redis redis:3.0.6
docker service create --mode global --name redis2 redis:3.0.6
docker service ls
# 创建具有 5 个副本任务的服务
docker service create --name redis --replicas=5 redis:3.0.6
# 使用机密创建服务
docker secret create secret.json  ./secret.json
docker service create --name redis --secret secret.json redis:3.0.6
# 创建指定密钥、目标、用户/组 ID 和模式的服务：
docker service create --name redis \
    --secret source=ssh-key,target=ssh \
    --secret source=app-key,target=app,uid=1000,gid=1001,mode=0400 \
    redis:3.0.6
# 使用配置创建服务
docker service create --name=redis --config redis-conf redis:3.0.6
# 使用配置创建服务并指定目标位置和文件模式：
docker service create --name redis \
  --config source=redis-conf,target=/etc/redis/redis.conf,mode=0400 redis:3.0.6

# 使用滚动更新策略创建服务:运行服务更新时，计划程序会更新 一次最多 2 个任务，两次更新之间
docker service create \
  --replicas 10 \
  --name redis \
  --update-delay 10s \
  --update-parallelism 2 \
  redis:3.0.6
# 要指定多个环境变量，请指定多个标志，每个标志 使用单独的键值对。--env
docker service create \
  --name redis_2 \
  --replicas 5 \
  --env MYVAR=foo \
  --env MYVAR2=bar \
  redis:3.0.6
  
 # 创建具有特定主机名（--主机名）的服务
 docker service create --name redis --hostname myredis redis:3.0.6
 #在服务上设置元数据（-l、--label）
 docker service create \
  --name redis_2 \
  --label com.example.foo="bar" \
  --label bar=baz \
  redis:3.0.6
  
  # 使用命名卷创建服务
  docker service create \
  --name my-service \
  --replicas 3 \
  --mount type=volume,source=my-volume,destination=/path/in/container,volume-label="color=red",volume-label="shape=round" \
  nginx:alpine
 

# 创建使用匿名卷的服务
docker service create \
  --name my-service \
  --replicas 3 \
  --mount type=volume,destination=/path/in/container \
  nginx:alpine
#创建使用绑定装载主机目录的服务
docker service create \
  --name my-service \
  --mount type=bind,source=/path/on/host,destination=/path/in/container \
  nginx:alpine
  
  # 创建全局服务
 docker service create \
 --name redis_2 \
 --mode global \
 redis:3.0.6

#将 redis 服务的任务限制为节点，其中 节点类型标签等于队列:如果服务约束排除群集中的所有节点，则打印一条消息 找不到合适的节点，但调度程序将启动对帐 在合适的节点可用后循环并部署服务。
docker service create \
  --name redis_2 \
  --constraint node.platform.os==linux \
  --constraint node.labels.type==queue \
  redis:3.0.6
[root@jenkins ~]# docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
58q6c3sjhzdq        redis_2             replicated          0/1                 redis:3.0.6
# 在下面的示例中，未找到满足约束的节点，导致 服务不与所需状态协调：
docker service create \
  --name web \
  --constraint node.labels.region==east \
  nginx:alpine

[root@jenkins ~]# docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
58q6c3sjhzdq        redis_2             replicated          0/1                 redis:3.0.6
lcmd4wj9ji1u        web                 replicated          0/1                 nginx:alpine
# 将标签添加到群集中的节点后，服务 协调，并部署所需数量的副本
 docker node update --label-add region=east ${node_id}
[root@jenkins ~]# docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
58q6c3sjhzdq        redis_2             replicated          1/1                 redis:3.0.6
lcmd4wj9ji1u        web                 replicated          1/1                 nginx:alpine

# 将服务设置为将任务平均划分到不同类别的 节点。这有用的一个示例是在一组上平衡任务 数据中心或可用性区域。（这个实验没有搞懂）
[root@jenkins ~]# docker service create \
>   --replicas 9 \
>   --name redis_2 \
>   --placement-pref spread=node.labels.datacenter \
>   redis:3.0.6
r5ime8pboh62m9mhat7eaf1gs
overall progress: 0 out of 9 tasks
1/9: pending   [================>                                  ]
2/9: pending   [================>                                  ]
overall progress: 9 out of 9 tasks
1/9: running   [==================================================>]
2/9: running   [==================================================>]
3/9: running   [==================================================>]
4/9: running   [==================================================>]
5/9: running   [==================================================>]
6/9: running   [==================================================>]
7/9: running   [==================================================>]
8/9: running   [==================================================>]
9/9: running   [==================================================>]
verify: Service converged
[root@jenkins ~]#
[root@jenkins ~]#
[root@jenkins ~]#
[root@jenkins ~]# docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
r5ime8pboh62        redis_2             replicated          9/9                 redis:3.0.6
[root@jenkins ~]# docker service ps redis_2
ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE                ERROR               PORTS
p6cun7rbkgt1        redis_2.1           redis:3.0.6         harbor              Running             Running about a minute ago
2nzcqq0kin5z        redis_2.2           redis:3.0.6         gitlab              Running             Running about a minute ago
42vot77jthp1        redis_2.3           redis:3.0.6         gitlab              Running             Running about a minute ago
nbwdd564e69b        redis_2.4           redis:3.0.6         jenkins             Running             Running about a minute ago
32okst02dtvq        redis_2.5           redis:3.0.6         jenkins             Running             Running about a minute ago
j7o3kfnp0n4c        redis_2.6           redis:3.0.6         gitlab              Running             Running about a minute ago
6gmutbxyjwve        redis_2.7           redis:3.0.6         harbor              Running             Running about a minute ago
vzhy36j3vpfz        redis_2.8           redis:3.0.6         jenkins             Running             Running about a minute ago
dz45ygxjhz2n        redis_2.9           redis:3.0.6         harbor              Running             Running about a minute ago
#指定服务的内存要求和约束（--保留内存和 --限制内存）：要求 4GB 内存可用且可保留 在给定节点上计划服务在该节点上运行之前。
 docker service create --reserve-memory=4GB --name=too-big nginx:alpine
# 用于确保任务使用不超过 节点上的给定内存量。此示例限制使用的内存量 由任务到 4GB。即使您的每个节点都有 只有2GB的内存，因为是上限。
docker service create --limit-memory=4GB --name=too-big nginx:alpine

简而言之，您可以采取更保守或更灵活的方法：
    保守：保留 500MB，限制为 500MB。基本上你现在 将服务容器视为虚拟机，您可能会失去一个很大的优势 容器，即每个主机的服务密度更高。
    灵活：假设服务需要，则限制为 500MB 超过500MB，它出现故障。保留100MB之间的内容 “正常”要求和500MB“峰值”要求”。这假设当 此服务处于“峰值”，其他服务或非容器工作负载可能 不会的。
#使用该标志设置可在节点上运行的最大副本任务数。 以下命令创建一个 nginx 服务，其中包含 2 个副本任务，但每个节点只有一个副本任务
docker service create \
  --name nginx \
  --replicas 2 \
  --replicas-max-per-node 1 \
  --placement-pref 'spread=node.labels.datacenter' \
  nginx
# 将服务附加到现有网络（--network）
docker network create --driver overlay my-network
docker service create \
  --replicas 3 \
  --network my-network \
  --name my-web \
  nginx

# 在群外部发布服务端口（-p、--发布）
docker service create --name my_web --replicas 3 --publish 8080:80 nginx
长格式：
docker service create --name my_web --replicas 3 --publish published=8080,target=80 nginx
# 创建请求通用资源的服务（--泛型资源）
docker service create \
    --name cuda \
    --generic-resource "NVIDIA-GPU=2" \
    --generic-resource "SSD=1" \
    nvidia/cuda
    
# 作业是一种特殊服务，旨在运行操作直至完成 然后停止，而不是运行长时间运行的守护程序。当任务时 属于某个作业成功退出（返回值 0），该任务标记为 “已完成”，并且不会再次运行。作业使用两种模式之一启动，或者replicated-jobglobal-job；此命令将运行一个任务，该任务将使用映像执行 命令，它将返回 0 然后退出
docker service create --name myjob \
                        --mode replicated-job \
                        bash "true"

# 复制的作业；复制的作业类似于复制的服务。设置标志 --replicas将指定要执行的作业的迭代总数。；默认情况下，复制作业的所有副本将同时启动。要控制 在任何时间同时执行的副本总数， 该标志可用于：--max-concurrent；上面的命令总共将执行 10 个任务，但其中只有 2 个是 在任何给定时间运行。
docker service create \
    --name mythrottledjob \
    --mode replicated-job \
    --replicas 10 \
    --max-concurrent 2 \
    bash "true"

```



| 名称，速记                                          | 违约         | 描述                                                         |
| --------------------------------------------------- | ------------ | ------------------------------------------------------------ |
| `--cap-add`                                         |              | [**接口 1.41+**](../../api/v1.41.html) 添加 Linux 功能       |
| `--cap-drop`                                        |              | [**接口 1.41+**](../../api/v1.41.html) 删除 Linux 功能       |
| [`--config`](#config)                               |              | [**接口 1.41+**](../../api/v1.41.html) 指定要向服务公开的配置 |
| [`--constraint`](#constraint)                       |              | 放置约束                                                     |
| `--container-label`                                 |              | 容器标签                                                     |
| `--credential-spec`                                 |              | 托管服务帐户的凭据规范（仅限 Windows）                       |
| `--detach`,`-d`                                     |              | 立即退出，而不是等待服务收敛                                 |
| `--dns`                                             |              | 设置自定义 DNS 服务器                                        |
| `--dns-option`                                      |              | 设置 DNS 选项                                                |
| `--dns-search`                                      |              | 设置自定义 DNS 搜索域                                        |
| `--endpoint-mode`                                   | `vip`        | 端点模式（VIP 或 dnsrr）                                     |
| `--entrypoint`                                      |              | 覆盖映像的默认入口点                                         |
| [`--env`](#env),[`-e`](#env)                        |              | 设置环境变量                                                 |
| `--env-file`                                        |              | 读入环境变量文件                                             |
| `--generic-resource`                                |              | 用户定义的资源                                               |
| `--group`                                           |              | 为容器设置一个或多个补充用户组                               |
| `--health-cmd`                                      |              | 要运行以检查运行状况的命令                                   |
| `--health-interval`                                 |              | 运行检查之间的时间 （ms\|s\|m\|h）                           |
| `--health-retries`                                  |              | 报告不正常所需的连续失败                                     |
| `--health-start-period`                             |              | 容器在将重试计数为不稳定之前初始化的开始周期 （ms\|s\|m\|h） |
| `--health-timeout`                                  |              | 允许运行一次检查的最长时间 （ms\|s\|m\|h）                   |
| `--host`                                            |              | 设置一个或多个自定义主机到 IP 映射（主机：ip）               |
| [`--hostname`](#hostname)                           |              | 容器主机名                                                   |
| `--init`                                            |              | 在每个服务容器中使用 init 来转发信号和收获进程               |
| [`--isolation`](#isolation)                         |              | 服务容器隔离模式                                             |
| [`--label`](#label),[`-l`](#label)                  |              | 服务标签                                                     |
| `--limit-cpu`                                       |              | 限制中央处理器                                               |
| `--limit-memory`                                    |              | 限制内存                                                     |
| `--limit-pids`                                      |              | [**接口 1.41+**](../../api/v1.41.html) 限制最大进程数（默认值 0 = 无限制） |
| `--log-driver`                                      |              | 用于服务的日志记录驱动程序                                   |
| `--log-opt`                                         |              | 日志记录驱动程序选项                                         |
| `--max-concurrent`                                  |              | [**接口 1.41+**](../../api/v1.41.html) 要同时运行的作业任务数（默认值等于 --副本） |
| `--mode`                                            | `replicated` | 服务模式 （、、、`replicated``global``replicated-job``global-job`) |
| [`--mount`](#mount)                                 |              | 将文件系统挂载附加到服务                                     |
| `--name`                                            |              | 服务名称                                                     |
| [`--network`](#network)                             |              | 网络附件                                                     |
| `--no-healthcheck`                                  |              | 禁用任何容器指定的运行状况检查                               |
| `--no-resolve-image`                                |              | 不要查询注册表以解析映像摘要和支持的平台                     |
| [`--placement-pref`](#placement-pref)               |              | 添加展示位置偏好设置                                         |
| [`--publish`](#publish),[`-p`](#publish)            |              | 将端口发布为节点端口                                         |
| `--quiet`,`-q`                                      |              | 禁止显示进度输出                                             |
| `--read-only`                                       |              | 将容器的根文件系统挂载为只读                                 |
| [`--replicas`](#replicas)                           |              | 任务数                                                       |
| [`--replicas-max-per-node`](#replicas-max-per-node) |              | [**接口 1.40+**](../../api/v1.40.html) 每个节点的最大任务数（默认值 0 = 无限制） |
| `--reserve-cpu`                                     |              | 预留中央处理器                                               |
| [`--reserve-memory`](#reserve-memory)               |              | 保留内存                                                     |
| `--restart-condition`                               |              | 满足条件时重新启动 （、、 ）（默认值）`none``on-failure``any``any`) |
| `--restart-delay`                                   |              | 重新启动尝试之间的延迟 （ns\|us\|ms\|s\|m\|h） （默认为 5 秒） |
| `--restart-max-attempts`                            |              | 放弃前的最大重新启动次数                                     |
| `--restart-window`                                  |              | 用于评估重新启动策略的窗口 （ns\|us\|ms\|s\|m\|h）           |
| `--rollback-delay`                                  |              | 任务回滚之间的延迟 （ns\|us\|ms\|s\|m\|h） （默认为 0s）     |
| `--rollback-failure-action`                         |              | 回滚失败时的操作 （， ） （默认值）`pause``continue``pause`) |
| `--rollback-max-failure-ratio`                      |              | 回滚期间允许的故障率（默认值 0）                             |
| `--rollback-monitor`                                |              | 每次任务回滚后监视故障的持续时间 （ns\|us\|ms\|s\|m\|h）（默认为 5 秒） |
| `--rollback-order`                                  |              | 回滚顺序 （， ） （默认值）`start-first``stop-first``stop-first`) |
| `--rollback-parallelism`                            | `1`          | 同时回滚的最大任务数（0 表示一次回滚所有任务）               |
| [`--secret`](#secret)                               |              | 指定要向服务公开的机密                                       |
| `--stop-grace-period`                               |              | 强制杀死容器之前等待的时间 （ns\|us\|ms\|s\|m\|h） （默认 10 秒） |
| `--stop-signal`                                     |              | 停止容器的信号                                               |
| `--sysctl`                                          |              | [**接口 1.40+**](../../api/v1.40.html) 系统选项              |
| `--tty`,`-t`                                        |              | [**接口 1.40+**](../../api/v1.40.html) 分配伪 TTY            |
| `--ulimit`                                          |              | [**接口 1.41+**](../../api/v1.41.html) Ulimit选项            |
| [`--update-delay`](#update-delay)                   |              | 更新之间的延迟 （ns\|us\|ms\|s\|m\|h） （默认为 0s）         |
| `--update-failure-action`                           |              | 更新失败时的操作 （， ， ） （默认）`pause``continue``rollback``pause`) |
| `--update-max-failure-ratio`                        |              | 更新期间允许的故障率（默认值 0）                             |
| `--update-monitor`                                  |              | 每次任务更新后监视故障的持续时间（ns\|us\|ms\|s\|m\|h）（默认为 5 秒） |
| `--update-order`                                    |              | 更新顺序 （， ） （默认）`start-first``stop-first``stop-first`) |
| `--update-parallelism`                              | `1`          | 同时更新的最大任务数（0 表示一次更新所有任务）               |
| `--user`,`-u`                                       |              | 用户名或 UID （格式： <name\|uid>[：<group\|gid>]）          |
| [`--with-registry-auth`](#with-registry-auth)       |              | 将注册表身份验证详细信息发送到群代理                         |
| `--workdir`,`-w`                                    |              | 容器内的工作目录                                             |
| `--help`                                            |              | 打印使用情况                                                 |



#### stack

管理群堆栈

```shell
#以下命令输出两个撰写文件的合并和插值的结果。
docker stack config --compose-file docker-compose.yml --compose-file docker-compose.prod.yml
# 撰写文件也可以作为标准输入提供：--compose-file -
cat docker-compose.yml | docker stack config --compose-file -
#  撰写文件（--撰写文件）
docker stack deploy --compose-file docker-compose.yml vossibility
docker service ls
# 显示所有堆栈和一些附加信息：
 docker stack ls
 # 删除堆栈
 docker stack rm myapp
```

### 使用

```shell
# leader节点进行初始化
[root@jenkins ~]# docker swarm init
Swarm initialized: current node (jdt2yhcp05w3an27l5rvp98o2) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-2o1a43u664vxzc7e4xq5yoap6t8jyzeavej89uk0d2ea551ce8-bepwlopaax2midyfptenffx6o 10.0.0.11:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

# work节点进行加入
[root@harbor ~]# docker swarm join --token SWMTKN-1-2o1a43u664vxzc7e4xq5yoap6t8jyzeavej89uk0d2ea551ce8-bepwlopaax2midyfptenffx6o 10.0.0.11:2377
This node joined a swarm as a worker.

# leader节点查看node个数
docker node ls

# 新建服务
docker service create --replicas 3 -p 80:80 --name nginx nginx:1.13.7-alpine
# 查看服务
docker service ls
# 使用 docker service ps 来查看某个服务的详情。
docker service ps nginx
# 使用 docker service logs 来查看某个服务的日志。
docker service logs nginx
# 当业务处于高峰期时，我们需要扩展服务运行的容器数量。
docker service scale nginx=5
# 当业务平稳时，我们需要减少服务运行的容器数量。
docker service scale nginx=2
# 删除服务
docker service rm nginx
# 正如之前使用 docker-compose.yml 来一次配置、启动多个容器，在 Swarm 集群中也可以使用 compose 文件 （docker-compose.yml） 来配置、启动多个服务。

[root@jenkins ~]# cat docker-compose.yaml
version: "3"

services:
  wordpress:
    image: wordpress
    ports:
      - 80:80
    networks:
      - overlay
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
    deploy:
      mode: replicated
      replicas: 3

  db:
    image: mysql
    networks:
       - overlay
    volumes:
      - db-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress
    deploy:
      placement:
        constraints: [node.role == manager]

  visualizer:
    image: dockersamples/visualizer:stable
    ports:
      - "8080:8080"
    stop_grace_period: 1m30s
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    deploy:
      placement:
        constraints: [node.role == manager]

volumes:
  db-data:
networks:
  overlay:

# 部署服务
docker stack deploy -c docker-compose.yml wordpress
# 查看服务
[root@localhost ~]# docker stack ls
NAME                SERVICES            ORCHESTRATOR
wordpress           3                   Swarm

# 移除服务
docker stack down
docker volume ls
docker volume rm wordpress_db-data

# 在 Swarm 集群中管理敏感数据
在动态的、大规模的分布式集群上，管理和分发 密码、证书 等敏感信息是极其重要的工作。传统的密钥分发方式（如密钥放入镜像中，设置环境变量，volume 动态挂载等）都存在着潜在的巨大的安全风险。
Docker 目前已经提供了 secrets 管理功能，用户可以在 Swarm 集群中安全地管理密码、密钥证书等敏感数据，并允许在多个 Docker 容器实例之间共享访问指定的敏感数据。
# 创建 secret
[root@localhost ~]# openssl rand -base64 20 | docker secret create mysql_password -
nb79f3fwxo1a422vzxxmx8vl2
[root@localhost ~]# openssl rand -base64 20 | docker secret create mysql_root_password -
icfht7m6o7fhkb5nazw5my9uv


#  查看 secret
[root@localhost ~]# docker secret ls
ID                          NAME                  DRIVER              CREATED             UPDATED
nb79f3fwxo1a422vzxxmx8vl2   mysql_password                            14 seconds ago      14 seconds ago
icfht7m6o7fhkb5nazw5my9uv   mysql_root_password                       11 seconds ago      11 seconds ago
# 创建 MySQL 服务
$ docker network create -d overlay mysql_private

$ docker service create \
     --name mysql \
     --replicas 1 \
     --network mysql_private \
     --mount type=volume,source=mydata,destination=/var/lib/mysql \
     --secret source=mysql_root_password,target=mysql_root_password \
     --secret source=mysql_password,target=mysql_password \
     -e MYSQL_ROOT_PASSWORD_FILE="/run/secrets/mysql_root_password" \
     -e MYSQL_PASSWORD_FILE="/run/secrets/mysql_password" \
     -e MYSQL_USER="wordpress" \
     -e MYSQL_DATABASE="wordpress" \
     mysql:latest
# 创建wordpress
$ docker service create \
     --name wordpress \
     --replicas 1 \
     --network mysql_private \
     -p 30000:80 \
     --mount type=volume,source=wpdata,destination=/var/www/html \
     --secret source=mysql_password,target=wp_db_password,mode=0400 \
     -e WORDPRESS_DB_USER="wordpress" \
     -e WORDPRESS_DB_PASSWORD_FILE="/run/secrets/wp_db_password" \
     -e WORDPRESS_DB_HOST="mysql:3306" \
     -e WORDPRESS_DB_NAME="wordpress" \
     wordpress:latest

# 查看服务
docker service ls


# 在 Swarm 集群中管理配置数据
vim redis.conf
port 6380
docker config create redis.conf redis.conf
# 查看 config
docker config ls

# 创建 redis 服务
docker service create \
     --name redis \
     --config source=redis.conf,target=/etc/redis.conf \
     -p 6379:6380 \
     redis:latest \
     redis-server /redis.conf
     
以前我们通过监听主机目录来配置 Redis，就需要在集群的每个节点放置该文件，如果采用 docker config 来管理服务的配置信息，我们只需在集群中的管理节点创建 config，当部署服务时，集群会自动的将配置文件分发到运行服务的各个节点中，大大降低了配置信息的管理和分发难度。

# SWarm mode 与滚动升级
docker service update \
    --image nginx:1.13.12-alpine \
    nginx
# 服务回退
 docker service rollback nginx
 # 查看服务详情。
 docker service ps nginx
 
 $ docker service ps nginx

ID                  NAME                IMAGE                  NODE                DESIRED STATE       CURRENT STATE                ERROR               PORTS
rt677gop9d4x        nginx.1             nginx:1.13.7-alpine   VM-20-83-debian     Running             Running about a minute ago
d9pw13v59d00         \_ nginx.1         nginx:1.13.12-alpine  VM-20-83-debian     Shutdown            Shutdown 2 minutes ago
i7ynkbg6ybq5         \_ nginx.1         nginx:1.13.7-alpine   VM-20-83-debian     Shutdown 

结果的输出详细记录了服务的部署、滚动升级、回退的过程。
```



问题

```text
swarm集群可以有几个leader节点？ 如果leader节点挂了怎么办？

swarm中删除节点怎么删除？

swarm中的网络模式有哪几种？如何做负载均衡？

swarm中指定某个service只能部署在node01节点上，怎么做？

swarm中滚动发布策略如何设置？发布失败如何回滚？
```

















## Mesos




# 综合案例

使用docker更新

```text
git ---> gitlab ---> jenkins ---> dockerfile ---> harbor ---> deploy
```



# 面试题

## docker０网桥的实现原理

```text

```



## **docker load vs docker import**

```text
Docker load 和 docker import 命令都是用来将 Docker 镜像或容器存储的数据加载或导入到本地 Docker 镜像库中。两者的区别在于：

容器快照：容器快照将会丢弃所有的历史记录和元数据信息，而镜像存储文件将保存完整记录，体积也会更大。从容器快照文件导入时，需要重新指定标签等元数据。
镜像存储文件：镜像存储文件将保存完整记录，体积也会更大。但是从镜像存储文件导入时，可以重新指定标签等元数据。
总之，Docker load 和 docker import 命令都可以用来将 Docker 镜像或容器存储的数据加载或导入到本地 Docker 镜像库中，但是它们的区别在于容器快照和镜像存储文件的处理方式。
```



## docker save vs docker export

```text
Docker save和Docker export的区别主要在于目的和保存的数据类型不同：

1.Docker save：保存的是镜像(image)，docker export保存的是容器(container)。
2. Docker load：用来载入镜像包，docker import用来载入容器包，但两者都会恢复为镜像。

此外，Docker save还可以对镜像进行压缩，而Docker export则不会。
```



## docker 镜像拉取策略

| 值                | 描述                                                         |
| :---------------- | :----------------------------------------------------------- |
| `missing`（默认） | 如果在映像缓存中找不到映像，请拉取映像，否则请使用缓存的映像。 |
| `never`           | 不要拉取映像，即使它丢失，如果映像缓存中不存在映像，则会产生错误。 |
| `always`          | 始终在创建容器之前执行拉取。                                 |



## docker重启策略

| 政策                       | 结果                                                         |
| :------------------------- | :----------------------------------------------------------- |
| `no`                       | 不要在容器退出时自动重新启动容器。这是默认值。               |
| `on-failure[:max-retries]` | 仅当容器以非零退出状态退出时，才重新启动。（可选）限制 Docker 守护程序尝试的重新启动重试次数。 |
| `unless-stopped`           | 重新启动容器，除非它被显式停止或 Docker 本身被停止或重新启动。 |
| `always`                   | 无论退出状态如何，始终重新启动容器。如果指定始终，Docker 守护程序将无限期地尝试重新启动容器。无论容器的当前状态如何，容器也将始终在守护程序启动时启动。 |



# 补充
将docker容器的配置导出为docker-compose.yml
https://www.cnblogs.com/wang2650/p/14713310.html



# 问题

## docker安装后其他机器无法访问端口

参考文档：

[(3条消息) docker安装后导致的网络问题_docker安装后网络_归去来 兮的博客-CSDN博客](https://blog.csdn.net/m0_46897923/article/details/121551965)

[主机访问docker映射的端口报错curl(56)解决 - binggogo - 博客园 (cnblogs.com)](https://www.cnblogs.com/bingogo/p/13031935.html)

描述：

1、容器可以ping 百度

```shell
[root@localhost ~]# docker exec -it busybox  sh
/ # ping baidu.com
PING baidu.com (39.156.66.10): 56 data bytes
64 bytes from 39.156.66.10: seq=0 ttl=127 time=35.578 ms
^C




# 离谱
[root@test ~]# getenforce
Disabled
[root@test ~]# systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; disabled; vendor preset: enabled)
   Active: inactive (dead)
[root@test ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
642f505fa1f9        nginx               "/docker-entrypoin..."   About an hour ago   Up 20 minutes       0.0.0.0:80->80/tcp   affectionate_ramanujan
[root@test ~]# netstat -ntpl
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      1181/sshd
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      1662/master
tcp6       0      0 :::3306                 :::*                    LISTEN      1213/mysqld
tcp6       0      0 :::80                   :::*                    LISTEN      2899/docker-proxy-c
tcp6       0      0 :::22                   :::*                    LISTEN      1181/sshd
tcp6       0      0 ::1:25                  :::*                    LISTEN      1662/master
[root@test ~]# curl 127.0.0.1
curl: (56) Recv failure: Connection reset by peer
[root@test ~]#



可能是这个原因导致的：https://programminghunter.com/article/52592590093/

有关文档：https://www.cnblogs.com/yinliang/p/13159083.html

```