## 什么是SonarQube
SonarQube是一种自我管理的自动代码审查工具，可系统地帮助您交付干净的代码。作为我们 Sonar 解决方案的核心元素，SonarQube 集成到您现有的工作流程中并检测代码中的问题，以帮助您对项目执行持续的代码检查。该工具分析 30 多种不同的编程语言 ，并集成到您的 CI 管道 和 DevOps 平台 中，以确保您的代码符合高质量标准。
**SonarQube的作用**
```text
1、以平台方式管理代码质量和安全扫描的数据
2、多维度分析代码：代码量、安全隐患、编写规范隐患、重复度、代码增量、测试覆盖率等
3、支持30多种编程语言的代码扫描和分析
4、涵盖了编程语言的静态扫描规则：代码编写规范+安全规范
5、能够与代码编辑器、CI/CD平台完美集成
6、能够与SCM集成，可以直接在平台上看到代码问题是由哪位开发人员提交。
7、帮助开发人员写出更干净、更安全的代码。
```

**SonarQube如何工作**
```text
SonarQube静态代码扫描由2部分组成：SonarQube平台、sonar-scanner扫描器

SonarQube：web界面管理平台
1、展示所有项目的代码质量数据
2、配置质量规则、管理项目、配置通知、配置SCM等

SonarScanner：代码扫描工具
专门用来扫描和分析代码
代码扫描和分析完成后，会将扫描结果存放到数据库中，在SonarQube平台可以看到扫描数据
```
![img_1.png](img_1.png)
## 安装
[先决条件&硬件要求](https://docs.sonarqube.org/latest/requirements/prerequisites-and-overview/)

```shell
DockerCompose  -f DockerCompose .yaml  up -d
```
![img.png](img.png)
## 集群安装

## 基本使用
**默认账密**
```shell
login: admin
password: admin
```
**安装插件**


**启动扫描**
```shell
DockerCompose run \
    --rm \
    -e SONAR_HOST_URL="http://10.0.0.30:9000" \
    -e SONAR_SCANNER_OPTS="-Dsonar.projectKey=flaskblog" \
    -e SONAR_LOGIN="aca6cbe0ad0994983ac48a42285ad21cf07dea90" \
    -v "$PWD:/usr/src" \
    sonarsource/sonar-scanner-cli
```

**分析扫描结果**


**SonarScanner日志分析**




## 进阶使用

**定制规则**

**只扫描某种语言的代码**

**过滤某类代码/某个目录**

## SonarQube api
（只提醒，不block流程）

**企业微信告警**

**邮件告警**


## Jenkins关联 SonarQube

## Gitlab-ci 关联SonarQube

## SonarQube关联Jira

## 利用SonarQube生成周报

## SonarQube性能提升

## SonarQube监控

## SonarQube日志分析

## SonarQube数据清理

## SonarQube升级

## SonarQube迁移

## 参考
[官方文档](https://docs.sonarqube.org/latest/)
