官网：https://tekton.dev/docs/

```shell
Tekton 是用于构建 CI/CD 系统的云原生解决方案。它包括 Tekton Pipelines，提供构建块和支持 组件，例如 Tekton CLI 和 Tekton Catalog，使 Tekton 成为一个完整的 生态系统。
```
