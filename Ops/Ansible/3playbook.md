## Ansible playbook

[官网地址](https://galaxy.ansible.com/)playbook是由一个或多个paly组成的列表paly的主要功能在于将事先归并为一组的主机装扮成事先通过ansible中的task定义好的角色。从根本上来讲，所谓task无法是调用ansible的一个module。将多个paly组成在一个playbook中，即可以让它们关联同起来按事先编排的机制，进行有序执行。

playbook采用yaml语言编写

### playbook核心元素

[详细参数请查看官方文档](https://docs.ansible.com/ansible/latest/reference_appendices/playbooks_keywords.html#role)

```text
hosts 执行的远程主机列表 
tasks 任务集
varniables 内置变量或自定义变量在playbook中调用
templates 模板文件，可替换模板文件中的变量并实现一些简单逻辑的文件
handlers和notify结合使用，由特定条件触发的操作，满足条件方才执行，否则不执行
tags 标签 指定某条任务执行，用于选择运行playbook中的部分代码。ansible具有幂等性，因此会自动跳过没有变化的部分，即便如此，有些代码为测试其确实没有发生变化的时间依然会非常长。此时，如果确信其没有变化，就可以通过tags跳过这些代码片段
ansible-playbook -t tagsname useradd.yaml
```

### hosts

```text
playbook中的每一个play的目的都是为了让某个或某些主机以某个指定的用户身份执行任务。hosts用于指定执行任务的主机，需要事先定义在主机清单中
```

### remote_user

```text
remote_user:可用于Host和task中。也可以通过指定其sudo的方式在远程主机上执行任务，其可用于play全局或某个任务；此外，甚至可以在sudo时指定sudo_user指定sudo时切换的用户
```

### task列表和action

```text
play的主机部分是task list。 task list中的各任务按次序逐个在hosts中指定的所有主机上执行，即在所有上完成第一个任务后再开始第二个。在自上而下运行某个playbook时，如果中途发生错误，所有已执行的任务都将回滚，因此，在更正playbook后重新执行一次即可。

task的目的是使用指定的参数执行模块，而在模块参数中可以使用变量。模块执行是幂等的，这意味着多次执行是安全的，因为其结果均一致。

每个task都应该是有其name，用于playbook的执行结果输出，建议其内容尽可能清晰的描述任务执行步骤。如果未提供name，则action的结果将用于输出。

task中shell和command模块后面跟命令，而非key value

某任务的状态在运行后为change时，可通过notify通知给相应的handlers

任务可以通过tags打标签，而后可在ansible-playbook命令上使用-t指定进行调用

如果命令或脚本退出状态码不为零，可以使用ignore_errors来忽略错误信息
```

### handlers和notify结合使用触发条件

```text
handlers：
    是task列表，这些task与前述的task并没有本质上的不同，用于当关注的资源发生变化时，才会采用一定的操作
notify：
    此action可用于在每个play的最后触发，这样可避免多次有改变时每次都执行指定的操作，仅在所有的变化发生完成后一次性地执行指定操作。在notify中列出的操作被称为handler，也就是notify中调用handler中定义的操作。
```

### tags

```yaml
# auth:
# dec: notify && handlers
---
  - hosts: 10.0.0.51
    remote_user: root
    tasks:
      - name: install httpd packge
        yum: name=httpd
        tags: installhttpd
      - name: copy config
        copy: src=files/httpd.conf dest=/etc/httpd/conf/ backup=yes
        notify: restart service
      - name: copy index.html
        copy: src=files/index.html dest=/var/www/html/ backup=yes
      - name: start service
        service: name=httpd state=started enabled=yes
        tags: starthttpd
    handlers:
      - name: restart service
        service: name=httpd state=restarted
```

### playbook中的变量

```text
变量名： 仅能字母 数字 下划线组成，只能以字母开头
变量的来源：
    1、ansible setup facts远程主机的所有变量都可直接调用
    2、在/etc/ansible/hosts中定义
    菩普通变量：主机组中主机单独定义，优先级高于公共变量
    公共(组)变量：针对主机组中所有主机定义统一变量
    3、通过命令行指定变量，优先级最高
    4、在playbook中定义：
        vars:
          - var1: value1
          - var2: value2
    5、在role中定义
```

### when

```text
条件测试：如果需要根据变量、facts或此前任务的执行结果来做为某task执行与否的前提时要用到条件测试，通过when语句实现，在task中使用，jinja2的语法格式

when语句

在task后添加when子句，即可使用条件测试；when语句支持jinja2表达式语法
```

### 迭代 with_items

```text
迭代： 当有需要重复行执行的任务时，可以使用迭代机制
    对迭代项的引用，固定变量为 item
    要在task中使用with_items给定要迭代的元素列表
    列表格式：
        字符串
        字典
```

