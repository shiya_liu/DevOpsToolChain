## Ansible roles

```text
ansible自1.2版本引入的新特性，用于层次性、结构化地组织playbook。roles能够根据层次型结构自动装载变量文件、tasks以及handlers等。要使用roles只需要在playbook中使用include指令即可。简单来讲，roles就是通过分别将变量、文件、任务、模板以及处理器放置于单独的目录中，并可以便捷地include它们的一种机制。角色一般用于基于主机构建服务的场景种，但也可是用于构建守护进程等场景中

复杂场景：建议使用roles，代码复用度高
    变更指定主机或主机组
    如果命名不规范维护成本会增大
    某些功能需要多个playbook，通过include即可实现。 
```

### roles目录结构

```text
projectname.yaml
roles/
  projectname/
    tasks/
    files/
    vars/
    default/
    templates/
    handlers/
    meta/
```

### roles各个目录作用

```text
roles/project/: 项目名称，有以下子目录
  files/:存放由copy或script模块等调用的文件
  templates/: template模块查找所需模板文件的目录
  tasks/:定义task,role的基本元素，至少应该包含一个名为main.yaml的文件；其他的文件需要在此文件中通过include进行包含
  handlers/: 至少应该包含一个名为main.yaml的文件；其他的文件需要在此文件中通过include进行包含
  vars/:定义变量，至少应该包含一个名为main.yaml的文件，其他的文件需要在此文件中通过includ进行包含
  meta/:定义当前角色的特殊设定以及依赖关系，至少应该包含一个名为main.yaml的文件
  default/: 设置默认变量时使用此目录中的main.yaml文件
```

## Ansible默认变量

| Parameter          | Description                                                  |
| ------------------ | ------------------------------------------------------------ |
| hostvars           | A dictionary whose keys are Ansible host names and values are dictionaries that map variable names to values |
| inventory_hostname | Name of the current host as known by Ansible                 |
| group_names        | A list of all groups that the current host is a member of    |
| groups             | A dictionary whose keys are Ansible group names and values are a list of hostnames that are members of the group. Includes all and ungrouped groups: {"all": […], "web": […], "ungrouped": […]} |
| play_hosts         | A list of inventory hostnames that are active in the current play |
| ansible_version    | A dictionary with Ansible version info: {'string': '2.9.13', 'full': '2.9.13', 'major': 2, 'minor': 9, 'revision': 13} |

# Ansible问题记录

## Invalid/incorrect password: Permission denied, please try again.

```text
ansible_ssh_pass 设置是为0开头的密码 就会报错：Invalid/incorrect password: Permission denied, please try again.
改为其他密码，比如123456则可以实现。
```



# Ansible常见面试题



# 综合案例

Ansible二进制部署kubernetes

github地址：https://github.com/LiuShiYa-github/ansible-deploy-kubernetes



# 新知识点补充

ansible serial关键字解释：
https://blog.csdn.net/ikkyphoenix/article/details/125502757


ansible roles编写：https://www.cnblogs.com/yanjieli/p/10971862.html



遇到问题：
在使用copy模块拷贝jvm文件时，直接拷贝解压后的文件很慢，370M传了将近10min。
使用copy模块直接拷贝压缩包，很快。
原理：https://www.raysync.cn/news/post-id-454


补充ansible界面：awx: https://github.com/ansible/awx

python脚本中使用ansible
https://ansible.leops.cn/dev/api/api-playbook/



