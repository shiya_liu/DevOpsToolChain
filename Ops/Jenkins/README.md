



[TOC]

# 简介

官网：[Jenkins](https://www.jenkins.io/)

Jenkins 是一款流行的开源持续集成（Continuous Integration）工具，广泛用于项目开发，具有自动 化构建、测试和部署等功能。

## 特征

- 开源的Java语言开发持续集成工具，支持持续集成，持续部署。

- 易于安装部署配置：可通过yum安装,或下载war包以及通过docker容器等快速实现安装部署，可方便web界面配置管理。

- 消息通知及测试报告：集成RSS/E-mail通过RSS发布构建结果或当构建完成时通过e-mail通知，生成JUnit/TestNG测试报告。

- 分布式构建：支持Jenkins能够让多台计算机一起构建/测试。

- 文件识别：Jenkins能够跟踪哪次构建生成哪些jar，哪次构建使用哪个版本的jar等。

- 丰富的插件支持：支持扩展插件，你可以开发适合自己团队使用的工具，如git，svn，maven，
  docker等。

# 如何使用

## Gitlab安装

```shell
#安装相关依赖
yum -y install policycoreutils openssh-server openssh-clients postfix
#启动ssh服务&设置为开机启动
systemctl enable sshd && sudo systemctl start sshd
#设置postfix开机自启，并启动，postfix支持gitlab发信功能
systemctl enable postfix && systemctl start postfix
#如果关闭防火墙就不需要做以上配置
systemctl stop firewalld && systemctl disabled firewalld
#下载gitlab包，并且安装
wget https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7/gitlab-ce-15.9.4-ce.0.el7.x86_64.rpm  --no-check-certificate
rpm -i gitlab-ce-12.4.2-ce.0.el6.x86_64.rpm
#修改gitlab配置
vi /etc/gitlab/gitlab.rb
external_url 'http://192.168.66.100:80'
#重载配置及启动gitlab
gitlab-ctl reconfigure
gitlab-ctl restart
```

或者使用docker-compose安装

```yaml
version: '3'
services:
  gitlab:
    image: 'gitlab/gitlab-ce:latest'
    restart: always
    hostname: '10.0.0.17'
    container_name: gitlab
    environment:
      TZ: 'Asia/Shanghai'
    ports:
      - '80:80'
      - '8443:443'
      - '222:22'
    volumes:
      - ./config:/etc/gitlab
      - ./data:/var/opt/gitlab
      - ./logs:/var/log/gitlab
```

默认用户名：root

密码：cat /etc/gitlab/initial_root_password

修改密码的步骤：

```shell
docker exec -it gitlab bash
gitlab-rails console
user = User.where(id: 1).first
user.password = 'NUma@numa1'
user.password_confirmation = 'NUma@numa1'
user.save!
```

官方修改密码步骤说明：[重置用户密码 |吉特实验室 (gitlab.com)](https://docs.gitlab.com/ee/security/reset_user_password.html#reset-your-root-password.)





**Gitlab添加组、创建用户、创建项目**

1）创建组 使用管理员 root 创建组，一个组里面可以有多个项目分支，可以将开发添加到组里面进行设置权限， 不同的组就是公司不同的开发项目或者服务模块，不同的组添加不同的开发即可实现对开发设置权限的 管理

![image-20230404100529532](assets/image-20230404100529532.png)

2）创建用户 创建用户的时候，可以选择Regular或Admin类型。

![image-20230404100644192](assets/image-20230404100644192.png)

![image-20230404101113499](assets/image-20230404101113499.png)

3）将用户添加到组中 选择某个用户组，进行Members管理组的成员

![image-20230404100911902](assets/image-20230404100911902.png)

Gitlab用户在组里面有5种不同权限：

```text
Guest：可以创建issue、发表评论，不能读写版本库 
Reporter：可以克隆代码，不能提交，QA、PM可以赋予这个权限 
Developer：可以克隆代码、开发、提交、push，普通开发可以赋予这个权限
Maintainer：可以创建项目、添加tag、保护分支、添加项目成员、编辑项目，核心开发可以赋予这个权限 
Owner：可以设置项目访问权限 - Visibility Level、删除项目、迁移项目、管理组成员，开发组组长可以赋予这个权限
```

4）在用户组中创建项

以刚才创建的新用户身份登录到Gitlab，然后在用户组中创建新的项目

![image-20230404101709496](assets/image-20230404101709496.png)

5)添加用户密钥
```shell
# 生成密钥对
ssh-keygen -t rsa -C "admin@example.com"
vim .ssh/config
Port 222
```



**源码上传到Gitlab仓库**

```shell
yum install -y git
git clone
git add 
git commit
git push
```





## Jenkins安装

```yaml
jenkins:
  image: jenkins/jenkins:lts
  volumes:
      - /usr/share/zoneinfo/Asia/Shanghai:/etc/localtime
      - ./data/jenkins/:/var/jenkins_home
      - /var/run/docker.sock:/var/run/docker.sock
      - /usr/bin/docker:/usr/bin/docker
  ports:
      - "8080:8080"
  expose:
      - "8080"
      - "50000"
  environment:
      - SET_CONTAINER_TIMEZONE=true
      - CONTAINER_TIMEZONE=Asia/Shanghai
  privileged: true
  user: root
  restart: always
  container_name: jenkins
  environment:
      JAVA_OPTS: '-Djava.util.logging.config.file=/var/jenkins_home/log.properties'

```



## 插件管理

Jenkins本身不提供很多功能，我们可以通过使用插件来满足我们的使用。例如从Gitlab拉取代码，使用 Maven构建项目等功能需要依靠插件完成。接下来演示如何下载插件。

**修改Jenkins插件下载地址**

```shell
cd /var/lib/jenkins/updates
sed -i 's/http:\/\/updates.jenkinsci.org\/download/https:\/\/mirrors.tuna.tsinghua.edu.cn\/jenkins/g' default.json && sed -i 's/http:\/\/www.google.com/https:\/\/www.baidu.com/g' default.json
```

最后，Manage Plugins点击Advanced，把Update Site改为国内插件下载地址 

```text
https://mirrors.tuna.tsinghua.edu.cn/jenkins/updates/update-center.json
```

![image-20230404094407641](assets/image-20230404094407641.png)





## 用户权限管理

我们可以利用Role-based Authorization Strategy 插件来管理Jenkins用户权限

**安装Role-based Authorization Strategy插件**

![image-20230404134601847](assets/image-20230404134601847.png)

![image-20230404134503194](assets/image-20230404134503194.png)

![image-20230404134523772](assets/image-20230404134523772.png)



![image-20230404134628830](assets/image-20230404134628830.png)

**创建角色**

![image-20230404134825923](assets/image-20230404134825923.png)



![image-20230404134950122](assets/image-20230404134950122.png)



![image-20230404141017226](assets/image-20230404141017226.png)

```text
Global roles（全局角色）：管理员等高级用户可以创建基于全局的角色 
Item roles（项目角色）：针对某个或者某些项目的角色 Slave roles（奴隶角色）：节点相关的权限

我们添加以下三个角色：
baseRole：该角色为全局角色。这个角色需要绑定Overall下面的Read权限，是为了给所有用户绑定最基本的Jenkins访问权限。注意：如果不给后续用户绑定这个角色，会报错误：用户名 is missing the Overall/Read permission
role1：该角色为项目角色。使用正则表达式绑定"Pipeline.*"，意思是只能操作Pipeline开头的项目。
role2：该角色也为项目角色。绑定"te.*"，意思是只能操作test开头的项目。


```



**创建用户**

![image-20230404135536023](assets/image-20230404135536023.png)



**给用户分配角色**

系统管理页面进入Manage and Assign Roles，点击Assign Roles

![image-20230404135807020](assets/image-20230404135807020.png)

**创建项目测试**

admin视角：

![image-20230404135936654](assets/image-20230404135936654.png)

zhangsan视角:

![image-20230404140754848](assets/image-20230404140754848.png)

lisi视角：

![image-20230404140814587](assets/image-20230404140814587.png)

## 凭证管理

凭据可以用来存储需要密文保护的数据库密码、Gitlab密码信息、Docker私有仓库密码等，以便 Jenkins可以和这些第三方的应用进行交互。

**安装Credentials Binding插件**

要在Jenkins使用凭证管理功能，需要安装Credentials Binding插件

![image-20230404141152146](assets/image-20230404141152146.png)

安装插件后，左边多了"凭证"菜单，在这里管理所有凭证

![image-20230404141226656](assets/image-20230404141226656.png)

可添加的凭证有5种：

```text
Username with password：用户名和密码

SSH Username with private key： 使用SSH用户和密钥

Secret file：需要保密的文本文件，使用时Jenkins会将文件复制到一个临时目录中，再将文件路径设置到一个变量中，等构建结束后，所复制的Secret file就会被删除。

Secret text：需要保存的一个加密的文本串，如钉钉机器人或Github的api token

Certificate：通过上传证书文件的方式
```

常用的凭证类型有：Username with password（用户密码）和SSH Username with private key（SSH 密钥）



![image-20230404141339954](assets/image-20230404141339954.png)



**安装Git插件和Git工具**

为了让Jenkins支持从Gitlab拉取源码，需要安装Git插件以及在CentOS7上安装Git工具

![image-20230404141509794](assets/image-20230404141509794.png)

**用户密码类型**

1）创建凭证 

Jenkins->凭证->系统->全局凭证->添加凭证

![image-20230404141905982](assets/image-20230404141905982.png)

![image-20230404142505961](assets/image-20230404142505961.png)

**测试**

![image-20230404142549604](assets/image-20230404142549604.png)

![image-20230404142646135](assets/image-20230404142646135.png)

**Maven安装和配置**

未找到合适的Java项目，找到后进行补充该内容

```shell
yum -y install make zlib zlib-devel gcc-c++ libtool openssl openssl-devel pcre pcre-devel bridge-utils vim net-tools telnet tree

systemctl stop firewalld
systemctl disable firewalld

setenforce 0

vim /etc/sysctl.conf
net.bridge.bridge-nf-call-ip6tables=1
net.bridge.bridge-nf-call-iptables=1
net.bridge.bridge-nf-call-arptables=1
net.ipv4.ip_forward=1

```



## Pipeline

Jenkins中自动构建项目的类型有很多，常用的有以下三种：

```text
自由风格软件项目（FreeStyle Project）
Maven项目（Maven Project）
流水线项目（Pipeline Project）
```

每种类型的构建其实都可以完成一样的构建过程与结果，只是在操作方式、灵活度等方面有所区别，在 实际开发中可以根据自己的需求和习惯来选择。（PS：个人推荐使用流水线类型，因为灵活度非常高）



我们使用nginx+网页html的方式作为demo来学习Pipeline



**nginx Dockerfile**

```shell
FROM nginx:1.23.4
COPY index.html /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```

**index.html**

```html
<p>你好</p>
```

**/etc/docker/daemon.json**

```shell
{
    "data-root": "/home/docker_data",
    "log-driver": "json-file",
    "log-opts": {"max-size": "500m", "max-file": "3"},
    "insecure-registries": ["10.0.0.13"],
    "registry-mirrors": ["https://yo3sdl2l.mirror.aliyuncs.com","https://registry.docker-cn.com","http://hub-mirror.c.163.com","https://docker.mirrors.ustc.edu.cn"]
}
```





### 自由风格项目构建

```shell
tag=$(date +%Y%m%d%H%M%S)
docker login 10.0.0.13 -u admin  --password Harbor12345
docker build -t 10.0.0.13/nginx_images/nginx:$tag .
docker build -t 10.0.0.13/nginx_images/nginx:latest .
docker push 10.0.0.13/nginx_images/nginx:$tag 
docker push 10.0.0.13/nginx_images/nginx:latest 
ssh root@10.0.0.16 "docker-compose down  && docker image rm  10.0.0.13/nginx_images/nginx:latest && docker-compose up -d"
```

**nginx docker-compose.yaml**

```yaml
version: '3'
services:
  nginx:
    container_name: nginx
    image: 10.0.0.13/nginx_images/nginx:latest
    restart: always
    ports:
      - 80:80
    privileged: true
    volumes:
      - /etc/localtime:/etc/localtime:ro
```



**声明式Pipeline**

```groovy
pipeline {
    agent any

    stages {
        stage('CloneCode') {
            steps {
                git branch: 'main', credentialsId: '14ccccbf-edb0-47a2-8830-3c6b7d035617', url: 'http://10.0.0.12/devops/cicd.git'
            }
        }
        stage('BuildImage') {
            steps {
                sh '''tag=$(date +%Y%m%d%H%M%S)
                    docker build -t 10.0.0.13/nginx_images/nginx:$tag .
                    docker build -t 10.0.0.13/nginx_images/nginx:latest .
                    docker push 10.0.0.13/nginx_images/nginx:$tag 
                    docker push 10.0.0.13/nginx_images/nginx:latest '''
            }
        }
        stage('UpdateConfig') {
            steps {
                sh 'ssh root@10.0.0.16 "docker-compose down  && docker image rm  10.0.0.13/nginx_images/nginx:latest && docker-compose up -d"'
            }
        }
    }
}

```



使用Jenkinsfile将pipeline存放到gitlab中

![image-20230406112214801](assets/image-20230406112214801.png)

![image-20230406112154042](assets/image-20230406112154042.png)

![image-20230406112228107](assets/image-20230406112228107.png)





## 触发器

远程构建

![image-20230406112702508](assets/image-20230406112702508.png)

```text

http://10.0.0.11:8080/job/cicd-pipeline/build?token=7788521
```

其他工程构建后触发

![image-20230406112939640](assets/image-20230406112939640.png)

**轮询SCM** 

轮询SCM，是指定时扫描本地代码仓库的代码是否有变更，如果代码有变更就触发项目构建。

![image-20230406113932108](assets/image-20230406113932108.png)



**git hook触发构建**

![image-20230406164613491](assets/image-20230406164613491.png)

管理员用户层面点击设置

![image-20230406165353209](assets/image-20230406165353209.png)

在仓库中点击设置

![image-20230406165423117](assets/image-20230406165423117.png)

此时点击测试出现如图提示

![image-20230406165535750](assets/image-20230406165535750.png)

此时需要在Jenkins中进行设置

![image-20230406165823295](assets/image-20230406165823295.png)





![image-20230406165854379](assets/image-20230406165854379.png)

重新触发测试

![image-20230406165809442](assets/image-20230406165809442.png)

![image-20230406165953407](assets/image-20230406165953407.png)





**参数化构建**

注意参数化构建时需要在Jenkinsfile中需要双引号"{变量名}"的方式引入

![image-20230406170628922](assets/image-20230406170628922.png)

然后修改pipeline，将branch改为参数引入

```groovy
pipeline {
    agent any

    stages {
        stage('CloneCode') {
            steps {
                git branch: '${branch}', credentialsId: '14ccccbf-edb0-47a2-8830-3c6b7d035617', url: 'http://10.0.0.12/devops/cicd.git'
            }
        }
        stage('BuildImage') {
            steps {
                sh '''tag=$(date +%Y%m%d%H%M%S)
                    docker build -t 10.0.0.13/nginx_images/nginx:$tag .
                    docker build -t 10.0.0.13/nginx_images/nginx:latest .
                    
                    docker push 10.0.0.13/nginx_images/nginx:$tag 
                    docker push 10.0.0.13/nginx_images/nginx:latest '''
            }
        }
        stage('UpdateConfig') {
            steps {
                sh 'ssh root@10.0.0.16 "docker-compose down  && docker image rm  10.0.0.13/nginx_images/nginx:latest && docker-compose up -d"'
            }
        }
    }
}

```









## 邮件通知

开启邮箱授权



Jenkins配置



因Jenkins无法发送邮件 而跳过













## 集成SonarQube

安装插件

![image-20230406183615858](assets/image-20230406183615858.png)

在SonarQube中生成凭证

![image-20230406183713117](assets/image-20230406183713117.png)



**添加SonarQube凭证**

![image-20230406183651942](assets/image-20230406183651942.png)



**Jenkins进行SonarQube配置**



![image-20230406183821862](assets/image-20230406183821862.png)

安装sonarscanner 

![image-20230406183855490](assets/image-20230406183855490.png)



自由风格项目使用sonarqube

![image-20230406185117596](assets/image-20230406185117596.png)

**Analysis properties**

```text
# must be unique in a given SonarQube instance
sonar.projectKey=nginx_demo

# this is the name and version displayed in the SonarQube UI. Was mandatory prior to SonarQube 6.1.
sonar.projectName=nginx_demo
sonar.projectVersion=1.0

# Path is relative to the sonar-project.properties file. Replace "\" by "/" on Windows.
# This property is optional if sonar.modules is set.
sonar.sources=.
sonar.exclusions=**/test/**,**/target/**

sonar.java.source=1.8
sonar.java.target=1.8

# Encoding of the source code. Default is default system encoding
sonar.sourceEncoding=UTF-8
```

![image-20230406185159221](assets/image-20230406185159221.png)



![image-20230406185318137](assets/image-20230406185318137.png)



Jenkinsfile中使用Sonarqube

```groovy
pipeline {
    agent any

    stages {
        stage('CloneCode') {
            steps {
                git branch: 'main', credentialsId: '14ccccbf-edb0-47a2-8830-3c6b7d035617', url: 'http://10.0.0.12/devops/cicd.git'
            }
        }
        stage('SonarQube代码审查') {
            steps{
                script {
                    scannerHome = tool 'sonar-scanner'
                }
                withSonarQubeEnv('sonarqube') {
                    sh "${scannerHome}/bin/sonar-scanner"
                }
            }
        }
        stage('BuildImage') {
            steps {
                sh '''tag=$(date +%Y%m%d%H%M%S)
                    docker build -t 10.0.0.13/nginx_images/nginx:$tag .
                    docker build -t 10.0.0.13/nginx_images/nginx:latest .
                    
                    docker push 10.0.0.13/nginx_images/nginx:$tag 
                    docker push 10.0.0.13/nginx_images/nginx:latest '''
            }
        }
        stage('UpdateConfig') {
            steps {
                sh 'ssh root@10.0.0.16 "docker-compose down  && docker image rm  10.0.0.13/nginx_images/nginx:latest && docker-compose up -d"'
            }
        }
    }
}

```



```groovy
        stage('SonarQube代码审查') {
            steps{
                script {
                    scannerHome = tool 'sonar-scanner' //名字和在系统管理 全局工具配置中配置的名字保持一致
                }
                withSonarQubeEnv('sonarqube') {
                    sh "${scannerHome}/bin/sonar-scanner"
                }
            }
        }
```

![image-20230407085847086](assets/image-20230407085847086.png)











## 基于Kubernetes部署Jenkins

### 安装k8s

















# 案例

```text
git ---> gitlab ---> jenkins ---> sonarqube ---> harbor ----> docker deploy
```



# 问题记录

## 配置密钥后git clone仍然让输入密码

```shell
# 生成密钥对
ssh-keygen -t rsa -C "admin@example.com"
#将公钥放到gitlab中，但是无法访问仓库
[root@gitlab .ssh]# cat id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1t8eFL71AoH9lt9WFAp0g9W48bZVaJmTATFNw0F/7stPOk6G5ljUy2zgtoDmJMBkWyOlaW7Hqg34pUzgIRy1WrSD8WA/IE+OvJpA/SfJz419MYOTBYCZQwIDA6977K1SOxuE1kxv4DJZsKSNXVNvUnznVam7EGhY+NmV10XgBFrD/KdDaHi0q0cxcqMtd/GiFj0wCH2IzmQA/Zv8RFNYvF6yXXm3grGwznijJ82WSpnPqa+I5DNwulLc/YrOU9pgNuTArobIXrPbAnX6o04B/IXpU2XuUrm32lYmEKHv3dXNdLsQhKv4B3PqeuoFuMxAMOSSMwjTJ0na3GK85MHaH root@gitlab

# 问题解决：
启动gitlab时将容器的22号端口映射为宿主机的222端口（避免和本地ssh冲突），所以git clone时需要设置端口，自建config文件，步骤如下：
[root@gitlab ~]# cat .ssh/config
Port 222
```

## docker网桥问题

```
问题描述：容器内无法访问百度，外部环境也无法访问容器提供的服务
问题解决：升级内核，参考文章:https://blog.csdn.net/qq_36059826/article/details/106550332
https://blog.csdn.net/weixin_42288415/article/details/105366176
```

## Jenkins配置邮箱服务，不识别smtp.163.com

```text
https://blog.csdn.net/sinat_21302587/article/details/74990482
```

## Jenkins发送邮件不成功

```text

```



## 运行代理agent.jar出错：

Error: A JNI error has occurred, please check your installation and try again
Exception in thread "main" java.lang.UnsupportedClassVersionError: hudson/remoting/Launcher has been compiled by a more recent version of the Java Runtime (class file version 55.0), this version of the Java Runtime only recognizes class file versions up to 52.0

```text
解决办法：
# 1.先查看本地是否自带java环境
yum list installed |grep java
# 卸载自带的java（输入su，输入root超级管理员的密码，切换到root用户模式）
yum -y remove java-1.8.0-openjdk* 
yum -y remove tzdata-java*
# 查看yum仓库中的java安装包
yum -y list java*
# 安装java
yum -y install java-1.8.0-openjdk*
# 查找Java安装路径
which java
ls -lrt /usr/bin/java（也就是上一步查询出来的路径），然后回车
输入ls -lrt /etc/alternatives/java（也就是上一步查询出来的路径），然后回车
从路径中可以看到在jvm目录下，输入cd /usr/lib/jvm，跳转到jvm的目录
输入ls 列出当前目录下的文件和文件夹

# 6.配置Java环境变量
vi /etc/profile
export JAVA_HOME=/usr/lib/jvm/java-1.8.0
export JRE_HOME=$JAVA_HOME/jre  
export PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib

source /etc/profile


```



使用k8s环境部署Jenkins出现问题，先跳过。

补充：
Jenkins参数设置单选框、多选框、Git分支框
https://blog.csdn.net/qq_37688023/article/details/105983960

jenkins的参数化构建：参数的下拉框和子菜单以及参数联动
https://www.cnblogs.com/zhiaijingming/p/9134975.html
