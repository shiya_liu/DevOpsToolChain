

pgsql truncate

```shell
# 登录：
https://www.lijiaocn.com/%E6%8A%80%E5%B7%A7/2017/08/31/postgre-usage.html
su - postgres
psql
# PGsql的基本操作
查看所有数据库：\l
进入某一数据库：\c 数据库名字
查看某一数据库中所有表名：select tablename from pg_tables;（后面别忘分号）
退出数据库：ctrl + z

# 按照大小排序
SELECT  relname,relpages,reltuples  FROM pg_class ORDER BY relpages DESC;

#  postgreSQL 表的 relpages 到底是什么?
https://devpress.csdn.net/postgresql/6331bcd42b9e466d077ab7ac.html

# 清理表数据 保留表结构
TRUNCATE TABLE  table_name;


# 清理homer_data
TRUNCATE TABLE logs_capture_p2013082901 ;

参考文档：https://www.lijiaocn.com/%E6%8A%80%E5%B7%A7/2017/08/31/postgre-usage.html

```